<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Models\Service;
use App\Models\ServiceOperator;

Route::get('/',function(){
  return redirect('/home');
});


Route::get('services/{slug}','User\ServiceController@index');
Route::get('contact',function()
{
  return view('contact');
});


Route::group(['prefix' => 'vue'], function () 
{
  Route::get('/navcomponent','User\VueFrontController@navComponent');
  Route::get('/navbarpattern','User\VueFrontController@navbarPattern');
  Route::any('/home','User\VueFrontController@home');
  Route::any('/whattodo','User\VueFrontController@whattodo');
  Route::any('/searchwhattodo','User\VueFrontController@searchWhatToDo');
  Route::any('/department/{slug}','User\VueFrontController@department');
  Route::get('/region/{slug}','User\VueFrontController@region');
  Route::any('/fairs','User\VueFrontController@fairs');
  Route::any('/specials','User\VueFrontController@specials');
  Route::any('/special/normalspecial/{slug}','User\VueFrontController@normalSpecial');
  Route::any('/special/special/{slug}','User\VueFrontController@special');
  Route::any('/special/specialsroute/{slug}','User\VueFrontController@specialRoute');
  Route::put('/getnamesites','User\VueFrontController@getnamesites'); //put
  Route::put('/getnearbypoints','User\VueFrontController@getnearbypoints'); //put
  Route::any('/routes','User\VueFrontController@routes');
  Route::any('/route/{slug}','User\VueFrontController@route');
  Route::any('/concession/{slug}','User\VueFrontController@concession');
  Route::any('/circuit/{slug}','User\VueFrontController@circuit');
  Route::any('/directory','User\VueFrontController@directory');
  Route::any('/hotels','User\VueFrontController@hotels');
  Route::any('/restaurants','User\VueFrontController@restaurants');
  Route::any('/operators','User\VueFrontController@services');
  Route::any('/hotel/{slug}','User\VueFrontController@hotel');
  Route::any('/restaurant/{slug}','User\VueFrontController@restaurant');
  Route::any('/municipality/{slug}','User\VueFrontController@municipality');
  Route::any('/operator/{slug}','User\VueFrontController@operator');
  Route::any('/interestsite/{slug}','User\VueFrontController@interestSite');
  Route::any('/fair/{slug}','User\VueFrontController@fair');
  Route::any('/department/{slug}','User\VueFrontController@department');
  Route::get('/sumclick/{id}','User\VueFrontController@sumClick');


});

Route::group(['prefix' => 'home'], function () {
  Route::get('/{vue_capture?}', function () {
   return view('welcome');
  })->where('vue_capture', '[\/\w\.-]*');

});

Route::post('storeemail','User\userController@saveEmail');
Route::get('reservation','User\ReservationController@index');
Route::post('make-reservation','User\ReservationController@prereservation');
Route::get('serviceslist','User\ReservationController@returnServices');

Route::put('add_cart/{id}','User\CartController@add_cart');
Route::get('allcart','User\CartController@allcart');
Route::get('subtotal','User\CartController@subtotal');
Route::get('remove_cart/{rowId}','User\CartController@removeCart');
Route::get('delete_cart','User\CartController@deleteCart');
Route::post('logincart','User\userController@loginCart')->name('logincart');
Route::post('registercart','User\userController@registerCart')->name('registercart');

Route::get('freehours/{id}/{date}','User\ReservationController@freeHours');



Route::group(['prefix' => 'admin'], function () {
  Route::get('/login', 'AdminAuth\LoginController@showLoginForm')->name('login');
  Route::post('/login', 'AdminAuth\LoginController@login');
  Route::post('/logout', 'AdminAuth\LoginController@logout')->name('logout');

  Route::get('/register', 'AdminAuth\RegisterController@showRegistrationForm')->name('register');
  Route::post('/register', 'AdminAuth\RegisterController@register');

  Route::post('/password/email', 'AdminAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
  Route::post('/password/reset', 'AdminAuth\ResetPasswordController@reset')->name('password.email');
  Route::get('/password/reset', 'AdminAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
  Route::get('/password/reset/{token}', 'AdminAuth\ResetPasswordController@showResetForm');
});

Route::group(['prefix' => 'reporter'], function () {
  Route::get('/login', 'ReporterAuth\LoginController@showLoginForm')->name('login');
  Route::post('/login', 'ReporterAuth\LoginController@login');
  Route::post('/logout', 'ReporterAuth\LoginController@logout')->name('logout');

  Route::get('/register', 'ReporterAuth\RegisterController@showRegistrationForm')->name('register');
  Route::post('/register', 'ReporterAuth\RegisterController@register');

  Route::post('/password/email', 'ReporterAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
  Route::post('/password/reset', 'ReporterAuth\ResetPasswordController@reset')->name('password.email');
  Route::get('/password/reset', 'ReporterAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
  Route::get('/password/reset/{token}', 'ReporterAuth\ResetPasswordController@showResetForm');
});

Route::group(['prefix' => 'user'], function () {
  Route::get('/login', 'UserAuth\LoginController@showLoginForm')->name('login');
  Route::post('/login', 'UserAuth\LoginController@login');
  Route::post('/logout', 'UserAuth\LoginController@logout')->name('logout');

  Route::get('/register', 'UserAuth\RegisterController@showRegistrationForm')->name('register');
  Route::post('/register', 'UserAuth\RegisterController@register');

  Route::post('/password/email', 'UserAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
  Route::post('/password/reset', 'UserAuth\ResetPasswordController@reset')->name('password.email');
  Route::get('/password/reset', 'UserAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
  Route::get('/password/reset/{token}', 'UserAuth\ResetPasswordController@showResetForm');
});


Route::get('user/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');



Route::post('savecomment','User\ReservationController@saveComment');
Route::post('encuesta','User\userController@sendQuestions');
Route::post('mailchimp_register', 'User\userController@mailchimpRegister');
Route::get('search','User\ServiceController@search');

Route::get('userprofile','User\userController@profile');
Route::post('updateprofile','User\userController@updateprofile');
Route::get('logeruser/reservation','User\ReservationController@reservationLogerUser');

Route::get('auth/{provider}', 'Auth\SocialAuthController@redirectToProvider')->name('social.auth');
Route::get('auth/{provider}/callback', 'Auth\SocialAuthController@handleProviderCallback');


Route::group(['prefix' => 'operator'], function () {
  Route::get('/login', 'OperatorAuth\LoginController@showLoginForm')->name('login');
  Route::post('/login', 'OperatorAuth\LoginController@login');
  Route::post('/logout', 'OperatorAuth\LoginController@logout')->name('logout');

  Route::get('/register', 'OperatorAuth\RegisterController@showRegistrationForm')->name('register');
  Route::post('/register', 'OperatorAuth\RegisterController@register');

  Route::post('/password/email', 'OperatorAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
  Route::post('/password/reset', 'OperatorAuth\ResetPasswordController@reset')->name('password.email');
  Route::get('/password/reset', 'OperatorAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
  Route::get('/password/reset/{token}', 'OperatorAuth\ResetPasswordController@showResetForm');
});
