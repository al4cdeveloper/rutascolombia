<?php

Route::get('/home', function () {
    $users[] = Auth::user();
    $users[] = Auth::guard()->user();
    $users[] = Auth::guard('reporter')->user();

    //dd($users);

    return view('reporter.home');
})->name('home');

