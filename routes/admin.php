<?php

Route::get('/home', function () {
    $users[] = Auth::user();
    $users[] = Auth::guard()->user();
    $users[] = Auth::guard('admin')->user();

    //dd($users);
    return view('admin.home');
})->name('home');


//ACTIVITIES

    //ROUTE SERVICE CATEGORY
    Route::resource('activities/servicecategory','Admin\ServiceCategoryController');
    Route::post('activities/servicecategory/store','Admin\ServiceCategoryController@store');
    Route::post('activities/servicecategory/update/{id}','Admin\ServiceCategoryController@update');
    //ROUTE SERVICES
    Route::resource('activities/service','Admin\ServiceController');
    Route::post('activities/service/store','Admin\ServiceController@store');
    Route::get('activities/service/edit/{slug}','Admin\ServiceController@edit');
    Route::post('activities/service/update/{id}','Admin\ServiceController@update');



//ROUTE REGIONS
Route::resource('regions','Admin\RegionController');
Route::post('regions/store','Admin\RegionController@store');
Route::get('regions/edit/{slug}','Admin\RegionController@edit');
Route::post('regions/update/{id}','Admin\RegionController@update');
Route::get('regions/desactivate/{id}','Admin\RegionController@desactivate');
Route::get('regions/activate/{id}','Admin\RegionController@activate');
Route::get('regions/images/{slug}','Admin\RegionController@images');
Route::put('regions/upload_images/{id}','Admin\RegionController@upload_images');
Route::delete('regions/delete_image/{id}','Admin\RegionController@delete_image');

//ROUTE DEPARTMENT
Route::resource('departments','Admin\DepartmentController');
Route::post('departments/store','Admin\DepartmentController@store');
Route::get('departments/edit/{slug}','Admin\DepartmentController@edit');
Route::post('departments/update/{id}','Admin\DepartmentController@update');
Route::get('departments/desactivate/{id}','Admin\DepartmentController@desactivate');
Route::get('departments/activate/{id}','Admin\DepartmentController@activate');
Route::get('departments/images/{slug}','Admin\DepartmentController@images');
Route::put('departments/upload_images/{id}','Admin\DepartmentController@upload_images');
Route::delete('departments/delete_image/{id}','Admin\DepartmentController@delete_image');

//ROUTE MUNICIPALY
route::resource('municipalities','Admin\MunicipalityController');
Route::post('municipalities/store','Admin\MunicipalityController@store');
Route::get('municipalities/edit/{slug}','Admin\MunicipalityController@edit');
Route::post('municipalities/update/{id}','Admin\MunicipalityController@update');
Route::get('municipalities/desactivate/{id}','Admin\MunicipalityController@desactivate');
Route::get('municipalities/activate/{id}','Admin\MunicipalityController@activate');
Route::get('municipalities/images/{slug}','Admin\MunicipalityController@images');
Route::put('municipalities/upload_images/{id}','Admin\MunicipalityController@upload_images');
Route::delete('municipalities/delete_image/{id}','Admin\MunicipalityController@delete_image');
Route::post('municipalities/upload_video/{id}','Admin\MunicipalityController@upload_video');
Route::delete('municipalities/delete_video/{id}','Admin\MunicipalityController@delete_video');
Route::get('municipalities/extra/{slug}','Admin\MunicipalityController@extra');
Route::post('municipalities/saveextra/{id}','Admin\MunicipalityController@saveextra');
Route::get('municipalities/keydata/{slug}','Admin\MunicipalityController@keydata');
Route::post('municipalities/keydata/{id}','Admin\MunicipalityController@saveKeydata');
Route::post('municipalities/change','Admin\MunicipalityController@change');


    // INTEREST SITES CATEGORY
    Route::get('interestsites/category','Admin\InterestSiteController@indexCategory');
    Route::get('interestsites/category/create','Admin\InterestSiteController@createCategory');
    Route::post('interestsites/category/store','Admin\InterestSiteController@storeCategory');
    Route::get('interestsites/category/edit/{slug}','Admin\InterestSiteController@editCategory');
    Route::post('interestsites/category/update/{id}','Admin\InterestSiteController@updateCategory');
    Route::get('interestsites/category/desactivate/{id}','Admin\InterestSiteController@desactivateCategory');
    Route::get('interestsites/category/activate/{id}','Admin\InterestSiteController@activateCategory');
	//CATEGORY - ACTIVITIES "WHAT TO DO"
	Route::get('whattodo','Admin\ActivityController@index');
	Route::get('whattodo/create','Admin\ActivityController@create');
	Route::post('whattodo/store','Admin\ActivityController@store');
	Route::get('whattodo/edit/{slug}','Admin\ActivityController@edit');
	Route::post('whattodo/update/{id}','Admin\ActivityController@update');
	Route::get('whattodo/desactivate/{id}','Admin\ActivityController@desactivate');
	Route::get('whattodo/activate/{id}','Admin\ActivityController@activate');
//INTEREST SITES

Route::resource('interestsites','Admin\InterestSiteController');
Route::post('interestsites/store','Admin\InterestSiteController@store');
Route::post('interestsite/register','Admin\InterestSiteController@register');
Route::get('interestsites/edit/{slug}','Admin\InterestSiteController@edit');
Route::post('interestsites/update/{id}','Admin\InterestSiteController@update');
Route::get('interestsites/desactivate/{id}','Admin\InterestSiteController@desactivate');
Route::get('interestsites/activate/{id}','Admin\InterestSiteController@activate');
Route::get('interestsites/images/{slug}','Admin\InterestSiteController@images');
Route::put('interestsites/upload_images/{id}','Admin\InterestSiteController@upload_images');
Route::delete('interestsites/delete_image/{id}','Admin\InterestSiteController@delete_image');
Route::get('interestsites/keydata/{slug}','Admin\InterestSiteController@keydata');
Route::post('interestsites/keydata/{id}','Admin\InterestSiteController@saveKeydata');

//CIRCUITOS
Route::post('circuits/getmunicipality','Admin\CircuitController@getMunicipality');
Route::post('circuits/getreladepartment','Admin\CircuitController@getRelationalDepartment');
Route::post('circuits/getdepartment','Admin\CircuitController@getDepartment');
Route::get('circuits/edit/{slug}','Admin\CircuitController@edit');
Route::post('circuits/update/{id}','Admin\CircuitController@update');
Route::get('circuits/desactivate/{id}','Admin\CircuitController@desactivate');
Route::get('circuits/activate/{id}','Admin\CircuitController@activate');
Route::resource('circuits','Admin\CircuitController');
Route::post('circuits/store','Admin\CircuitController@store');
Route::get('circuits/detailitems/{slug}','Admin\CircuitController@detailitems');
Route::get('circuits/detailitems/create/{slug}','Admin\CircuitController@createDetailItems');
Route::post('circuits/detailitems/store/{id}','Admin\CircuitController@storeDetailItems');
Route::get('circuits/detailitems/edit/{id}','Admin\CircuitController@editDetailItems');
Route::post('circuits/detailitems/update/{id}','Admin\CircuitController@updateDetailItems');
Route::get('circuits/detailitems/destroy/{id}','Admin\CircuitController@destroyDetailItem');
Route::get('circuits/images/{slug}','Admin\CircuitController@images');
Route::put('circuits/upload_images/{id}','Admin\CircuitController@upload_images');
Route::delete('circuits/delete_image/{id}','Admin\CircuitController@delete_image');
//RUTAS
Route::get('routes/edit/{slug}','Admin\RouteController@edit');
Route::post('routes/update/{id}','Admin\RouteController@update');
Route::get('routes/desactivate/{id}','Admin\RouteController@desactivate');
Route::get('routes/activate/{id}','Admin\RouteController@activate');
Route::resource('routes','Admin\RouteController');
Route::post('routes/store','Admin\RouteController@store');
Route::get('routes/images/{slug}','Admin\RouteController@images');
Route::put('routes/upload_images/{id}','Admin\RouteController@upload_images');
Route::delete('routes/delete_image/{id}','Admin\RouteController@delete_image');
//Puntos de rutas
Route::get('routes/points/new/{slug}','Admin\RouteController@createPoints');
Route::post('routes/points/store/{slug}','Admin\RouteController@storePoint');
Route::get('routes/points/edit/{id}','Admin\RouteController@editPoint');
Route::post('routes/points/update/{id}','Admin\RouteController@updatePoint');
Route::get('routes/points/delete/{id}','Admin\RouteController@deleteItem');
Route::get('routes/points/{slug}','Admin\RouteController@listPoints');

//FERIAS
Route::post('fairs/upload_video/{id}','Admin\FairController@upload_video');
Route::delete('fairs/delete_video/{id}','Admin\FairController@delete_video');
Route::delete('fairs/delete_pdf/{id}','Admin\FairController@delete_pdf');
Route::put('fairs/upload_images/{id}','Admin\FairController@upload_images');
Route::put('fairs/upload_pdf/{id}','Admin\FairController@upload_pdf');
Route::resource('fairs','Admin\FairController');
Route::post('fairs/store','Admin\FairController@store');
Route::get('fairs/edit/{slug}','Admin\FairController@edit');
Route::post('fairs/update/{id}','Admin\FairController@update');
Route::get('fairs/images/{slug}','Admin\FairController@images');
Route::delete('fairs/delete_image/{id}','Admin\FairController@delete_image');
Route::get('fairs/desactivate/{id}','Admin\FairController@desactivate');
Route::get('fairs/activate/{id}','Admin\FairController@activate');
Route::post('fairs/change','Admin\FairController@change');

//CONSESSIONS
Route::resource('concessions','Admin\ConcessionController');
Route::post('concessions/store','Admin\ConcessionController@store');
Route::get('concessions/edit/{slug}','Admin\ConcessionController@edit');
Route::post('concessions/update/{id}','Admin\ConcessionController@update');
Route::get('concessions/desactivate/{id}','Admin\ConcessionController@desactivate');
Route::get('concessions/activate/{id}','Admin\ConcessionController@activate');
//Toll
Route::get('concessions/tolls/{slug}','Admin\ConcessionController@tolls');
Route::get('concessions/tolls/new/{slug}','Admin\ConcessionController@createTolls');
Route::post('concessions/tolls/store/{slug}','Admin\ConcessionController@storeTolls');
Route::get('concessions/tolls/edit/{id}','Admin\ConcessionController@editTolls');
Route::post('concessions/tolls/update/{id}','Admin\ConcessionController@updateTolls');
Route::get('concessions/tolls/delete/{id}','Admin\ConcessionController@deleteTolls');

//SPECIALS
Route::post('specials/change','Admin\SpecialController@change');
Route::post('specials/store','Admin\SpecialController@store');
Route::get('specials/edit/{slug}','Admin\SpecialController@edit');
Route::post('specials/getinterestsites','Admin\SpecialController@getInterestSites');
Route::post('specials/update/{id}','Admin\SpecialController@update');
Route::resource('specials','Admin\SpecialController');
Route::get('specials/images/{slug}','Admin\SpecialController@images');
Route::get('specials/items/{slug}','Admin\SpecialController@items');
Route::post('specials/items/store/{id}','Admin\SpecialController@storeItem');
Route::post('specials/items/update/{id}','Admin\SpecialController@updateItem');
Route::get('specials/items/destroy/{id}','Admin\SpecialController@destroyItem');
Route::get('specials/detailitems/{slug}','Admin\SpecialController@detailitems');
Route::get('specials/detailitems/create/{slug}','Admin\SpecialController@createDetailItems');
Route::post('specials/detailitems/store/{id}','Admin\SpecialController@storeDetailItems');
Route::get('specials/detailitems/edit/{id}','Admin\SpecialController@editDetailItems');
Route::post('specials/detailitems/update/{id}','Admin\SpecialController@updateDetailItems');
Route::get('specials/detailitems/destroy/{id}','Admin\SpecialController@destroyDetailItem');

Route::post('specials/routes/store/{slug}','Admin\SpecialController@storeRoute');
Route::get('specials/routes/edit/{slug}','Admin\SpecialController@editRoute');
Route::post('specials/routes/update/{id}','Admin\SpecialController@updateRoute');
Route::get('specials/routes/desactivate/{slug}','Admin\SpecialController@desactivateRoute');
Route::get('specials/routes/activate/{slug}','Admin\SpecialController@activateRoute');
Route::get('specials/routes/points/{slug}','Admin\SpecialController@points');
Route::get('specials/routes/points/new/{slug}','Admin\SpecialController@createPoint');
Route::post('specials/routes/points/store/{slug}','Admin\SpecialController@storePoint');
Route::get('specials/routes/points/edit/{id}','Admin\SpecialController@editPoint');
Route::post('specials/routes/points/update/{id}','Admin\SpecialController@updatePoint');
Route::get('specials/routes/points/delete/{id}','Admin\SpecialController@deleteItem');
Route::get('specials/routes/points/reorder/{order}/{new}','Admin\SpecialController@reorderPoint');
Route::post('specials/routes/getreladepartment','Admin\SpecialController@getRelationalDepartment');
Route::get('specials/routes/getrelamunicipality','Admin\SpecialController@getRelationalMunicipality');
Route::get('specials/routes/{slug}','Admin\SpecialController@routes');
Route::get('specials/routes/new/{slug}','Admin\SpecialController@routesCreate');
Route::get('specials/routes/images/{slug}','Admin\SpecialController@imagesRoute');
Route::put('specials/routes/upload_images/{id}','Admin\SpecialController@upload_imagesRoute');
Route::delete('specials/routes/delete_image/{id}','Admin\SpecialController@delete_imageRoute');

Route::put('specials/upload_images/{id}','Admin\SpecialController@upload_images');
Route::get('specials/delete_image/{id}','Admin\SpecialController@delete_image');
Route::post('specials/update_image','Admin\SpecialController@updateImage');



//ESTABLECIMIENTOS
Route::get('establishment/{type}','Admin\EstablishmentController@index');
Route::get('establishment/{type}/create','Admin\EstablishmentController@create');
Route::post('establishment/{type}/store','Admin\EstablishmentController@store');
Route::get('establishment/edit/{slug}','Admin\EstablishmentController@edit');
Route::put('establishment/update/{slug}','Admin\EstablishmentController@update');
Route::get('establishment/desactivate/{slug}','Admin\EstablishmentController@desactivate');
Route::get('establishment/activate/{slug}','Admin\EstablishmentController@activate');
Route::get('establishment/images/{slug}','Admin\EstablishmentController@images');
Route::put('establishment/upload_images/{id}','Admin\EstablishmentController@upload_images');
Route::get('establishment/delete_image/{id}','Admin\EstablishmentController@delete_image');
Route::post('establishment/update_images/','Admin\EstablishmentController@update_images');
Route::post('establishment/transfer/','Admin\EstablishmentController@transfer');
Route::post('establishment/change','Admin\EstablishmentController@change');
//WALLPAPERS
Route::post('wallpapers/store','Admin\WallpaperController@store');
Route::put('wallpapers/update/{id}','Admin\WallpaperController@update');
Route::get('wallpapers/edit/{id}','Admin\WallpaperController@edit');
Route::resource('wallpapers','Admin\WallpaperController');
Route::get('wall/desactivate/{id}','Admin\WallpaperController@desactivate');
Route::get('wall/activate/{id}','Admin\WallpaperController@activate');

//OPERADORES
Route::get('operators','Admin\AdminController@operators');
Route::get('operators/services/{id}','Admin\AdminController@operatorServices');
Route::get('operators/services/edit/{id}','Admin\AdminController@editServiceOperator');
Route::put('operators/services/update/{id}','Admin\AdminController@updateServiceOperator');
Route::delete('operators/service/delete_image/{id}','Admin\AdminController@delete_image');
Route::get('operators/serviceimages/{id}','Admin\AdminController@imagesServiceOperator');
Route::put('operators/service/upload_images/{id}','Admin\AdminController@upload_images');
Route::get('operators/service/desactivate/{id}','Admin\AdminController@desactivate');
Route::get('operators/service/activate/{id}','Admin\AdminController@activate');
Route::get('operators/items/{id}','Admin\AdminController@itemsOperator');
Route::get('operators/items/edit/{id}','Admin\AdminController@editItemOperator');
Route::put('operators/items/update/{id}','Admin\AdminController@updateItemOperator');
Route::get('operators/items/desactivate/{id}','Admin\AdminController@desactivateItem');
Route::get('operators/items/activate/{id}','Admin\AdminController@activateItem');
Route::get('operators/desactivate/{id}','Admin\AdminController@desactivateOperator');
Route::get('operators/activate/{id}','Admin\AdminController@activateOperator');
Route::post('operators/change','Admin\AdminController@change');
//CLIENTES
Route::resource('customers','Admin\CustomerController');
Route::post('customers/store','Admin\CustomerController@store');
Route::get('customers/edit/{slug}','Admin\CustomerController@edit');
Route::post('customers/update/{id}','Admin\CustomerController@update');
Route::get('customers/desactivate/{id}','Admin\CustomerController@desactivate');
Route::get('customers/activate/{id}','Admin\CustomerController@activate');

//PARTES DE PÁGINA (PARTES DE PUBLICIDAD)
Route::resource('pageparts','Admin\PagePartController');
Route::post('pageparts/store','Admin\PagePartController@store');
Route::get('pageparts/edit/{slug}','Admin\PagePartController@edit');
Route::post('pageparts/update/{id}','Admin\PagePartController@update');
Route::get('pageparts/desactivate/{id}','Admin\PagePartController@desactivate');
Route::get('pageparts/activate/{id}','Admin\PagePartController@activate');

//PÁGINAS
Route::resource('pages','Admin\PageController');
Route::post('pages/store','Admin\PageController@store');
Route::get('pages/edit/{slug}','Admin\PageController@edit');
Route::post('pages/update/{id}','Admin\PageController@update');

//PATTERNS-> PUBLICIDAD! :D 
Route::resource('patterns','Admin\PatternController');
Route::post('patterns/store','Admin\PatternController@store');
Route::get('patterns/edit/{id}','Admin\PatternController@edit');
Route::post('patterns/update/{id}','Admin\PatternController@update');
Route::get('patterns/desactivate/{id}','Admin\PatternController@desactivate');
Route::get('patterns/activate/{id}','Admin\PatternController@activate');

Route::get('users','Admin\AdminController@users');
Route::get('contact','Admin\AdminController@contact');