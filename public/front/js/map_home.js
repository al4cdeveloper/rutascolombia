(function(A) {

	if (!Array.prototype.forEach)
		A.forEach = A.forEach || function(action, that) {
			for (var i = 0, l = this.length; i < l; i++)
				if (i in this)
					action.call(that, this[i], i, this);
			};

		})(Array.prototype);

		var
		mapObject,
		markers = [],
		markersData = {
			'Historic': [
			{
				name: 'Bogotá D.C.',
				location_latitude: 4.710989, 
				location_longitude: -74.072090,
				map_image_url: 'img/thumb_map_1.jpg',
				name_point: 'Bogotá D.C.',
				description_point: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
				get_directions_start_address: '',
				phone: '+3934245255',
				url_point: '5destinations.html'
			},
			{
				name: 'Tunja',
				location_latitude: 5.538590, 
				location_longitude: -73.366379,
				map_image_url: 'img/thumb_map_1.jpg',
				name_point: 'Tunja',
				description_point: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
				get_directions_start_address: '',
				phone: '+3934245255',
				url_point: '5destinations.html'
			}
			],
			'Sightseeing': [
			{
				name: 'Bucaramanga',
				location_latitude: 7.131180, 
				location_longitude: -73.125031,
				map_image_url: 'img/thumb_map_1.jpg',
				name_point: 'Bucaramanga',
				description_point: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
				get_directions_start_address: '',
				phone: '+3934245255',
				url_point: '5destinations.html'
			},
			{
				name: 'Villavicencio',
				location_latitude: 4.144230,
				location_longitude: -73.634529,
				map_image_url: 'img/thumb_map_1.jpg',
				name_point: 'Villavicencio',
				description_point: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
				get_directions_start_address: '',
				phone: '+3934245255',
				url_point: '5destinations.html'
			}
			],
			'Museums': [
			{
				name: 'Suesca',
				location_latitude: 5.103256, 
				location_longitude: -73.799977,
				map_image_url: 'img/thumb_map_1.jpg',
				name_point: 'Suesca',
				description_point: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
				get_directions_start_address: '',
				phone: '+3934245255',
				url_point: '5destinations.html'
			},
			{
				name: 'Ibagué ',
				location_latitude: 4.444676,
				location_longitude: -75.242439,
				map_image_url: 'img/thumb_map_1.jpg',
				name_point: 'Ibagué',
				description_point: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
				get_directions_start_address: '',
				phone: '+3934245255',
				url_point: '5destinations.html'
			}
			],
			'Skyline': [
			{
				name: 'Puerto Gaitán',
				location_latitude: 4.312072, 
				location_longitude: -72.082951,
				map_image_url: 'img/thumb_map_1.jpg',
				name_point: 'Puerto Gaitán',
				description_point: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
				url_point: '5destinations.html'
			},
			{
				name: 'Medellín',
				location_latitude: 6.248220,
				location_longitude: -75.580032,
				map_image_url: 'img/thumb_map_1.jpg',
				name_point: 'Medellín',
				description_point: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
				get_directions_start_address: '',
				phone: '+3934245255',
				url_point: '5destinations.html'
			}
			],
			'Eat_drink': [
			{
				name: 'Honda',
				location_latitude: 5.206490, 
				location_longitude: -74.736412,
				map_image_url: 'img/thumb_map_1.jpg',
				name_point: 'Honda',
				description_point: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
				get_directions_start_address: '',
				phone: '+3934245255',
				url_point: '5destinations.html'
			},
			{
				name: 'Pereira',
				location_latitude: 4.812220,
				location_longitude: -75.692047,
				map_image_url: 'img/thumb_map_1.jpg',
				name_point: 'Pereira',
				description_point: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
				get_directions_start_address: '',
				phone: '+3934245255',
				url_point: '5destinations.html'
			}
			],
			'Walking': [
			{
				name: 'Quibdó',
				location_latitude: 5.684571, 
				location_longitude: -76.654046,
				map_image_url: 'img/thumb_map_1.jpg',
				name_point: 'Quibdó',
				description_point: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
				get_directions_start_address: '',
				phone: '+3934245255',
				url_point: '5destinations.html'
			},
			{
				name: 'Bahía Solano',
				location_latitude: 6.224564,
				location_longitude: -77.403442,
				map_image_url: 'img/thumb_map_1.jpg',
				name_point: 'Bahía Solano',
				description_point: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
				get_directions_start_address: '',
				phone: '+3934245255',
				url_point: '5destinations.html'
			}
			],
			'Churches': [
			{
				name: 'San Gil',
				location_latitude: 6.555286, 
				location_longitude: -73.131235,
				map_image_url: 'img/thumb_map_1.jpg',
				name_point: 'San Gil',
				description_point: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
				get_directions_start_address: '',
				phone: '+3934245255',
				url_point: '5destinations.html'
			},
			{
				name: 'Girardot',
				location_latitude: 4.306643, 
				location_longitude: -74.801567,
				map_image_url: 'img/thumb_map_1.jpg',
				name_point: 'Girardot',
				description_point: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
				get_directions_start_address: '',
				phone: '+3934245255',
				url_point: '5destinations.html'
			}
			]

		};


			var mapOptions = {
				zoom: 7,
				center: new google.maps.LatLng(5.538590, -73.366379),
				mapTypeId: google.maps.MapTypeId.ROADMAP,

				mapTypeControl: false,
				mapTypeControlOptions: {
					style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
					position: google.maps.ControlPosition.LEFT_CENTER
				},
				panControl: false,
				panControlOptions: {
					position: google.maps.ControlPosition.TOP_RIGHT
				},
				zoomControl: true,
				zoomControlOptions: {
					style: google.maps.ZoomControlStyle.LARGE,
					position: google.maps.ControlPosition.TOP_RIGHT
				},
				 scrollwheel: false,
				scaleControl: false,
				scaleControlOptions: {
					position: google.maps.ControlPosition.LEFT_CENTER
				},
				streetViewControl: true,
				streetViewControlOptions: {
					position: google.maps.ControlPosition.LEFT_TOP
				},
				styles: [
											 {
					"featureType": "landscape",
					"stylers": [
						{
							"hue": "#FFBB00"
						},
						{
							"saturation": 43.400000000000006
						},
						{
							"lightness": 37.599999999999994
						},
						{
							"gamma": 1
						}
					]
				},
				{
					"featureType": "road.highway",
					"stylers": [
						{
							"hue": "#FFC200"
						},
						{
							"saturation": -61.8
						},
						{
							"lightness": 45.599999999999994
						},
						{
							"gamma": 1
						}
					]
				},
				{
					"featureType": "road.arterial",
					"stylers": [
						{
							"hue": "#FF0300"
						},
						{
							"saturation": -100
						},
						{
							"lightness": 51.19999999999999
						},
						{
							"gamma": 1
						}
					]
				},
				{
					"featureType": "road.local",
					"stylers": [
						{
							"hue": "#FF0300"
						},
						{
							"saturation": -100
						},
						{
							"lightness": 52
						},
						{
							"gamma": 1
						}
					]
				},
				{
					"featureType": "water",
					"stylers": [
						{
							"hue": "#0078FF"
						},
						{
							"saturation": -13.200000000000003
						},
						{
							"lightness": 2.4000000000000057
						},
						{
							"gamma": 1
						}
					]
				},
				{
					"featureType": "poi",
					"stylers": [
						{
							"hue": "#00FF6A"
						},
						{
							"saturation": -1.0989010989011234
						},
						{
							"lightness": 11.200000000000017
						},
						{
							"gamma": 1
						}
					]
				}
				]
			};
			var
			marker;
			mapObject = new google.maps.Map(document.getElementById('map'), mapOptions);
			for (var key in markersData)
				markersData[key].forEach(function (item) {
					marker = new google.maps.Marker({
						position: new google.maps.LatLng(item.location_latitude, item.location_longitude),
						map: mapObject,
						icon: 'img/pins/' + key + '.png',
					});

					if ('undefined' === typeof markers[key])
						markers[key] = [];
					markers[key].push(marker);
					google.maps.event.addListener(marker, 'click', (function () {
      closeInfoBox();
      getInfoBox(item).open(mapObject, this);
      mapObject.setCenter(new google.maps.LatLng(item.location_latitude, item.location_longitude));
     }));
					
	});
	

		function hideAllMarkers () {
			for (var key in markers)
				markers[key].forEach(function (marker) {
					marker.setMap(null);
				});
		};

		function closeInfoBox() {
			$('div.infoBox').remove();
		};

		function getInfoBox(item) {
			return new InfoBox({
				content:
				'<div class="marker_info" id="marker_info">' +
				'<img src="' + item.map_image_url + '" alt="Image"/>' +
				'<h3>'+ item.name_point +'</h3>' +
				'<span>'+ item.description_point +'</span>' +
				'<div class="marker_tools">' +
				'<form action="http://maps.google.com/maps" method="get" target="_blank" style="display:inline-block""><input name="saddr" value="'+ item.get_directions_start_address +'" type="hidden"><input type="hidden" name="daddr" value="'+ item.location_latitude +',' +item.location_longitude +'"><button type="submit" value="Get directions" class="btn_infobox_get_directions">Directions</button></form>' +
					'<a href="tel://'+ item.phone +'" class="btn_infobox_phone">'+ item.phone +'</a>' +
					'</div>' +
					'<a href="'+ item.url_point + '" class="btn_infobox">Details</a>' +
				'</div>',
				disableAutoPan: false,
				maxWidth: 0,
				pixelOffset: new google.maps.Size(10, 125),
				closeBoxMargin: '5px -20px 2px 2px',
				closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif",
				isHidden: false,
				alignBottom: true,
				pane: 'floatPane',
				enableEventPropagation: true
			});


		};


