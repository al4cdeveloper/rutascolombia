
function openURwithTarget( url, target ) {
	var win = window.open(url, target);
	win.focus();
}
	var regionsMetadata =
        { 
            'antioquia-eje-cafetero':      { 'text': "Antioquia y "+"",'text2': "eje cafetero",'position':1,'darkColor':'#EB5B56','lightColor':'#F9CDD0','label_offset_x': -10, 'label_offset_y': 10,'label_offset_x2': -20, 'label_offset_y2': 40},  
            'amazonia':            { 'text': "Amazonia" ,'position':6,'darkColor':'#BE8E71','lightColor':'#EBD4C4','label_offset_x': 0, 'label_offset_y': 40},
            'llanos-orientales':            { 'text': "Llanos Orientales" ,'position':4,'darkColor':'#F7A824','lightColor':'#FFDF9A','label_offset_x': -50, 'label_offset_y': -20}, 
            'centro':               { 'text': "Centro" ,'position':3,'darkColor':'#019983','lightColor':'#BFE0D6','label_offset_x': 30, 'label_offset_y': 20,},
            'caribe':            { 'text': "Costa Caribe" ,'position':0,'darkColor':'#00ABC8','lightColor':'#9AD5E2','label_offset_x': -100, 'label_offset_y': 50},	
            'pacifico':                { 'text': "Costa pacífica",'position':2,'darkColor':'#0081C1','lightColor':'#91D2E5','label_offset_x': -100, 'label_offset_y': 90},  
            'suroccidente':             { 'text': "Suroccidente" ,'position':5,'darkColor':'#A2559D','lightColor':'#EED7E9','label_offset_x': 0, 'label_offset_y': 40},	
			
	};


window.onload = function () {

    var s           = Snap("#svgMap");
    var map_x_offset = 85, map_y_offset = 55;
    var isFullyCompatibleBrowser =  ! ( $.browser.msie || $.browser.msedge || $.browser["windows phone"] );
    var shadow      = s.filter(Snap.filter.shadow(0, 9, 3));
    var noShadow    = s.filter(Snap.filter.shadow(0,0,0));
    var invert      = s.filter(Snap.filter.invert(1.0));
    var noInvert    = s.filter(Snap.filter.invert(0));
    var saturate    = s.filter(Snap.filter.saturate(1.5));
    var saturateFilterChild = saturate.node.firstChild;
    var activeBounds = {};
    var state = 0;
    var defaultRegionURL = '#';
    var title = Snap.parse('<title style="color=red"></title>');
    s.append( title );


	Snap.plugin( function( Snap, Element, Paper, global ) {
		Element.prototype.addTransform = function( t ) {
			return this.transform( this.transform().localMatrix.toTransformString() + t );
		};
	});

	Snap.load("assets/colombia_departments.svg", function (f) {

        /**
         * [drawBounds description]
         * @param  {[type]} targetObject [description]
         * @return {[type]}              [description]
         */
        function drawBounds(targetObject) {

            targetId = targetObject.attr('id');

            if ( typeof activeBounds[ targetId ] == 'undefined')  
            activeBounds[ targetId ] = [];

            var tmp_polyline, tmp_outer_disc, tmp_inner_disc, tmp_innermost_disc;

            var bbox = targetObject.getBBox();

            // calculate middle point
            var x_center = ( bbox.x2 + bbox.x ) / 2 + map_x_offset; 
            var y_center = ( bbox.y2 + bbox.y ) / 2 + map_y_offset;

            if (false) {

                tmp_polyline = s.polyline( bbox.x + map_x_offset, bbox.y + map_y_offset, 
                    bbox.x2 + map_x_offset, bbox.y + map_y_offset,
                    bbox.x2 + map_x_offset, bbox.y2 + map_y_offset,
                    bbox.x + map_x_offset, bbox.y2 + map_y_offset,
                    bbox.x + map_x_offset, bbox.y + map_y_offset
                     )
                    .attr({
                    stroke: "rgb(206,43,55)",
                    strokeWidth: "2",
                    opacity: "0.3",
                    fill: "none"
                });
                activeBounds[ targetId ].push( tmp_polyline );

                tmp_outer_disc = s.circle(x_center, y_center, bbox.r0 ).attr({
                    fill: "none",
                    stroke: "rgb(255, 87, 51 )",
                    strokeWidth: "3",
                    opacity: "0.3",
                });

                    activeBounds[ targetId ].push( tmp_outer_disc );

                tmp_inner_disc = s.circle(x_center, y_center, bbox.r1 ).attr({
                    fill: "none",
                    stroke: "rgb(206,43,55)",
                    strokeWidth: "3",
                    opacity: "0.3",
                });
                activeBounds[ targetId ].push( tmp_inner_disc );
            }
        }

        /**
         * [mouseOutSVG description]
         * @param  {[type]} event [description]
         * @return {[type]}       [description]
         */
        function mouseOutSVG(event) {
                // console.log('Out of SVG!!!!!');
                // console.log(event.target);
        }

        /**
         * [hoverRegion description]
         * @param  {[type]} event [description]
         * @return {[type]}       [description]
         */
        function hoverRegion(event) {

            if ( isFullyCompatibleBrowser ) 
            {
                if(state==0)
                {
                    var that = $("#centro");
                    color = regionsMetadata[ 'centro' ]['lightColor'];
                    that.attr({'fill' : color,  'stroke': 'blue', 'stroke-width': 0.2 });
                   state=1;
                }


                var $lastChild = $('#regions').children().last();
                var lastSnapObject = s.select( '#'+$lastChild.attr('id') );

                lastSnapObject.after(this);
                var lastcolor = regionsMetadata[ lastSnapObject.attr('id') ]['lightColor'];

                lastSnapObject.attr({'fill' : lastcolor,  'stroke': 'blue', 'stroke-width': 0.2 });
                /* regions labels */

                
                //animacion
                //that.attr({ filter: shadow }).animate( { transform: "s1.6,1.6,"+originalBBox.cx+","+originalBBox.cy }, 600, mina.elastic );
                var that = this;
                color = regionsMetadata[ this.attr('id') ]['darkColor'];
                that.attr({'fill' : color,  'stroke': 'blue', 'stroke-width': 0.2 });

                var position = regionsMetadata[ this.attr('id') ]['position'];
                $('.your-class').slick('slickGoTo', position);

            }
            else // not fully compatible browser
            {
                this.attr({ filter: invert });
            }
        }

        /**
         * [removeBounds description]
         * @return {[type]} [description]
         */
        function removeBounds() {

            // cleans the activeBounds Queue
            var nActiveBounds = Object.keys(activeBounds).length;
            if ( nActiveBounds > 0 )
            { 
                for (var region in activeBounds) {
                    for ( var i = 0; i < activeBounds[region].length; i++ ) {
                        activeBounds[region][i].remove();
                    }
                    activeBounds[region] = [];
                }
            } 
        }

        /**
         * [noHoverRegion description]
         * @param  {[type]} event [description]
         * @return {[type]}       [description]
         */
        function noHoverRegion(event) {
            if (weakOverlays != null) weakOverlays.animate( { 'opacity': '1' }, 200, mina.easeout );

            if ( isFullyCompatibleBrowser ) {

                /* regions labels */

                var originalBBox        = regions[ this.attr('id') ].originalBBox;
                var originalTransform   = regions[ this.attr('id') ].originalTransform; 

                this.animate({ transform: "s1,1,"+originalBBox.cx+","+originalBBox.cy }, 700, mina.elastic).attr({ filter: noShadow });


                
            }
            else
            {
                this.animate({ transform: "s1,1" }, 700, mina.elastic).attr({ filter: noInvert });
            }
        }

        /**
         * [clickRegion description]
         * @param  {[type]} event [description]
         * @return {[type]}       [description]
         */
        function clickRegion(event) {

            var regionURL = defaultRegionURL, target = '_self'; 
            if ( typeof regionsMetadata[ this.attr('id') ] != 'undefined') 
            {		
                if ( typeof regionsMetadata[ this.attr('id') ]['url'] != 'undefined')
                    regionURL = regionsMetadata[ this.attr('id') ]['url'];

                if ( typeof regionsMetadata[ this.attr('id') ]['target'] != 'undefined')
                    target = regionsMetadata[ this.attr('id') ]['target'];
            }

            // top.location.href = regionURL; 
            openURwithTarget( regionURL, target );
        }

        /* Main callback code for Snap.load ************************************************************************/
  var regionsContainer = f.select("#regions");
        s.append(regionsContainer); 
        var weakOverlays = f.select("#weak-overlays");

        if ( weakOverlays != null)
        {
            s.append(weakOverlays); 
            weakOverlays.attr({ 'pointer-events': 'none' });            
        } 
        var elemRegions     = $('#regions').children(),
                regions         = {};

        s.mouseout( function(event) {
                mouseOutSVG(event);
        } );

        var regionBBox;

        for ( var i = 0; i < elemRegions.length; i++ ) {
            
            regions[ elemRegions[i].id ] = {};
            regions[ elemRegions[i].id ].id = elemRegions[i].id;
            var regionId = regions[ elemRegions[i].id ].id;
            regions[ elemRegions[i].id ].snapObject = s.select( '#'+elemRegions[i].id );
            regions[ elemRegions[i].id ].pathLength = Snap.path.getTotalLength( regions[ elemRegions[i].id ].snapObject );
            regions[ elemRegions[i].id ].originalBBox = regions[ elemRegions[i].id ].snapObject.getBBox();
            regions[ elemRegions[i].id ].originalTransform = regions[ elemRegions[i].id ].snapObject.transform();

            /* region bounding box */
            var regionBBox = regions[ elemRegions[i].id ].snapObject.getBBox();

            var x_center = ( regionBBox.x2 + regionBBox.x ) / 2 + map_x_offset; 
            var y_center = ( regionBBox.y2 + regionBBox.y ) / 2 + map_y_offset;

            /* event handlers */
            regions[ elemRegions[i].id ].snapObject.hover( hoverRegion, noHoverRegion );
            regions[ elemRegions[i].id ].snapObject.click( clickRegion );

            /* regions labels */
            var label_x = x_center;
            var label_y = y_center;
            var label_text = regionId;
            var label_text2 = regionId;
            var label_rotation = 0;

            if ( typeof regionsMetadata[ elemRegions[i].id ] != 'undefined') 
            {               
                if ( typeof regionsMetadata[ elemRegions[i].id ]['label_offset_x'] != 'undefined')
                    label_x += regionsMetadata[ elemRegions[i].id ]['label_offset_x'];

                if ( typeof regionsMetadata[ elemRegions[i].id ]['label_offset_y'] != 'undefined')
                    label_y += regionsMetadata[ elemRegions[i].id ]['label_offset_y'];

                if ( typeof regionsMetadata[ elemRegions[i].id ]['text'] != 'undefined')
                    label_text = regionsMetadata[ elemRegions[i].id ]['text'];

//                  label_text = "Hola";

                if ( typeof regionsMetadata[ elemRegions[i].id ]['rotation'] != 'undefined')
                    label_rotation = regionsMetadata[ elemRegions[i].id ]['rotation'];
            }
            var label       = s.text(label_x, label_y, label_text);
            var labelBBox   = label.getBBox();
            label.transform('t-'+labelBBox.width/2+',-'+ labelBBox.height/2 +' r' + label_rotation );
            label.attr({ 'pointer-events': 'none' });
            regions[ elemRegions[i].id ].snapLabel = label;
            regions[ elemRegions[i].id ].snapLabel.attr({'font-size': '40px', 'opacity': '0.8'});
            regions[ elemRegions[i].id ].snapLabelTransform = regions[ elemRegions[i].id ].snapLabel.transform();
            if(elemRegions[i].id == 'centro')
            {
                var that = $("#"+elemRegions[i].id);
                color = regionsMetadata[ elemRegions[i].id ]['darkColor'];
                that.attr({'fill' : color,  'stroke': 'blue', 'stroke-width': 0.2 });
            }

            if ( typeof regionsMetadata[ elemRegions[i].id ]['text2'] != 'undefined')
            {
                if ( typeof regionsMetadata[ elemRegions[i].id ]['label_offset_x2'] != 'undefined')
                    label_x += regionsMetadata[ elemRegions[i].id ]['label_offset_x2'];

                if ( typeof regionsMetadata[ elemRegions[i].id ]['label_offset_y2'] != 'undefined')
                    label_y += regionsMetadata[ elemRegions[i].id ]['label_offset_y2'];

                label_text2 = regionsMetadata[ elemRegions[i].id ]['text2'];

                var label       = s.text(label_x, label_y, label_text2);
                var labelBBox   = label.getBBox();
                label.transform('t-'+labelBBox.width/2+',-'+ labelBBox.height/2 +' r' + label_rotation );
                label.attr({ 'pointer-events': 'none' });
                regions[ elemRegions[i].id ].snapLabel = label;
                regions[ elemRegions[i].id ].snapLabel.attr({'font-size': '40px', 'opacity': '0.8'});
                regions[ elemRegions[i].id ].snapLabelTransform = regions[ elemRegions[i].id ].snapLabel.transform();
            }
        }
        $('.your-class').slick('slickGoTo', 3);
        /* /End Main callback code for Snap.load ************************************************************************/
    });
};

