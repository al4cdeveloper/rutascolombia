<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => 'techgrowth.com.co',
        'secret' => 'key-441abc145b16a4b04f447bc6a4704444',
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    
    'facebook' => [
    'client_id' => '2065265787039502',
    'client_secret' => '83f87ebe48130e817915f2489178a797',
    'redirect' => 'http://localhost:8000/auth/facebook/callback'
    ],

    'google' => [
    'client_id' => '101027048138-4017u9b5okl6dn7m0em1cnd7d4ml04hp.apps.googleusercontent.com',
    'client_secret' => 'mA0_E509siRJg_8y4_73BjeM',
    'redirect' => 'http://localhost:8000/auth/google/callback'
    ]

];
