-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 08-01-2019 a las 21:46:09
-- Versión del servidor: 5.6.35
-- Versión de PHP: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `rutaswebprod`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `activities`
--

CREATE TABLE `activities` (
  `id_activity` int(10) UNSIGNED NOT NULL,
  `activity_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `icon_class` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `activities`
--

INSERT INTO `activities` (`id_activity`, `activity_name`, `image_link`, `title`, `keywords`, `icon_class`, `meta_description`, `slug`, `state`, `created_at`, `updated_at`) VALUES
(1, 'Aventura', 'images/activity/activity_201812032231371344825646.png', 'ELIGE LAS HISTORIAS QUE ESTÁN POR SUCEDER', 'alalalalalallaa', 'fas fa-bezier-curve', '<p>asdas dasd asd asd sad asd</p>', 'aventura', 'activo', '2018-12-04 03:31:37', '2018-12-28 23:17:29'),
(2, 'Ecoturismo', 'images/activity/activity_201812032231401472538081.png', 'ELIGE LAS HISTORIAS QUE ESTÁN POR SUCEDER', 'alalalalalallaa', 'fas fa-crow', '<p>asdas dasd asd asd sad asd</p>', 'aventura-1', 'activo', '2018-12-04 03:31:40', '2018-12-28 23:20:57'),
(3, 'Parques Nacionales', 'images/activity/activity_20181203223141851436773.png', 'ELIGE LAS HISTORIAS QUE ESTÁN POR SUCEDER', 'alalalalalallaa', 'fas fa-flag', '<p>asdas dasd asd asd sad asd</p>', 'aventura-2', 'activo', '2018-12-04 03:31:41', '2018-12-28 23:21:22'),
(4, 'Sol y Playa', 'images/activity/activity_201812032231431952505496.png', 'ELIGE LAS HISTORIAS QUE ESTÁN POR SUCEDER', 'alalalalalallaa', 'fas fa-glass-martini-alt', '<p>asdas dasd asd asd sad asd</p>', 'aventura-3', 'activo', '2018-12-04 03:31:43', '2018-12-28 23:22:38'),
(5, 'Parques temáticos', 'images/activity/activity_20181203223144445325452.png', 'ELIGE LAS HISTORIAS QUE ESTÁN POR SUCEDER', 'alalalalalallaa', 'fas fa-parachute-box', '<p>asdas dasd asd asd sad asd</p>', 'aventura-4', 'activo', '2018-12-04 03:31:44', '2018-12-28 23:15:02'),
(6, 'De compras', 'images/activity/activity_201812032231501820768632.png', 'ELIGE LAS HISTORIAS QUE ESTÁN POR SUCEDER', 'alalalalalallaa', 'fas fa-shopping-bag', '<p>asdas dasd asd asd sad asd</p>', 'aventura-5', 'activo', '2018-12-04 03:31:50', '2018-12-28 23:14:35');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `activity_pivots`
--

CREATE TABLE `activity_pivots` (
  `id_activity_pivot` int(10) UNSIGNED NOT NULL,
  `fk_actity` int(10) UNSIGNED NOT NULL,
  `fk_relation` int(11) NOT NULL,
  `type_relation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `activity_pivots`
--

INSERT INTO `activity_pivots` (`id_activity_pivot`, `fk_actity`, `fk_relation`, `type_relation`, `created_at`, `updated_at`) VALUES
(2, 1, 2, 'municipality', NULL, NULL),
(3, 2, 2, 'municipality', NULL, NULL),
(4, 4, 2, 'municipality', NULL, NULL),
(5, 1, 1, 'municipality', NULL, NULL),
(6, 2, 1, 'municipality', NULL, NULL),
(7, 3, 1, 'municipality', NULL, NULL),
(8, 5, 1, 'municipality', NULL, NULL),
(9, 6, 1, 'municipality', NULL, NULL),
(10, 2, 5, 'municipality', NULL, NULL),
(11, 3, 5, 'municipality', NULL, NULL),
(13, 2, 7, 'municipality', NULL, NULL),
(14, 1, 6, 'interest_site', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'al4cdeveloper', 'dev@tupaseo.travel', '$2y$10$Q8tO81UTNhUwBTHh.7Qcx.5jCmzcmCXiA4/ajctk63oaJ1zAAcg6y', 'gubCHkVIRhxSykjlqUVLym7vxv1M6cGgPHPHePfWSFtbEmIBTE51wwi2bann', '2018-12-04 03:21:35', '2018-12-04 03:21:35');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_password_resets`
--

CREATE TABLE `admin_password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `circuits`
--

CREATE TABLE `circuits` (
  `id_circuit` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `short_description` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `description_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `recommendations` text COLLATE utf8_unicode_ci,
  `recommendation_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `text_images` text COLLATE utf8_unicode_ci,
  `card_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `introduction_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `download_file` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `circuit_detail_items`
--

CREATE TABLE `circuit_detail_items` (
  `id_detail_item` int(10) UNSIGNED NOT NULL,
  `image_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `fk_circuit` int(10) UNSIGNED NOT NULL,
  `type_relation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fk_relation` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `circuit_images`
--

CREATE TABLE `circuit_images` (
  `id_circuit_image` int(10) UNSIGNED NOT NULL,
  `link_image` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `fk_circuit` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `concessions`
--

CREATE TABLE `concessions` (
  `id_concession` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `acronym` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(191) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacts`
--

CREATE TABLE `contacts` (
  `id_contact` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `customers`
--

CREATE TABLE `customers` (
  `id_customer` int(10) UNSIGNED NOT NULL,
  `nit` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone_contact` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_contact` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departments`
--

CREATE TABLE `departments` (
  `id_department` int(10) UNSIGNED NOT NULL,
  `department_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `short_description` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `multimedia_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `latitude` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `longitude` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description_image` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `text_images` text COLLATE utf8_unicode_ci,
  `title_extra_description` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `extra_description` text COLLATE utf8_unicode_ci,
  `video_youtube` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type_last_user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fk_last_edition` int(11) NOT NULL,
  `fk_region` int(10) UNSIGNED NOT NULL,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `department_images`
--

CREATE TABLE `department_images` (
  `id_department_image` int(10) UNSIGNED NOT NULL,
  `link_image` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `fk_department` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `establishments`
--

CREATE TABLE `establishments` (
  `id_establishment` int(10) UNSIGNED NOT NULL,
  `type_establishment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `establishment_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` int(11) NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `latitude` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `longitude` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instagram` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kitchen_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fk_operator` int(11) DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `url_redireccion` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `card_image` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `introduction_image` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `internal_state` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'admin',
  `fk_municipality` int(10) UNSIGNED NOT NULL,
  `outstanding` double NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `establishment_images`
--

CREATE TABLE `establishment_images` (
  `id_establishment_image` int(10) UNSIGNED NOT NULL,
  `link_image` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `fk_establishment` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `establishment_schedules`
--

CREATE TABLE `establishment_schedules` (
  `id_establishment_schedule` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `start` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `end` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  `fk_establishment` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fairs`
--

CREATE TABLE `fairs` (
  `id_fair` int(10) UNSIGNED NOT NULL,
  `fair_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `fk_municipality` int(10) UNSIGNED NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `multimedia_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `link_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `outstanding` tinyint(1) NOT NULL DEFAULT '0',
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fair_images`
--

CREATE TABLE `fair_images` (
  `id_fair_image` int(10) UNSIGNED NOT NULL,
  `link_image` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `fk_fair` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `history_regions`
--

CREATE TABLE `history_regions` (
  `id_history_region` int(10) UNSIGNED NOT NULL,
  `description_to_approve` text COLLATE utf8_unicode_ci NOT NULL,
  `fk_region` int(10) UNSIGNED NOT NULL,
  `fk_reporter` int(10) UNSIGNED NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `approved_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `interest_sites`
--

CREATE TABLE `interest_sites` (
  `id_site` int(10) UNSIGNED NOT NULL,
  `site_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fk_category` int(10) UNSIGNED NOT NULL,
  `fk_municipality` int(10) UNSIGNED NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `web` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instagram` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `youtube` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img_360` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video_youtube` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `iframe` text COLLATE utf8_unicode_ci,
  `multimedia_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `latitude` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `longitude` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link_icon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `interest_site_categories`
--

CREATE TABLE `interest_site_categories` (
  `id_category` int(10) UNSIGNED NOT NULL,
  `category_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `default_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `default_icon` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `interest_site_images`
--

CREATE TABLE `interest_site_images` (
  `id_site_image` int(10) UNSIGNED NOT NULL,
  `link_image` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `fk_site` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(114, '2014_10_12_000000_create_users_table', 1),
(115, '2014_10_12_100000_create_password_resets_table', 1),
(116, '2018_01_22_154619_create_admins_table', 1),
(117, '2018_01_22_154620_create_admin_password_resets_table', 1),
(118, '2018_01_22_195432_create_regions_table', 1),
(119, '2018_01_22_200727_create_reporters_table', 1),
(120, '2018_01_22_200728_create_reporter_password_resets_table', 1),
(121, '2018_01_22_203511_create_history_regions_table', 1),
(122, '2018_01_23_181617_create_region_images_table', 1),
(123, '2018_01_25_172538_create_departments_table', 1),
(124, '2018_01_25_204838_create_department_images_table', 1),
(125, '2018_01_25_212046_create_municipalities_table', 1),
(126, '2018_01_29_170933_create_municipality_images_table', 1),
(127, '2018_01_29_180040_create_interest_site_categories_table', 1),
(128, '2018_01_29_214958_create_interest_sites_table', 1),
(129, '2018_02_01_175959_create_interest_site_images_table', 1),
(130, '2018_02_05_213347_create_circuits_table', 1),
(131, '2018_02_05_213625_create_relation_circuits_table', 1),
(132, '2018_02_06_211404_create_fairs_table', 1),
(133, '2018_02_07_210812_create_fair_images_table', 1),
(134, '2018_02_08_172030_create_portable_documents_table', 1),
(135, '2018_02_08_174141_create_videos_table', 1),
(136, '2018_02_08_204645_create_user_password_resets_table', 1),
(137, '2018_02_08_211010_create_operators_table', 1),
(138, '2018_02_08_211011_create_operator_password_resets_table', 1),
(139, '2018_02_09_165451_create_service_categories_table', 1),
(140, '2018_02_09_165823_create_services_table', 1),
(141, '2018_02_09_165903_create_service_operators_table', 1),
(142, '2018_02_09_170002_create_contacts_table', 1),
(143, '2018_02_09_170049_create_schedules_table', 1),
(144, '2018_02_09_170158_create_reservations_table', 1),
(145, '2018_02_09_170744_create_service_pictures_table', 1),
(146, '2018_02_09_175954_create_polls_table', 1),
(147, '2018_02_09_203947_create_service_items_table', 1),
(148, '2018_02_12_212803_create_wallpapers_table', 1),
(149, '2018_03_07_161448_create_customers_table', 1),
(150, '2018_03_09_164310_create_page_parts_table', 1),
(151, '2018_03_09_175043_create_pages_table', 1),
(152, '2018_03_09_175907_create_page_part_pages_table', 1),
(153, '2018_03_12_170357_create_patterns_table', 1),
(154, '2018_03_12_182258_create_pattern_parts_table', 1),
(155, '2018_09_10_205131_create_specials_table', 1),
(156, '2018_09_14_172421_create_special_images_table', 1),
(157, '2018_09_17_211952_create_establishments_table', 1),
(158, '2018_09_19_174530_create_service_establishments_table', 1),
(159, '2018_09_19_174923_create_service_establishment_pivots_table', 1),
(160, '2018_09_19_175045_create_plates_table', 1),
(161, '2018_09_19_175415_create_establishment_schedules_table', 1),
(162, '2018_09_19_175533_create_rooms_table', 1),
(163, '2018_09_20_155445_create_establishment_images_table', 1),
(164, '2018_10_29_175850_create_activities_table', 1),
(165, '2018_10_29_214330_create_activity_pivots_table', 1),
(166, '2018_11_21_221335_create_special_items_table', 1),
(167, '2018_11_21_221700_create_special_detail_items_table', 1),
(168, '2018_11_22_181952_create_special_routes_table', 1),
(169, '2018_11_27_173839_create_route_points_table', 1),
(170, '2018_12_03_210010_create_routes_table', 1),
(172, '2018_12_03_210250_create_real_route_points_table', 2),
(175, '2018_12_05_003615_create_concessions_table', 3),
(176, '2018_12_05_174138_create_tolls_table', 4),
(177, '2018_12_06_013554_create_route_images_table', 5),
(178, '2018_12_06_030604_create_circuits_table', 6),
(180, '2018_12_06_053201_create_circuit_detail_items_table', 7),
(181, '2018_12_06_182111_create_circuit_images_table', 8),
(183, '2018_12_07_023303_create_municipality_keydatas_table', 9),
(184, '2018_12_07_030419_create_site_keydatas_table', 10),
(185, '2018_12_12_190624_create_special_route_images_table', 11);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `municipalities`
--

CREATE TABLE `municipalities` (
  `id_municipality` int(10) UNSIGNED NOT NULL,
  `municipality_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `multimedia_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `latitude` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `longitude` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type_last_user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fk_last_edition` int(11) NOT NULL,
  `fk_department` int(10) UNSIGNED NOT NULL,
  `city` tinyint(1) NOT NULL,
  `mayor_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mayor_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mayor_description` text COLLATE utf8_unicode_ci,
  `logo_partner` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flag` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shield` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `show_additional_data` tinyint(1) NOT NULL DEFAULT '0',
  `img_360` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video_youtube` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `iframe` text COLLATE utf8_unicode_ci,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `outstanding` tinyint(1) NOT NULL DEFAULT '0',
  `front_state` varchar(191) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'inactivo',
  `front_state_hotel` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'inactivo',
  `front_state_restaurant` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'inactivo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `municipality_images`
--

CREATE TABLE `municipality_images` (
  `id_municipality_image` int(10) UNSIGNED NOT NULL,
  `link_image` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `fk_municipality` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `municipality_keydatas`
--

CREATE TABLE `municipality_keydatas` (
  `id_keydata` int(10) UNSIGNED NOT NULL,
  `keydata_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keydata_value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fk_municipality` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `operators`
--

CREATE TABLE `operators` (
  `id_operator` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `national_register` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_personal` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nit_rut` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `service_category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `web` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instagram` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `days_for_reservation` int(11) NOT NULL DEFAULT '2',
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'operator',
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `service_panel` tinyint(1) NOT NULL DEFAULT '0',
  `cuantity_services` int(11) NOT NULL DEFAULT '3',
  `slug` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `outstanding` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `operator_password_resets`
--

CREATE TABLE `operator_password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pages`
--

CREATE TABLE `pages` (
  `id_page` int(10) UNSIGNED NOT NULL,
  `page_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `page_parts`
--

CREATE TABLE `page_parts` (
  `id_part` int(10) UNSIGNED NOT NULL,
  `part_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `page_part_pages`
--

CREATE TABLE `page_part_pages` (
  `id_part_page` int(10) UNSIGNED NOT NULL,
  `fk_page` int(10) UNSIGNED NOT NULL,
  `fk_part` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `patterns`
--

CREATE TABLE `patterns` (
  `id_pattern` int(10) UNSIGNED NOT NULL,
  `fk_customer` int(10) UNSIGNED NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `multimedia_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `clicks` int(11) DEFAULT NULL,
  `redirection` int(11) DEFAULT NULL,
  `publication_day` date NOT NULL,
  `publication_finish` date NOT NULL,
  `pattern` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pattern_parts`
--

CREATE TABLE `pattern_parts` (
  `id_pattern_part` int(10) UNSIGNED NOT NULL,
  `fk_pattern` int(10) UNSIGNED NOT NULL,
  `fk_pagepart` int(10) UNSIGNED NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `clicks` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plates`
--

CREATE TABLE `plates` (
  `id_plate` int(10) UNSIGNED NOT NULL,
  `plate` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cost` int(11) NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fk_establishment` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `polls`
--

CREATE TABLE `polls` (
  `id_poll` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` int(11) NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `activities` text COLLATE utf8_unicode_ci NOT NULL,
  `observations` text COLLATE utf8_unicode_ci,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `portable_documents`
--

CREATE TABLE `portable_documents` (
  `id_pdf` int(10) UNSIGNED NOT NULL,
  `link_pdf` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `type_relation` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `fk_relation` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `real_route_points`
--

CREATE TABLE `real_route_points` (
  `id_point` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `kilometer` tinyint(4) NOT NULL,
  `type_relation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fk_relation` int(11) NOT NULL,
  `fk_route` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `regions`
--

CREATE TABLE `regions` (
  `id_region` int(10) UNSIGNED NOT NULL,
  `region_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sentence` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `dark_color` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `light_color` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `multimedia_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type_last_user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fk_last_edition` int(11) NOT NULL,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `regions`
--

INSERT INTO `regions` (`id_region`, `region_name`, `sentence`, `description`, `dark_color`, `light_color`, `multimedia_type`, `slug`, `type_last_user`, `link_image`, `fk_last_edition`, `keywords`, `state`, `created_at`, `updated_at`) VALUES
(1, 'Amazonia', 'Un refugio de naturaleza y tranquilidad', 'Caracterizada por su clima húmedo y una biodiversidad increíble, la región de la Amazonía es la más extensa de nuestro país. Es el destino perfecto para los interesados en una alternativa ecológica, etnoturística y de aventura.<br>\nAdemás de ser el hábitat de cientos de especies de animales y plantas, la región acoge varios grupos indígenas en resguardos, reflejo de la multiplicidad étnica y herencia ancestral que se erige como uno de los rasgos más representativos de esta región.<br>\nTe recomendamos:<br>\n1. Reserva Natural Tanimboca (Amazonas)<br>\n2. Sibundoy (Putumayo)<br>\n3. Los cerros de Mavecure (Guainía)', '#BF8E71', '#EBD4C5', 'images', 'amazonia', 'admin', 'fotos_regions/regionamazonas.jpg', 1, 'region amazonas', 'activo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Antioquia y Eje Cafetero', 'La tradición de Antioquia y la región Cafetera', 'Antioquia y la región Cafetera son una amalgama de cultura, tradición y, por supuesto, mucho café. Sus pueblos conservan en gran medida el estilo arquitectónico propio de la colonia y su gente es reconocida por la amabilidad y trato cordial.<br>\nEs una región biodiversa que combina las zonas rurales con la ciudad. Ofrece escenarios naturales que van desde las aguas termales hasta los páramos; se reconoce por sus montañas verdes y sus terrenos que contrastan con plantaciones de café.<br>\nPuedes visitar:<br>\n1. Valle del Cocora (Quindío)<br>\n2. Termales de Santa Rosa de Cabal (Risaralda)<br>\n3. Guatapé (Antioquia)', '#EB5B56', '#F9CDD0', 'images', 'antioquia_y_eje_cafetero', 'admin', 'fotos_regions/regioncafetera.jpg', 1, 'region antioquia y eje cafetero', 'activo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Caribe', 'Sabor y alegría en la región Caribe', 'Ubicada en el norte del país, la región Caribe evoca el folclor. Su hidrografía, parques naturales, serranías, santuarios de flora y fauna la convierten en un destino  que ofrece bellos paisajes y planes ecológicos.<br>\nSu amplia oferta cultural comprende festivales y ritmos como el vallenato, el merecumbé y la cumbia. Su economía, derivada de su ubicación y clima cálido, se basa en el turismo, la agricultura, la pesca y la industria.<br>\nConoce más:<br>\n1. Ciudad Perdida (Magdalena)<br>\n2. Parque Nacional Macuira (La Guajira)<br>\n3. Santa Cruz de Lorica (Córdoba)', '#009984', '#C0E1D7', 'images', 'caribe', 'admin', 'fotos_regions/regioncaribe.jpg', 1, 'region caribe', 'activo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Centro', 'Un espacio de diversidad', 'En el corazón del país, la región central se caracteriza por su riqueza natural. La variedad de pisos térmicos que posee, permiten que aquí se encuentren nevados, bosques, valles, mesetas y páramos. Así mismo, su oferta gastronómica es amplia y exquisita.<br>\nEsta zona que baila al ritmo del bambuco, el pasillo y la carranga, le brinda al turista un sinfín de actividades de carácter recreativo, ecológico, cultural, deportivo, de relajación o aventura y más.<br>\nPuedes visitar:<br>\n1. Museo Nacional de Colombia (Cundinamarca)<br>\n2. Playa de Belén (Norte de Santander)<br>\n3. Santuario de Flora y Fauna Iguaque (Boyacá)', '#009984', '#c0e1d7', 'images', 'centro', 'admin', 'fotos_regions/regioncentro.jpg', 1, 'region centro', 'activo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Pacífico', 'Tierra de paraísos naturales', 'Esta región es la única del país con vista a ambos océanos: el Atlántico y el Pacífico. Sus playas se reconocen por su arena oscura y la economía se basa en el turismo, la pesca y el comercio.<br>\nSus paisajes inalterados por el ser humano, que van desde la playa hasta la selva, se convierten en la mejor opción para el ecoturismo y las actividades de aventura. Entre los destinos están los manglares, cascadas junto al mar, pozos de aguas termales y reservas naturales.<br>\nPuedes visitar:<br>\n1. Bahía Solano (Chocó)<br>\n2. Juanchaco y Ladrilleros (Buenaventura, Valle del Cauca)<br>\n3. Arco Natural del Morro (Tumaco, Nariño)', '#0081c1', '#92D2E5', 'images', 'pacifico', 'admin', 'fotos_regions/regionpacifico.jpg', 1, 'region pacifico', 'activo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'Suroccidente', 'Historia y religión en el Suroccidente', 'Quien visite la región suroccidental puede contemplar paisajes conformados por montañas, lagunas, ríos, al igual que volcanes y planicies áridas.<br>\nEn cuanto a la cultura, el suroccidente del país es reconocido por su fe y devoción, sus carnavales y su riqueza histórica. Las celebraciones de Semana Santa, en Popayán, el Festival de Negros y Blancos, las fiestas de San Juan o los bailes a ritmo del famoso sanjuanero huilense son un reflejo de ello.<br>\nTe recomendamos:<br>\n1. Santuario de Flora y Fauna volcán Galeras (Nariño)<br>\n2. Parque Nacional Natural Puracé (Cauca)<br>\n3. Represa del Betania (Huila)', '#A2559D', '#EDD7EA', 'images', 'suroccidente', 'admin', 'fotos_regions/regionsuroccidente.jpg', 1, 'region suroccidente', 'activo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'Llanos Orientales', 'La cultura ganadera en la región de los Llanos', 'Atravesada por el río Orinoco, además de sus corrientes hídricas, su variedad ecosistémica permite el desarrollo de varias especies de peces, aves y mamíferos. Sus atardeceres enamoran a propios y visitantes mientras recorren sus planicies.<br>\nLa actividad ganadera y las labores del campo se han convertido en parte fundamental de la cultura que define el estilo de vida, los hábitos y las prácticas de sus habitantes. Los pasos rápidos del joropo y los festivales musicales son un claro ejemplo de la tradición artística de esta región.<br>\nPuedes visitar:<br>\n1. Ciudad de Piedra (Guaviare)<br>\n2. Acuaparque Las Toninas (Arauca)<br>\n3. Parque Nacional El Tuparro (Vichada)', '#D7A823', '#FFDF9B', 'images', 'llanos_orientales', 'admin', 'fotos_regions/regionllanos.jpg', 1, 'region llanos', 'activo', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `region_images`
--

CREATE TABLE `region_images` (
  `id_region_image` int(10) UNSIGNED NOT NULL,
  `link_image` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `fk_region` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reporters`
--

CREATE TABLE `reporters` (
  `id_reporter` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rol` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reporter_password_resets`
--

CREATE TABLE `reporter_password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reservations`
--

CREATE TABLE `reservations` (
  `id_reservation` int(10) UNSIGNED NOT NULL,
  `fk_service` int(10) UNSIGNED NOT NULL,
  `fk_user` int(10) UNSIGNED NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cuantity` int(11) NOT NULL,
  `comment` text COLLATE utf8_unicode_ci,
  `calification` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rooms`
--

CREATE TABLE `rooms` (
  `id_room` int(10) UNSIGNED NOT NULL,
  `room` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cuantity` int(11) NOT NULL,
  `capacity` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bed` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `suite` tinyint(1) NOT NULL,
  `fk_establishment` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `routes`
--

CREATE TABLE `routes` (
  `id_route` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sentence` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_map` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `description_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `recommendation` text COLLATE utf8_unicode_ci NOT NULL,
  `recommendation_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `card_image` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `introduction_image` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `images` text COLLATE utf8_unicode_ci,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `route_images`
--

CREATE TABLE `route_images` (
  `id_route_image` int(10) UNSIGNED NOT NULL,
  `link_image` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `fk_route` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `route_points`
--

CREATE TABLE `route_points` (
  `id_point` int(10) UNSIGNED NOT NULL,
  `subtitle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `order` tinyint(4) NOT NULL,
  `fk_route` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `schedules`
--

CREATE TABLE `schedules` (
  `id_schedule` int(10) UNSIGNED NOT NULL,
  `fk_service` int(10) UNSIGNED NOT NULL,
  `day` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hora_inicio` time DEFAULT NULL,
  `hora_receso` time DEFAULT NULL,
  `tiempo_receso` int(11) DEFAULT NULL,
  `hora_final` time DEFAULT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `services`
--

CREATE TABLE `services` (
  `id_service` int(10) UNSIGNED NOT NULL,
  `service` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type_service` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fk_service_category` int(10) UNSIGNED NOT NULL,
  `slug` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `service_categories`
--

CREATE TABLE `service_categories` (
  `id_category` int(10) UNSIGNED NOT NULL,
  `service_category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `service_categories`
--

INSERT INTO `service_categories` (`id_category`, `service_category`, `created_at`, `updated_at`) VALUES
(1, 'Aire', '2018-12-11 23:02:26', '2018-12-11 23:02:26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `service_establishments`
--

CREATE TABLE `service_establishments` (
  `id_service_establishment` int(10) UNSIGNED NOT NULL,
  `service_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `service_establishment_pivots`
--

CREATE TABLE `service_establishment_pivots` (
  `id_service_establishment_pivot` int(10) UNSIGNED NOT NULL,
  `fk_service` int(10) UNSIGNED NOT NULL,
  `fk_establishment` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `service_items`
--

CREATE TABLE `service_items` (
  `id_service_item` int(10) UNSIGNED NOT NULL,
  `item_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cost` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `fk_service` int(10) UNSIGNED NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `service_operators`
--

CREATE TABLE `service_operators` (
  `id_service_operator` int(10) UNSIGNED NOT NULL,
  `fk_service` int(10) UNSIGNED NOT NULL,
  `fk_operator` int(10) UNSIGNED NOT NULL,
  `service_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `latitude` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `longitude` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `capacity` int(11) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `video` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `requisites` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `policies` text COLLATE utf8_unicode_ci NOT NULL,
  `fk_municipality` int(10) UNSIGNED NOT NULL,
  `fk_interest_site` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Inhabilitado',
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `service_pictures`
--

CREATE TABLE `service_pictures` (
  `id_picture` int(10) UNSIGNED NOT NULL,
  `link_image` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `fk_service` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `site_keydatas`
--

CREATE TABLE `site_keydatas` (
  `id_keydata` int(10) UNSIGNED NOT NULL,
  `keydata_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keydata_value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fk_site` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `specials`
--

CREATE TABLE `specials` (
  `id_special` int(10) UNSIGNED NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `short_description` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `recommendations` text COLLATE utf8_unicode_ci,
  `card_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `introduction_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `recommendation_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_to_site` text COLLATE utf8_unicode_ci,
  `text_images` text COLLATE utf8_unicode_ci,
  `video` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `iframe` text COLLATE utf8_unicode_ci,
  `download_file` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `outstanding` tinyint(1) NOT NULL DEFAULT '0',
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `special_detail_items`
--

CREATE TABLE `special_detail_items` (
  `id_detail_item` int(10) UNSIGNED NOT NULL,
  `image_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `fk_special` int(10) UNSIGNED NOT NULL,
  `type_relation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fk_relation` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `special_images`
--

CREATE TABLE `special_images` (
  `id_special_image` int(10) UNSIGNED NOT NULL,
  `link_image` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `fk_special` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `special_items`
--

CREATE TABLE `special_items` (
  `id_item` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `fk_special` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `special_routes`
--

CREATE TABLE `special_routes` (
  `id_route` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sentence` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `length` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `difficulty` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `video` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_map` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `highest_point` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `lower_point` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `description_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `card_image` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `introduction_image` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `recommendation` text COLLATE utf8_unicode_ci NOT NULL,
  `text_images` text COLLATE utf8_unicode_ci,
  `recommendation_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type_startpoint` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `start_point` int(11) NOT NULL,
  `type_endpoint` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `end_point` int(11) NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fk_special` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `special_route_images`
--

CREATE TABLE `special_route_images` (
  `id_route_image` int(10) UNSIGNED NOT NULL,
  `link_image` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `fk_route` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tolls`
--

CREATE TABLE `tolls` (
  `id_toll` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cost_1` int(11) DEFAULT NULL,
  `cost_2` int(11) DEFAULT NULL,
  `cost_3` int(11) DEFAULT NULL,
  `cost_4` int(11) DEFAULT NULL,
  `cost_5` int(11) DEFAULT NULL,
  `cost_6` int(11) DEFAULT NULL,
  `cost_7` int(11) DEFAULT NULL,
  `fk_site` int(10) UNSIGNED NOT NULL,
  `fk_concession` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'user',
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_change` tinyint(1) NOT NULL DEFAULT '1',
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_password_resets`
--

CREATE TABLE `user_password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `videos`
--

CREATE TABLE `videos` (
  `id_video` int(10) UNSIGNED NOT NULL,
  `link_video` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `type_relation` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `fk_relation` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wallpapers`
--

CREATE TABLE `wallpapers` (
  `id_wallpaper` int(10) UNSIGNED NOT NULL,
  `link_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `primary_text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `primary_color` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `secundary_text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `secundary_color` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url_redirection` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`id_activity`);

--
-- Indices de la tabla `activity_pivots`
--
ALTER TABLE `activity_pivots`
  ADD PRIMARY KEY (`id_activity_pivot`),
  ADD KEY `activity_pivots_fk_actity_foreign` (`fk_actity`);

--
-- Indices de la tabla `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indices de la tabla `admin_password_resets`
--
ALTER TABLE `admin_password_resets`
  ADD KEY `admin_password_resets_email_index` (`email`),
  ADD KEY `admin_password_resets_token_index` (`token`);

--
-- Indices de la tabla `circuits`
--
ALTER TABLE `circuits`
  ADD PRIMARY KEY (`id_circuit`);

--
-- Indices de la tabla `circuit_detail_items`
--
ALTER TABLE `circuit_detail_items`
  ADD PRIMARY KEY (`id_detail_item`),
  ADD KEY `circuit_detail_items_fk_circuit_foreign` (`fk_circuit`);

--
-- Indices de la tabla `circuit_images`
--
ALTER TABLE `circuit_images`
  ADD PRIMARY KEY (`id_circuit_image`),
  ADD KEY `circuit_images_fk_circuit_foreign` (`fk_circuit`);

--
-- Indices de la tabla `concessions`
--
ALTER TABLE `concessions`
  ADD PRIMARY KEY (`id_concession`);

--
-- Indices de la tabla `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id_contact`);

--
-- Indices de la tabla `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id_customer`);

--
-- Indices de la tabla `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id_department`),
  ADD KEY `departments_fk_region_foreign` (`fk_region`);

--
-- Indices de la tabla `department_images`
--
ALTER TABLE `department_images`
  ADD PRIMARY KEY (`id_department_image`),
  ADD KEY `department_images_fk_department_foreign` (`fk_department`);

--
-- Indices de la tabla `establishments`
--
ALTER TABLE `establishments`
  ADD PRIMARY KEY (`id_establishment`),
  ADD KEY `establishments_fk_municipality_foreign` (`fk_municipality`);

--
-- Indices de la tabla `establishment_images`
--
ALTER TABLE `establishment_images`
  ADD PRIMARY KEY (`id_establishment_image`),
  ADD KEY `establishment_images_fk_establishment_foreign` (`fk_establishment`);

--
-- Indices de la tabla `establishment_schedules`
--
ALTER TABLE `establishment_schedules`
  ADD PRIMARY KEY (`id_establishment_schedule`),
  ADD KEY `establishment_schedules_fk_establishment_foreign` (`fk_establishment`);

--
-- Indices de la tabla `fairs`
--
ALTER TABLE `fairs`
  ADD PRIMARY KEY (`id_fair`),
  ADD KEY `fairs_fk_municipality_foreign` (`fk_municipality`);

--
-- Indices de la tabla `fair_images`
--
ALTER TABLE `fair_images`
  ADD PRIMARY KEY (`id_fair_image`),
  ADD KEY `fair_images_fk_fair_foreign` (`fk_fair`);

--
-- Indices de la tabla `history_regions`
--
ALTER TABLE `history_regions`
  ADD PRIMARY KEY (`id_history_region`),
  ADD KEY `history_regions_fk_region_foreign` (`fk_region`),
  ADD KEY `history_regions_fk_reporter_foreign` (`fk_reporter`);

--
-- Indices de la tabla `interest_sites`
--
ALTER TABLE `interest_sites`
  ADD PRIMARY KEY (`id_site`),
  ADD KEY `interest_sites_fk_category_foreign` (`fk_category`),
  ADD KEY `interest_sites_fk_municipality_foreign` (`fk_municipality`);

--
-- Indices de la tabla `interest_site_categories`
--
ALTER TABLE `interest_site_categories`
  ADD PRIMARY KEY (`id_category`);

--
-- Indices de la tabla `interest_site_images`
--
ALTER TABLE `interest_site_images`
  ADD PRIMARY KEY (`id_site_image`),
  ADD KEY `interest_site_images_fk_site_foreign` (`fk_site`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `municipalities`
--
ALTER TABLE `municipalities`
  ADD PRIMARY KEY (`id_municipality`),
  ADD KEY `municipalities_fk_department_foreign` (`fk_department`);

--
-- Indices de la tabla `municipality_images`
--
ALTER TABLE `municipality_images`
  ADD PRIMARY KEY (`id_municipality_image`),
  ADD KEY `municipality_images_fk_municipality_foreign` (`fk_municipality`);

--
-- Indices de la tabla `municipality_keydatas`
--
ALTER TABLE `municipality_keydatas`
  ADD PRIMARY KEY (`id_keydata`),
  ADD KEY `municipality_keydatas_fk_municipality_foreign` (`fk_municipality`);

--
-- Indices de la tabla `operators`
--
ALTER TABLE `operators`
  ADD PRIMARY KEY (`id_operator`),
  ADD UNIQUE KEY `operators_email_unique` (`email`);

--
-- Indices de la tabla `operator_password_resets`
--
ALTER TABLE `operator_password_resets`
  ADD KEY `operator_password_resets_email_index` (`email`),
  ADD KEY `operator_password_resets_token_index` (`token`);

--
-- Indices de la tabla `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id_page`);

--
-- Indices de la tabla `page_parts`
--
ALTER TABLE `page_parts`
  ADD PRIMARY KEY (`id_part`);

--
-- Indices de la tabla `page_part_pages`
--
ALTER TABLE `page_part_pages`
  ADD PRIMARY KEY (`id_part_page`),
  ADD KEY `page_part_pages_fk_page_foreign` (`fk_page`),
  ADD KEY `page_part_pages_fk_part_foreign` (`fk_part`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `patterns`
--
ALTER TABLE `patterns`
  ADD PRIMARY KEY (`id_pattern`),
  ADD KEY `patterns_fk_customer_foreign` (`fk_customer`);

--
-- Indices de la tabla `pattern_parts`
--
ALTER TABLE `pattern_parts`
  ADD PRIMARY KEY (`id_pattern_part`),
  ADD KEY `pattern_parts_fk_pattern_foreign` (`fk_pattern`),
  ADD KEY `pattern_parts_fk_pagepart_foreign` (`fk_pagepart`);

--
-- Indices de la tabla `plates`
--
ALTER TABLE `plates`
  ADD PRIMARY KEY (`id_plate`),
  ADD KEY `plates_fk_establishment_foreign` (`fk_establishment`);

--
-- Indices de la tabla `polls`
--
ALTER TABLE `polls`
  ADD PRIMARY KEY (`id_poll`);

--
-- Indices de la tabla `portable_documents`
--
ALTER TABLE `portable_documents`
  ADD PRIMARY KEY (`id_pdf`);

--
-- Indices de la tabla `real_route_points`
--
ALTER TABLE `real_route_points`
  ADD PRIMARY KEY (`id_point`),
  ADD KEY `real_route_points_fk_route_foreign` (`fk_route`);

--
-- Indices de la tabla `regions`
--
ALTER TABLE `regions`
  ADD PRIMARY KEY (`id_region`);

--
-- Indices de la tabla `region_images`
--
ALTER TABLE `region_images`
  ADD PRIMARY KEY (`id_region_image`),
  ADD KEY `region_images_fk_region_foreign` (`fk_region`);

--
-- Indices de la tabla `reporters`
--
ALTER TABLE `reporters`
  ADD PRIMARY KEY (`id_reporter`),
  ADD UNIQUE KEY `reporters_email_unique` (`email`);

--
-- Indices de la tabla `reporter_password_resets`
--
ALTER TABLE `reporter_password_resets`
  ADD KEY `reporter_password_resets_email_index` (`email`),
  ADD KEY `reporter_password_resets_token_index` (`token`);

--
-- Indices de la tabla `reservations`
--
ALTER TABLE `reservations`
  ADD PRIMARY KEY (`id_reservation`),
  ADD KEY `reservations_fk_service_foreign` (`fk_service`),
  ADD KEY `reservations_fk_user_foreign` (`fk_user`);

--
-- Indices de la tabla `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id_room`),
  ADD KEY `rooms_fk_establishment_foreign` (`fk_establishment`);

--
-- Indices de la tabla `routes`
--
ALTER TABLE `routes`
  ADD PRIMARY KEY (`id_route`);

--
-- Indices de la tabla `route_images`
--
ALTER TABLE `route_images`
  ADD PRIMARY KEY (`id_route_image`),
  ADD KEY `route_images_fk_route_foreign` (`fk_route`);

--
-- Indices de la tabla `route_points`
--
ALTER TABLE `route_points`
  ADD PRIMARY KEY (`id_point`),
  ADD KEY `route_points_fk_route_foreign` (`fk_route`);

--
-- Indices de la tabla `schedules`
--
ALTER TABLE `schedules`
  ADD PRIMARY KEY (`id_schedule`),
  ADD KEY `schedules_fk_service_foreign` (`fk_service`);

--
-- Indices de la tabla `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id_service`),
  ADD KEY `services_fk_service_category_foreign` (`fk_service_category`);

--
-- Indices de la tabla `service_categories`
--
ALTER TABLE `service_categories`
  ADD PRIMARY KEY (`id_category`);

--
-- Indices de la tabla `service_establishments`
--
ALTER TABLE `service_establishments`
  ADD PRIMARY KEY (`id_service_establishment`);

--
-- Indices de la tabla `service_establishment_pivots`
--
ALTER TABLE `service_establishment_pivots`
  ADD PRIMARY KEY (`id_service_establishment_pivot`),
  ADD KEY `service_establishment_pivots_fk_service_foreign` (`fk_service`),
  ADD KEY `service_establishment_pivots_fk_establishment_foreign` (`fk_establishment`);

--
-- Indices de la tabla `service_items`
--
ALTER TABLE `service_items`
  ADD PRIMARY KEY (`id_service_item`),
  ADD KEY `service_items_fk_service_foreign` (`fk_service`);

--
-- Indices de la tabla `service_operators`
--
ALTER TABLE `service_operators`
  ADD PRIMARY KEY (`id_service_operator`),
  ADD KEY `service_operators_fk_service_foreign` (`fk_service`),
  ADD KEY `service_operators_fk_operator_foreign` (`fk_operator`),
  ADD KEY `service_operators_fk_municipality_foreign` (`fk_municipality`);

--
-- Indices de la tabla `service_pictures`
--
ALTER TABLE `service_pictures`
  ADD PRIMARY KEY (`id_picture`),
  ADD KEY `service_pictures_fk_service_foreign` (`fk_service`);

--
-- Indices de la tabla `site_keydatas`
--
ALTER TABLE `site_keydatas`
  ADD PRIMARY KEY (`id_keydata`),
  ADD KEY `site_keydatas_fk_site_foreign` (`fk_site`);

--
-- Indices de la tabla `specials`
--
ALTER TABLE `specials`
  ADD PRIMARY KEY (`id_special`);

--
-- Indices de la tabla `special_detail_items`
--
ALTER TABLE `special_detail_items`
  ADD PRIMARY KEY (`id_detail_item`),
  ADD KEY `special_detail_items_fk_special_foreign` (`fk_special`);

--
-- Indices de la tabla `special_images`
--
ALTER TABLE `special_images`
  ADD PRIMARY KEY (`id_special_image`),
  ADD KEY `special_images_fk_special_foreign` (`fk_special`);

--
-- Indices de la tabla `special_items`
--
ALTER TABLE `special_items`
  ADD PRIMARY KEY (`id_item`),
  ADD KEY `special_items_fk_special_foreign` (`fk_special`);

--
-- Indices de la tabla `special_routes`
--
ALTER TABLE `special_routes`
  ADD PRIMARY KEY (`id_route`),
  ADD KEY `special_routes_fk_special_foreign` (`fk_special`);

--
-- Indices de la tabla `special_route_images`
--
ALTER TABLE `special_route_images`
  ADD PRIMARY KEY (`id_route_image`),
  ADD KEY `special_route_images_fk_route_foreign` (`fk_route`);

--
-- Indices de la tabla `tolls`
--
ALTER TABLE `tolls`
  ADD PRIMARY KEY (`id_toll`),
  ADD KEY `tolls_fk_site_foreign` (`fk_site`),
  ADD KEY `tolls_fk_concession_foreign` (`fk_concession`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indices de la tabla `user_password_resets`
--
ALTER TABLE `user_password_resets`
  ADD KEY `user_password_resets_email_index` (`email`),
  ADD KEY `user_password_resets_token_index` (`token`);

--
-- Indices de la tabla `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id_video`);

--
-- Indices de la tabla `wallpapers`
--
ALTER TABLE `wallpapers`
  ADD PRIMARY KEY (`id_wallpaper`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `activities`
--
ALTER TABLE `activities`
  MODIFY `id_activity` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `activity_pivots`
--
ALTER TABLE `activity_pivots`
  MODIFY `id_activity_pivot` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT de la tabla `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `circuits`
--
ALTER TABLE `circuits`
  MODIFY `id_circuit` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `circuit_detail_items`
--
ALTER TABLE `circuit_detail_items`
  MODIFY `id_detail_item` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `circuit_images`
--
ALTER TABLE `circuit_images`
  MODIFY `id_circuit_image` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `concessions`
--
ALTER TABLE `concessions`
  MODIFY `id_concession` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id_contact` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `customers`
--
ALTER TABLE `customers`
  MODIFY `id_customer` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `departments`
--
ALTER TABLE `departments`
  MODIFY `id_department` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `department_images`
--
ALTER TABLE `department_images`
  MODIFY `id_department_image` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `establishments`
--
ALTER TABLE `establishments`
  MODIFY `id_establishment` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `establishment_images`
--
ALTER TABLE `establishment_images`
  MODIFY `id_establishment_image` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `establishment_schedules`
--
ALTER TABLE `establishment_schedules`
  MODIFY `id_establishment_schedule` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `fairs`
--
ALTER TABLE `fairs`
  MODIFY `id_fair` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `fair_images`
--
ALTER TABLE `fair_images`
  MODIFY `id_fair_image` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `history_regions`
--
ALTER TABLE `history_regions`
  MODIFY `id_history_region` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `interest_sites`
--
ALTER TABLE `interest_sites`
  MODIFY `id_site` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `interest_site_categories`
--
ALTER TABLE `interest_site_categories`
  MODIFY `id_category` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `interest_site_images`
--
ALTER TABLE `interest_site_images`
  MODIFY `id_site_image` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=186;
--
-- AUTO_INCREMENT de la tabla `municipalities`
--
ALTER TABLE `municipalities`
  MODIFY `id_municipality` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `municipality_images`
--
ALTER TABLE `municipality_images`
  MODIFY `id_municipality_image` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `municipality_keydatas`
--
ALTER TABLE `municipality_keydatas`
  MODIFY `id_keydata` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `operators`
--
ALTER TABLE `operators`
  MODIFY `id_operator` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `pages`
--
ALTER TABLE `pages`
  MODIFY `id_page` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `page_parts`
--
ALTER TABLE `page_parts`
  MODIFY `id_part` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `page_part_pages`
--
ALTER TABLE `page_part_pages`
  MODIFY `id_part_page` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `patterns`
--
ALTER TABLE `patterns`
  MODIFY `id_pattern` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `pattern_parts`
--
ALTER TABLE `pattern_parts`
  MODIFY `id_pattern_part` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `plates`
--
ALTER TABLE `plates`
  MODIFY `id_plate` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `polls`
--
ALTER TABLE `polls`
  MODIFY `id_poll` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `portable_documents`
--
ALTER TABLE `portable_documents`
  MODIFY `id_pdf` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `real_route_points`
--
ALTER TABLE `real_route_points`
  MODIFY `id_point` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `regions`
--
ALTER TABLE `regions`
  MODIFY `id_region` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `region_images`
--
ALTER TABLE `region_images`
  MODIFY `id_region_image` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `reporters`
--
ALTER TABLE `reporters`
  MODIFY `id_reporter` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `reservations`
--
ALTER TABLE `reservations`
  MODIFY `id_reservation` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id_room` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `routes`
--
ALTER TABLE `routes`
  MODIFY `id_route` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `route_images`
--
ALTER TABLE `route_images`
  MODIFY `id_route_image` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `route_points`
--
ALTER TABLE `route_points`
  MODIFY `id_point` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `schedules`
--
ALTER TABLE `schedules`
  MODIFY `id_schedule` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `services`
--
ALTER TABLE `services`
  MODIFY `id_service` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `service_categories`
--
ALTER TABLE `service_categories`
  MODIFY `id_category` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `service_establishments`
--
ALTER TABLE `service_establishments`
  MODIFY `id_service_establishment` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `service_establishment_pivots`
--
ALTER TABLE `service_establishment_pivots`
  MODIFY `id_service_establishment_pivot` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `service_items`
--
ALTER TABLE `service_items`
  MODIFY `id_service_item` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `service_operators`
--
ALTER TABLE `service_operators`
  MODIFY `id_service_operator` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `service_pictures`
--
ALTER TABLE `service_pictures`
  MODIFY `id_picture` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `site_keydatas`
--
ALTER TABLE `site_keydatas`
  MODIFY `id_keydata` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `specials`
--
ALTER TABLE `specials`
  MODIFY `id_special` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `special_detail_items`
--
ALTER TABLE `special_detail_items`
  MODIFY `id_detail_item` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `special_images`
--
ALTER TABLE `special_images`
  MODIFY `id_special_image` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `special_items`
--
ALTER TABLE `special_items`
  MODIFY `id_item` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `special_routes`
--
ALTER TABLE `special_routes`
  MODIFY `id_route` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `special_route_images`
--
ALTER TABLE `special_route_images`
  MODIFY `id_route_image` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tolls`
--
ALTER TABLE `tolls`
  MODIFY `id_toll` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `videos`
--
ALTER TABLE `videos`
  MODIFY `id_video` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `wallpapers`
--
ALTER TABLE `wallpapers`
  MODIFY `id_wallpaper` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `activity_pivots`
--
ALTER TABLE `activity_pivots`
  ADD CONSTRAINT `activity_pivots_fk_actity_foreign` FOREIGN KEY (`fk_actity`) REFERENCES `activities` (`id_activity`);

--
-- Filtros para la tabla `circuit_detail_items`
--
ALTER TABLE `circuit_detail_items`
  ADD CONSTRAINT `circuit_detail_items_fk_circuit_foreign` FOREIGN KEY (`fk_circuit`) REFERENCES `circuits` (`id_circuit`);

--
-- Filtros para la tabla `circuit_images`
--
ALTER TABLE `circuit_images`
  ADD CONSTRAINT `circuit_images_fk_circuit_foreign` FOREIGN KEY (`fk_circuit`) REFERENCES `circuits` (`id_circuit`);

--
-- Filtros para la tabla `departments`
--
ALTER TABLE `departments`
  ADD CONSTRAINT `departments_fk_region_foreign` FOREIGN KEY (`fk_region`) REFERENCES `regions` (`id_region`);

--
-- Filtros para la tabla `department_images`
--
ALTER TABLE `department_images`
  ADD CONSTRAINT `department_images_fk_department_foreign` FOREIGN KEY (`fk_department`) REFERENCES `departments` (`id_department`);

--
-- Filtros para la tabla `establishments`
--
ALTER TABLE `establishments`
  ADD CONSTRAINT `establishments_fk_municipality_foreign` FOREIGN KEY (`fk_municipality`) REFERENCES `municipalities` (`id_municipality`);

--
-- Filtros para la tabla `establishment_images`
--
ALTER TABLE `establishment_images`
  ADD CONSTRAINT `establishment_images_fk_establishment_foreign` FOREIGN KEY (`fk_establishment`) REFERENCES `establishments` (`id_establishment`);

--
-- Filtros para la tabla `establishment_schedules`
--
ALTER TABLE `establishment_schedules`
  ADD CONSTRAINT `establishment_schedules_fk_establishment_foreign` FOREIGN KEY (`fk_establishment`) REFERENCES `establishments` (`id_establishment`);

--
-- Filtros para la tabla `fairs`
--
ALTER TABLE `fairs`
  ADD CONSTRAINT `fairs_fk_municipality_foreign` FOREIGN KEY (`fk_municipality`) REFERENCES `municipalities` (`id_municipality`);

--
-- Filtros para la tabla `fair_images`
--
ALTER TABLE `fair_images`
  ADD CONSTRAINT `fair_images_fk_fair_foreign` FOREIGN KEY (`fk_fair`) REFERENCES `fairs` (`id_fair`);

--
-- Filtros para la tabla `history_regions`
--
ALTER TABLE `history_regions`
  ADD CONSTRAINT `history_regions_fk_region_foreign` FOREIGN KEY (`fk_region`) REFERENCES `regions` (`id_region`),
  ADD CONSTRAINT `history_regions_fk_reporter_foreign` FOREIGN KEY (`fk_reporter`) REFERENCES `reporters` (`id_reporter`);

--
-- Filtros para la tabla `interest_sites`
--
ALTER TABLE `interest_sites`
  ADD CONSTRAINT `interest_sites_fk_category_foreign` FOREIGN KEY (`fk_category`) REFERENCES `interest_site_categories` (`id_category`),
  ADD CONSTRAINT `interest_sites_fk_municipality_foreign` FOREIGN KEY (`fk_municipality`) REFERENCES `municipalities` (`id_municipality`);

--
-- Filtros para la tabla `interest_site_images`
--
ALTER TABLE `interest_site_images`
  ADD CONSTRAINT `interest_site_images_fk_site_foreign` FOREIGN KEY (`fk_site`) REFERENCES `interest_sites` (`id_site`);

--
-- Filtros para la tabla `municipalities`
--
ALTER TABLE `municipalities`
  ADD CONSTRAINT `municipalities_fk_department_foreign` FOREIGN KEY (`fk_department`) REFERENCES `departments` (`id_department`);

--
-- Filtros para la tabla `municipality_images`
--
ALTER TABLE `municipality_images`
  ADD CONSTRAINT `municipality_images_fk_municipality_foreign` FOREIGN KEY (`fk_municipality`) REFERENCES `municipalities` (`id_municipality`);

--
-- Filtros para la tabla `municipality_keydatas`
--
ALTER TABLE `municipality_keydatas`
  ADD CONSTRAINT `municipality_keydatas_fk_municipality_foreign` FOREIGN KEY (`fk_municipality`) REFERENCES `municipalities` (`id_municipality`);

--
-- Filtros para la tabla `page_part_pages`
--
ALTER TABLE `page_part_pages`
  ADD CONSTRAINT `page_part_pages_fk_page_foreign` FOREIGN KEY (`fk_page`) REFERENCES `pages` (`id_page`),
  ADD CONSTRAINT `page_part_pages_fk_part_foreign` FOREIGN KEY (`fk_part`) REFERENCES `page_parts` (`id_part`);

--
-- Filtros para la tabla `patterns`
--
ALTER TABLE `patterns`
  ADD CONSTRAINT `patterns_fk_customer_foreign` FOREIGN KEY (`fk_customer`) REFERENCES `customers` (`id_customer`);

--
-- Filtros para la tabla `pattern_parts`
--
ALTER TABLE `pattern_parts`
  ADD CONSTRAINT `pattern_parts_fk_pagepart_foreign` FOREIGN KEY (`fk_pagepart`) REFERENCES `page_part_pages` (`id_part_page`),
  ADD CONSTRAINT `pattern_parts_fk_pattern_foreign` FOREIGN KEY (`fk_pattern`) REFERENCES `patterns` (`id_pattern`);

--
-- Filtros para la tabla `plates`
--
ALTER TABLE `plates`
  ADD CONSTRAINT `plates_fk_establishment_foreign` FOREIGN KEY (`fk_establishment`) REFERENCES `establishments` (`id_establishment`);

--
-- Filtros para la tabla `real_route_points`
--
ALTER TABLE `real_route_points`
  ADD CONSTRAINT `real_route_points_fk_route_foreign` FOREIGN KEY (`fk_route`) REFERENCES `routes` (`id_route`);

--
-- Filtros para la tabla `region_images`
--
ALTER TABLE `region_images`
  ADD CONSTRAINT `region_images_fk_region_foreign` FOREIGN KEY (`fk_region`) REFERENCES `regions` (`id_region`);

--
-- Filtros para la tabla `reservations`
--
ALTER TABLE `reservations`
  ADD CONSTRAINT `reservations_fk_service_foreign` FOREIGN KEY (`fk_service`) REFERENCES `service_operators` (`id_service_operator`),
  ADD CONSTRAINT `reservations_fk_user_foreign` FOREIGN KEY (`fk_user`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `rooms`
--
ALTER TABLE `rooms`
  ADD CONSTRAINT `rooms_fk_establishment_foreign` FOREIGN KEY (`fk_establishment`) REFERENCES `establishments` (`id_establishment`);

--
-- Filtros para la tabla `route_images`
--
ALTER TABLE `route_images`
  ADD CONSTRAINT `route_images_fk_route_foreign` FOREIGN KEY (`fk_route`) REFERENCES `routes` (`id_route`);

--
-- Filtros para la tabla `route_points`
--
ALTER TABLE `route_points`
  ADD CONSTRAINT `route_points_fk_route_foreign` FOREIGN KEY (`fk_route`) REFERENCES `special_routes` (`id_route`);

--
-- Filtros para la tabla `schedules`
--
ALTER TABLE `schedules`
  ADD CONSTRAINT `schedules_fk_service_foreign` FOREIGN KEY (`fk_service`) REFERENCES `service_operators` (`id_service_operator`);

--
-- Filtros para la tabla `services`
--
ALTER TABLE `services`
  ADD CONSTRAINT `services_fk_service_category_foreign` FOREIGN KEY (`fk_service_category`) REFERENCES `service_categories` (`id_category`);

--
-- Filtros para la tabla `service_establishment_pivots`
--
ALTER TABLE `service_establishment_pivots`
  ADD CONSTRAINT `service_establishment_pivots_fk_establishment_foreign` FOREIGN KEY (`fk_establishment`) REFERENCES `establishments` (`id_establishment`),
  ADD CONSTRAINT `service_establishment_pivots_fk_service_foreign` FOREIGN KEY (`fk_service`) REFERENCES `service_establishments` (`id_service_establishment`);

--
-- Filtros para la tabla `service_items`
--
ALTER TABLE `service_items`
  ADD CONSTRAINT `service_items_fk_service_foreign` FOREIGN KEY (`fk_service`) REFERENCES `service_operators` (`id_service_operator`);

--
-- Filtros para la tabla `service_operators`
--
ALTER TABLE `service_operators`
  ADD CONSTRAINT `service_operators_fk_municipality_foreign` FOREIGN KEY (`fk_municipality`) REFERENCES `municipalities` (`id_municipality`),
  ADD CONSTRAINT `service_operators_fk_operator_foreign` FOREIGN KEY (`fk_operator`) REFERENCES `operators` (`id_operator`),
  ADD CONSTRAINT `service_operators_fk_service_foreign` FOREIGN KEY (`fk_service`) REFERENCES `services` (`id_service`);

--
-- Filtros para la tabla `service_pictures`
--
ALTER TABLE `service_pictures`
  ADD CONSTRAINT `service_pictures_fk_service_foreign` FOREIGN KEY (`fk_service`) REFERENCES `service_operators` (`id_service_operator`);

--
-- Filtros para la tabla `site_keydatas`
--
ALTER TABLE `site_keydatas`
  ADD CONSTRAINT `site_keydatas_fk_site_foreign` FOREIGN KEY (`fk_site`) REFERENCES `interest_sites` (`id_site`);

--
-- Filtros para la tabla `special_detail_items`
--
ALTER TABLE `special_detail_items`
  ADD CONSTRAINT `special_detail_items_fk_special_foreign` FOREIGN KEY (`fk_special`) REFERENCES `specials` (`id_special`);

--
-- Filtros para la tabla `special_images`
--
ALTER TABLE `special_images`
  ADD CONSTRAINT `special_images_fk_special_foreign` FOREIGN KEY (`fk_special`) REFERENCES `specials` (`id_special`);

--
-- Filtros para la tabla `special_items`
--
ALTER TABLE `special_items`
  ADD CONSTRAINT `special_items_fk_special_foreign` FOREIGN KEY (`fk_special`) REFERENCES `specials` (`id_special`);

--
-- Filtros para la tabla `special_routes`
--
ALTER TABLE `special_routes`
  ADD CONSTRAINT `special_routes_fk_special_foreign` FOREIGN KEY (`fk_special`) REFERENCES `specials` (`id_special`);

--
-- Filtros para la tabla `special_route_images`
--
ALTER TABLE `special_route_images`
  ADD CONSTRAINT `special_route_images_fk_route_foreign` FOREIGN KEY (`fk_route`) REFERENCES `special_routes` (`id_route`);

--
-- Filtros para la tabla `tolls`
--
ALTER TABLE `tolls`
  ADD CONSTRAINT `tolls_fk_concession_foreign` FOREIGN KEY (`fk_concession`) REFERENCES `concessions` (`id_concession`),
  ADD CONSTRAINT `tolls_fk_site_foreign` FOREIGN KEY (`fk_site`) REFERENCES `interest_sites` (`id_site`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
