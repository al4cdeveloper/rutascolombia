<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpecialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('specials', function (Blueprint $table) {
            $table->increments('id_special');
            $table->string('type');
            $table->string('name');
            $table->text('keywords');
            $table->mediumText('short_description');
            $table->text('description');
            $table->text('recommendations')->nullable();
            $table->string('card_image')->nullable();
            $table->string('introduction_image')->nullable();
            $table->string('description_image')->nullable();
            $table->string('recommendation_image')->nullable();
            $table->text('description_to_site')->nullable();
            $table->string('video')->nullable();
            $table->text('iframe')->nullable();
            $table->string('download_file')->nullable();
            $table->string('slug');
            $table->boolean('outstanding')->default(0);
            $table->string('state')->default('activo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('specials');
    }
}
