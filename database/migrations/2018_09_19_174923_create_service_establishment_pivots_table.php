<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceEstablishmentPivotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_establishment_pivots', function (Blueprint $table) {
            $table->increments('id_service_establishment_pivot');
            $table->integer('fk_service')->unsigned();
            $table->foreign('fk_service')->references('id_service_establishment')->on('service_establishments');
            $table->integer('fk_establishment')->unsigned();
            $table->foreign('fk_establishment')->references('id_establishment')->on('establishments');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_establishment_pivots');
    }
}
