<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInterestSiteImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interest_site_images', function (Blueprint $table) {
            $table->increments('id_site_image');
            $table->mediumText('link_image');
            $table->integer('fk_site')->unsigned();
            $table->foreign('fk_site')->references('id_site')->on('interest_sites');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interest_site_images');
    }
}
