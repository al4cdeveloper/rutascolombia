<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFairsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fairs', function (Blueprint $table) {
            $table->increments('id_fair');
            $table->string('fair_name');
            $table->date('start_date');
            $table->date('end_date');
            $table->integer('fk_municipality')->unsigned();
            $table->foreign('fk_municipality')->references('id_municipality')->on('municipalities');
            $table->string('state')->default('activo');
            $table->string('multimedia_type');
            $table->text('keywords');
            $table->text('description');
            $table->string('link_image')->nullable();
            $table->boolean('outstanding')->default(0);
            $table->string('slug');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fairs');
    }
}
