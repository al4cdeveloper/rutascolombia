<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRealRoutePointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('real_route_points', function (Blueprint $table) {
            $table->increments('id_point');
            $table->string('title');
            $table->text('description');
            $table->tinyInteger('kilometer');
            $table->string('type_relation');
            $table->integer('fk_relation');
            $table->unsignedInteger('fk_route');
            $table->foreign('fk_route')->references('id_route')->on('routes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('real_route_points');
    }
}
