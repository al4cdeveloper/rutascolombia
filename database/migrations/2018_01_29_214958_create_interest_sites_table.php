<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInterestSitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interest_sites', function (Blueprint $table) {
            $table->increments('id_site');
            $table->string('site_name');
             $table->integer('fk_category')->unsigned();
            $table->foreign('fk_category')->references('id_category')->on('interest_site_categories');
            $table->integer('fk_municipality')->unsigned();
            $table->foreign('fk_municipality')->references('id_municipality')->on('municipalities');
            $table->string('address');
            $table->string('phone');
            $table->string('web')->nullable();
            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();
            $table->string('instagram')->nullable();
            $table->string('youtube')->nullable();
            $table->string('img_360')->nullable();
            $table->string('video_youtube')->nullable();
            $table->text('iframe')->nullable();//street view
            $table->string('multimedia_type');
            $table->string('latitude');
            $table->string('longitude');
            $table->string('slug');
            $table->string('link_image')->nullable();
            $table->string('link_icon')->nullable();
            $table->text('description')->nullable();
            $table->text('keywords');
            $table->string('state')->default('activo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interest_sites');
    }
}
