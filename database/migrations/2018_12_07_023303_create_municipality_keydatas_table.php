<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMunicipalityKeydatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('municipality_keydatas', function (Blueprint $table) {
            $table->increments('id_keydata');
            $table->string('keydata_name');
            $table->string('keydata_value');
            $table->string('category');
            $table->integer('fk_municipality')->unsigned();
            $table->foreign('fk_municipality')->references('id_municipality')->on('municipalities');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('municipality_keydatas');
    }
}
