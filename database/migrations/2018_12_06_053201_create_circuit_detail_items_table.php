<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCircuitDetailItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('circuit_detail_items', function (Blueprint $table) {
           $table->increments('id_detail_item');
            $table->string('image_link');
            $table->text('description');
            $table->integer('fk_circuit')->unsigned();
            $table->foreign('fk_circuit')->references('id_circuit')->on('circuits');
            $table->string('type_relation');
            $table->integer('fk_relation');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('circuit_detail_items');
    }
}
