<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoryRegionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_regions', function (Blueprint $table) {
            $table->increments('id_history_region');
            $table->text('description_to_approve');
            $table->integer('fk_region')->unsigned();
            $table->foreign('fk_region')->references('id_region')->on('regions');
            $table->integer('fk_reporter')->unsigned();
            $table->foreign('fk_reporter')->references('id_reporter')->on('reporters');
            $table->string('state');
            $table->integer('approved_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_regions');
    }
}
