<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('regions', function (Blueprint $table) {
            $table->increments('id_region');
            $table->string('region_name');
            $table->string('sentence');
            $table->text('description');
            $table->string('dark_color');
            $table->string('light_color');
            $table->string('multimedia_type');
            $table->string('slug');
            $table->string('type_last_user');
            //Existirán 2 tipos de edición, en caso de ser realizada por administrador quedará directamente guardado el id. En caso de que sea por un reporter quedará con la referencia del historial
            $table->string('link_image');
            $table->integer('fk_last_edition');
            $table->text('keywords');
            $table->string('state')->default('activo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('regions');
    }
}
