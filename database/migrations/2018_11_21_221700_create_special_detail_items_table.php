<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpecialDetailItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('special_detail_items', function (Blueprint $table) {
            $table->increments('id_detail_item');
            $table->string('image_link');
            $table->text('description');
            $table->integer('fk_special')->unsigned();
            $table->foreign('fk_special')->references('id_special')->on('specials');
            $table->string('type_relation');
            $table->integer('fk_relation');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('special_detail_items');
    }
}
