<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiteKeydatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_keydatas', function (Blueprint $table) {
            $table->increments('id_keydata');
            $table->string('keydata_name');
            $table->string('keydata_value');
            $table->string('category');
            $table->integer('fk_site')->unsigned();
            $table->foreign('fk_site')->references('id_site')->on('interest_sites');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_keydatas');
    }
}
