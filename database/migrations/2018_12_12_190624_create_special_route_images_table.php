<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpecialRouteImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('special_route_images', function (Blueprint $table) {
            $table->increments('id_route_image');
            $table->mediumText('link_image');
            $table->text('description');
            $table->integer('fk_route')->unsigned();
            $table->foreign('fk_route')->references('id_route')->on('special_routes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('special_route_images');
    }
}
