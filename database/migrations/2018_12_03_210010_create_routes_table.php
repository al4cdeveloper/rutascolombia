<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoutesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('routes', function (Blueprint $table) {
            $table->increments('id_route');
            $table->string('name');
            $table->string('sentence');
            $table->string('keywords');
            $table->string('file_map')->nullable();
            $table->text('description');
            $table->string('description_image')->nullable();
            $table->text('recommendation');
            $table->string('recommendation_image')->nullable();
            $table->string('card_image')->nullable();
            $table->string('introduction_image')->nullable();
            $table->text('images');
            $table->string('state')->default('activo');
            $table->string('slug');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('routes');
    }
}
