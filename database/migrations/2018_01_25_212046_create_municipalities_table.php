<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMunicipalitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('municipalities', function (Blueprint $table) {
            $table->increments('id_municipality');
            $table->string('municipality_name');
            $table->text('description');
            $table->string('multimedia_type');
            $table->string('latitude');
            $table->string('longitude');
            $table->string('slug');
            $table->string('type_last_user');
            //Existirán 2 tipos de edición, en caso de ser realizada por administrador quedará directamente guardado el id. En caso de que sea por un reporter quedará con la referencia del historial
            $table->string('link_image');
            $table->integer('fk_last_edition');
            $table->integer('fk_department')->unsigned();
            $table->foreign('fk_department')->references('id_department')->on('departments');
            $table->boolean('city');
            $table->string('mayor_name')->nullable();
            $table->string('mayor_image')->nullable();
            $table->text('mayor_description')->nullable();
            $table->string('logo_partner')->nullable();
            $table->string('flag')->nullable();
            $table->string('shield')->nullable();
            $table->boolean('show_additional_data')->default(0);
            $table->string('img_360')->nullable();
            $table->string('video_youtube')->nullable();
            $table->text('iframe')->nullable();//street view
            $table->text('keywords');
            $table->string('state')->default('activo');
            $table->boolean('outstanding')->default(0);
            $table->string('front_state_hotel')->default('inactivo');
            $table->string('front_state_restaurant')->default('inactivo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('municipalities');
    }
}
