<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatternsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patterns', function (Blueprint $table) {
            $table->increments('id_pattern');
            $table->integer('fk_customer')->unsigned();
            $table->foreign('fk_customer')->references('id_customer')->on('customers');
            $table->string('type');
            $table->string('multimedia_type');
            $table->integer('clicks')->nullable();
            $table->integer('redirection')->nullable();
            $table->date('publication_day');
            $table->date('publication_finish');
            $table->string('pattern');
            $table->string('state')->default('activo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patterns');
    }
}
