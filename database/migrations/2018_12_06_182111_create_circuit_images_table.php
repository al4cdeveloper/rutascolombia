<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCircuitImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('circuit_images', function (Blueprint $table) {
            $table->increments('id_circuit_image');
            $table->mediumText('link_image');
            $table->integer('fk_circuit')->unsigned();
            $table->foreign('fk_circuit')->references('id_circuit')->on('circuits');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('circuit_images');
    }
}
