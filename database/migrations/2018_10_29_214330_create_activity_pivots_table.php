<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivityPivotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity_pivots', function (Blueprint $table) {
            $table->increments('id_activity_pivot');
            $table->integer('fk_actity')->unsigned();
            $table->foreign('fk_actity')->references('id_activity')->on('activities');
            $table->integer('fk_relation');
            $table->string('type_relation');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activity_pivots');
    }
}
