<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpecialRoutesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('special_routes', function (Blueprint $table) {
            $table->increments('id_route');
            $table->string('name');
            $table->string('sentence');
            $table->string('length',10);
            $table->string('difficulty');
            $table->string('keywords');
            $table->string('video')->nullable();
            $table->mediumText('url_map');
            $table->string('highest_point',10);
            $table->string('lower_point',10);
            $table->text('description');
            $table->string('description_image')->nullable();
            $table->text('recommendation');
            $table->text('text_images');
            $table->string('recommendation_image')->nullable();
            $table->string('card_image')->nullable();
            $table->string('introduction_image')->nullable();
            $table->string('type_startpoint');
            $table->integer('start_point');
            $table->string('type_endpoint');
            $table->integer('end_point');
            $table->string('state')->default('activo');
            $table->string('slug');
            $table->integer('fk_special')->unsigned();
            $table->foreign('fk_special')->references('id_special')->on('specials');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('special_routes');
    }
}
