<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepartmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('departments', function (Blueprint $table) {
            $table->increments('id_department');
            $table->string('department_name');
            $table->text('description');
            $table->mediumText('short_description');
            $table->string('multimedia_type');
            $table->string('latitude');
            $table->string('longitude');
            $table->string('description_image')->nullable();
            $table->text('text_images')->nullable();
            $table->string('title_extra_description')->nullable();
            $table->text('extra_description')->nullable();
            $table->string('video_youtube')->nullable();
            $table->string('slug');
            $table->string('type_last_user');
            //Existirán 2 tipos de edición, en caso de ser realizada por administrador quedará directamente guardado el id. En caso de que sea por un reporter quedará con la referencia del historial
            $table->string('link_image');
            $table->integer('fk_last_edition');
            $table->integer('fk_region')->unsigned();
            $table->foreign('fk_region')->references('id_region')->on('regions');
            $table->text('keywords');
            $table->string('state')->default('activo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('departments');
    }
}
