<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTollsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tolls', function (Blueprint $table) {
            $table->increments('id_toll');
            $table->string('name');
            $table->string('link_image')->nullable();
            $table->integer('cost_1')->nullable();
            $table->integer('cost_2')->nullable();
            $table->integer('cost_3')->nullable();
            $table->integer('cost_4')->nullable();
            $table->integer('cost_5')->nullable();
            $table->integer('cost_6')->nullable();
            $table->integer('cost_7')->nullable();
            $table->integer('fk_site')->unsigned();
            $table->foreign('fk_site')->references('id_site')->on('interest_sites');
            $table->integer('fk_concession')->unsigned();
            $table->foreign('fk_concession')->references('id_concession')->on('concessions');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tolls');
    }
}
