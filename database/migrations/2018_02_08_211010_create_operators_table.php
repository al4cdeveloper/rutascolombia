<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOperatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('operators', function (Blueprint $table) {
            $table->increments('id_operator');
            $table->string('name');
            $table->string('national_register');
            $table->string('contact_personal');
            $table->string('contact_phone');
            $table->string('nit_rut');
            $table->string('email')->unique();
            $table->string('service_category');
            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();
            $table->string('instagram')->nullable();
            $table->string('address')->nullable();
            $table->text('description')->nullable();
            $table->string('web');
            $table->integer('days_for_reservation')->default(2);
            $table->string('avatar')->nullable();
            $table->string('role')->default("operator");
            $table->string('status')->default('active');
            $table->integer('cuantity_services')->default(3);
            $table->string('slug');
            $table->string('password');
            $table->boolean('outstanding')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('operators');
    }
}
