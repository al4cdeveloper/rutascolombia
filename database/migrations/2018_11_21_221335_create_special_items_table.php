<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpecialItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('special_items', function (Blueprint $table) {
            $table->increments('id_item');
            $table->string('title');
            $table->text('description');
            $table->integer('fk_special')->unsigned();
            $table->foreign('fk_special')->references('id_special')->on('specials');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('special_items');
    }
}
