<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatternPartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pattern_parts', function (Blueprint $table) {
            $table->increments('id_pattern_part');
            $table->integer('fk_pattern')->unsigned();
            $table->foreign('fk_pattern')->references('id_pattern')->on('patterns');
            $table->integer('fk_pagepart')->unsigned();
            $table->foreign('fk_pagepart')->references('id_part_page')->on('page_part_pages');
            $table->string('state')->nullable();
            $table->integer('clicks')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pattern_parts');
    }
}
