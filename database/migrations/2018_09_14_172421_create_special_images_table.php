<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpecialImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('special_images', function (Blueprint $table) {
            $table->increments('id_special_image');
            $table->mediumText('link_image');
            $table->text('description');
            $table->integer('fk_special')->unsigned();
            $table->foreign('fk_special')->references('id_special')->on('specials');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('special_images');
    }
}
