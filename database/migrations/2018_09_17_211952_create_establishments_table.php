<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstablishmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('establishments', function (Blueprint $table) {
            $table->increments('id_establishment');
            $table->string('type_establishment');
            $table->string('establishment_name');
            $table->integer('phone');
            $table->string('address');
            $table->string('latitude');
            $table->string('longitude');
            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();
            $table->string('instagram')->nullable();
            $table->string('website')->nullable();
            $table->string('video')->nullable();
            $table->string('kitchen_type')->nullable();
            $table->string('slug');
            $table->integer('fk_operator')->nullable();
            $table->text('description');
            $table->string('url_redireccion')->nullable(); //URL PARA QUE REDIRIGIRLO A ALGÚN SITIO Y HACER RESERVA
            $table->string('card_image')->nullable();
            $table->string('introduction_image')->nullable();
            $table->string('state')->default('activo');
            $table->string('internal_state')->default('admin'); // Si la administración está por parte del administrador o a un operador
            $table->integer('fk_municipality')->unsigned();
            $table->foreign('fk_municipality')->references('id_municipality')->on('municipalities');
            $table->boolean('outstanding')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('establishments');
    }
}
