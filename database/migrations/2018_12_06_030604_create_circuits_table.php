<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCircuitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('circuits', function (Blueprint $table) {
            $table->increments('id_circuit');
            $table->string('name');
            $table->text('keywords');
            $table->mediumText('short_description');
            $table->text('description');
            $table->string('description_image')->nullable();
            $table->text('recommendations')->nullable();
            $table->string('recommendation_image')->nullable();
            $table->text('text_images')->nullable();
            $table->string('card_image')->nullable();
            $table->string('introduction_image')->nullable();
            $table->string('video')->nullable();
            $table->string('download_file')->nullable();
            $table->string('slug');
            $table->string('state')->default('activo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('circuits');
    }
}
