<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RutasColombiaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('service_establishments')->insert([
		'service_name'=>'Wifi'
		]);

		DB::table('service_establishments')->insert([
		'service_name'=>'Plasma Tv'
		]);

		DB::table('service_establishments')->insert([
		'service_name'=>'Carta de vinos y licores'
		]);

		DB::table('service_establishments')->insert([
		'service_name'=>'Eventos especiales'
		]);

		DB::table('service_establishments')->insert([
		'service_name'=>'Cenas V.I.P.'
		]);

		DB::table('service_establishments')->insert([
		'service_name'=>'Salón de espera'
		]);

		DB::table('service_establishments')->insert([
		'service_name'=>'Música en vivo'
		]);

		DB::table('service_establishments')->insert([
		'service_name'=>'Música ambiental'
		]);

		DB::table('service_establishments')->insert([
		'service_name'=>'Acceso para sillas de ruedas'
		]);

		DB::table('service_establishments')->insert([
		'service_name'=>'Opciones sin gluten'
		]);

		DB::table('service_establishments')->insert([
		'service_name'=>'Parqueadero'
		]);

		DB::table('service_establishments')->insert([
		'service_name'=>'Valet parking'
		]);

		DB::table('service_establishments')->insert([
		'service_name'=>'Traductor'
		]);

		DB::table('service_establishments')->insert([
		'service_name'=>'Caja de seguridad'
		]);

		DB::table('service_establishments')->insert([
		'service_name'=>'Catering para eventos'
		]);

		DB::table('service_establishments')->insert([
		'service_name'=>'Pet Friendly'
		]);

		DB::table('service_establishments')->insert([
		'service_name'=>'Entrega a domicilio'
		]);

		DB::table('service_establishments')->insert([
		'service_name'=>'Acepta tarjetas de crédito'
		]);

		DB::table('service_establishments')->insert([
		'service_name'=>'Opciones veganas'
		]);

		DB::table('service_establishments')->insert([
		'service_name'=>'Apto para vegetarianos'
		]);

		DB::table('service_establishments')->insert([
		'service_name'=>'No se permiten mascotas'
		]);

		DB::table('service_establishments')->insert([
		'service_name'=>'Zona de fumadores'
		]);
    }
}
