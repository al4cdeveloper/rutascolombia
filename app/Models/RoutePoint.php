<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoutePoint extends Model
{
	protected $primaryKey= 'id_point';

	public function route()
	{
		return $this->belongsTo(SpecialRoute::class,'fk_route');
	}
}
