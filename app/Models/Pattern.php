<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pattern extends Model
{
	protected $primaryKey = 'id_pattern';
	protected $fillable = ['fk_customer','type','clicks','redirection','publication_day','publication_finish','link','state','slug'];

	public function Customer()
	{
		return $this->belongsTo(Customer::class,'fk_customer','id_customer');
	}

	public function Parts()
	{
		return $this->belongsToMany(PagePartPage::class,'pattern_parts','fk_pattern','fk_pagepart')->withPivot('state', 'clicks','id_pattern_part','fk_pattern','fk_pagepart');
	}

	public function activeParts()
	{
		return $this->belongsToMany(PagePartPage::class,'pattern_parts','fk_pattern','fk_pagepart')->where('state','activo')->withPivot('state', 'clicks','id_pattern_part','fk_pattern','fk_pagepart');
	}

	public function  activeByPart($fk_pagepart)
	{
		return $this->belongsToMany(PagePartPage::class,'pattern_parts','fk_pattern','fk_pagepart')->where('state','activo')->where('fk_pagepart',$fk_pagepart);
	}
}
