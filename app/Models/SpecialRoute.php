<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class SpecialRoute extends Model
{
	use Sluggable;

	protected $primaryKey = 'id_route';

	public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function special()
    {
        return $this->belongsTo(Special::class,'fk_special');
    }

    public function startMunicipality()
    {
    	return $this->belongsTo(Municipality::class,'start_point','id_municipality');
    }

    public function startSite()
    {
    	return $this->belongsTo(InterestSite::class,'start_point','id_site');
    }

    public function endMunicipality()
    {
    	return $this->belongsTo(Municipality::class,'end_point','id_municipality');
    }

    public function endSite()
    {
    	return $this->belongsTo(InterestSite::class,'end_point','id_site');
    }

    public function points()
    {
        return $this->hasMany(RoutePoint::class,'fk_route')->orderBy('order');
    }
    public function imagesrelation()
    {
        return $this->hasMany(SpecialRouteImage::class,'fk_route');
    }
}
