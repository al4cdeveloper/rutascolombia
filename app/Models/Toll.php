<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Toll extends Model
{
	protected $primaryKey='id_toll';

	public function site()
	{
		return $this->belongsTo(InterestSite::class,'fk_site')->with('relMunicipality');
	}

	public function concession()
	{
		return $this->belongsTo(Concession::class,'fk_concession');
	}


}
