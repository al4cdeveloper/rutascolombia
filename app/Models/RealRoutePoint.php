<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RealRoutePoint extends Model
{
	protected $primaryKey = 'id_point';

	public function municipality()
	{
		return $this->belongsTo(Municipality::class,'fk_relation');
	}

	public function site()
	{
		return $this->belongsTo(InterestSite::class,'fk_relation');
	}

	public function route()
	{
		return $this->belongsTo(Route::class,'fk_route');
	}
}
