<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SpecialDetailItem extends Model
{
	protected $primaryKey='id_detail_item';
	protected $fillable = ['image_link','description','fk_special','type_relation','fk_relation','created_at','','updated_at'];

	public function special()
	{
		return $this->belongsTo(Special::class,'fk_special');
	}

	public function municipality()
	{
		return $this->belongsTo(Municipality::class,'fk_relation');
	}

	public function site()
	{
		return $this->belongsTo(InterestSite::class,'fk_relation');
	}
}
