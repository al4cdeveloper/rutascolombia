<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SpecialRouteImage extends Model
{
	protected $primaryKey = 'id_route_image';

	protected $fillable = ['link_image','fk_route'];
}
