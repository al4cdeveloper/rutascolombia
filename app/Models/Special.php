<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Special extends Model
{
	use Sluggable;

	protected $primaryKey = 'id_special';

	public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function Municipalities()
    {
        return $this->hasMany('App\Models\RelationSpecial','fk_special','id_special')->where('type_relation','municipality');
    }

    public function Sites()
    {
        return $this->hasMany('App\Models\RelationSpecial','fk_special','id_special')->where('type_relation','site');
    }

    public function Relations()
    {
        return $this->hasMany('App\Models\RelationSpecial','fk_special','id_special');
    }

    public function images()
    {
        return $this->hasMany(SpecialImage::class,'fk_special');
    }

    public function items()
    {
        return $this->hasMany(SpecialItem::class,'fk_special');
    }

    public function detailItems()
    {
        return $this->hasMany(SpecialDetailItem::class,'fk_special');
    }

    public function routes()
    {
        return $this->hasMany(SpecialRoute::class,'fk_special');
    }

    public function activeRoutes()
    {
        return $this->hasMany(SpecialRoute::class,'fk_special')->where('state','activo');
    }
}
