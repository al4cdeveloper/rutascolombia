<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EstablishmentImage extends Model
{
	protected $primaryKey = 'id_establishment_image';
	protected $fillable = ['id_establishment_image','description','link_image','fk_establishment'];
}
