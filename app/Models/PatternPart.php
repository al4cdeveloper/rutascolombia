<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PatternPart extends Model
{
	protected $primaryKey = 'id_pattern_part';
	protected $fillable = ['state'];
}
