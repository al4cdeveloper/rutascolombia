<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MunicipalityKeydata extends Model
{
	protected $primaryKey = 'id_keydata';
}
