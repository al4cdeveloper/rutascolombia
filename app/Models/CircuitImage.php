<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CircuitImage extends Model
{
	protected $primaryKey = 'id_circuit_image';

	protected $fillable = ['link_image','fk_circuit'];
}
