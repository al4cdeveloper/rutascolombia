<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Activity extends Model
{
	use Sluggable;
	
	protected $primaryKey="id_activity";
	protected $fillable = ['activity_name','image_link','title','keywords','meta_description','state','slug'];

	public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'activity_name'
            ]
        ];
    }

    public function allsites()
    {
        return $this->hasMany(ActivityPivot::class,'fk_actity','id_activity');
    }

    public function fiveSites()
    {
        return $this->hasMany(ActivityPivot::class,'fk_actity','id_activity')->inRandomOrder()->limit(5);
    }

    public function sevenSites()
    {
        return $this->hasMany(ActivityPivot::class,'fk_actity','id_activity')->inRandomOrder()->limit(7);
    }

    public function Municipalities()
    {
        return $this->belongsToMany(Activity::class,'activity_pivots','fk_relation','fk_actity')->where('type_relation','municipality');
    }

    public function InterestSite()
    {
        return $this->belongsToMany(Activity::class,'activity_pivots','fk_relation','fk_actity')->where('type_relation','interest_site');
    }
}
