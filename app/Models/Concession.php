<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Concession extends Model
{
	use Sluggable;

	protected $primaryKey='id_concession';

	public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function tolls()
    {
        return $this->hasMany(Toll::class,'fk_concession');
    }

    public function frontTolls()
    {
        return $this->hasMany(Toll::class,'fk_concession')->with('site');
    }

    public function sites()
    {
        return $this->belongsToMany(InterestSite::class,'tolls','fk_concession','fk_site');
    }
}
