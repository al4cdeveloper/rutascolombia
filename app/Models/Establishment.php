<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Establishment extends Model
{
	use Sluggable;

	protected $primaryKey = 'id_establishment';
	protected $fillable = ['id_establishment','type_establishment','establishment_name','phone','address','latitude','longitude','facebook','twitter','instagram','website','video','kitchen_type','internal_state','slug','fk_operator','description'];

	public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'establishment_name'
            ]
        ];
    }
    
    public function Services()
    {
        return $this->belongsToMany(ServiceEstablishment::class,'service_establishment_pivots','fk_establishment','fk_service');
    }

    public function Schedule()
    {
        return $this->hasMany(EstablishmentSchedule::class,'fk_establishment');
    }

    public function Municipality()
    {
        return $this->belongsTo('App\Models\Municipality','fk_municipality','id_municipality');
    }

    public function relMunicipality()
    {
        return $this->belongsTo('App\Models\Municipality','fk_municipality','id_municipality')->with('relDepartment');
    }

    public function Plates()
    {
        return $this->hasMany(Plate::class,'fk_establishment');
    }

    public function Rooms()
    {
        return $this->hasMany(Room::class,'fk_establishment');
    }

    public function Images()
    {
        return $this->hasMany(EstablishmentImage::class,'fk_establishment');
    }

    public function Operator()
    {
        return $this->belongsTo('App\Operator','fk_operator','id_operator');
    }
}
