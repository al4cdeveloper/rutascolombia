<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Region extends Model
{
	use Sluggable;


	protected $primaryKey="id_region";
	protected $fillable=['id_region','region_name','description','multimedia_type','slug','type_last_user','keywords','link_image','fk_last_edition','state'];

	public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'region_name'
            ]
        ];
    }

    public function images()
    {
        return $this->hasMany('App\Models\RegionImage', 'fk_region');
    }

    public function departments()
    {
        return $this->hasMany(Department::class,'fk_region');
    }

    public function fairDepartments()
    {
        return $this->hasMany(Department::class,'fk_region');
    }
    public function fairDepartmentsFilter($month)
    {
        return $this->hasMany(Department::class,'fk_region')->with(['Fairs' => function ($query) use ($month) 
            { $query->where('fairs.state','activo')->whereMonth('start_date', '=', $month)->with('PrincipalMunicipality'); }]);
    }

    public function FiveDepartmentsFront()
    {
        return $this->hasMany(Department::class,'fk_region')->inRandomOrder()->take(5);
    }

    public function Municipalities()
    {
        return $this->hasManyThrough(Municipality::class,Department::class,'fk_region','fk_department')->with('Fairs');
    }
}
