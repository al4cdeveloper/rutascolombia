<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CircuitDetailItem extends Model
{
	protected $primaryKey='id_detail_item';

	public function circuit()
	{
		return $this->belongsTo(Circuit::class,'fk_circuit');
	}

	public function municipality()
	{
		return $this->belongsTo(Municipality::class,'fk_relation');
	}

	public function site()
	{
		return $this->belongsTo(InterestSite::class,'fk_relation');
	}
}
