<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SpecialImage extends Model
{
	protected $primaryKey = 'id_special_image';

	protected $fillable = ['link_image','fk_special'];
}
