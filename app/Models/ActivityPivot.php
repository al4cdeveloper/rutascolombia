<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActivityPivot extends Model
{
	public function municipality()
	{
		return $this->belongsTo(Municipality::class,'fk_relation');
	}

	public function site()
	{
		return $this->belongsTo(InterestSite::class,'fk_relation');
	}

	public function activity()
	{
		return $this->belongsTo(Activity::class,'fk_actity');
	}
}
