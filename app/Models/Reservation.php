<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
	protected $primaryKey="id_reservation";

	public function Service()
	{
		return $this->hasOne('App\Models\ServiceOperator','id_service_operator','fk_service');
	}

	public function user()
	{
		return $this->hasOne('App\User','id','fk_user');
	}
}
