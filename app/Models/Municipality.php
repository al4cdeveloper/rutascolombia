<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Municipality extends Model
{
    use Sluggable;


	protected $primaryKey = 'id_municipality';

	protected $fillable = ['id_municipality','municipality_name','description','multimedia_type','latitude','longitude','slug','link_image','type_last_user','fk_last_edition','fk_department','city','keywords','state'];

	public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'municipality_name'
            ]
        ];
    }	

    public function Department()
    {
        return $this->hasOne('App\Models\Department','id_department','fk_department');
    }

    public function relDepartment()
    {
    	return $this->hasOne('App\Models\Department','id_department','fk_department')->with('relRegion');
    }

    public function images()
    {
        return $this->hasMany('App\Models\MunicipalityImage', 'fk_municipality');
    }

    public function InterestSite()
    {
        return $this->hasMany('App\Models\InterestSite','fk_municipality','id_municipality');
    }

    public function frontInterestSite()
    {
        return $this->hasMany('App\Models\InterestSite','fk_municipality','id_municipality')->inRandomOrder(6);
    }
    public function Video()
    {
        return $this->hasMany('App\Models\Video','fk_relation','id_municipality')->where('type_relation','municipality');
    }

    public function Activities()
    {
        return $this->belongsToMany(Activity::class,'activity_pivots','fk_relation','fk_actity')->where('type_relation','municipality');
    }

    public function getPertinentRegionAttribute($region)
    {
        $department = $this->department;
        $region = $department->Region;
        return $region;
    }

    public function Fairs()
    {
        return $this->hasMany(Fair::class,'fk_municipality');
    }

    public function FrontFairs()
    {
        return $this->hasMany(Fair::class,'fk_municipality')->with('PrincipalMunicipality')->inRandomOrder(5);
    }

    public function KeyData()
    {
        return $this->hasMany(MunicipalityKeydata::class,'fk_municipality','id_municipality');
    }

    public function Hotels()
    {
        return $this->hasMany(Establishment::class,'fk_municipality','id_municipality')->where('type_establishment','hotel')->where('state','activo')->with('Municipality');
    }

    public function Restaurants()
    {
        return $this->hasMany(Establishment::class,'fk_municipality','id_municipality')->where('type_establishment','restaurant')->where('state','activo');
    }

    public function Plans()
    {
        return $this->hasMany('App\Models\ServiceOperator','fk_municipality','id_municipality')->where('state','activo');
    }
}
