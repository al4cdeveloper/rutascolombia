<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Department extends Model
{
	use Sluggable;
	
	protected $primaryKey = "id_department";
	protected $fillable = ['department_name','description','multimedia_type','latitude','longitude','slug','type_last_user','link_image','fk_region','fk_last_edition','keywords','state'];

	public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'department_name'
            ]
        ];
    }

    public function Region()
    {
        return $this->hasOne('App\Models\Region','id_region','fk_region');
    }

    public function relRegion()
    {
        return $this->hasOne('App\Models\Region','id_region','fk_region');
    }

    public function images()
    {
        return $this->hasMany('App\Models\DepartmentImage', 'fk_department');
    }

    public function Municipalities()
    {
        return $this->hasMany('App\Models\Municipality','fk_department','id_department')->where('state','activo');
    }

    public function HotelLimit()
    {
        return $this->hasManyThrough(Establishment::class,Municipality::class,'fk_department','fk_municipality')->with('relMunicipality')->where('type_establishment','hotel')->inRandomOrder()->limit(2);
    }

    public function RestaurantLimit()
    {
        return $this->hasManyThrough(Establishment::class,Municipality::class,'fk_department','fk_municipality')->with('relMunicipality')->where('type_establishment','restaurant')->inRandomOrder()->limit(2);
    }

    public function ThreeMunicipalitiesFront()
    {
        return $this->hasMany(Municipality::class,'fk_department','id_department')->where('state','activo')->inRandomOrder()->limit(3);
    }

    public function NineMunicipalitiesFront()
    {
        return $this->hasMany(Municipality::class,'fk_department','id_department')->where('state','activo')->orWhere('front_state_hotel','activo')->orWhere('front_state_restaurant','activo')->take(9);
    }

    public function Fairs()
    {
        return $this->hasManyThrough(Fair::class,Municipality::class,'fk_department','fk_municipality')->inRandomOrder()->with('PrincipalMunicipality');
    }

    public function FairsLimit()
    {
        return $this->hasManyThrough(Fair::class,Municipality::class,'fk_department','fk_municipality')->inRandomOrder()->with('PrincipalMunicipality')->limit(5);
    }

    public function FairsFilter($month)
    {
        return $this->hasManyThrough(Fair::class,Municipality::class,'fk_department','fk_municipality')->whereMonth('start_date','=',$month)->inRandomOrder()->with('PrincipalMunicipality')->limit(5);
    }
}
