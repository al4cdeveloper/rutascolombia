<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Circuit extends Model
{
	use Sluggable;
	protected $primaryKey = 'id_circuit';
	public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
    
    public function detailItems()
    {
        return $this->hasMany(CircuitDetailItem::class,'fk_circuit');
    }

    public function images()
    {
        return $this->hasMany(CircuitImage::class,'fk_circuit');
    }
}
