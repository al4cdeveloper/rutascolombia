<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Route extends Model
{
	use Sluggable;

	protected $primaryKey='id_route';

	public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function points()
    {
        return $this->hasMany(RealRoutePoint::class,'fk_route')->orderBy('kilometer');
    }

    public function imagesrelation()
    {
        return $this->hasMany(RouteImage::class,'fk_route');
    }
}
