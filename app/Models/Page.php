<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Page extends Model
{
	use Sluggable;

    protected $primaryKey = 'id_page';
    protected $fillable = ['page_name','description','keywords','url','slug'];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'page_name'
            ]
        ];
    }
    public function Parts()
    {
        return $this->belongsToMany('App\Models\PagePart','page_part_pages','fk_page','fk_part')->withPivot('id_part_page','fk_page','fk_part');
    }
}
