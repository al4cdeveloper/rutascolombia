<?php

namespace App;

use App\Notifications\OperatorResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Cviebrock\EloquentSluggable\Sluggable;

class Operator extends Authenticatable
{
    use Notifiable;
    use Sluggable;

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    protected $primaryKey='id_operator';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'national_register','contact_personal','contact_phone','nit_rut','email','service_category','web','facebook','twitter','instagram','address','description','role' ,'password','status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new OperatorResetPassword($token));
    }

    public function directOption()
    {
        return $this->hasMany('App\Models\ServiceOperator','fk_operator','id_operator');
    }
    
    public function services()
    {
        return $this->belongsToMany('App\Models\Service', 'service_operators','fk_operator', 'fk_service')->withPivot('id_service_operator','address','capacity','duration','requisites','description','state');
    }

    public function groupservices()
    {
        return $this->belongsToMany('App\Models\Service', 'service_operators','fk_operator', 'fk_service')->groupBy('id_service');
    }

    public function directService()
    {
        return $this->hasMany('App\Models\ServiceOperator','fk_operator');
    }
    public function restaurants()
    {
        return $this->hasMany(Models\Establishment::class,'fk_operator')->where('type_establishment','restaurant')->where('internal_state','operator');
    }

    public function hotels()
    {
        return $this->hasMany(Models\Establishment::class,'fk_operator')->where('type_establishment','hotel')->where('internal_state','operator');
    }
}
