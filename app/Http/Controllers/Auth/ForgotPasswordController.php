<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use App\Http\Controllers\OperatorAuth\ForgotPasswordController as Operator;
use Illuminate\Support\Facades\Password;
use Illuminate\Http\Request;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function sendResetLinkEmail(Request $request) //SOBRE ESCRITURA DEL METODO ORIGINAL
    {
        $this->validateEmail($request);

        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.
        $response = $this->broker()->sendResetLink(
            $request->only('email')
        );

        //HASTA ACA ES EL PROCEDIMIENTO NORMAL

        //Se compara si la respuesta es igual, si es igual ya se ha enviado el correo (Linea 59); en caso de que no se haya enviado se llama a la clase operador, en la cual se instancia desde arriba y se intenta enviar correo como operador. Luego se hace la comparacion normal.

        // El metodo broker() de operador se tuvo que convertir en static para evitar errores.

        if($response != Password::RESET_LINK_SENT)
        {
            $responseOperator = Operator::broker()->sendResetLink(
                            $request->only('email'));

            return $responseOperator == Password::RESET_LINK_SENT
                    ? $this->sendResetLinkResponse($responseOperator)
                    : $this->sendResetLinkFailedResponse($request, $responseOperator);
        }
        else
        {
            return $this->sendResetLinkResponse($response);
        }

        //Este es el codigo original

        // return $response == Password::RESET_LINK_SENT
        //             ? $this->sendResetLinkResponse($response)
        //             : $this->sendResetLinkFailedResponse($request, $response);
    }
}
