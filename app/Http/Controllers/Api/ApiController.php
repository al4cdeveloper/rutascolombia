<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Municipality;
use App\Models\InterestSite;
use App\Models\PagePart;
use App\Models\Page;
use App\Models\PatternPart;
use App\Models\Order;

class ApiController extends Controller
{
	public function getSectionPart(Request $request)
	{	
		$pages = Page::all();

		$response = [];
		foreach ($pages as $page) 
		{
			if(count($page->Parts->where('type',$request->typePublicity))>0)
			{
				if($request->typePublicity=="Estatica")
				{
					foreach($page->Parts->where('type',$request->typePublicity) as $part)
					{
						$pattern = PatternPart::where('fk_pagepart',$part->pivot->id_part_page)->count();
						if($pattern == 0)
						{
							$response[$page->page_name][]=$part;
						}
					}
				}
				else
				$response[$page->page_name]=$page->Parts->where('type',$request->typePublicity);
			}
		}
		return response()->json($response);
	}
	
	public function getMunicipalities(Request $request)
	{
		$municipalities = Municipality::all()->where('state','activo')->where('fk_department',$request->department)->pluck('municipality_name','id_municipality');
		return response()->json($municipalities);
	}
	public function getInterestSites(Request $request)
	{
		$interestSites = InterestSite::all()->where('state','activo')->where('fk_municipality',$request->municipality)->pluck('site_name','id_site');
		return response()->json(['number'=>count($interestSites),
			'sites'=>$interestSites]);
	}

}
