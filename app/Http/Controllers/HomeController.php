<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Reservation;
use Cart;
use App\Models\Order;
use App\Models\OrderProduct;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $reservations = Reservation::where('fk_user',Auth::user()->id)->get();
        if(count($reservations)>0)
        {
            return view('home',compact('reservations'));
        }
        else
        {
            return redirect('/');
        }
    }
}