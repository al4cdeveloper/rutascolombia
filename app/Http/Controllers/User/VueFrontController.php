<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Wallpaper;
use App\Models\Page;
use App\Models\PagePartPage;
use App\Models\Pattern;
use App\Models\Region;
use App\Models\Activity;
use App\Models\PatternPart;
use App\Models\Municipality;
use App\Models\InterestSite;
use App\Models\Establishment;
use App\Models\EstablishmentImage;
use App\Models\Fair;
use App\Models\Department;
use App\Models\Special;
use App\Models\SpecialRoute;
use App\Models\Route;
use App\Models\Concession;
use App\Models\ServiceOperator;
use App\Models\Circuit;
use App\Models\Service;
use App\Models\ActivityPivot;
use App\Operator;
use DB;
use Crypt;
class VueFrontController extends Controller
{
	public $defaultimage='img/defaultCarousel.png';
	public $defaultcardimage='img/defaultCardSpecial.jpeg';
	public $defaultcardroute='img/defaultcardroute.jpeg';
	public $defaultlogoconcession='img/defaultlogoconcession.jpg';
	public $defaultrestaurant='front/img/restaurant1.jpg';
	public $defaulthotel='front/img/hotel1.jpg';
	public $defaultoperator='img/defaultimgoperator.png';

	public function navComponent(Request $request)
	{
		$finalPattern = [];
		$fullPath = $request->fullpath;
		$path = $request->path;
		if($path =='/home')
		{
			$page = Page::where('url','/')->first();
		}
		else
		{
			$path = substr($fullPath, 5);
			$page = Page::where('url',$path)->first();
		}
		if(isset($page))
		{	
			$finalPattern = $this->pattern($page);
		}
		$regions = Region::where('state','activo')->orderBy('region_name')->pluck('slug','region_name');

		$whattodo = Activity::all()->where('state','activo')->pluck('slug','activity_name');
		$token = csrf_token();



		/*$user = [];

		if(Auth::guard('operator')->user())
		{
			$user = Auth::guard('operator')->user();
			array_add($user,'type_user','operator');
			array_add($user,'loginState','active');
		}
		elseif(Auth::user())
		{
			$user = Auth::user();
			array_add($user,'type_user','customer');
			array_add($user,'loginState','active');
		}
		else
		{
			$user = ['loginState'=>'inactive'];
		}*/
		return response()->json(['regions'=>$regions,
								'whattodo'=>$whattodo,
								'patterns'=>$finalPattern,
								//'user'=>$user,
								]);
	}

	public function navbarPattern(Request $request)
	{
		$finalPattern = [];
		$fullPath = $request->fullpath;
		$path = $request->path;
		if($path =='/home')
		{
			$page = Page::where('url','/')->first();
		}
		else
		{
			$path = substr($fullPath, 5);
			$page = Page::where('url',$path)->first();
		}
		if(isset($page))
		{	
			$finalPattern = $this->pattern($page);
		}
		return response()->json([
								'patterns'=>$finalPattern,
								]);
	}

	public function whattodo()
	{
		$page = Page::where('url','/whattodo')->first();
		$finalPattern=[];
		if(isset($page))
		{	
			$finalPattern = $this->pattern($page);
		}
		$regions = Region::where('state','activo')->orderBy('region_name')->pluck('slug','region_name');

		$whattodo = Activity::where('state','activo')->get();
		foreach ($whattodo as $activity) 
		{
			array_add($activity,'cuantity',count($activity->allsites));
			if(count($activity->fiveSites)>0)
			{
				$allsites = [];
				foreach($activity->fiveSites as $site)
				{
					if($site->type_relation == 'municipality')
					{
						$thissite = Municipality::find($site->fk_relation);
						array_add($thissite,'type','municipality');
						if($thissite->link_image)
						{
							array_add($site,'image_link',$thissite->link_image);
						}
						else
							array_add($thissite,'image_link','img/defaultbiglist.png');
						
						array_add($thissite,'department',$thissite->department);
						array_add($thissite,'region',$thissite->PertinentRegion);

						$allsites[] = $thissite;
					}
					else
					{
						$thissite = InterestSite::find($site->fk_relation);
						array_add($thissite,'type','site');
						if($thissite->link_image)
						{
							array_add($site,'image_link',$thissite->link_image);
						}
						else
							array_add($thissite,'image_link','img/defaultbiglist.png');

						array_add($thissite,'department',$thissite->PertinentDepartment);
						array_add($thissite,'region',$thissite->PertinentRegion);
						$allsites[] = $thissite;
					}
				}
				array_add($allsites[array_rand($allsites)],'class',true);
				$activity['sites']=$allsites;
			}
		}
		return response()->json(['patterns'=>$finalPattern,
								'whattodo'=>$whattodo,
								'page'=>$page,
								]);
	}

	public function searchWhatToDo(Request $request)
	{
		$page = Page::where('url','/searchwhattodo')->first();
		$outstanding = Municipality::where('state','activo')->where('outstanding',1)->get();
		$finalPattern=[];
		if(isset($page))
		{	
			$finalPattern = $this->pattern($page);
		}
		$regions = Region::where('state','activo')->orderBy('region_name')->get();
		$whattodo = Activity::where('state','activo')->get();
		$departments = [];
		$municipalities= [];
		if($request->region)
		{	
			$departments = Department::with('Region')->whereHas('Region',function($query1) use($request)
						{
							$regionsSearch = explode(',', substr($request->region, 0,-1));
							$query1->whereIn('slug',$regionsSearch);
						})->get()->groupBy('region.region_name');
		}
		if($request->department)
		{
			$municipalities = Municipality::whereHas('Department',function($q1) use ($request)
				{
					$q1->where('slug',$request->department);
				})->get();

		}

		//End sites será la que almacene todo lo que tenga los filtros
		$endsites = [];

		if($request->municipality && $request->type_whattodo)
		{
			$municipality = Municipality::where('slug',$request->municipality)->first();

			$endsites = ActivityPivot::where('type_relation','interest_site')->whereHas('site',function ($q1) use($municipality)
			{
				$idMunicipality = $municipality->id_municipality;
				$q1->whereHas('Municipality',function($q2) use ($idMunicipality)
					{
						$q2->where('fk_municipality',$idMunicipality);
					});
			})->groupBy('fk_relation')->whereHas('activity',function($q3) use ($request)
			{
				$q3->where('slug',$request->type_whattodo);
			})->paginate(20);
		}
		else if($request->department && $request->type_whattodo)
		{
			$endsites = ActivityPivot::where('type_relation','interest_site')->whereHas('site',function($q0) use($request)
			{
				$q0->whereHas('Municipality',function($q1) use($request)
				{
					$departmentSearch = $request->department;
					$q1->whereHas('Department',function($q2)  use ($departmentSearch)
						{
							$q2->where('slug',$departmentSearch);
						});
				});
			})->groupBy('fk_relation')->orWhere('type_relation','municipality')->whereHas('Municipality',function($q4) use($request)
			{
				$departmentSearch = $request->department;
				$q4->whereHas('Department',function($q5)  use ($departmentSearch)
					{
						$q5->where('slug',$departmentSearch);
					});
			})->groupBy('fk_relation')->whereHas('activity',function($q3) use ($request)
			{
				$q3->where('slug',$request->type_whattodo);
			})->paginate(20);

		}
		else if($request->region && $request->type_whattodo)
		{
			$endsites = ActivityPivot::where('type_relation','municipality')->whereHas('Municipality',function($q1) use($request)
			{
				$regionsSearch = explode(',', substr($request->region, 0,-1));
				$q1->whereHas('Department',function($q2)  use ($regionsSearch)
					{
						$aux = $regionsSearch;
						$q2->whereHas('Region',function($q3)  use ($aux)
						{
							$q3->whereIn('slug',$aux);
						});
					});
			})->groupBy('fk_relation')->orWhere('type_relation','interest_site')->whereHas('site',function($q0) use($request)
			{
				$regionsSearch = explode(',', substr($request->region, 0,-1));
				$q0->whereHas('Municipality',function($q4) use($regionsSearch)
				{
					$aux1 =$regionsSearch;
					$q4->whereHas('Department',function($q5)  use ($aux1)
						{
							$aux = $aux1;
							$q5->whereHas('Region',function($q6)  use ($aux)
							{
								$q6->whereIn('slug',$aux);
							});
						});
				});
			})->groupBy('fk_relation')->whereHas('activity',function($q7) use ($request)
			{
				$q7->where('slug',$request->type_whattodo);
			})->paginate(20);

		}
		elseif($request->municipality)
		{
			$municipality = Municipality::where('slug',$request->municipality)->first();

			$endsites = ActivityPivot::where('type_relation','interest_site')->whereHas('site',function ($q1) use($municipality)
			{
				$idMunicipality = $municipality->id_municipality;
				$q1->whereHas('Municipality',function($q2) use ($idMunicipality)
					{
						$q2->where('fk_municipality',$idMunicipality);
					});
			})->groupBy('fk_relation')->paginate(20);
		}
		else if($request->department)
		{
			$endsites = ActivityPivot::where('type_relation','interest_site')->whereHas('site',function($q0) use($request)
			{
				$q0->whereHas('Municipality',function($q1) use($request)
				{
					$departmentSearch = $request->department;
					$q1->whereHas('Department',function($q2)  use ($departmentSearch)
						{
							$q2->where('slug',$departmentSearch);
						});
				});
			})->groupBy('fk_relation')->orWhere('type_relation','municipality')->whereHas('Municipality',function($q4) use($request)
			{
				$departmentSearch = $request->department;
				$q4->whereHas('Department',function($q5)  use ($departmentSearch)
					{
						$q5->where('slug',$departmentSearch);
					});
			})->groupBy('fk_relation')->paginate(20);
		}
		else if($request->region)
		{
			$endsites = ActivityPivot::where('type_relation','municipality')->whereHas('Municipality',function($q1) use($request)
			{
				$regionsSearch = explode(',', substr($request->region, 0,-1));
				$q1->whereHas('Department',function($q2)  use ($regionsSearch)
					{
						$aux = $regionsSearch;
						$q2->whereHas('Region',function($q3)  use ($aux)
						{
							$q3->whereIn('slug',$aux);
						});
					});
			})->groupBy('fk_relation')->orWhere('type_relation','interest_site')->whereHas('site',function($q0) use($request)
			{
				$regionsSearch = explode(',', substr($request->region, 0,-1));
				$q0->whereHas('Municipality',function($q4) use($regionsSearch)
				{
					$aux1 =$regionsSearch;
					$q4->whereHas('Department',function($q5)  use ($aux1)
						{
							$aux = $aux1;
							$q5->whereHas('Region',function($q6)  use ($aux)
							{
								$q6->whereIn('slug',$aux);
							});
						});
				});
			})->groupBy('fk_relation')->paginate(20);
		}
		else if($request->type_whattodo)
		{
			$endsites = ActivityPivot::whereHas('activity',function($q7) use ($request)
			{
				$q7->where('slug',$request->type_whattodo);
			})->paginate(20);
		}
		else
		{
			$endsites = ActivityPivot::groupBy('fk_relation')->paginate(5);
		}




		//All sites es con las modificaciones aparentes :3
		foreach($endsites as $site)
		{
			if($site->type_relation == 'municipality')
			{
				$thissite = Municipality::find($site->fk_relation);
				array_add($site,'type','municipality');
				if($thissite->link_image)
				{
					array_add($site,'image_link',$thissite->link_image);
				}
				else
					array_add($site,'image_link','img/defaultbiglist.png');
				
				array_add($site,'municipality',$thissite);
				array_add($site,'department',$thissite->department);
				array_add($site,'region',$thissite->PertinentRegion);

			}
			else
			{
				$thissite = InterestSite::find($site->fk_relation);
				array_add($site,'type','site');
				if($thissite->images)
				{
					array_add($site,'image_link',$thissite->link_image);
				}
				else
					array_add($site,'image_link','img/defaultbiglist.png');

				array_add($site,'site',$thissite);
				array_add($site,'department',$thissite->PertinentDepartment);
				array_add($site,'region',$thissite->PertinentRegion);
			}
		}
		

		$cant = (int)round($endsites['total']/2);
		$endsites = $endsites->toArray();
		if(count($endsites['data'])>0)
		{
			array_add($endsites['data'][array_rand($endsites['data'])],'class',true);
			array_add($endsites['data'][array_rand($endsites['data'])],'class',true);
		}


		return response()->json(['patterns'=>$finalPattern,
								'page'=>$page,
								'sites'=>$endsites,
								'whattodo'=>$whattodo,
								'regions'=>$regions,
								'departments'=>$departments,
								'municipalities'=>$municipalities,
								'outstanding'=>$outstanding,
								'cuantity'=>$cant,
								'defaultimage'=>$this->defaultimage,
								'defaultcardimage'=>$this->defaultcardimage,
								'pagination'=>[
									'total'			=>$endsites['total'],
									'current_page'	=>$endsites['current_page'],
									'per_page'		=>$endsites['per_page'],
									'from'			=>$endsites['from'],
									'last_page'		=>$endsites['last_page'],
									'to'			=>$endsites['to'],
									],
								]);
	}

	public function Region($slug)
	{
		$region = Region::with('images')->where('slug',$slug)->first();
		$page = Page::where('url',"/region/$slug")->first();
		$hotels = [];
		$operators = [];
		$images = $region->images;
		$finalOperators = [];
		$restaurants = [];
		$InterestSites = [];
		//Patterns
		$finalPattern=[];
		if(isset($page))
		{	
			$finalPattern = $this->pattern($page);
		}

		$municipalities = DB::table('municipalities')
							->join('departments','municipalities.fk_department','=','departments.id_department')
							->join('regions','departments.fk_region','=','regions.id_region')
							->select('municipalities.*')
							->where('municipalities.state','activo')
							->where('regions.id_region',$region->id_region)
							->inRandomOrder()->take(6)->get();

		
		$departments =[];
		foreach($region->FiveDepartmentsFront as $department)
		{

			$departments[$department->department_name] = ['name'=>$department->department_name,
														'slug'=>$department->slug,
														'short_description'=>$department->short_description,
														'listMunicipalities'=>$department->ThreeMunicipalitiesFront];
		}

		foreach($municipalities as $municipalityname)
		{
			$municipality = Municipality::where('slug',$municipalityname->slug)->first();
			if(count($hotels)<=5)
			{
				foreach($municipality->hotels as $newHotel)
				{
					if(count($hotels)<=5)
					{
						$toAdd = ['name'=>$newHotel->establishment_name,
								'slug'=>$newHotel->slug,
							'municipality'=>$municipality->municipality_name];
						array_push($hotels,$toAdd);
					}
				}
			}
			if(count($restaurants)<=5)
			{
				foreach($municipality->restaurants as $newResturant)
				{
					if(count($restaurants)<=5)
					{
						$toAdd = ['name'=>$newResturant->establishment_name,
								'slug'=>$newResturant->slug,
							'municipality'=>$municipality->municipality_name];
						array_push($restaurants,$toAdd);
					}
					
				}
			}
			if(count($operators)<=5)
			{
				foreach($municipality->Plans as  $service)
				{
					if(!array_key_exists($service->operador->slug,$operators))
					{
						if(count($operators)<=5)
						{
							if(isset($service->operator->slug))
							{
								$operators = array_merge($operators,[$service->operador->slug=>$service->operador->name]);
								$toAdd = ['name'=>$service->operator->name,
									'slug'=>$service->operator->slug,
									'municipality'=>$municipality->municipality_name];
								array_push($finalOperators,$toAdd);
							}
							
						}
					}
				}
			}

			if(count($operators)<=5)
			{
				foreach($municipality->hotels as $hotel)
				{
					$operator = $hotel->Operator;
					if($operator)
					{
						if(!array_key_exists($operator->slug,$operators))
						{
							if(count($operators)<=5)
							{
								$operators = array_merge($operators,[$operator->slug=>$operator->name]);
								$toAdd = ['name'=>$operator->name,
									'slug'=>$operator->slug,
									'municipality'=>$municipality->municipality_name];
								array_push($finalOperators,$toAdd);
							}
						}
					}
				}
			}
			if(count($operators)<=5)
			{
				foreach($municipality->restaurants as $restaurant)
				{
					$operator = $restaurant->Operator;
					if($operator)
					{	
						if(!array_key_exists($operator->slug,$operators))
						{
							if(count($operators)<=5)
							{
								$operators = array_merge($operators,[$operator->slug=>$operator->name]);
								$toAdd = [
									'name'=>$operator->name,
									'slug'=>$operator->slug,
									'municipality'=>$municipality->municipality_name];
								array_push($finalOperators,$toAdd);
							}
						}
					}
				}
			}
			if(count($InterestSites)<=6)
			{
				if($municipality->InterestSite)
				{
					foreach($municipality->InterestSite as $site)
					{
						if(count($InterestSites)<=6)
						{
							$toAdd = ['name'=>$site->site_name,
										'slug'=>$site->slug,
										'link_image'=>$site->link_image];
							array_push($InterestSites,$toAdd);
						}

					}
				}
			}
		}

		$outstandingDepartments = $region->FiveDepartmentsFront;

		return response()->json(['region'=>$region,
								'patterns'=>$finalPattern,
								'municipalities'=>$municipalities,
								'departments'=>$departments,
								'page'=>$page,
								'defaultImage'=>$this->defaultimage,
								'defaultcardimage'=>$this->defaultcardimage,
								'images' => $images,
								'hotels'=>$hotels,
								'restaurants'=>$restaurants,
								'operators'=>$finalOperators,
								'InterestSites'=>$InterestSites,
								'outstandingDepartments'=>$outstandingDepartments,
								]);
	}

	public function fairs(Request $request)
	{
		$page = Page::where('url',"/fairs")->first();
		$regionList = Region::where('state','activo')->pluck('region_name','slug');
		//Patterns
		$finalPattern=[];
		$departments=[];
        $fairs = Fair::all()->where('state','activo');
        $state = true;
		if(isset($page))
		{	
			$finalPattern = $this->pattern($page);
		}
       	if($request->region && $request->month)
		{
			$month = $request->month;
			$region = Region::where('state','activo')->where('slug',$request->region)->first();
			foreach($region->departments as $key => $department)
			{
				if($department->FairsFilter($month)->count()>0)
				{
					$department['fairs'] = $department->FairsFilter($month)->get();
					$region['fair_departments']= [$department];
				}
				else
				{
					unset($region->departments[$key]);
				}
			}
			$regions = [$region];
			$departments= $region->fair_departments;
		}
		else if($request->department && $request->month)
		{
			$month = $request->month;
			$department = Department::where('slug',$request->department)->first();
			$region = $department->Region->first();
			if($department->FairsFilter($month)->count()>0)
			{
				$department['fairs'] = $department->FairsFilter($month)->get();
				$region['fair_departments']= $department;
			}
			foreach($region->departments as $key => $department)
			{
				if(!$department->FairsFilter($month)->count()>0)
				{
					unset($region->departments[$key]);
				}
			}
			$departments= $region->departments;
			$regions = [$region];
		}
		else if($request->region)
		{
			$region = Region::where('state','activo')->where('slug',$request->region)->first();
			foreach($region->departments as $key => $department)
			{
				if($department->fairs->count()>0)
				{
					$region['fair_departments']= $department->with('fairs');
				}
				else
				{
					unset($region->departments[$key]);
				}
			}
			foreach($region->departments as $key => $department)
			{
				if(!$department->Fairs->count()>0)
				{
					unset($region->departments[$key]);
				}
			}
			$departments= $region->departments;

			$regions = [$region];
		}
		else if($request->department)
		{
			$department = Department::where('slug',$request->department)->first();
			$region = $department->Region->first();
			if($department->fairs->count()>0)
			{
				$department['fairs'] = $department->Fairs;
				$region['fair_departments']= [$department];
			}
			foreach($region->departments as $key => $department)
			{
				if(!$department->fairs->count()>0)
				{
					unset($region->departments[$key]);
				}
			}
			$departments= $region->departments;
			$regions = [$region];
		}
		else if($request->month)
		{
			$month = $request->month;
			$regions = Region::where('state','activo')->get();
			foreach($regions as $region)
			{
				foreach($region->departments as $key => $department)
				{
					if($department->FairsFilter($month)->count()>0)
					{
						$department['fairs'] = $department->FairsFilter($month)->get();
						$region['fair_departments']= $department;
					}
					else
					{
						unset($region->departments[$key]);
					}
				}
			}
		}
		else
		{
			$regions = Region::where('state','activo')->get();
			foreach($regions as $region)
			{
				foreach($region->departments as $key => $department)
				{
					if($department->fairs->count()>0)
					{
						$region['fair_departments']= $department->with('FairsLimit');
					}
					else
					{
						unset($region->departments[$key]);
					}
				}
			}
		}

        $outstanding = Fair::all()->where('state','activo')->where('outstanding',1);
        $counts = ['Feb'=>0,
                    'Jan'=>0,
                    'Mar'=>0,
                    'Apr'=>0,
                    'May'=>0,
                    'Jun'=>0,
                    'Jul'=>0,
                    'Aug'=>0,
                    'Sep'=>0,
                    'Oct'=>0,
                    'Nov'=>0,
                    'Dec'=>0,
                ];

         
        foreach ($fairs as $fair) 
        {
            $monthText = date("M",strtotime($fair->start_date));
            $counts["$monthText"]=$counts["$monthText"]+1 ;
        }
        return response()->json(['fairs'=>$fairs,
								'patterns'=>$finalPattern,
    							'counts'=>$counts,
    							'outstanding'=>$outstanding,
    							'regions'=>$regions,
								'page'=>$page,
    							'departments'=>$departments,
    							'regionList'=>$regionList,
    							]);
	}

	public function specials()
	{
		$page = Page::where('url',"/specials")->first();
		$finalPattern = [];
		if(isset($page))
		{	
			$finalPattern = $this->pattern($page);
		}

		$specials = Special::where('state','activo')->get();
        $outstanding = Special::all()->where('state','activo')->where('outstanding',1);

		return response()->json(['specials'=>$specials,
								'patterns'=>$finalPattern,
    							'outstanding'=>$outstanding,
								'page'=>$page,
								'defaultImage'=>$this->defaultimage,
								'defaultcardimage'=>$this->defaultcardimage,
    							]);
	}

	public function normalSpecial($slug)
	{
		$page = Page::where('url',"/special/normalspecial/$slug")->first();
		$finalPattern = [];
		if(isset($page))
		{	
			$finalPattern = $this->pattern($page);
		}

		$special = Special::where('slug',$slug)->first();
		$points = $special->detailItems;
		$images = $special->images;
		$hotels = [];
		$restaurants = [];
		$municipalities = [];
		$operators = [];
		$InterestSites = [];
		$finalOperators = [];
		foreach($points as $point)
		{
			if($point->type_relation == 'site')
			{
				$site = InterestSite::find($point->fk_relation);
				$municipality = $site->municipality;
				array_add($point,'latitude',$site->latitude);
				array_add($point,'longitude',$site->longitude);
				array_add($point,'name',$site->site_name);
				array_add($point,'image',$site->link_image);
				array_add($point,'description',$site->description);
				array_add($point,'slug',$site->slug);
				if(!array_key_exists($municipality->slug,$municipalities))
				{
					if(count($municipalities)<=5)
					$municipalities = array_merge($municipalities,[$municipality->slug=>$municipality->municipality_name]);
				}
			}
			else
			{
				$municipality = Municipality::find($point->fk_relation);
				array_add($point,'latitude',$municipality->latitude);
				array_add($point,'longitude',$municipality->longitude);
				array_add($point,'name',$municipality->municipality_name);
				array_add($point,'image',$municipality->link_image);
				array_add($point,'description',$municipality->description);
				array_add($point,'slug',$municipality->slug);
				if(!array_key_exists($municipality->slug,$municipalities))
				{
					if(count($municipalities)<=5)
					$municipalities = array_merge($municipalities,[$municipality->slug=>$municipality->municipality_name]);
				}
			}
		}

		foreach($municipalities as $slug => $municipalityname)
		{
			$municipality = Municipality::where('slug',$slug)->first();
			if(count($hotels)<=5)
			{
				foreach($municipality->hotels as $newHotel)
				{
					if(count($hotels)<=5)
					{
						$toAdd = ['name'=>$newHotel->establishment_name,
								'slug'=>$newHotel->slug,
							'municipality'=>$municipality->municipality_name];
						array_push($hotels,$toAdd);
					}
				}
			}
			if(count($restaurants)<=5)
			{
				foreach($municipality->restaurants as $newResturant)
				{
					if(count($restaurants)<=5)
					{
						$toAdd = ['name'=>$newResturant->establishment_name,
								'slug'=>$newResturant->slug,
							'municipality'=>$municipality->municipality_name];
						array_push($restaurants,$toAdd);
					}
					
				}
			}
			if(count($operators)<=5)
			{
				foreach($municipality->Plans as  $service)
				{
					if(!array_key_exists($service->operador->slug,$operators))
					{
						if(count($operators)<=5)
						{
							if(isset($service->operator->slug))
							{
								$operators = array_merge($operators,[$service->operador->slug=>$service->operador->name]);
								$toAdd = ['name'=>$service->operator->name,
									'slug'=>$service->operator->slug,
									'municipality'=>$municipality->municipality_name];
								array_push($finalOperators,$toAdd);
							}
							
						}
					}
				}
			}

			if(count($operators)<=5)
			{
				foreach($municipality->hotels as $hotel)
				{
					$operator = $hotel->Operator;
					if($operator)
					{
						if(!array_key_exists($operator->slug,$operators))
						{
							if(count($operators)<=5)
							{
								$operators = array_merge($operators,[$operator->slug=>$operator->name]);
								$toAdd = ['name'=>$operator->name,
									'slug'=>$operator->slug,
									'municipality'=>$municipality->municipality_name];
								array_push($finalOperators,$toAdd);
							}
						}
					}
				}
			}
			if(count($operators)<=5)
			{
				foreach($municipality->restaurants as $restaurant)
				{
					$operator = $restaurant->Operator;
					if($operator)
					{	
						if(!array_key_exists($operator->slug,$operators))
						{
							if(count($operators)<=5)
							{
								$operators = array_merge($operators,[$operator->slug=>$operator->name]);
								$toAdd = [
									'name'=>$operator->name,
									'slug'=>$operator->slug,
									'municipality'=>$municipality->municipality_name];
								array_push($finalOperators,$toAdd);
							}
						}
					}
				}
			}
			if(count($InterestSites)<=6)
			{
				if($municipality->InterestSite)
				{
					foreach($municipality->InterestSite as $site)
					{
						if(count($InterestSites)<=6)
						{
							$toAdd = ['name'=>$site->site_name,
										'slug'=>$site->slug,
										'link_image'=>$site->link_image];
							array_push($InterestSites,$toAdd);
						}

					}
				}
			}
		}
		return response()->json(['special'=>$special,
								'patterns'=>$finalPattern,
								'page'=>$page,
								'defaultImage'=>$this->defaultimage,
								'defaultcardimage'=>$this->defaultcardimage,
								'points'=> $points,
								'images' => $images,
								'hotels'=>$hotels,
								'restaurants'=>$restaurants,
								'operators'=>$finalOperators,
								'InterestSites'=>$InterestSites,
    							]);
	}

	public function special($slug)
	{
		$page = Page::where('url',"/special/special/$slug")->first();
		$finalPattern = [];
		if(isset($page))
		{	
			$finalPattern = $this->pattern($page);
		}

		$special = Special::where('slug',$slug)->first();
		$points = $special->items;
		$routes = $special->activeRoutes;
		foreach($special->activeRoutes as $route)
		{
			if($route->type_startpoint=='municipality')
			{
				$municipality = Municipality::find($route->start_point);
				array_add($route,'department',$municipality->Department->department_name);
				array_add($route,'department_slug',$municipality->Department->slug);
				array_add($route,'region',$municipality->PertinentRegion->region_name);
				array_add($route,'region_slug',$municipality->PertinentRegion->slug);
			}
			else
			{
				$site = InterestSite::find($route->start_point);
				$municipality = $site->municipality;
				array_add($route,'department',$municipality->Department->department_name);
				array_add($route,'department_slug',$municipality->Department->slug);
				array_add($route,'region',$municipality->PertinentRegion->region_name);
				array_add($route,'region_slug',$municipality->PertinentRegion->slug);
			}
		}
		return response()->json(['special'=>$special,
								'patterns'=>$finalPattern,
								'page'=>$page,
								'defaultImage'=>$this->defaultimage,
								'defaultcardimage'=>$this->defaultcardimage,
								'points'=> $points,
								'routes'=>$routes,
    							]);
	}

	public function specialRoute($slug)
	{
		$page = Page::where('url',"/special/special/$slug")->first();
		$finalPattern = [];
		if(isset($page))
		{	
			$finalPattern = $this->pattern($page);
		}

		$route = SpecialRoute::where('slug',$slug)->first();
		$points = $route->points;
		$hotels = [];
		$restaurants = [];
		$municipalities = [];
		$operators = [];
		$InterestSites = [];
		$finalOperators = [];
		if($route->type_startpoint=='municipality')
		{
			$municipality = Municipality::find($route->start_point);
			array_add($route,'name_start_point',$municipality->municipality_name);
			if(!array_key_exists($municipality->slug,$municipalities))
			{
				$municipalities = array_merge($municipalities,[$municipality->slug=>$municipality->municipality_name]);
				if(count($hotels)<=5 || count($operators)<=5)
				{
					foreach($municipality->hotels as $newHotel)
					{
						$operator = $newHotel->Operator;
						if($operator)
						{
							if(!array_key_exists($operator->slug,$operators))
							{
								if(count($operators)<=5)
								{
									$operators = array_merge($operators,[$operator->slug=>$operator->name]);
									$toAdd = ['name'=>$operator->name,
										'slug'=>$operator->slug,
										'municipality'=>$municipality->municipality_name];
									array_push($finalOperators,$toAdd);
								}
							}
						}
						if(count($hotels)<=5)
						{
							$toAdd = ['name'=>$newHotel->establishment_name,
									'slug'=>$newHotel->slug,
								'municipality'=>$municipality->municipality_name];
							array_push($hotels,$toAdd);
						}
					}
				}
				if(count($restaurants)<=5 || count($operators)<=5)
				{
					foreach($municipality->restaurants as $newResturant)
					{
						$operator = $newResturant->Operator;
						if($operator)
						{
							if(!array_key_exists($operator->slug,$operators))
							{
								if(count($operators)<=5)
								{
									$operators = array_merge($operators,[$operator->slug=>$operator->name]);
									$toAdd = ['name'=>$operator->name,
										'slug'=>$operator->slug,
										'municipality'=>$municipality->municipality_name];
									array_push($finalOperators,$toAdd);
								}
							}
						}
						if(count($restaurants)<=5)
						{
							$toAdd = ['name'=>$newResturant->establishment_name,
									'slug'=>$newResturant->slug,
								'municipality'=>$municipality->municipality_name];
							array_push($restaurants,$toAdd);
						}
						
					}
				}
				if(count($InterestSites)<=6)
				{
					if($municipality->InterestSite)
					{
						foreach($municipality->InterestSite as $site)
						{
							if(count($InterestSites)<=6)
							{
								$toAdd = ['name'=>$site->site_name,
											'slug'=>$site->slug,
											'link_image'=>$site->link_image];
								array_push($InterestSites,$toAdd);
							}

						}
					}
				}
			}
		}
		else
		{
			$site = InterestSite::find($route->start_point);
			$municipality = $site->municipality;
			array_add($route,'name_start_point',$municipality->municipality_name);

			if(!array_key_exists($municipality->slug,$municipalities))
			{
				$municipalities = array_merge($municipalities,[$municipality->slug=>$municipality->municipality_name]);
				if(count($hotels)<=5 || count($operators)<=5)
				{
					foreach($municipality->hotels as $newHotel)
					{
						$operator = $newHotel->Operator;
						if($operator)
						{
							if(!array_key_exists($operator->slug,$operators))
							{
								if(count($operators)<=5)
								{
									$operators = array_merge($operators,[$operator->slug=>$operator->name]);
									$toAdd = ['name'=>$operator->name,
										'slug'=>$operator->slug,
										'municipality'=>$municipality->municipality_name];
									array_push($finalOperators,$toAdd);
								}
							}
						}
						if(count($hotels)<=5)
						{
							$toAdd = ['name'=>$newHotel->establishment_name,
									'slug'=>$newHotel->slug,
								'municipality'=>$municipality->municipality_name];
							array_push($hotels,$toAdd);
						}
					}
				}
				if(count($restaurants)<=5 || count($operators)<=5)
				{
					foreach($municipality->restaurants as $newResturant)
					{
						$operator = $newResturant->Operator;
						if($operator)
						{
							if(!array_key_exists($operator->slug,$operators))
							{
								if(count($operators)<=5)
								{
									$operators = array_merge($operators,[$operator->slug=>$operator->name]);
									$toAdd = ['name'=>$operator->name,
										'slug'=>$operator->slug,
										'municipality'=>$municipality->municipality_name];
									array_push($finalOperators,$toAdd);
								}
							}
						}
						if(count($restaurants)<=5)
						{
							$toAdd = ['name'=>$newResturant->establishment_name,
									'slug'=>$newResturant->slug,
								'municipality'=>$municipality->municipality_name];
							array_push($restaurants,$toAdd);
						}
						
					}
				}
				if(count($InterestSites)<=6)
				{
					if($municipality->InterestSite)
					{
						foreach($municipality->InterestSite as $site)
						{
							if(count($InterestSites)<=6)
							{
								$toAdd = ['name'=>$site->site_name,
											'slug'=>$site->slug,
											'link_image'=>$site->link_image];
								array_push($InterestSites,$toAdd);
							}

						}
					}
				}
			}
		}

		if($route->type_endpoint=='municipality')
		{
			$municipality = Municipality::find($route->end_point);
			array_add($route,'name_end_point',$municipality->municipality_name);
			if(!array_key_exists($municipality->slug,$municipalities))
			{
				$municipalities = array_merge($municipalities,[$municipality->slug=>$municipality->municipality_name]);
				if(count($hotels)<=5 || count($operators)<=5)
				{
					foreach($municipality->hotels as $newHotel)
					{
						$operator = $newHotel->Operator;
						if($operator)
						{
							if(!array_key_exists($operator->slug,$operators))
							{
								if(count($operators)<=5)
								{
									$operators = array_merge($operators,[$operator->slug=>$operator->name]);
									$toAdd = ['name'=>$operator->name,
										'slug'=>$operator->slug,
										'municipality'=>$municipality->municipality_name];
									array_push($finalOperators,$toAdd);
								}
							}
						}
						if(count($hotels)<=5)
						{
							$toAdd = ['name'=>$newHotel->establishment_name,
									'slug'=>$newHotel->slug,
								'municipality'=>$municipality->municipality_name];
							array_push($hotels,$toAdd);
						}
					}
				}
				if(count($restaurants)<=5 || count($operators)<=5)
				{
					foreach($municipality->restaurants as $newResturant)
					{
						$operator = $newResturant->Operator;
						if($operator)
						{
							if(!array_key_exists($operator->slug,$operators))
							{
								if(count($operators)<=5)
								{
									$operators = array_merge($operators,[$operator->slug=>$operator->name]);
									$toAdd = ['name'=>$operator->name,
										'slug'=>$operator->slug,
										'municipality'=>$municipality->municipality_name];
									array_push($finalOperators,$toAdd);
								}
							}
						}
						if(count($restaurants)<=5)
						{
							$toAdd = ['name'=>$newResturant->establishment_name,
									'slug'=>$newResturant->slug,
								'municipality'=>$municipality->municipality_name];
							array_push($restaurants,$toAdd);
						}
						
					}
				}
				if(count($InterestSites)<=6)
				{
					if($municipality->InterestSite)
					{
						foreach($municipality->InterestSite as $site)
						{
							if(count($InterestSites)<=6)
							{
								$toAdd = ['name'=>$site->site_name,
											'slug'=>$site->slug,
											'link_image'=>$site->link_image];
								array_push($InterestSites,$toAdd);
							}

						}
					}
				}
			}
		}
		else
		{
			$site = InterestSite::find($route->start_point);
			$municipality = $site->municipality;
			array_add($route,'name_end_point',$municipality->municipality_name);

			if(!array_key_exists($municipality->slug,$municipalities))
			{
				$municipalities = array_merge($municipalities,[$municipality->slug=>$municipality->municipality_name]);
				if(count($hotels)<=5 || count($operators)<=5)
				{
					foreach($municipality->hotels as $newHotel)
					{
						$operator = $newHotel->Operator;
						if($operator)
						{
							if(!array_key_exists($operator->slug,$operators))
							{
								if(count($operators)<=5)
								{
									$operators = array_merge($operators,[$operator->slug=>$operator->name]);
									$toAdd = ['name'=>$operator->name,
										'slug'=>$operator->slug,
										'municipality'=>$municipality->municipality_name];
									array_push($finalOperators,$toAdd);
								}
							}
						}
						if(count($hotels)<=5)
						{
							$toAdd = ['name'=>$newHotel->establishment_name,
									'slug'=>$newHotel->slug,
								'municipality'=>$municipality->municipality_name];
							array_push($hotels,$toAdd);
						}
					}
				}
				if(count($restaurants)<=5 || count($operators)<=5)
				{
					foreach($municipality->restaurants as $newResturant)
					{
						$operator = $newResturant->Operator;
						if($operator)
						{
							if(!array_key_exists($operator->slug,$operators))
							{
								if(count($operators)<=5)
								{
									$operators = array_merge($operators,[$operator->slug=>$operator->name]);
									$toAdd = ['name'=>$operator->name,
										'slug'=>$operator->slug,
										'municipality'=>$municipality->municipality_name];
									array_push($finalOperators,$toAdd);
								}
							}
						}
						if(count($restaurants)<=5)
						{
							$toAdd = ['name'=>$newResturant->establishment_name,
									'slug'=>$newResturant->slug,
								'municipality'=>$municipality->municipality_name];
							array_push($restaurants,$toAdd);
						}
						
					}
				}
				if(count($InterestSites)<=6)
				{
					if($municipality->InterestSite)
					{
						foreach($municipality->InterestSite as $site)
						{
							if(count($InterestSites)<=6)
							{
								$toAdd = ['name'=>$site->site_name,
											'slug'=>$site->slug,
											'link_image'=>$site->link_image];
								array_push($InterestSites,$toAdd);
							}

						}
					}
				}
			}
		}

		$images = $route->imagesrelation;

		return response()->json(['route'=>$route,
								'special'=>$route->special,
								'patterns'=>$finalPattern,
								'page'=>$page,
								'defaultImage'=>$this->defaultimage,
								'defaultcardimage'=>$this->defaultcardimage,
								'points'=> $points,
								'images' => $images,
								'hotels'=>$hotels,
								'restaurants'=>$restaurants,
								'operators'=>$finalOperators,
								'InterestSites'=>$InterestSites,
    							]);
	}

	public function getnamesites(Request $request)
	{
		$search = $request->name;
		$result=[];
		$municipalities = Municipality::where('municipality_name','like','%'.$search."%")->where('state','activo')->get();
		if(count($municipalities)>0)
		{
			foreach($municipalities as $municipality)
			{
				array_push($result,['slug'=>$municipality->slug,
								'type'=>'municipality',
								'name'=>$municipality->municipality_name]);
			}
		}
		$sites = InterestSite::where('site_name','like','%'.$search."%")->where('state','activo')->get();
		if(count($sites)>0)
		{
			foreach($sites as $site)
			{
				array_push($result,['slug'=>$site->slug,
								'type'=>'site',
								'name'=>$site->site_name]);
			}
		}
		return response()->json($result);
	}

	public function getnearbypoints(Request $request)
	{
		$start = [];
		$end = [];
		$points =[];
		$municipalities =[];
		if($request->origin['type']=='municipality')
		{
			$municipality = Municipality::where('slug',$request->origin['slug'])->first();
			$start=['name'=>$municipality->municipality_name,
					'description'=>$municipality->description,
					'latitude'=>$municipality->latitude,
					'longitude'=>$municipality->longitude,
					'link_image'=>$municipality->link_image,
					'code'=>$municipality->id_municipality,
					'type_relation'=>'start'];
		}
		else
		{
			$site = InterestSite::where('slug',$request->origin['slug'])->first();
			$start=['name'=>$site->site_name,
					'description'=>$site->description,
					'latitude'=>$site->latitude,
					'longitude'=>$site->longitude,
					'link_image'=>$site->link_image,
					'code'=>$site->id_site,
					'type_relation'=>'start'];
		}
		if($request->destiny['type']=='municipality')
		{
			$municipality = Municipality::where('slug',$request->destiny['slug'])->first();
			$end=['name'=>$municipality->municipality_name,
					'description'=>$municipality->description,
					'latitude'=>$municipality->latitude,
					'longitude'=>$municipality->longitude,
					'link_image'=>$municipality->link_image,
					'code'=>$municipality->id_municipality,
					'type_relation'=>'end'];
		}
		else
		{
			$site = InterestSite::where('slug',$request->destiny['slug'])->first();
			$end=['name'=>$site->site_name,
					'description'=>$site->description,
					'latitude'=>$site->latitude,
					'longitude'=>$site->longitude,
					'link_image'=>$site->link_image,
					'code'=>$site->id_site,
					'type_relation'=>'end'];
		}


		$minimoLat=min($end['latitude'], $start['latitude'])-0.05;
		$maximoLat=max($end['latitude'], $start['latitude'])+0.05;
		$maximoLng=min($end['longitude'], $start['longitude'])-0.05;
		$minimoLng=max($end['longitude'], $start['longitude'])+0.05;

		if($request->origin['type']=='municipality' && $request->destiny['type']=='municipality')
		{
			$municipalities = Municipality::where('state','activo')
										->whereBetween('latitude',[$minimoLat,$maximoLat])
										->whereBetween('longitude',[$maximoLng,$minimoLng])
										->where('id_municipality','!=',$start['code'])
										->where('id_municipality','!=',$end['code'])
								    	->get();
		}
		elseif($request->origin['type']=='municipality')
		{
			$municipalities = Municipality::where('state','activo')
										->whereBetween('latitude',[$minimoLat,$maximoLat])
										->whereBetween('longitude',[$maximoLng,$minimoLng])
										->where('id_municipality','!=',$start['code'])
								    	->get();
		}
		elseif($request->destiny['type']=='municipality')
		{
			$municipalities = Municipality::where('state','activo')
										->whereBetween('latitude',[$minimoLat,$maximoLat])
										->whereBetween('longitude',[$maximoLng,$minimoLng])
										->where('id_municipality','!=',$end['code'])
								    	->get();
		}
		else
		{
			$municipalities = Municipality::where('state','activo')
										->whereBetween('latitude',[$minimoLat,$maximoLat])
										->whereBetween('longitude',[$maximoLng,$minimoLng])
								    	->get();
		}
		if(count($municipalities)>0)
		{
			foreach($municipalities as $municipality)
			{
				array_push($points,['name'=>$municipality->municipality_name,
									'description'=>$municipality->description,
									'latitude'=>$municipality->latitude,
									'longitude'=>$municipality->longitude,
									'image'=>$municipality->link_image,
									'type_relation'=>'municipality']);
			}
		}
		if($request->origin['type']=='site' && $request->destiny['type']=='site')
		{
			$sites = InterestSite::where('state','activo')
										->whereBetween('latitude',[$minimoLat,$maximoLat])
										->whereBetween('longitude',[$maximoLng,$minimoLng])
										->where('id_site','!=',$start['code'])
										->where('id_site','!=',$end['code'])
										->get();
		}
		elseif($request->origin['type']=='site')
		{
			$sites = InterestSite::where('state','activo')
										->whereBetween('latitude',[$minimoLat,$maximoLat])
										->whereBetween('longitude',[$maximoLng,$minimoLng])
										->where('id_site','!=',$start['code'])
										->get();
		}
		elseif($request->destiny['type']=='site')
		{
			$sites = InterestSite::where('state','activo')
										->whereBetween('latitude',[$minimoLat,$maximoLat])
										->whereBetween('longitude',[$maximoLng,$minimoLng])
										->where('id_site','!=',$end['code'])
										->get();
		}
		else
		{
			$sites = InterestSite::where('state','activo')
										->whereBetween('latitude',[$minimoLat,$maximoLat])
										->whereBetween('longitude',[$maximoLng,$minimoLng])
										->get();
		}
		if(count($sites)>0)
		{
			foreach($sites as $site)
			{
				array_push($points,['name'=>$site->site_name,
									'description'=>$site->description,
									'latitude'=>$site->latitude,
									'longitude'=>$site->longitude,
									'image'=>$site->link_image,
									'type_relation'=>'site']);
			}
		}

		array_push($points,$end);
		array_push($points,$start);

		return response()->json(['start'=>$start,
								'end'=>$end,
								'points'=>$points]);
	}

	public function routes()
	{
		$page = Page::where('url',"/routes")->first();
		$finalPattern = [];
		if(isset($page))
		{	
			$finalPattern = $this->pattern($page);
		}

		$routes = Route::where('state','activo')->get();
		$concessions = Concession::where('state','activo')->get();
		$circuits = Circuit::where('state','activo')->get();

		$cantConcessions = (int)round(count($concessions)/3);

		return response()->json(['routes'=>$routes,
								'patterns'=>$finalPattern,
    							'concessions'=>$concessions,
    							'cantConcessions'=>$cantConcessions,
    							'circuits'=>$circuits,
    							'defaultlogoconcession'=>$this->defaultlogoconcession,
    							'defaultcardroute'=>$this->defaultcardroute,
								'page'=>$page,
								'defaultImage'=>$this->defaultimage,
								'defaultcardimage'=>$this->defaultcardimage,
    							]);
	}

	public function route($slug)
	{
		$page = Page::where('url',"/route/$slug")->first();
		$finalPattern = [];
		if(isset($page))
		{	
			$finalPattern = $this->pattern($page);
		}
		$route = Route::where('slug',$slug)->first();
		$points = $route->points;
		$images = $route->imagesrelation;
		$hotels = [];
		$restaurants = [];
		$municipalities = [];
		$operators = [];
		$InterestSites = [];
		$finalOperators = [];

		$colors = ['primary','success','info','danger','warning','default'];

		foreach($points as $point)
		{
			array_add($point,'classColor',$colors[array_rand($colors)]);
			if($point->type_relation == 'site')
			{
				$site = InterestSite::find($point->fk_relation);
				$municipality = $site->municipality;
				array_add($point,'latitude',$site->latitude);
				array_add($point,'longitude',$site->longitude);
				array_add($point,'name',$site->site_name);
				array_add($point,'image',$site->link_image);
				array_add($point,'description',$site->description);
				array_add($point,'slug',$site->slug);
				if(!array_key_exists($municipality->slug,$municipalities))
				{
					if(count($municipalities)<=5)
					$municipalities = array_merge($municipalities,[$municipality->slug=>$municipality->municipality_name]);
				}
			}
			else
			{
				$municipality = Municipality::find($point->fk_relation);
				array_add($point,'latitude',$municipality->latitude);
				array_add($point,'longitude',$municipality->longitude);
				array_add($point,'name',$municipality->municipality_name);
				array_add($point,'image',$municipality->link_image);
				array_add($point,'description',$municipality->description);
				array_add($point,'slug',$municipality->slug);
				if(!array_key_exists($municipality->slug,$municipalities))
				{
					if(count($municipalities)<=5)
					$municipalities = array_merge($municipalities,[$municipality->slug=>$municipality->municipality_name]);
				}
			}
		}

		foreach($municipalities as $slug => $municipalityname)
		{
			$municipality = Municipality::where('slug',$slug)->first();
			if(count($hotels)<=5)
			{
				foreach($municipality->hotels as $newHotel)
				{
					if(count($hotels)<=5)
					{
						$toAdd = ['name'=>$newHotel->establishment_name,
								'slug'=>$newHotel->slug,
							'municipality'=>$municipality->municipality_name];
						array_push($hotels,$toAdd);
					}
				}
			}
			if(count($restaurants)<=5)
			{
				foreach($municipality->restaurants as $newResturant)
				{
					if(count($restaurants)<=5)
					{
						$toAdd = ['name'=>$newResturant->establishment_name,
								'slug'=>$newResturant->slug,
							'municipality'=>$municipality->municipality_name];
						array_push($restaurants,$toAdd);
					}
					
				}
			}
			if(count($operators)<=5)
			{
				foreach($municipality->Plans as  $service)
				{
					if(!array_key_exists($service->operador->slug,$operators))
					{
						if(count($operators)<=5)
						{
							if(isset($service->operator->slug))
							{
								$operators = array_merge($operators,[$service->operador->slug=>$service->operador->name]);
								$toAdd = ['name'=>$service->operator->name,
									'slug'=>$service->operator->slug,
									'municipality'=>$municipality->municipality_name];
								array_push($finalOperators,$toAdd);
							}
							
						}
					}
				}
			}

			if(count($operators)<=5)
			{
				foreach($municipality->hotels as $hotel)
				{
					$operator = $hotel->Operator;
					if($operator)
					{
						if(!array_key_exists($operator->slug,$operators))
						{
							if(count($operators)<=5)
							{
								$operators = array_merge($operators,[$operator->slug=>$operator->name]);
								$toAdd = ['name'=>$operator->name,
									'slug'=>$operator->slug,
									'municipality'=>$municipality->municipality_name];
								array_push($finalOperators,$toAdd);
							}
						}
					}
				}
			}
			if(count($operators)<=5)
			{
				foreach($municipality->restaurants as $restaurant)
				{
					$operator = $restaurant->Operator;
					if($operator)
					{	
						if(!array_key_exists($operator->slug,$operators))
						{
							if(count($operators)<=5)
							{
								$operators = array_merge($operators,[$operator->slug=>$operator->name]);
								$toAdd = [
									'name'=>$operator->name,
									'slug'=>$operator->slug,
									'municipality'=>$municipality->municipality_name];
								array_push($finalOperators,$toAdd);
							}
						}
					}
				}
			}
			if(count($InterestSites)<=6)
			{
				if($municipality->InterestSite)
				{
					foreach($municipality->InterestSite as $site)
					{
						if(count($InterestSites)<=6)
						{
							$toAdd = ['name'=>$site->site_name,
										'slug'=>$site->slug,
										'link_image'=>$site->link_image];
							array_push($InterestSites,$toAdd);
						}

					}
				}
			}
		}
		return response()->json(['route'=>$route,
								'patterns'=>$finalPattern,
								'page'=>$page,
								'defaultImage'=>$this->defaultimage,
								'defaultcardimage'=>$this->defaultcardimage,
								'points'=> $points,
								'images' => $images,
								'hotels'=>$hotels,
								'restaurants'=>$restaurants,
								'operators'=>$finalOperators,
								'InterestSites'=>$InterestSites,
    							]);
	}
	
	public function concession($slug)
	{
		$page = Page::where('url',"/concession/$slug")->first();
		$finalPattern = [];
		$finalSites = [];
		if(isset($page))
		{	
			$finalPattern = $this->pattern($page);
		}
		$concession = Concession::where('slug',$slug)->first();
		$tolls = $concession->frontTolls;
		$sites = $concession->sites;


		return response()->json(['concession'=>$concession,
								'patterns'=>$finalPattern,
								'page'=>$page,
								'defaultcardimage'=>$this->defaultcardimage,
								'tolls'=>$tolls,
								'sites'=>$sites,
    							]);
	}

	public function circuit($slug)
	{
		$page = Page::where('url',"/route/$slug")->first();
		$finalPattern = [];
		if(isset($page))
		{	
			$finalPattern = $this->pattern($page);
		}
		$circuit = Circuit::where('slug',$slug)->first();
		$points = $circuit->detailItems;
		$images = $circuit->images;
		$hotels = [];
		$restaurants = [];
		$municipalities = [];
		$operators = [];
		$InterestSites = [];
		$finalOperators = [];
		foreach($points as $point)
		{
			if($point->type_relation == 'site')
			{
				$site = InterestSite::find($point->fk_relation);
				$municipality = $site->municipality;
				array_add($point,'latitude',$site->latitude);
				array_add($point,'longitude',$site->longitude);
				array_add($point,'name',$site->site_name);
				array_add($point,'image',$site->link_image);
				array_add($point,'description',$site->description);
				array_add($point,'slug',$site->slug);
				if(!array_key_exists($municipality->slug,$municipalities))
				{
					if(count($municipalities)<=5)
					$municipalities = array_merge($municipalities,[$municipality->slug=>$municipality->municipality_name]);
				}
			}
			else
			{
				$municipality = Municipality::find($point->fk_relation);
				array_add($point,'latitude',$municipality->latitude);
				array_add($point,'longitude',$municipality->longitude);
				array_add($point,'name',$municipality->municipality_name);
				array_add($point,'image',$municipality->link_image);
				array_add($point,'description',$municipality->description);
				array_add($point,'slug',$municipality->slug);
				if(!array_key_exists($municipality->slug,$municipalities))
				{
					if(count($municipalities)<=5)
					$municipalities = array_merge($municipalities,[$municipality->slug=>$municipality->municipality_name]);
				}
			}
		}

		foreach($municipalities as $slug => $municipalityname)
		{
			$municipality = Municipality::where('slug',$slug)->first();
			if(count($hotels)<=5)
			{
				foreach($municipality->hotels as $newHotel)
				{
					if(count($hotels)<=5)
					{
						$toAdd = ['name'=>$newHotel->establishment_name,
								'slug'=>$newHotel->slug,
							'municipality'=>$municipality->municipality_name];
						array_push($hotels,$toAdd);
					}
				}
			}
			if(count($restaurants)<=5)
			{
				foreach($municipality->restaurants as $newResturant)
				{
					if(count($restaurants)<=5)
					{
						$toAdd = ['name'=>$newResturant->establishment_name,
								'slug'=>$newResturant->slug,
							'municipality'=>$municipality->municipality_name];
						array_push($restaurants,$toAdd);
					}
					
				}
			}
			if(count($operators)<=5)
			{
				foreach($municipality->Plans as  $service)
				{
					if(!array_key_exists($service->operador->slug,$operators))
					{
						if(count($operators)<=5)
						{
							if(isset($service->operator->slug))
							{
								$operators = array_merge($operators,[$service->operador->slug=>$service->operador->name]);
								$toAdd = ['name'=>$service->operator->name,
									'slug'=>$service->operator->slug,
									'municipality'=>$municipality->municipality_name];
								array_push($finalOperators,$toAdd);
							}
							
						}
					}
				}
			}

			if(count($operators)<=5)
			{
				foreach($municipality->hotels as $hotel)
				{
					$operator = $hotel->Operator;
					if($operator)
					{
						if(!array_key_exists($operator->slug,$operators))
						{
							if(count($operators)<=5)
							{
								$operators = array_merge($operators,[$operator->slug=>$operator->name]);
								$toAdd = ['name'=>$operator->name,
									'slug'=>$operator->slug,
									'municipality'=>$municipality->municipality_name];
								array_push($finalOperators,$toAdd);
							}
						}
					}
				}
			}
			if(count($operators)<=5)
			{
				foreach($municipality->restaurants as $restaurant)
				{
					$operator = $restaurant->Operator;
					if($operator)
					{	
						if(!array_key_exists($operator->slug,$operators))
						{
							if(count($operators)<=5)
							{
								$operators = array_merge($operators,[$operator->slug=>$operator->name]);
								$toAdd = [
									'name'=>$operator->name,
									'slug'=>$operator->slug,
									'municipality'=>$municipality->municipality_name];
								array_push($finalOperators,$toAdd);
							}
						}
					}
				}
			}
			if(count($InterestSites)<=6)
			{
				if($municipality->InterestSite)
				{
					foreach($municipality->InterestSite as $site)
					{
						if(count($InterestSites)<=6)
						{
							$toAdd = ['name'=>$site->site_name,
										'slug'=>$site->slug,
										'link_image'=>$site->link_image];
							array_push($InterestSites,$toAdd);
						}

					}
				}
			}
		}


		return response()->json(['circuit'=>$circuit,
								'patterns'=>$finalPattern,
								'page'=>$page,
								'defaultImage'=>$this->defaultimage,
								'defaultcardimage'=>$this->defaultcardimage,
								'points'=> $points,
								'images' => $images,
								'hotels'=>$hotels,
								'restaurants'=>$restaurants,
								'operators'=>$finalOperators,
								'InterestSites'=>$InterestSites,
    							]);
	}

	public function directory()
	{
		$page = Page::where('url',"/directory")->first();
		$finalPattern = [];
		if(isset($page))
		{	
			$finalPattern = $this->pattern($page);
		}

		$hotelsOutstanding = Establishment::where('state','activo')
									->where('outstanding',1)
									->where('type_establishment','hotel')
									->with('municipality')
									->get();

		$restaurantsOutstanding = Establishment::where('state','activo')
									->where('outstanding',1)
									->where('type_establishment','restaurant')
									->with('municipality')
									->get();

		$operatorsOutstanding = Operator::where('status','active')
									->where('outstanding',1)
									->get();

		$hotels = Establishment::where('state','activo')
									->where('type_establishment','hotel')
									->inRandomOrder(4)
									->with('Municipality')
									->get();

		$restaurants = Establishment::where('state','activo')
									->where('type_establishment','restaurant')
									->with('Municipality')
									->inRandomOrder(4)
									->get();

		$operators = Operator::where('status','active')
									->inRandomOrder(4)
									->get();

		foreach ($operators as $operator) 
		{
			if($operator->directService->count()>0)
			{
				$service = $operator->directService->first();
				array_add($operator,'municipality_name',$service->municipality->municipality_name);
				if(count($service->images)>0)
				{
					array_add($operator,'card_image',$service->images->first()->link_image);
				}
			}
			elseif($operator->restaurants->count()>0)
			{
				$establishment = $operator->restaurants->first();
				array_add($operator,'municipality_name',$establishment->municipality->municipality_name);
				if($establishment->card_image)
				{
					array_add($operator,'card_image',$establishment->card_image);
				}

			}
			elseif($operator->hotels->count()>0)
			{
				$establishment = $operator->hotels->first();
				array_add($operator,'municipality_name',$establishment->municipality->municipality_name);
				if($establishment->card_image)
				{
					array_add($operator,'card_image',$establishment->card_image);
				}
			}
			else
			{
				array_add($operator,'municipality_name','');
				array_add($operator,'card_image','');
			}
		}

		$allHotels = Establishment::where('state','activo')
									->where('type_establishment','hotel')
									->count();

		$allRestaurants = Establishment::where('state','activo')
									->where('type_establishment','restaurant')
									->count();

		$allOperators = Operator::where('status','active')
									->count();

		$specials = Special::where('state','activo')->get();

		return response()->json(['hotelsOutstanding'=>$hotelsOutstanding,
								'restaurantsOutstanding'=>$restaurantsOutstanding,
								'operatorsOutstanding'=> $operatorsOutstanding,
								'page'=>$page,
								'hotels'=>$hotels,
								'restaurants'=>$restaurants,
								'defaultrestaurant'=>$this->defaultrestaurant,
								'defaulthotel'=>$this->defaulthotel,
								'defaultimage'=>$this->defaultimage,
								'defaultoperator'=>$this->defaultoperator,
								'defaultcardimage'=>$this->defaultcardimage,
								'allHotels'=>$allHotels,
								'allRestaurants'=>$allRestaurants,
								'specials'=>$specials,
								'allOperators'=>$allOperators,
								'operators'=>$operators,
								'patterns'=>$finalPattern,
    							]);
		
	}

	public function hotels(Request $request)
	{
		$cantHotels = Establishment::where('state','activo')->where('type_establishment','hotel')->count();
		//publicidad
		$hotels = [];
		$page = Page::where('url',"/hotels")->first();
		$finalPattern = [];
		if(isset($page))
		{	
			$finalPattern = $this->pattern($page);
		}

		if($request->name)
		{
			$hotels = Establishment::with('municipality')
									->where('type_establishment','hotel')
									->where('state','activo')	
									->where('establishment_name','like',"%$request->name%")
									->paginate(10);
		}
		else if($request->municipality)
		{
			$hotels = Establishment::with('municipality')->where('type_establishment','hotel')->whereHas('municipality',function ($q1) use($request)
			{
				$q1->where('slug',$request->municipality);
			})->where('state','activo')->paginate(10);
		}
		else
		{
			$hotels = Establishment::with('municipality')->where('state','activo')->where('type_establishment','hotel')->paginate(10);
		}
		
		$municipalities = Municipality::with('hotels')->whereHas('Hotels',function ($q1)
		{
			$q1->where('state','activo');
		})->get();

		foreach ($municipalities as $municipality) 
		{
			array_add($municipality,'cant_hotels',$municipality->hotels->count());
		}

		$hotelsOutstanding = Establishment::where('state','activo')
									->where('outstanding',1)
									->where('type_establishment','hotel')
									->with('municipality')
									->get();

		$hotels = collect($hotels);


		return response()->json(['cantHotels'=>$cantHotels,
								'municipalities'=>$municipalities,
								'patterns'=>$finalPattern,
								'hotels'=>$hotels,
								'hotelsOutstanding'=>$hotelsOutstanding,
								'defaulthotel'=>$this->defaulthotel,
								'defaultimage'=>$this->defaultimage,
								'pagination'=>[
									'total'			=>$hotels['total'],
									'current_page'	=>$hotels['current_page'],
									'per_page'		=>$hotels['per_page'],
									'from'			=>$hotels['from'],
									'last_page'		=>$hotels['last_page'],
									'to'			=>$hotels['to'],
									],
								]);
	}
	public function restaurants(Request $request)
	{
		$cantRestaurants = Establishment::where('state','activo')->where('type_establishment','restaurant')->count();
		//publicidad
		$restaurants = [];
		$typeskitchen = [];
		$page = Page::where('url',"/restaurants")->first();
		$finalPattern = [];
		if(isset($page))
		{	
			$finalPattern = $this->pattern($page);
		}

		if($request->name)
		{
			$restaurants = Establishment::with('municipality')
									->where('type_establishment','restaurant')
									->where('state','activo')	
									->where('establishment_name','like',"%$request->name%")
									->paginate(10);
		}
		else if($request->municipality && $request->type_kitchen)
		{
			$restaurants = Establishment::with('municipality')->where('type_establishment','restaurant')->whereHas('municipality',function ($q1) use($request)
			{
				$q1->where('slug',$request->municipality);
			})->where('state','activo')->where('kitchen_type',$request->type_kitchen)->paginate(10);

			$typeskitchen = Establishment::with('municipality')->where('type_establishment','restaurant')->whereHas('municipality',function ($q1) use($request)
			{
				$q1->where('slug',$request->municipality);
			})->where('state','activo')->groupBy('kitchen_type')->pluck('kitchen_type');

		}
		else if($request->type_kitchen)
		{
			$restaurants = Establishment::with('municipality')->where('type_establishment','restaurant')->where('state','activo')->where('kitchen_type',$request->type_kitchen)->paginate(10);

			$typeskitchen = Establishment::with('municipality')->where('type_establishment','restaurant')->where('state','activo')->groupBy('kitchen_type')->pluck('kitchen_type');

		}
		else if($request->municipality)
		{
			$restaurants = Establishment::with('municipality')->where('type_establishment','restaurant')->whereHas('municipality',function ($q1) use($request)
			{
				$q1->where('slug',$request->municipality);
			})->where('state','activo')->paginate(10);

			$typeskitchen = Establishment::with('municipality')->where('type_establishment','restaurant')->whereHas('municipality',function ($q1) use($request)
			{
				$q1->where('slug',$request->municipality);
			})->where('state','activo')->groupBy('kitchen_type')->pluck('kitchen_type');

		}
		else
		{
			$restaurants = Establishment::with('municipality')->where('state','activo')->where('type_establishment','restaurant')->paginate(10);
			$typeskitchen = Establishment::with('municipality')->where('state','activo')->where('type_establishment','restaurant')->groupBy('kitchen_type')->pluck('kitchen_type'); 
		}
		
		$municipalities = Municipality::with('restaurants')->whereHas('Restaurants',function ($q1)
		{
			$q1->where('state','activo');
		})->get();

		foreach ($municipalities as $municipality) 
		{
			array_add($municipality,'cant_restaurants',$municipality->restaurants->count());
		}

		$restaurantsOutstanding = Establishment::where('state','activo')
									->where('outstanding',1)
									->where('type_establishment','restaurant')
									->with('municipality')
									->get();

		$restaurants = collect($restaurants);

		return response()->json(['cantRestaurants'=>$cantRestaurants,
								'municipalities'=>$municipalities,
								'patterns'=>$finalPattern,
								'restaurants'=>$restaurants,
								'typeskitchen'=>$typeskitchen,
								'restaurantsOutstanding'=>$restaurantsOutstanding,
								'defaultrestaurant'=>$this->defaultrestaurant,
								'defaultimage'=>$this->defaultimage,
								'pagination'=>[
									'total'			=>$restaurants['total'],
									'current_page'	=>$restaurants['current_page'],
									'per_page'		=>$restaurants['per_page'],
									'from'			=>$restaurants['from'],
									'last_page'		=>$restaurants['last_page'],
									'to'			=>$restaurants['to'],
									],
								]);
	}

	public function services(Request $request)
	{
		$regions = Region::all()->where('state','activo');
		$services = [];
		$departments = [];
		$municipalities = [];
		//publicidad
		$page = Page::where('url',"/services")->first();
		$finalPattern = [];

		$activities =  Service::whereHas('ActiveServices', function ($query) 
		{
			$query->where('id_service','!=',0);

		})->get();

		if(isset($page))
		{	
			$finalPattern = $this->pattern($page);
		}

		if($request->region)
		{	
			$departments = Department::with('Region')->whereHas('Region',function($query1) use($request)
						{
							$regionsSearch = explode(',', substr($request->region, 0,-1));
							$query1->whereIn('slug',$regionsSearch);
						})->get()->groupBy('region.region_name');
		}
		if($request->department)
		{
			$municipalities = Municipality::whereHas('Department',function($q1) use ($request)
				{
					$q1->where('slug',$request->department);
				})->whereHas('Plans',function($q2)
				{
					$q2->where('state','Activo');

				})->get();

		}
		//SE FILTRA POR MUNICIPIOS
		if($request->municipality && $request->activity)
		{
			$services = ServiceOperator::whereHas('Municipality',function($q1) use($request)
			{
				$q1->where('slug',$request->municipality);
			})->whereHas('PrincipalService',function($q4) use ($request)
			{
				$q4->where('slug',explode(',',$request->activity));
			})->where('state','Activo')->with('operador')->groupBy('fk_operator')->paginate(10);
		}
		else if($request->department && $request->activity)
		{
			$services = ServiceOperator::whereHas('Municipality',function($q1) use($request)
			{
				$departmentSearch = $request->department;
				$q1->whereHas('Department',function($q2)  use ($departmentSearch)
					{
						$q2->where('slug',$departmentSearch);
					});
			})->whereHas('PrincipalService',function($q4) use ($request)
			{
				$q4->where('slug',explode(',',$request->activity));
			})->where('state','Activo')->with('operador')->groupBy('fk_operator')->paginate(10);
		}
		else if($request->region && $request->activity)
		{
			$services = ServiceOperator::whereHas('Municipality',function($q1) use($request)
			{
				$regionsSearch = explode(',', substr($request->region, 0,-1));
				$q1->whereHas('Department',function($q2)  use ($regionsSearch)
					{
						$aux = $regionsSearch;
						$q2->whereHas('Region',function($q3)  use ($aux)
						{
							$q3->whereIn('slug',$aux);
						});
					});
			})->whereHas('PrincipalService',function($q4) use ($request)
			{
				$q4->where('slug',explode(',',$request->activity));
			})->where('state','Activo')->with('operador')->groupBy('fk_operator')->paginate(10);

		}
		else if($request->municipality)
		{
			$services = ServiceOperator::whereHas('Municipality',function($q1) use($request)
			{
				$q1->where('slug',$request->municipality);
			})->where('state','Activo')->with('operador')->groupBy('fk_operator')->paginate(10);
		}
		else if($request->department)
		{
			$services = ServiceOperator::whereHas('Municipality',function($q1) use($request)
			{
				$departmentSearch = $request->department;
				$q1->whereHas('Department',function($q2)  use ($departmentSearch)
					{
						$q2->where('slug',$departmentSearch);
					});
			})->where('state','Activo')->with('operador')->groupBy('fk_operator')->paginate(10);
		}
		else if($request->region )
		{
			$services = ServiceOperator::whereHas('Municipality',function($q1) use($request)
			{
				$regionsSearch = explode(',', substr($request->region, 0,-1));
				$q1->whereHas('Department',function($q2)  use ($regionsSearch)
					{
						$aux = $regionsSearch;
						$q2->whereHas('Region',function($q3)  use ($aux)
						{
							$q3->whereIn('slug',$aux);
						});
					});
			})->where('state','Activo')->with('operador')->groupBy('fk_operator')->paginate(10);
		}
		else if($request->activity)
		{
			$services = ServiceOperator::whereHas('PrincipalService',function($q4) use ($request)
			{
				$q4->where('slug',explode(',',$request->activity));
			})->where('state','Activo')->with('operador')->groupBy('fk_operator')->paginate(10);
		}
		else
		{
			$services = ServiceOperator::whereHas('PrincipalService',function($q4) 
			{
				$q4->where('state','Activo');
			})->with('operador')->groupBy('fk_operator')->paginate(10);
		}

		//ADAPTACIÓN FINAL A CUALQUIERA QUE SEAN LOS SERVICIOS FINALES
		foreach ($services as $service) 
		{
			if($service->link_image)
			{
				array_add($service,'card_image',$service->link_image);
			}
			else
				array_add($service,'card_image','');
			array_add($service,'municipality',$service->Municipality->municipality_name);
			array_add($service,'region',$service->municipality->PertinentRegion);
		}

		$operatorsOutstanding = Operator::where('status','active')
									->where('outstanding',1)
									->get();

        $cant = (int)round($services['total']/3);

		return response()->json(['regions'=>$regions,
								'services'=>$services,
								'activities'=>$activities,
								'departments'=>$departments,
								'municipalities'=>$municipalities,
								'patterns'=>$finalPattern,
								'cuantity'=>$cant,
								'operatorsOutstanding'=> $operatorsOutstanding,
								'defaultoperator'=>$this->defaultoperator,
								'defaultimage'=>$this->defaultimage,
								'defaultcardimage'=>$this->defaultcardimage,
								'pagination'=>[
									'total'			=>$services['total'],
									'current_page'	=>$services['current_page'],
									'per_page'		=>$services['per_page'],
									'from'			=>$services['from'],
									'last_page'		=>$services['last_page'],
									'to'			=>$services['to'],
									],
								]);
	}

	public function hotel($slug)
	{
		$hotel = Establishment::with('relMunicipality','schedule')->where('slug',$slug)->first();
		$chain = $hotel->relMunicipality;
		$fianlServices = $hotel->Services;
		$schedule = $hotel->schedule;
		$municipality = $hotel->Municipality;
		$images = $hotel->images;
		$hotels = [];
		$restaurants = [];
		$operators = [];
		$InterestSites = [];
		$finalOperators = [];
		$finalPattern = [];

		$rooms = $hotel->Rooms->groupBy('suite');
		$socialNetworks = false;

		if($hotel->facebook!='' || $hotel->twitter!='' || $hotel->instagram!='' || $hotel->website!='')
		{
			$socialNetworks = true;
		}	
		//publicidad
		$page = Page::where('url',"/hotel/".$hotel->slug)->first();
		if(isset($page))
		{	
			$finalPattern = $this->pattern($page);
		}

		if(count($hotels)<=5)
		{
			foreach($municipality->hotels as $newHotel)
			{
				if(count($hotels)<=5 && $newHotel->slug!=$hotel->slug)
				{
					$toAdd = ['name'=>$newHotel->establishment_name,
							'slug'=>$newHotel->slug,
						'municipality'=>$municipality->municipality_name];
					array_push($hotels,$toAdd);
				}
			}
		}
		if(count($restaurants)<=5)
		{
			foreach($municipality->restaurants as $newResturant)
			{
				if(count($restaurants)<=5)
				{
					$toAdd = ['name'=>$newResturant->establishment_name,
							'slug'=>$newResturant->slug,
						'municipality'=>$municipality->municipality_name];
					array_push($restaurants,$toAdd);
				}
				
			}
		}
		if(count($operators)<=5)
		{
			foreach($municipality->Plans as  $service)
			{
				if(!array_key_exists($service->operador->slug,$operators))
				{
					if(count($operators)<=5)
					{
						if(isset($service->operator->slug))
						{
							$operators = array_merge($operators,[$service->operador->slug=>$service->operador->name]);
							$toAdd = ['name'=>$service->operator->name,
								'slug'=>$service->operator->slug,
								'municipality'=>$municipality->municipality_name];
							array_push($finalOperators,$toAdd);
						}
						
					}
				}
			}
		}
		if(count($operators)<=5)
		{
			foreach($municipality->hotels as $hotel)
			{
				$operator = $hotel->Operator;
				if($operator)
				{
					if(!array_key_exists($operator->slug,$operators))
					{
						if(count($operators)<=5)
						{
							$operators = array_merge($operators,[$operator->slug=>$operator->name]);
							$toAdd = ['name'=>$operator->name,
								'slug'=>$operator->slug,
								'municipality'=>$municipality->municipality_name];
							array_push($finalOperators,$toAdd);
						}
					}
				}
			}
		}
		if(count($operators)<=5)
		{
			foreach($municipality->restaurants as $restaurant)
			{
				$operator = $restaurant->Operator;
				if($operator)
				{	
					if(!array_key_exists($operator->slug,$operators))
					{
						if(count($operators)<=5)
						{
							$operators = array_merge($operators,[$operator->slug=>$operator->name]);
							$toAdd = [
								'name'=>$operator->name,
								'slug'=>$operator->slug,
								'municipality'=>$municipality->municipality_name];
							array_push($finalOperators,$toAdd);
						}
					}
				}
			}
		}
		if(count($InterestSites)<=6)
		{
			if($municipality->InterestSite)
			{
				foreach($municipality->InterestSite as $site)
				{
					if(count($InterestSites)<=6)
					{
						$toAdd = ['name'=>$site->site_name,
									'slug'=>$site->slug,
									'link_image'=>$site->link_image];
						array_push($InterestSites,$toAdd);
					}

				}
			}
		}	

		$fairs = Fair::all()->where('state','activo');
		$sitefairs = $municipality->FrontFairs;
		$counts = ['Feb'=>0,
                    'Jan'=>0,
                    'Mar'=>0,
                    'Apr'=>0,
                    'May'=>0,
                    'Jun'=>0,
                    'Jul'=>0,
                    'Aug'=>0,
                    'Sep'=>0,
                    'Oct'=>0,
                    'Nov'=>0,
                    'Dec'=>0,
                ];
        foreach ($fairs as $fair) 
        {
            $monthText = date("M",strtotime($fair->start_date));
            $counts["$monthText"]=$counts["$monthText"]+1 ;
        }

		return response()->json(['hotel'=>$hotel,
								'rooms'=>$rooms,
								'simplerooms'=>$hotel->Rooms,
								'socialNetworks'=>$socialNetworks,
								'patterns'=>$finalPattern,
								'page'=>$page,
    							'counts'=>$counts,
								'defaultImage'=>$this->defaultimage,
								'defaultcardimage'=>$this->defaultcardimage,
								'images' => $images,
								'finalServices'=>$fianlServices,
								'hotels'=>$hotels,
								'restaurants'=>$restaurants,
								'operators'=>$finalOperators,
								'InterestSites'=>$InterestSites,
								'chain'=>$chain,
								'schedule'=>$schedule,
								'sitefairs'=>$sitefairs,
								]);

	}

	public function restaurant($slug)
	{
		$restaurant = Establishment::with('relMunicipality','schedule')->where('slug',$slug)->first();
		$chain = $restaurant->relMunicipality;
		$fianlServices = $restaurant->Services;
		$schedule = $restaurant->schedule;
		$municipality = $restaurant->Municipality;
		$images = $restaurant->images;
		$hotels = [];
		$restaurants = [];
		$operators = [];
		$InterestSites = [];
		$finalOperators = [];
		$finalPattern = [];

		$plates = $restaurant->plates->groupBy('category');

		$socialNetworks = false;

		if($restaurant->facebook!='' || $restaurant->twitter!='' || $restaurant->instagram!='' || $restaurant->website!='')
		{
			$socialNetworks = true;
		}	
		//publicidad
		$page = Page::where('url',"/restaurant/".$restaurant->slug)->first();
		if(isset($page))
		{	
			$finalPattern = $this->pattern($page);
		}

		if(count($hotels)<=5)
		{
			foreach($municipality->hotels as $newHotel)
			{
				if(count($hotels)<=5)
				{
					$toAdd = ['name'=>$newHotel->establishment_name,
							'slug'=>$newHotel->slug,
						'municipality'=>$municipality->municipality_name];
					array_push($hotels,$toAdd);
				}
			}
		}
		if(count($restaurants)<=5)
		{
			foreach($municipality->restaurants as $newResturant)
			{
				if(count($restaurants)<=5 && $newResturant->slug!=$restaurant->slug)
				{
					$toAdd = ['name'=>$newResturant->establishment_name,
							'slug'=>$newResturant->slug,
						'municipality'=>$municipality->municipality_name];
					array_push($restaurants,$toAdd);
				}
				
			}
		}
		if(count($operators)<=5)
		{
			foreach($municipality->Plans as  $service)
			{
				if(!array_key_exists($service->operador->slug,$operators))
				{
					if(count($operators)<=5)
					{
						if(isset($service->operator->slug))
						{
							$operators = array_merge($operators,[$service->operador->slug=>$service->operador->name]);
							$toAdd = ['name'=>$service->operator->name,
								'slug'=>$service->operator->slug,
								'municipality'=>$municipality->municipality_name];
							array_push($finalOperators,$toAdd);
						}
						
					}
				}
			}
		}
		if(count($operators)<=5)
		{
			foreach($municipality->hotels as $hotel)
			{
				$operator = $hotel->Operator;
				if($operator)
				{
					if(!array_key_exists($operator->slug,$operators))
					{
						if(count($operators)<=5)
						{
							$operators = array_merge($operators,[$operator->slug=>$operator->name]);
							$toAdd = ['name'=>$operator->name,
								'slug'=>$operator->slug,
								'municipality'=>$municipality->municipality_name];
							array_push($finalOperators,$toAdd);
						}
					}
				}
			}
		}
		if(count($operators)<=5)
		{
			foreach($municipality->restaurants as $restaurant)
			{
				$operator = $restaurant->Operator;
				if($operator)
				{	
					if(!array_key_exists($operator->slug,$operators))
					{
						if(count($operators)<=5)
						{
							$operators = array_merge($operators,[$operator->slug=>$operator->name]);
							$toAdd = [
								'name'=>$operator->name,
								'slug'=>$operator->slug,
								'municipality'=>$municipality->municipality_name];
							array_push($finalOperators,$toAdd);
						}
					}
				}
			}
		}
		if(count($InterestSites)<=6)
		{
			if($municipality->InterestSite)
			{
				foreach($municipality->InterestSite as $site)
				{
					if(count($InterestSites)<=6)
					{
						$toAdd = ['name'=>$site->site_name,
									'slug'=>$site->slug,
									'link_image'=>$site->link_image];
						array_push($InterestSites,$toAdd);
					}

				}
			}
		}	

		$fairs = Fair::all()->where('state','activo');
		$sitefairs = $municipality->FrontFairs;
		$counts = ['Feb'=>0,
                    'Jan'=>0,
                    'Mar'=>0,
                    'Apr'=>0,
                    'May'=>0,
                    'Jun'=>0,
                    'Jul'=>0,
                    'Aug'=>0,
                    'Sep'=>0,
                    'Oct'=>0,
                    'Nov'=>0,
                    'Dec'=>0,
                ];
        foreach ($fairs as $fair) 
        {
            $monthText = date("M",strtotime($fair->start_date));
            $counts["$monthText"]=$counts["$monthText"]+1 ;
        }

		return response()->json(['restaurant'=>$restaurant,
								'plates'=>$plates,
								'socialNetworks'=>$socialNetworks,
								'patterns'=>$finalPattern,
								'page'=>$page,
    							'counts'=>$counts,
								'defaultImage'=>$this->defaultimage,
								'defaultcardimage'=>$this->defaultcardimage,
								'images' => $images,
								'finalServices'=>$fianlServices,
								'hotels'=>$hotels,
								'restaurants'=>$restaurants,
								'operators'=>$finalOperators,
								'InterestSites'=>$InterestSites,
								'chain'=>$chain,
								'schedule'=>$schedule,
								'sitefairs'=>$sitefairs,
								]);
	}

	public function municipality($slug)
	{
		$municipality = Municipality::where('slug',$slug)->first();
		$department = $municipality->department;
		$images = $municipality->images;
		$hotels = [];
		$restaurants = [];
		$operators = [];
		$InterestSites = [];
		$finalOperators = [];
		$finalPattern = [];

		//publicidad
		$page = Page::where('url',"/municipality/".$municipality->slug)->first();
		if(isset($page))
		{	
			$finalPattern = $this->pattern($page);
		}

		if(count($hotels)<=5)
		{
			foreach($municipality->hotels as $newHotel)
			{
				if(count($hotels)<=5)
				{
					$toAdd = ['name'=>$newHotel->establishment_name,
							'slug'=>$newHotel->slug,
						'municipality'=>$municipality->municipality_name];
					array_push($hotels,$toAdd);
				}
			}
		}
		if(count($restaurants)<=5)
		{
			foreach($municipality->restaurants as $newResturant)
			{
				if(count($restaurants)<=5)
				{
					$toAdd = ['name'=>$newResturant->establishment_name,
							'slug'=>$newResturant->slug,
						'municipality'=>$municipality->municipality_name];
					array_push($restaurants,$toAdd);
				}
				
			}
		}
		if(count($operators)<=5)
		{
			foreach($municipality->Plans as  $service)
			{
				if(!array_key_exists($service->operador->slug,$operators))
				{
					if(count($operators)<=5)
					{
						if(isset($service->operator->slug))
						{
							$operators = array_merge($operators,[$service->operador->slug=>$service->operador->name]);
							$toAdd = ['name'=>$service->operator->name,
								'slug'=>$service->operator->slug,
								'municipality'=>$municipality->municipality_name];
							array_push($finalOperators,$toAdd);
						}
						
					}
				}
			}
		}
		if(count($operators)<=5)
		{
			foreach($municipality->hotels as $hotel)
			{
				$operator = $hotel->Operator;
				if($operator)
				{
					if(!array_key_exists($operator->slug,$operators))
					{
						if(count($operators)<=5)
						{
							$operators = array_merge($operators,[$operator->slug=>$operator->name]);
							$toAdd = ['name'=>$operator->name,
								'slug'=>$operator->slug,
								'municipality'=>$municipality->municipality_name];
							array_push($finalOperators,$toAdd);
						}
					}
				}
			}
		}
		if(count($operators)<=5)
		{
			foreach($municipality->restaurants as $restaurant)
			{
				$operator = $restaurant->Operator;
				if($operator)
				{	
					if(!array_key_exists($operator->slug,$operators))
					{
						if(count($operators)<=5)
						{
							$operators = array_merge($operators,[$operator->slug=>$operator->name]);
							$toAdd = [
								'name'=>$operator->name,
								'slug'=>$operator->slug,
								'municipality'=>$municipality->municipality_name];
							array_push($finalOperators,$toAdd);
						}
					}
				}
			}
		}
		if(count($InterestSites)<=6)
		{
			if($municipality->InterestSite)
			{
				foreach($municipality->InterestSite as $site)
				{
					if(count($InterestSites)<=6)
					{
						$toAdd = ['name'=>$site->site_name,
									'slug'=>$site->slug,
									'link_image'=>$site->link_image];
						array_push($InterestSites,$toAdd);
					}

				}
			}
		}	

		$fairs = Fair::all()->where('state','activo');
		$sitefairs = $municipality->FrontFairs;
		$counts = ['Feb'=>0,
                    'Jan'=>0,
                    'Mar'=>0,
                    'Apr'=>0,
                    'May'=>0,
                    'Jun'=>0,
                    'Jul'=>0,
                    'Aug'=>0,
                    'Sep'=>0,
                    'Oct'=>0,
                    'Nov'=>0,
                    'Dec'=>0,
                ];
        foreach ($fairs as $fair) 
        {
            $monthText = date("M",strtotime($fair->start_date));
            $counts["$monthText"]=$counts["$monthText"]+1 ;
        }

        if($municipality->img_360)
        {
        	$municipality->img_360 = url('/').'/'.$municipality->img_360;
        }

        $servicesNames = [];
		foreach($municipality->Plans->groupBy('fk_service') as $service => $services)
		{
			$serviceFind = Service::find($service);
			$servicesNames[] = ['name'=>$serviceFind->service, 'slug'=>$serviceFind->slug];
		}

		$interestSitesMunicipality = $municipality->frontInterestSite;


		return response()->json(['municipality'=>$municipality,
								'patterns'=>$finalPattern,
								'page'=>$page,
    							'counts'=>$counts,
								'defaultImage'=>$this->defaultimage,
								'defaultcardimage'=>$this->defaultcardimage,
								'images' => $images,
								'hotels'=>$hotels,
								'servicesNames'=>$servicesNames,
								'restaurants'=>$restaurants,
								'operators'=>$finalOperators,
								'InterestSites'=>$InterestSites,
								'interestSitesMunicipality'=>$interestSitesMunicipality,
								'keydata'=>$municipality->KeyData->groupBy('category'),
								'cant_keydata'=>$municipality->KeyData->count(),
								'sitefairs'=>$sitefairs,
								]);
	}

	public function operator($slug)
	{
		$thisoperator = Operator::where('slug',$slug)->first();
		$services= $thisoperator->directService;
		$hotels= $thisoperator->hotels;
		$restaurants= $thisoperator->restaurants;
		$images =[];
		$finalhotels = [];
		$finalrestaurants = [];
		$operators = [];
		$InterestSites = [];
		$finalOperators = [];
		$finalPattern = [];
		$municipality = [];
		$sitefairs = [];

		if($restaurants->count()>0)
		{
			$establishment = $restaurants->first();
			array_add($thisoperator,'municipality_name',$establishment->municipality->municipality_name);
			array_add($thisoperator,'municipality_slug',$establishment->municipality->slug);
			$municipality = $establishment->municipality;
			if($establishment->card_image)
			{
				array_add($thisoperator,'card_image',$establishment->card_image);
			}
			if($establishment->introduction_image)
			{
				array_add($thisoperator,'introduction_image',$establishment->introduction_image);
			}

		}
		elseif($hotels->count()>0)
		{
			$establishment = $hotels->first();
			array_add($thisoperator,'municipality_name',$establishment->municipality->municipality_name);
			array_add($thisoperator,'municipality_slug',$establishment->municipality->slug);
			$municipality = $establishment->municipality;
			if($establishment->card_image)
			{
				array_add($thisoperator,'card_image',$establishment->card_image);
			}
			if($establishment->introduction_image)
			{
				array_add($thisoperator,'introduction_image',$establishment->introduction_image);
			}
		}
		elseif($services->count()>0)
		{
			$service = $services->first();
			array_add($thisoperator,'municipality_name',$service->municipality->municipality_name);
			array_add($thisoperator,'municipality_slug',$service->municipality->slug);
			$municipality = $service->municipality;
			if(count($service->images)>0)
			{
				array_add($thisoperator,'card_image',$service->images->first()->link_image);
			}
			array_add($thisoperator,'introduction_image','');
		}
		else
		{
			array_add($thisoperator,'municipality_name','');
			array_add($thisoperator,'municipality_slug','');
			array_add($thisoperator,'card_image','');
			array_add($thisoperator,'introduction_image','');
		}

		if($services->count()>0)
		{
			foreach($services as $service)
			{
				if($service->images->count()>0)
				{
					$images = array_merge($images,$service->images->toArray());
				}
			}
		}
		if($hotels->count()>0)
		{
			foreach($hotels as $hotel)
			{
				if($hotel->images->count()>0)
				{
					$images = array_merge($images,$hotel->images->toArray());
				}
			}
		}

		if($restaurants->count()>0)
		{
			foreach($restaurants as $restaurant)
			{
				if($restaurant->images->count()>0)
				{
					$images = array_merge($images,$restaurant->images->toArray());
				}
			}
		}

		$page = Page::where('url',"/operator/".$slug)->first();
		if(isset($page))
		{	
			$finalPattern = $this->pattern($page);
		}

		if($municipality)
		{
			//publicidad
			if(count($finalhotels)<=5)
			{
				foreach($municipality->hotels as $newHotel)
				{
					if(count($finalhotels)<=5)
					{
						$toAdd = ['name'=>$newHotel->establishment_name,
								'slug'=>$newHotel->slug,
							'municipality'=>$municipality->municipality_name];
						array_push($finalhotels,$toAdd);
					}
				}
			}
			if(count($finalrestaurants)<=5)
			{
				foreach($municipality->restaurants as $newResturant)
				{
					if(count($finalrestaurants)<=5)
					{
						$toAdd = ['name'=>$newResturant->establishment_name,
								'slug'=>$newResturant->slug,
							'municipality'=>$municipality->municipality_name];
						array_push($finalrestaurants,$toAdd);
					}
					
				}
			}
			if(count($operators)<=5)
			{
				foreach($municipality->Plans as  $service)
				{
					if(!array_key_exists($service->operador->slug,$operators))
					{
						if(count($operators)<=5)
						{
							if(isset($service->operator->slug))
							{
								$operators = array_merge($operators,[$service->operador->slug=>$service->operador->name]);
								$toAdd = ['name'=>$service->operator->name,
									'slug'=>$service->operator->slug,
									'municipality'=>$municipality->municipality_name];
								array_push($finalOperators,$toAdd);
							}
							
						}
					}
				}
			}
			if(count($operators)<=5)
			{
				foreach($municipality->hotels as $hotel)
				{
					$operator = $hotel->Operator;
					if($operator)
					{
						if(!array_key_exists($operator->slug,$operators))
						{
							if(count($operators)<=5)
							{
								$operators = array_merge($operators,[$operator->slug=>$operator->name]);
								$toAdd = ['name'=>$operator->name,
									'slug'=>$operator->slug,
									'municipality'=>$municipality->municipality_name];
								array_push($finalOperators,$toAdd);
							}
						}
					}
				}
			}
			if(count($operators)<=5)
			{
				foreach($municipality->restaurants as $restaurant)
				{
					$operator = $restaurant->Operator;
					if($operator)
					{	
						if(!array_key_exists($operator->slug,$operators))
						{
							if(count($operators)<=5)
							{
								$operators = array_merge($operators,[$operator->slug=>$operator->name]);
								$toAdd = [
									'name'=>$operator->name,
									'slug'=>$operator->slug,
									'municipality'=>$municipality->municipality_name];
								array_push($finalOperators,$toAdd);
							}
						}
					}
				}
			}
			if(count($InterestSites)<=6)
			{
				if($municipality->InterestSite)
				{
					foreach($municipality->InterestSite as $site)
					{
						if(count($InterestSites)<=6)
						{
							$toAdd = ['name'=>$site->site_name,
										'slug'=>$site->slug,
										'link_image'=>$site->link_image];
							array_push($InterestSites,$toAdd);
						}

					}
				}
			}	

			$sitefairs = $municipality->FrontFairs;
		}//End if municipality validation

		$fairs = Fair::all()->where('state','activo');
		$counts = ['Feb'=>0,
                    'Jan'=>0,
                    'Mar'=>0,
                    'Apr'=>0,
                    'May'=>0,
                    'Jun'=>0,
                    'Jul'=>0,
                    'Aug'=>0,
                    'Sep'=>0,
                    'Oct'=>0,
                    'Nov'=>0,
                    'Dec'=>0,
                ];
        foreach ($fairs as $fair) 
        {
            $monthText = date("M",strtotime($fair->start_date));
            $counts["$monthText"]=$counts["$monthText"]+1 ;
        }


        $operatorServices = $thisoperator->groupservices;
        $rel = $municipality->relDepartment;

        return response()->json(['operator'=>$thisoperator,
    							'images'=>$images,
    							'municipality'=>$municipality,
    							'patterns'=>$finalPattern,
								'page'=>$page,
    							'counts'=>$counts,
								'defaultImage'=>$this->defaultimage,
								'defaultcardimage'=>$this->defaultcardimage,
								'hotels'=>$finalhotels,
								'department'=>$rel,
								//'operatorServices'=>$operatorServices,
								'restaurants'=>$finalrestaurants,
								'operators'=>$finalOperators,
								'InterestSites'=>$InterestSites,
								'sitefairs'=>$sitefairs,

    						]);
	}

	public function interestSite($slug)
	{
		$finalsite =InterestSite::where('slug',$slug)->first();
		$municipality = $finalsite->municipality;
		$department = $municipality->department;
		$images = $finalsite->images;
		$hotels = [];
		$restaurants = [];
		$operators = [];
		$InterestSites = [];
		$finalOperators = [];
		$finalPattern = [];

		//publicidad
		$page = Page::where('url',"/interestsite/".$finalsite->slug)->first();
		if(isset($page))
		{	
			$finalPattern = $this->pattern($page);
		}

		if(count($hotels)<=5)
		{
			foreach($municipality->hotels as $newHotel)
			{
				if(count($hotels)<=5)
				{
					$toAdd = ['name'=>$newHotel->establishment_name,
							'slug'=>$newHotel->slug,
						'municipality'=>$municipality->municipality_name];
					array_push($hotels,$toAdd);
				}
			}
		}
		if(count($restaurants)<=5)
		{
			foreach($municipality->restaurants as $newResturant)
			{
				if(count($restaurants)<=5)
				{
					$toAdd = ['name'=>$newResturant->establishment_name,
							'slug'=>$newResturant->slug,
						'municipality'=>$municipality->municipality_name];
					array_push($restaurants,$toAdd);
				}
				
			}
		}
		if(count($operators)<=5)
		{
			foreach($municipality->Plans as  $service)
			{
				if(!array_key_exists($service->operador->slug,$operators))
				{
					if(count($operators)<=5)
					{
						if(isset($service->operator->slug))
						{
							$operators = array_merge($operators,[$service->operador->slug=>$service->operador->name]);
							$toAdd = ['name'=>$service->operator->name,
								'slug'=>$service->operator->slug,
								'municipality'=>$municipality->municipality_name];
							array_push($finalOperators,$toAdd);
						}
						
					}
				}
			}
		}
		if(count($operators)<=5)
		{
			foreach($municipality->hotels as $hotel)
			{
				$operator = $hotel->Operator;
				if($operator)
				{
					if(!array_key_exists($operator->slug,$operators))
					{
						if(count($operators)<=5)
						{
							$operators = array_merge($operators,[$operator->slug=>$operator->name]);
							$toAdd = ['name'=>$operator->name,
								'slug'=>$operator->slug,
								'municipality'=>$municipality->municipality_name];
							array_push($finalOperators,$toAdd);
						}
					}
				}
			}
		}
		if(count($operators)<=5)
		{
			foreach($municipality->restaurants as $restaurant)
			{
				$operator = $restaurant->Operator;
				if($operator)
				{	
					if(!array_key_exists($operator->slug,$operators))
					{
						if(count($operators)<=5)
						{
							$operators = array_merge($operators,[$operator->slug=>$operator->name]);
							$toAdd = [
								'name'=>$operator->name,
								'slug'=>$operator->slug,
								'municipality'=>$municipality->municipality_name];
							array_push($finalOperators,$toAdd);
						}
					}
				}
			}
		}
		if(count($InterestSites)<=6)
		{
			if($municipality->InterestSite)
			{
				foreach($municipality->InterestSite as $site)
				{
					if(count($InterestSites)<=6)
					{
						$toAdd = ['name'=>$site->site_name,
									'slug'=>$site->slug,
									'link_image'=>$site->link_image];
						array_push($InterestSites,$toAdd);
					}

				}
			}
		}	

		$fairs = Fair::all()->where('state','activo');
		$sitefairs = $municipality->FrontFairs;
		$counts = ['Feb'=>0,
                    'Jan'=>0,
                    'Mar'=>0,
                    'Apr'=>0,
                    'May'=>0,
                    'Jun'=>0,
                    'Jul'=>0,
                    'Aug'=>0,
                    'Sep'=>0,
                    'Oct'=>0,
                    'Nov'=>0,
                    'Dec'=>0,
                ];
        foreach ($fairs as $fair) 
        {
            $monthText = date("M",strtotime($fair->start_date));
            $counts["$monthText"]=$counts["$monthText"]+1 ;
        }

        if($finalsite->img_360)
        {
        	$finalsite->img_360 = url('/').'/'.$finalsite->img_360;
        }

        $servicesNames = [];
		foreach($municipality->Plans->groupBy('fk_service') as $service => $services)
		{
			$serviceFind = Service::find($service);
			$servicesNames[] = ['name'=>$serviceFind->service, 'slug'=>$serviceFind->slug];
		}

		$interestSitesMunicipality = $municipality->frontInterestSite;


		return response()->json(['site'=>$finalsite,
								'municipality'=>$municipality,
								'department'=>$municipality->department,
								'patterns'=>$finalPattern,
								'page'=>$page,
    							'counts'=>$counts,
								'defaultImage'=>$this->defaultimage,
								'defaultcardimage'=>$this->defaultcardimage,
								'images' => $images,
								'hotels'=>$hotels,
								'servicesNames'=>$servicesNames,
								'restaurants'=>$restaurants,
								'operators'=>$finalOperators,
								'InterestSites'=>$InterestSites,
								'interestSitesMunicipality'=>$interestSitesMunicipality,
								'keydata'=>$site->KeyData->groupBy('category'),
								'cant_keydata'=>$site->KeyData->count(),
								'sitefairs'=>$sitefairs,
								]);
	}

	public function fair($slug)
	{
		$thisfair = Fair::with('PDF','Video')->where('slug',$slug)->first();
		$municipality = $thisfair->PrincipalMunicipality;
		$department = $municipality->department;
		$images = $thisfair->images;
		$hotels = [];
		$restaurants = [];
		$operators = [];
		$InterestSites = [];
		$finalOperators = [];
		$finalPattern = [];

		//publicidad
		$page = Page::where('url',"/fair/".$slug)->first();
		if(isset($page))
		{	
			$finalPattern = $this->pattern($page);
		}

		if(count($hotels)<=5)
		{
			foreach($municipality->hotels as $newHotel)
			{
				if(count($hotels)<=5)
				{
					$toAdd = ['name'=>$newHotel->establishment_name,
							'slug'=>$newHotel->slug,
						'municipality'=>$municipality->municipality_name];
					array_push($hotels,$toAdd);
				}
			}
		}
		if(count($restaurants)<=5)
		{
			foreach($municipality->restaurants as $newResturant)
			{
				if(count($restaurants)<=5)
				{
					$toAdd = ['name'=>$newResturant->establishment_name,
							'slug'=>$newResturant->slug,
						'municipality'=>$municipality->municipality_name];
					array_push($restaurants,$toAdd);
				}
				
			}
		}
		if(count($operators)<=5)
		{
			foreach($municipality->Plans as  $service)
			{
				if(!array_key_exists($service->operador->slug,$operators))
				{
					if(count($operators)<=5)
					{
						if(isset($service->operator->slug))
						{
							$operators = array_merge($operators,[$service->operador->slug=>$service->operador->name]);
							$toAdd = ['name'=>$service->operator->name,
								'slug'=>$service->operator->slug,
								'municipality'=>$municipality->municipality_name];
							array_push($finalOperators,$toAdd);
						}
						
					}
				}
			}
		}
		if(count($operators)<=5)
		{
			foreach($municipality->hotels as $hotel)
			{
				$operator = $hotel->Operator;
				if($operator)
				{
					if(!array_key_exists($operator->slug,$operators))
					{
						if(count($operators)<=5)
						{
							$operators = array_merge($operators,[$operator->slug=>$operator->name]);
							$toAdd = ['name'=>$operator->name,
								'slug'=>$operator->slug,
								'municipality'=>$municipality->municipality_name];
							array_push($finalOperators,$toAdd);
						}
					}
				}
			}
		}
		if(count($operators)<=5)
		{
			foreach($municipality->restaurants as $restaurant)
			{
				$operator = $restaurant->Operator;
				if($operator)
				{	
					if(!array_key_exists($operator->slug,$operators))
					{
						if(count($operators)<=5)
						{
							$operators = array_merge($operators,[$operator->slug=>$operator->name]);
							$toAdd = [
								'name'=>$operator->name,
								'slug'=>$operator->slug,
								'municipality'=>$municipality->municipality_name];
							array_push($finalOperators,$toAdd);
						}
					}
				}
			}
		}
		if(count($InterestSites)<=6)
		{
			if($municipality->InterestSite)
			{
				foreach($municipality->InterestSite as $site)
				{
					if(count($InterestSites)<=6)
					{
						$toAdd = ['name'=>$site->site_name,
									'slug'=>$site->slug,
									'link_image'=>$site->link_image];
						array_push($InterestSites,$toAdd);
					}

				}
			}
		}	

		$fairs = Fair::all()->where('state','activo');
		$sitefairs = $municipality->FrontFairs;
		$counts = ['Feb'=>0,
                    'Jan'=>0,
                    'Mar'=>0,
                    'Apr'=>0,
                    'May'=>0,
                    'Jun'=>0,
                    'Jul'=>0,
                    'Aug'=>0,
                    'Sep'=>0,
                    'Oct'=>0,
                    'Nov'=>0,
                    'Dec'=>0,
                ];
        foreach ($fairs as $fair) 
        {
            $monthText = date("M",strtotime($fair->start_date));
            $counts["$monthText"]=$counts["$monthText"]+1 ;
        }

		return response()->json(['fair'=>$thisfair,
								'patterns'=>$finalPattern,
								'page'=>$page,
								'municipality'=>$municipality,
    							'counts'=>$counts,
								'defaultImage'=>$this->defaultimage,
								'defaultcardimage'=>$this->defaultcardimage,
								'images' => $images,
								'hotels'=>$hotels,
								'restaurants'=>$restaurants,
								'operators'=>$finalOperators,
								'InterestSites'=>$InterestSites,
								'department'=>$department,
								'sitefairs'=>$sitefairs,
								]);
	}

	public function department($slug)
	{
		$finalPattern = [];

		//publicidad
		$page = Page::where('url',"/department/".$slug)->first();
		if(isset($page))
		{	
			$finalPattern = $this->pattern($page);
		}

		$department = Department::where('slug',$slug)->first();
		$region = $department->region;
		$outstanding = $department->municipalities->where('outstanding',1);
		$hotelsOutstanding = $department->HotelLimit;
		$restaurantsOutstanding = $department->RestaurantLimit;
		$municipalitiesToMap = $department->Municipalities;
		$municipalities = $department->NineMunicipalitiesFront;

		$circuits = Circuit::where('state','activo')->inRandomOrder(8)->get();

		$images = $department->images;
		$hotels = [];
		$restaurants = [];
		$operators = [];
		$InterestSites = [];
		$finalOperators = [];
		foreach($municipalitiesToMap as $municipality)
		{
			if(count($hotels)<=5)
			{
				foreach($municipality->hotels as $newHotel)
				{
					if(count($hotels)<=5)
					{
						$toAdd = ['name'=>$newHotel->establishment_name,
								'slug'=>$newHotel->slug,
							'municipality'=>$municipality->municipality_name];
						array_push($hotels,$toAdd);
					}
				}
			}
			if(count($restaurants)<=5)
			{
				foreach($municipality->restaurants as $newResturant)
				{
					if(count($restaurants)<=5)
					{
						$toAdd = ['name'=>$newResturant->establishment_name,
								'slug'=>$newResturant->slug,
							'municipality'=>$municipality->municipality_name];
						array_push($restaurants,$toAdd);
					}
					
				}
			}
			if(count($operators)<=5)
			{
				foreach($municipality->Plans as  $service)
				{
					if(!array_key_exists($service->operador->slug,$operators))
					{
						if(count($operators)<=5)
						{
							if(isset($service->operator->slug))
							{
								$operators = array_merge($operators,[$service->operador->slug=>$service->operador->name]);
								$toAdd = ['name'=>$service->operator->name,
									'slug'=>$service->operator->slug,
									'municipality'=>$municipality->municipality_name];
								array_push($finalOperators,$toAdd);
							}
							
						}
					}
				}
			}

			if(count($operators)<=5)
			{
				foreach($municipality->hotels as $hotel)
				{
					$operator = $hotel->Operator;
					if($operator)
					{
						if(!array_key_exists($operator->slug,$operators))
						{
							if(count($operators)<=5)
							{
								$operators = array_merge($operators,[$operator->slug=>$operator->name]);
								$toAdd = ['name'=>$operator->name,
									'slug'=>$operator->slug,
									'municipality'=>$municipality->municipality_name];
								array_push($finalOperators,$toAdd);
							}
						}
					}
				}
			}
			if(count($operators)<=5)
			{
				foreach($municipality->restaurants as $restaurant)
				{
					$operator = $restaurant->Operator;
					if($operator)
					{	
						if(!array_key_exists($operator->slug,$operators))
						{
							if(count($operators)<=5)
							{
								$operators = array_merge($operators,[$operator->slug=>$operator->name]);
								$toAdd = [
									'name'=>$operator->name,
									'slug'=>$operator->slug,
									'municipality'=>$municipality->municipality_name];
								array_push($finalOperators,$toAdd);
							}
						}
					}
				}
			}
			if(count($InterestSites)<=6)
			{
				if($municipality->InterestSite)
				{
					foreach($municipality->InterestSite as $site)
					{
						if(count($InterestSites)<=6)
						{
							$toAdd = ['name'=>$site->site_name,
										'slug'=>$site->slug,
										'link_image'=>$site->link_image];
							array_push($InterestSites,$toAdd);
						}

					}
				}
			}
		}



        return response()->json(['department'=>$department,
        						'region'=>$region,
        						'circuits'=>$circuits,
								'patterns'=>$finalPattern,
								'page'=>$page,
								'municipalitiesToMap'=>$municipalitiesToMap,
								'municipalities'=>$municipalities,
								'outstanding'=>$outstanding,
								'defaultImage'=>$this->defaultimage,
								'defaultcardimage'=>$this->defaultcardimage,
								'images' => $images,
								'hotels'=>$hotels,
								'restaurants'=>$restaurants,
								'operators'=>$finalOperators,
								'InterestSites'=>$InterestSites,
								'hotelsOutstanding'=>$hotelsOutstanding,
								'restaurantsOutstanding'=>$restaurantsOutstanding,
								'defaulthotel'=>$this->defaulthotel,
								'defaultrestaurant'=>$this->defaultrestaurant,
								]);

	}

	public function home()
	{
		$wallpapers = Wallpaper::all()->where('state','activo');
		$page = Page::where('url','/')->first();
		$finalPattern=[];
		if(isset($page))
		{	
			$finalPattern = $this->pattern($page);
		}
		$costaCaribe = Region::where('slug','costa-caribe')->first();
		$antioquiaYEjeCafetero = Region::where('slug','antioquia-y-eje-cafetero')->first();
		$costaPacifica = Region::where('slug','pacifico')->first();
		$centro = Region::where('slug','centro')->first();
		$llanosOrientales = Region::where('slug','llanos-orientales')->first();
		$suroccidente = Region::where('slug','suroccidente')->first();
		$amazonia = Region::where('slug','amazonia')->first();

		$municipalities = Municipality::where('state','activo')->inRandomOrder()->limit(6)->get();

		$whattodo = Activity::where('state','activo')->get();

		$randomActivity = array_random($whattodo->toArray());
		$activity = Activity::find($randomActivity['id_activity']);

		$activitiesfilter =  Service::whereHas('ActiveServices', function ($query) 
		{
			$query->where('id_service','!=',0);

		})->get();

		$allsites = [];
		if(count($activity->sevenSites)>0)
		{
			foreach($activity->sevenSites as $site)
			{
				if($site->type_relation == 'municipality')
				{
					$thissite = Municipality::find($site->fk_relation);
					array_add($thissite,'type','municipality');
					if($thissite->link_image)
					{
						array_add($site,'image_link',$thissite->link_image);
					}
					else
						array_add($thissite,'image_link','img/defaultbiglist.png');
					
					array_add($site,'municipality',$thissite);
					array_add($site,'department',$thissite->department);
					array_add($site,'region',$thissite->PertinentRegion);


					$allsites[] = $site;
				}
				else
				{
					$thissite = InterestSite::find($site->fk_relation);
					array_add($thissite,'type','site');
					if($thissite->images)
					{
						array_add($site,'image_link',$thissite->link_image);
					}
					else
						array_add($thissite,'image_link','img/defaultbiglist.png');

					array_add($site,'site',$thissite);
					array_add($site,'department',$thissite->PertinentDepartment);
					array_add($site,'region',$thissite->PertinentRegion);
					$allsites[] = $site;
				}
			}
			array_add($allsites[array_rand($allsites)],'class',true);
			array_add($allsites[array_rand($allsites)],'class',true);
		}


		$fairs = Fair::all()->where('state','activo');
		$counts = ['Feb'=>0,
                    'Jan'=>0,
                    'Mar'=>0,
                    'Apr'=>0,
                    'May'=>0,
                    'Jun'=>0,
                    'Jul'=>0,
                    'Aug'=>0,
                    'Sep'=>0,
                    'Oct'=>0,
                    'Nov'=>0,
                    'Dec'=>0,
                ];

        $specials = Special::where('state','activo')->get();
        foreach ($fairs as $fair) 
        {
            $monthText = date("M",strtotime($fair->start_date));
            $counts["$monthText"]=$counts["$monthText"]+1 ;
        }

		$sitefairs = Fair::with('PrincipalMunicipality')->inRandomOrder()->limit(2)->get();

		return response()->json(['wallpapers'=>$wallpapers,
				'patterns'=>$finalPattern,
				'costaCaribe'=>$costaCaribe,
				'antioquiaYEjeCafetero'=>$antioquiaYEjeCafetero,
				'costaPacifica'=>$costaPacifica,
				'centro'=>$centro,
				'llanosOrientales'=>$llanosOrientales,
				'suroccidente'=>$suroccidente,
				'amazonia'=>$amazonia,
				'municipalities'=>$municipalities,
				'whattodo'=>$whattodo,
				'sites'=>$allsites,
				'sitefairs'=>$sitefairs,
				'counts'=>$counts,
				'activitiesfilter'=>$activitiesfilter,
				'specials'=>$specials,
				'defaultImage'=>$this->defaultimage,
			]);

	}

	public function sumClick($id)
	{
		$pattern = PatternPart::findOrfail($id);
		if($pattern->clicks =="" || $pattern->clicks==null)
		{
			$pattern->clicks=0;
			$pattern->save();
		}
		$pattern->clicks++;
		$pattern->save();
	}

	private function pattern(Page $page)
	{
		$parts = $page->Parts;
		$finalPattern = [];
		foreach($parts as $part)
		{
			$partPage  = PagePartPage::find($part->pivot->id_part_page);
			if(count($partPage->Pattern->where('state','activo'))>0)
			{	
				foreach($partPage->Pattern->where('state','activo') as $pattern)
				{
					if($pattern->pivot->state!='' && $pattern->pivot->state=='activo')
					{
						$finalPattern[$part->slug][] = $pattern;
					}
				}
			}
		}
		return $finalPattern;
	}
}
