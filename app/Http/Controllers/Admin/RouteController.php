<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ImageRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Route;
use App\Models\Department;
use App\Models\Municipality;
use App\Models\RelationRoute;
use App\Models\InterestSiteCategory;
use App\Models\Activity;
use App\Models\InterestSite;
use App\Models\RealRoutePoint;
use App\Models\RouteImage;
use Session;
use Validator;
use File;


class RouteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $routes = Route::all();
        return view('admin.Route.listRoute',compact('routes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.Route.newEditRoute');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
             'name'=>'required',
             'sentence'  => 'required',
             'keywords'  => 'required',
             'description'  => 'required',
             'recommendation'  => 'required',
             'images'  => 'required'
            ])->validate(); 

        $route = new Route;
        $route->name = $request->name;
        $route->sentence = $request->sentence;
        $route->description = $request->description;
        $route->keywords = $request->keywords;
        $route->description = $request->description;
        $route->images = $request->images;
        if($request->card_image)
        {
            $files = $request->file('card_image');

            $card_image = cargar_imagen($files,'route');

            $route->card_image = $card_image;
        }
        if($request->introduction_image)
        {
            $files = $request->file('introduction_image');

            $introduction_image = cargar_imagen($files,'route');

            $route->introduction_image = $introduction_image;
        }
        if($request->description_image)
        {
            $files = $request->file('description_image');
            $description_image = cargar_imagen($files,'route');
            $route->description_image = $description_image;
        }
        if($request->recommendation_image)
        {
            $files = $request->file('recommendation_image');
            $recommendation_image = cargar_imagen($files,'route');
            $route->recommendation_image = $recommendation_image;
        }
        if($request->file_map)
        {
            $files = $request->file('file_map');
            $file_map = cargar_imagen($files,'route');
            $route->file_map = $file_map;
        }
        
        $route->save();
        Session::flash('message', 'Se ha realizado correctamente el registro de información.');
        return redirect('admin/routes');
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $route = Route::where('slug',$slug)->first();
        return view('admin.Route.newEditRoute',compact('route','municipalities','departments','sites'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $validator = \Validator::make($request->all(), [
             'name'=>'required',
             'sentence'  => 'required',
             'keywords'  => 'required',
             'description'  => 'required',
             'recommendation'  => 'required',
             'images'  => 'required'
        ])->validate(); 

        $route = Route::find($id);
        $route->name = $request->name;
        $route->sentence = $request->sentence;
        $route->description = $request->description;
        $route->keywords = $request->keywords;
        $route->description = $request->description;
        $route->images = $request->images;
        if($request->card_image)
        {
            $files = $request->file('card_image');

            $card_image = cargar_imagen($files,'route',$route->card_image);

            $route->card_image = $card_image;
        }
        if($request->introduction_image)
        {
            $files = $request->file('introduction_image');

            $introduction_image = cargar_imagen($files,'route',$route->introduction_image);

            $route->introduction_image = $introduction_image;
        }
        if($request->description_image)
        {
            $files = $request->file('description_image');
            $description_image = cargar_imagen($files,'route',$route->description_image);
            $route->description_image = $description_image;
        }
        if($request->recommendation_image)
        {
            $files = $request->file('recommendation_image');
            $recommendation_image = cargar_imagen($files,'route',$route->recommendation_image);
            $route->recommendation_image = $recommendation_image;
        }
        if($request->file_map)
        {
            $files = $request->file('file_map');
            $file_map = cargar_imagen($files,'route',$route->file_map);
            $route->file_map = $file_map;
        }
        
        $route->save();
        Session::flash('message', 'Se ha realizado correctamente el registro de información.');
        return redirect('admin/routes');
    }

    public function listPoints($slug)
    {
        $route = Route::where('slug',$slug)->first();
        if($route)
        {
            return view('admin.Route.listPoint',compact('route'));
        }
        else
        {
            Session::flash('message-error','No se ha encontrado la ruta.');
            return redirect()->back();
        }
    }

    public function createPoints($slug)
    {
        $route = Route::where('slug',$slug)->first();
        $departments = Department::where('state','activo')->pluck('department_name','id_department')->prepend('');
        $categories = InterestSiteCategory::where('state','activo')->pluck('category_name','id_category')->prepend(['Selecciona'=>null]);
        $activities = Activity::where('state','activo')->pluck('activity_name', 'id_activity');
        $municipalities = Municipality::where('state','activo')->pluck('municipality_name','id_municipality');
        return view('admin.Route.newEditPoint',compact('route','departments','categories','activities','municipalities'));
    }

    public function storePoint(Request $request, $slug)
    {
        Validator::make($request->all(),[
                'title'=>'required',
                'kilometer'=>'required',
                'fk_municipality'=>'required',
                'description'=>'required',
                'type_relation'=>'required',
        ])->validate();
        $route = Route::where('slug',$slug)->first();
        $point = new RealRoutePoint;
        $point->title = $request->title;
        $point->description = $request->description;
        $point->kilometer = $request->kilometer;
        $point->type_relation = $request->type_relation;
        if($request->type_relation == 'municipality')
            $point->fk_relation = $request->fk_municipality;
        else
            $point->fk_relation = $request->fk_site;
        $point->fk_route = $route->id_route;
        $point->save();

        Session::flash('message',"Se ha creado el punto para la ruta.");
        return redirect("/admin/routes/points/$route->slug");
    }

    public function editPoint($id)
    {
        $point = RealRoutePoint::find($id);
        $route = $point->route;
        $departments = Department::where('state','activo')->pluck('department_name','id_department')->prepend('');
        $categories = InterestSiteCategory::where('state','activo')->pluck('category_name','id_category')->prepend(['Selecciona'=>null]);
        $activities = Activity::where('state','activo')->pluck('activity_name', 'id_activity');

        if($point->type_relation=='municipality')
        {
            $municipalityStart = Municipality::find($point->fk_relation);
            $departmentStart = $municipalityStart->department;
            $municipalitiesStart = $departmentStart->municipalities->pluck('municipality_name','id_municipality');
            $sitesStart = $municipalityStart->InterestSite->pluck('site_name','id_site');
        }
        else
        {
            $siteStart = InterestSite::find($point->fk_relation);
            $municipalityStart = $siteStart->municipality;
            $departmentStart = $municipalityStart->department;
            $municipalitiesStart = $departmentStart->municipalities->pluck('municipality_name','id_municipality');
            $sitesStart = $municipalityStart->InterestSite->pluck('site_name','id_site');
        }

        $municipalities = Municipality::where('state','activo')->pluck('municipality_name','id_municipality');

        return view('admin.Route.newEditPoint',compact('point','route','departments','categories','activities','municipalities','municipalityStart','departmentStart','municipalitiesStart','sitesStart'));
    }

    public function updatePoint(Request $request,$id)
    {
        Validator::make($request->all(),[
                'title'=>'required',
                'kilometer'=>'required',
                'fk_municipality'=>'required',
                'description'=>'required',
                'type_relation'=>'required',
        ])->validate();

        $point = RealRoutePoint::find($id);
        $point->title = $request->title;
        $point->description = $request->description;
        $point->kilometer = $request->kilometer;
        $point->type_relation = $request->type_relation;
        if($request->type_relation == 'municipality')
            $point->fk_relation = $request->fk_municipality;
        else
            $point->fk_relation = $request->fk_site;
        $point->save();

        Session::flash('message',"Se ha creado el punto para la ruta.");
        return redirect("/admin/routes/points/".$point->route->slug);
    }

    public function deleteItem($id)
    {
        RealRoutePoint::destroy($id);
        Session::flash('message','Se ha realizado la eliminación correctamente.');
        return redirect()->back();
    }

    public function images($slug)
    {
        $route = Route::where('slug',$slug)->first();
        return view('admin.Route.imageRoute', compact('route'));
    }

    public function upload_images($id, ImageRequest $request)
    {
        $files = $request->file('file');

        $new_service = [];

        foreach ($files as $file) {
            $imageName = $this->cargar_imagen($file);

            if ($imageName)  
            {
                $new_image = ['link_image' => 'images/routes/'.$imageName, 'fk_route' => $id];
                $service = RouteImage::create($new_image);
                $new_service[] = $service->toArray();
                unset($new_image);
            }
        }

        if (count($new_service) > 0) {
            return \Response::json(['response' => 'Subida de imágenes completa'], 200);
        } else {
            return \Response::json(['response' => 'Falla en la subida de imágenes'], 404);
        }
    }

    public function delete_image($id)
    {
        $image = RouteImage::find($id);

        $exists = File::exists(public_path("images/routes/".$image->link_imagen));
        if ($exists) {
            File::delete(public_path("images/routes/".$image->link_imagen));
        }

        if ($image->delete()) 
        {
            Session::flash('message', 'Imagen Borrada Correctamente');
        } 
        else 
        {
            Session::flash('message-error', 'La Imagen no puede ser borrada');
        }

        return redirect()->back();
    }

    private function cargar_imagen($file, $imageName = false)
    {
        if ($imageName) 
        {
            $exists = File::exists(public_path("images/routes/".$imageName));
            if ($exists) 
            {
                File::delete(public_path("images/routes/".$imageName));
            }

            $image = explode('.', $imageName);
            $imageName = $image[0].'.'.$file->getClientOriginalExtension();
        } 
        else 
        {
            $imageName = 'route'.date('YmdHis', time()).rand().'.'.$file->getClientOriginalExtension();
        }

        $file->move(public_path('images/routes'), $imageName);

        $exists = File::exists(public_path("images/routes/".$imageName));

        if ($exists) 
        {
            return $imageName;
        } 
        else 
        {
            return false;
        }
    }

    public function desactivate($id)
    {
        $route = Route::find($id);
        if($route)
        {
            $route->state = "inactivo";
            $route->save();
            Session::flash('message','Se ha desactivado la ruta correctamente');
            return redirect('admin/routes');
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado la ruta.");
            return redirect('admin/routes');
        }
    }

    public function activate($id)
    {
        $route = Route::find($id);
        if($route)
        {
            $route->state = "activo";
            $route->save();
            Session::flash('message','Se ha activado la ruta correctamente');
            return redirect('admin/routes');
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado la ruta.");
            return redirect('admin/routes');
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
