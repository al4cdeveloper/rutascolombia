<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ImageRequest;
use Illuminate\Support\Facades\URL;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Municipality;
use App\Models\MunicipalityImage;
use App\Models\Department;
use App\Models\Video;
use App\Models\Activity;
use App\Models\MunicipalityKeydata;
use App\Admin;
use Session;
use Auth;
use File;


class MunicipalityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $municipalities = Municipality::all();
        foreach ($municipalities as $municipality) 
        {
            if($municipality->type_last_user =="admin")
            {
                $admin = Admin::find($municipality->fk_last_edition);
                array_add($municipality,'last_edition',$admin->name." - Administrador");
            }

            //WORK HERE WHERE HAVE DIFFFERENT EDITIONS
        }
        return view('admin.Municipality.listMunicipality',compact('municipalities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments = Department::where('state','activo')->pluck('department_name', 'id_department');
        $activities = Activity::where('state','activo')->pluck('activity_name', 'id_activity');
        if(count($departments)==0)
        {
            Session::flash('message-error','No se encuentran departamentos creadas. Para crear un municipio se requiere un departamento.');
            return redirect()->back();
        }
        else
        {
            return view('admin.Municipality.createEditMunicipality',compact('departments','activities'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
             'fk_department'=>'required',
             'municipality_name' => 'required|max:50',
             'latitude' => 'required|max:50',
             'latitude' => 'required|max:50',
             'longitude'  => 'required',
             'keywords'  => 'required',
             'description'  => 'required',
             'link_image'  => 'required',
            ]); 

        if ($validator->fails()) 
        {
            foreach ($validator->errors()->all() as $error)
            {
                Session::flash('message-error', $error);
            }

            return redirect()->back()->withErrors($validator)->withInput();
        } 
        else 
        {
            $files = $request->file('link_image');
            $link_image = $this->cargar_imagen($files);

            $municipality = new Municipality;
            $municipality->municipality_name = $request->municipality_name;
            $municipality->description = $request->description;
            $municipality->multimedia_type = "images";
            $municipality->latitude = $request->latitude;
            $municipality->longitude = $request->longitude;
            $municipality->type_last_user = "admin";
            $municipality->video_youtube = youtube_match($request->video_youtube);
            $municipality->iframe = $request->iframe;
            $municipality->link_image = "images/municipalities/".$link_image;
            $municipality->fk_last_edition = Auth::user()->id;
            $municipality->fk_department = $request->fk_department;
            $municipality->keywords = $request->keywords;
            if(isset($request->city))
                $municipality->city = true;
             if($request->img_360)
            {
                $files = $request->file('img_360');
                $img_360 = cargar_imagen($files,'municipality');
                $municipality->img_360 = $img_360;
            }
            if(isset($request->show_additional_data))
                $municipality->show_additional_data = true;
            $municipality->save();

            $elementos  = (array) $request->activities;
            $datosAdicionales = array_fill(0, count($elementos), ['type_relation' => 'municipality']);
            $valores  = array_combine($elementos,$datosAdicionales);

            $municipality->Activities()->sync($valores);

            Session::flash('message', 'Se ha realizado correctamente el registro de información.');
            return redirect('admin/municipalities');
        }
    }


    public function upload_video(Request $request,$id)
    {
        // dd(youtube_match('https://www.youtube.com/watch?v=pXRviuL6vMY'));
        // https://www.youtube.com/embed/pXRviuL6vMY
        // <a href="#{{slugify_text($additional->title)}}" aria-controls="videos" role="tab" data-toggle="tab">{{$additional->title}}</a>
        
        $url = youtube_match($request->link_url);
        
        $newvideo = ['link_video' => $url, 'type_relation'=>'municipality','fk_relation' => $id];
        $create = Video::create($newvideo);

        Session::flash('message', 'Vídeo cargado correctamente');

        return redirect()->back();        
    }

    public function delete_video($id)
    {
        Video::destroy($id);
        Session::flash('message', 'El vídeo se ha borrado');
        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $municipality = Municipality::where('slug',$slug)->first();
        if($municipality)
        {
            $departments = Department::where('state','activo')->pluck('department_name', 'id_department');
            $activities = Activity::where('state','activo')->pluck('activity_name', 'id_activity');
            return view('admin.Municipality.createEditMunicipality',compact('municipality','departments','activities'));
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado el municipio.");
            return redirect('admin/municipalities');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $validator = \Validator::make($request->all(), [
             'fk_department'=>'required',
             'municipality_name' => 'required|max:50',
             'latitude' => 'required|max:50',
             'latitude' => 'required|max:50',
             'longitude'  => 'required',
             'keywords'  => 'required',
             'description'  => 'required',
            ]); 
        if ($validator->fails()) 
        {
            foreach ($validator->errors()->all() as $error)
            {
                Session::flash('message-error', $error);
            }

            return redirect()->back()->withErrors($validator)->withInput();
        } 
        else 
        {
            $municipality = Municipality::find($id);
            $municipality->municipality_name = $request->municipality_name;
            $municipality->description = $request->description;
            $municipality->latitude = $request->latitude;
            $municipality->longitude = $request->longitude;
            $municipality->video_youtube = youtube_match($request->video_youtube);
            
            if(isset($request->iframe))            $municipality->iframe = $request->iframe;
            if($request->link_image)
            {
                $exists = File::exists(public_path($municipality->link_image));
                if ($exists) 
                {
                    File::delete(public_path($municipality->link_image));
                }
                $files = $request->file('link_image');

                $link_image = $this->cargar_imagen($files);

                $municipality->link_image = "images/municipalities/".$link_image;
            }
            if($request->img_360)
            {
                $files = $request->file('img_360');
                $img_360 = cargar_imagen($files,'municipality',$municipality->img_360);
                $municipality->img_360 = $img_360;
            }
            if(isset($request->show_additional_data))
                $municipality->show_additional_data = true;
             else
                $municipality->show_additional_data = false;
            $municipality->type_last_user = "admin";
            $municipality->fk_last_edition = Auth::user()->id;
            $municipality->fk_department = $request->fk_department;
            $municipality->keywords = $request->keywords;
            if(isset($request->city))
                $municipality->city = true;
            else
                $municipality->city = false;

            $municipality->save();

            $elementos  = (array) $request->activities;
            $datosAdicionales = array_fill(0, count($elementos), ['type_relation' => 'municipality']);
            $valores  = array_combine($elementos,$datosAdicionales);
            $municipality->Activities()->sync($valores);

            Session::flash('message', 'Se ha realizado correctamente la actualización de información.');
            if($request->typesubmit=="guardartodo")
            {
                return redirect('admin/municipalities/edit/'.$municipality->slug);
            }
            else
            return redirect('admin/municipalities');  
        }
    }

    public function extra($slug)
    {
        $municipality = Municipality::where('slug',$slug)->first();
        return view('admin.Municipality.extradata',compact('municipality'));
    }

    public function saveextra(Request $request, $id)
    {
        $municipality = Municipality::find($id);
        $municipality->mayor_name = $request->mayor_name;
        $municipality->mayor_description = $request->mayor_description;
        if($request->mayor_image)
        {
            $files = $request->file('mayor_image');
            $mayor_image = cargar_imagen($files,'municipality');
            $municipality->mayor_image = $mayor_image;
        }
        if($request->logo_partner)
        {
            $files = $request->file('logo_partner');
            $logo_partner = cargar_imagen($files,'municipality');
            $municipality->logo_partner = $logo_partner;
        }
        if($request->flag)
        {
            $files = $request->file('flag');
            $flag = cargar_imagen($files,'municipality');
            $municipality->flag = $flag;
        }
        if($request->shield)
        {
            $files = $request->file('shield');
            $shield = cargar_imagen($files,'municipality');
            $municipality->shield = $shield;
        }
        if(isset($request->show_additional_data))
                $municipality->show_additional_data = true;
             else
                $municipality->show_additional_data = false;
        $municipality->save();
        Session::flash('message', 'Se ha realizado correctamente el registro de información.');
        if($request->typesubmit=="guardartodo")
        {
            return redirect('admin/municipalities/extra/'.$municipality->slug);
        }
        else
        return redirect('admin/municipalities/edit/'.$municipality->slug);  
    }

    public function keydata($slug)
    {
        $municipality = Municipality::where('slug',$slug)->first();
        $categories = ['Datos Útiles'=>'Datos Útiles',
                        'Distancia Estimada Terrestre'=>'Distancia Estimada Terrestre',
                        'Tiempo Estimado de Vuelo'=>'Tiempo Estimado de Vuelo',
                        'Información de Contacto'=>'Información de Contacto'];
        return view('admin.Municipality.keyData', compact('municipality','categories'));
    }

    public function change(Request $request)
    {
        try
        {
            $municipality = Municipality::find($request->cod);
            if($municipality->outstanding==0)
                $municipality->outstanding = 1;
            else
                $municipality->outstanding = 0;
            
            $municipality->save();
            return 'ok';
        }
        catch(\Exception $e)
        {
            return 'error';
        }
    }

    public function saveKeydata($id,Request $request)
    {
        $municipality = Municipality::find($id);
        if(count($municipality->KeyData)>0)
        {
            foreach ($municipality->KeyData as $key) 
            {
                $key->delete();
            }
        }
        if(isset($request->name))
        {
            foreach ($request->name as $key => $value) 
            {
                if($value!="" && $request->value[$key])
                {
                    $keyData = new MunicipalityKeydata;
                    $keyData->keydata_name = $value;
                    $keyData->keydata_value = $request->value[$key];
                    $keyData->category = $request->category[$key];
                    $keyData->fk_municipality = $id;
                    $keyData->save();
                }
            }
        }
        

        Session::flash('message','Información registrada correctamente');
        return redirect('admin/municipalities/keydata/'.$municipality->slug);
    }



    public function images($slug)
    {
        $municipality = Municipality::where('slug',$slug)->first();
        return view('admin.Municipality.imagesMunicipality', compact('municipality'));
    }

    public function upload_images($id, ImageRequest $request)
    {
        $files = $request->file('file');

        $new_service = [];

        foreach ($files as $file) {
            $imageName = $this->cargar_imagen($file);

            if ($imageName)  
            {
                $new_image = ['link_image' => 'images/municipalities/'.$imageName, 'fk_municipality' => $id];
                $service = MunicipalityImage::create($new_image);
                $new_service[] = $service->toArray();
                unset($new_image);
            }
        }

        if (count($new_service) > 0) {
            return \Response::json(['response' => 'Subida de imágenes completa'], 200);
        } else {
            return \Response::json(['response' => 'Falla en la subida de imágenes2'], 404);
        }
    }

    public function delete_image($id)
    {
        $image = MunicipalityImage::find($id);

        $exists = File::exists(public_path("images/municipalities/".$image->link_imagen));
        if ($exists) {
            File::delete(public_path("images/municipalities/".$image->link_imagen));
        }

        if ($image->delete()) 
        {
            \Session::flash('message', 'Imagen Borrada Correctamente');
        } 
        else 
        {
            \Session::flash('message-error', 'La Imagen no puede ser borrada');
        }

        return redirect()->back();
    }

    public function desactivate($id)
    {
        $municipality = Municipality::find($id);
        if($municipality)
        {
            $municipality->state = "inactivo";
            $municipality->save();
            Session::flash('message','Se ha desactivado el municipio correctamente');
            return redirect('admin/municipalities');
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado el municipio.");
            return redirect('admin/municipalities');
        }
    }

    public function activate($id)
    {
        $municipality = Municipality::find($id);
        if($municipality)
        {
            $municipality->state = "activo";
            $municipality->save();
            Session::flash('message','Se ha activado el municipio correctamente');
            return redirect('admin/municipalities');
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado el municipio.");
            return redirect('admin/municipalities');
        }
    }


    private function cargar_imagen($file, $imageName = false)
    {
        if ($imageName) 
        {
            $exists = File::exists(public_path("images/municipalities/".$imageName));
            if ($exists) 
            {
                File::delete(public_path("images/municipalities/".$imageName));
            }

            $image = explode('.', $imageName);
            $imageName = $image[0].'.'.$file->getClientOriginalExtension();
        } 
        else 
        {
            $imageName = 'Municipality_'.date('YmdHis', time()).rand().'.'.$file->getClientOriginalExtension();
        }

        $file->move(public_path('images/municipalities'), $imageName);

        $exists = File::exists(public_path("images/municipalities/".$imageName));

        if ($exists) 
        {
            return $imageName;
        } 
        else 
        {
            return false;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
