<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Activity;
use Session;

class ActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $activities = Activity::all();
        return view('admin.Activity.listActivity',compact('activities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $icons = [
                    'icon_set_2_icon-102' => 'icon_set_2_icon-102',
                    'icon_set_2_icon-103' => 'icon_set_2_icon-103',
                    'icon_set_2_icon-105' => 'icon_set_2_icon-105',
                    'icon_set_2_icon-106' => 'icon_set_2_icon-106',
                    'icon_set_2_icon-107' => 'icon_set_2_icon-107',
                    'icon_set_2_icon-108' => 'icon_set_2_icon-108',
                    'icon_set_2_icon-109' => 'icon_set_2_icon-109',
                    'icon_set_2_icon-110' => 'icon_set_2_icon-110',
                    'icon_set_2_icon-111' => 'icon_set_2_icon-111',
                    'icon_set_2_icon-112' => 'icon_set_2_icon-112',
                    'icon_set_2_icon-104' => 'icon_set_2_icon-104',
                    'icon_set_2_icon-114' => 'icon_set_2_icon-114',
                    'icon_set_2_icon-115' => 'icon_set_2_icon-115',
                    'icon_set_2_icon-116' => 'icon_set_2_icon-116',
                    'icon_set_2_icon-117' => 'icon_set_2_icon-117',
                    'icon_set_2_icon-118' => 'icon_set_2_icon-118',
                    ];
        return view('admin.Activity.createEditActivity',compact('icons'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
         'activity_name'=>'required',
         'title'=>'required',
         'keywords'=>'required',
         'meta_description'=>'required',
         'image_link'  => 'required',
        ]); 

        if ($validator->fails()) 
        {
            foreach ($validator->errors()->all() as $error)
            {
                Session::flash('message-error', $error);
            }

            return redirect()->back()->withErrors($validator)->withInput();
        } 

        $files = $request->file('image_link');
        $image_link = cargar_imagen($files,'activity');

        $activity = new Activity;
        $activity->activity_name = $request->activity_name;
        $activity->title = $request->title;
        $activity->keywords = $request->keywords;
        $activity->icon_class=$request->icon_class;
        $activity->meta_description = $request->meta_description;
        $activity->image_link = $image_link;
        $activity->save();
        Session::flash('message', 'Se ha realizado correctamente el registro de información.');
        return redirect('admin/whattodo');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $activity = Activity::where('slug',$slug)->first();
        if($activity)
        {
            $icons = [
                    'icon_set_2_icon-102' => 'icon_set_2_icon-102',
                    'icon_set_2_icon-103' => 'icon_set_2_icon-103',
                    'icon_set_2_icon-105' => 'icon_set_2_icon-105',
                    'icon_set_2_icon-106' => 'icon_set_2_icon-106',
                    'icon_set_2_icon-107' => 'icon_set_2_icon-107',
                    'icon_set_2_icon-108' => 'icon_set_2_icon-108',
                    'icon_set_2_icon-109' => 'icon_set_2_icon-109',
                    'icon_set_2_icon-110' => 'icon_set_2_icon-110',
                    'icon_set_2_icon-111' => 'icon_set_2_icon-111',
                    'icon_set_2_icon-112' => 'icon_set_2_icon-112',
                    'icon_set_2_icon-104' => 'icon_set_2_icon-104',
                    'icon_set_2_icon-114' => 'icon_set_2_icon-114',
                    'icon_set_2_icon-115' => 'icon_set_2_icon-115',
                    'icon_set_2_icon-116' => 'icon_set_2_icon-116',
                    'icon_set_2_icon-117' => 'icon_set_2_icon-117',
                    'icon_set_2_icon-118' => 'icon_set_2_icon-118',
                    ];
            return view('admin.Activity.createEditActivity',compact('activity','icons'));
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado la categoría.");
            return redirect('admin/whattodo');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
             'activity_name'=>'required',
             'title'=>'required',
             'keywords'=>'required',
             'meta_description'=>'required',
            ]); 
        if ($validator->fails()) 
        {
            foreach ($validator->errors()->all() as $error)
            {
                Session::flash('message-error', $error);
            }

            return redirect()->back()->withErrors($validator)->withInput();
        }
        $activity = Activity::find($id);
        $activity->activity_name = $request->activity_name;
        $activity->icon_class = $request->icon_class;
        $activity->icon_class=$request->icon_class;

        if($request->image_link)
        {
            $exists = File::exists(public_path($activity->image_link));
            if ($exists) 
            {
                File::delete(public_path($activity->image_link));
            }
            $files = $request->file('image_link');

            $image_link = cargar_imagen($files,'activity');

            $activity->image_link = $image_link;
        }
        $activity->save();
        Session::flash('message', 'Se ha realizado correctamente la actualización de información.');
        return redirect('admin/whattodo');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function desactivate($id)
    {
        $activity = Activity::find($id);
        if($activity)
        {
            $activity->state = "inactivo";
            $activity->save();
            Session::flash('message','Se ha desactivado la categoria correctamente');
            return redirect('admin/whattodo');
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado la categoria.");
            return redirect('admin/whattodo');
        }
    }

    public function activate($id)
    {
        $activity = Activity::find($id);
        if($activity)
        {
            $activity->state = "activo";
            $activity->save();
            Session::flash('message','Se ha activado la categoria correctamente');
            return redirect('admin/whattodo');
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado la categoria.");
            return redirect('admin/whattodo');
        }
    }
    public function destroy($id)
    {
        //
    }
}
