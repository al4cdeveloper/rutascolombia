<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PagePart;
use Session;

class PagePartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $parts = PagePart::all();
        return view('admin.PagePart.listPagePart',compact('parts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $type = ['Carrusel'=>'Carrusel','Estatica'=>'Estática'];
        return view('admin.PagePart.createEditPagePart',compact('type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
             'part_name'=>'required',
             'type'=>'required',
            ]); 

        if ($validator->fails()) 
        {
            foreach ($validator->errors()->all() as $error)
            {
                Session::flash('message-error', $error);
            }

            return redirect()->back()->withErrors($validator)->withInput();
        } 
        else 
        {
            $part = new PagePart;
            $part->part_name = $request->part_name;
            $part->type = $request->type;
            $part->save();


            Session::flash('message', 'Se ha realizado correctamente el registro de información.');
            return redirect('admin/pageparts');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $part = PagePart::where('slug',$slug)->first();
        if($part)
        {
            $type = ['Carrusel'=>'Carrusel','Estatica'=>'Estática'];
            return view('admin.PagePart.createEditPagePart',compact('type','part'));
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado la sección.");
            return redirect('admin/pageparts');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
             'part_name'=>'required',
             'type'=>'required',
            ]); 

        if ($validator->fails()) 
        {
            foreach ($validator->errors()->all() as $error)
            {
                Session::flash('message-error', $error);
            }

            return redirect()->back()->withErrors($validator)->withInput();
        } 
        else 
        {
            $part = PagePart::find($id);
            $part->part_name = $request->part_name;
            $part->type = $request->type;
            $part->save();


            Session::flash('message', 'Se ha realizado correctamente la actualización de información.');
            return redirect('admin/pageparts');
        }
    }

    public function desactivate($id)
    {
        $part = PagePart::find($id);
        if($part)
        {
            $part->state = "inactivo";
            $part->save();
            Session::flash('message','Se ha desactivado la sección correctamente');
            return redirect('admin/pageparts');
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado la sección.");
            return redirect('admin/pageparts');
        }
    }

    public function activate($id)
    {
        $part = PagePart::find($id);
        if($part)
        {
            $part->state = "activo";
            $part->save();
            Session::flash('message','Se ha activado la sección correctamente');
            return redirect('admin/pageparts');
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado la sección.");
            return redirect('admin/pageparts');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
