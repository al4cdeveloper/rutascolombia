<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\ImageRequest;
use App\Http\Controllers\Controller;
use App\Models\Department;
use App\Models\Region;
use App\Models\DepartmentImage;
use App\Models\DepartmentCircuit;
use App\Models\InterestSite;
use App\Models\Municipality;
use App\Admin;
use Session;
use Auth;
use File;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departments = Department::all();
        foreach ($departments as $department) 
        {
            if($department->type_last_user =="admin")
            {
                $admin = Admin::find($department->fk_last_edition);
                array_add($department,'last_edition',$admin->name." - Administrador");
            }

            //WORK HERE WHERE HAVE DIFFFERENT EDITIONS
        }
        return view('admin.Department.listDepartment',compact('departments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $regions = Region::where('state','activo')->pluck('region_name', 'id_region');
        if(count($regions)==0)
        {
            Session::flash('message-error','No se encuentran regiones creadas. Para crear un departamento se requiere una región.');
            return redirect()->back();
        }
        else
        {
            return view('admin.Department.createEditDepartment',compact('regions'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
             'fk_region'=>'required',
             'department_name' => 'required|max:50',
             'latitude' => 'required|max:50',
             'latitude' => 'required|max:50',
             'longitude'  => 'required',
             'keywords'  => 'required',
             'description'  => 'required',
             'link_image'  => 'required',
            ]); 

        if ($validator->fails()) 
        {
            foreach ($validator->errors()->all() as $error)
            {
                Session::flash('message-error', $error);
            }

            return redirect()->back()->withErrors($validator)->withInput();
        } 
        else 
        {
            $files = $request->file('link_image');
            $link_image = $this->cargar_imagen($files);


            $department = new Department;
            $department->department_name = $request->department_name;
            $department->short_description = $request->short_description;
            $department->description = $request->description;
            $department->multimedia_type = "images";
            $department->latitude = $request->latitude;
            $department->longitude = $request->longitude;
            $department->type_last_user = "admin";
            $department->link_image = "images/departments/".$link_image;
            $department->fk_last_edition = Auth::user()->id;

            if($request->description_image)
            {
                $files = $request->file('description_image');
                $description_image = cargar_imagen($files,'department');
                $department->description_image = $description_image;
            }

            $department->video_youtube = youtube_match($request->video_youtube);
            $department->text_images = $request->text_images;
            $department->title_extra_description = $request->title_extra_description;
            $department->extra_description = $request->extra_description;
            $department->fk_region = $request->fk_region;
            $department->keywords = $request->keywords;
            $department->save();

            Session::flash('message', 'Se ha realizado correctamente el registro de información.');
            return redirect('admin/departments');
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $department = Department::where('slug',$slug)->first();
        if($department)
        {
            $regions = Region::where('state','activo')->pluck('region_name', 'id_region');
            return view('admin.Department.createEditDepartment',compact('department','regions'));
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado el departamento.");
            return redirect('admin/departments');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
             'fk_region'=>'required',
             'department_name' => 'required|max:50',
             'latitude' => 'required|max:50',
             'latitude' => 'required|max:50',
             'longitude'  => 'required',
             'keywords'  => 'required',
             'description'  => 'required',
            ]); 
        if ($validator->fails()) 
        {
            foreach ($validator->errors()->all() as $error)
            {
                Session::flash('message-error', $error);
            }

            return redirect()->back()->withErrors($validator)->withInput();
        } 
        else 
        {
            $department = Department::find($id);
            $department->department_name = $request->department_name;
            $department->short_description = $request->short_description;
            $department->description = $request->description;
            $department->latitude = $request->latitude;
            $department->longitude = $request->longitude;
            $department->type_last_user = "admin";
            if($request->link_image)
            {
                $exists = File::exists(public_path($department->link_image));
                if ($exists) 
                {
                    File::delete(public_path($department->link_image));
                }
                $files = $request->file('link_image');

                $link_image = $this->cargar_imagen($files);

                $department->link_image = "images/departments/".$link_image;

            }
            if($request->description_image)
            {
                $files = $request->file('description_image');
                $description_image = cargar_imagen($files,'department',$department->description_image);
                $department->description_image = $description_image;
            }
            $department->fk_last_edition = Auth::user()->id;
            $department->fk_region = $request->fk_region;
            $department->keywords = $request->keywords;

            $department->video_youtube = youtube_match($request->video_youtube);
            $department->text_images = $request->text_images;
            $department->title_extra_description = $request->title_extra_description;
            $department->extra_description = $request->extra_description;
            $department->save();

            Session::flash('message', 'Se ha realizado correctamente la actualización de información.');
            return redirect('admin/departments');
        }
    }

    public function images($slug)
    {
        $department = Department::where('slug',$slug)->first();
        return view('admin.Department.imagesDepartment', compact('department'));
    }

    public function upload_images($id, ImageRequest $request)
    {
        $files = $request->file('file');

        $new_service = [];

        foreach ($files as $file) {
            $imageName = $this->cargar_imagen($file);

            if ($imageName)  
            {
                $new_image = ['link_image' => 'images/departments/'.$imageName, 'fk_department' => $id];
                $service = DepartmentImage::create($new_image);
                $new_service[] = $service->toArray();
                unset($new_image);
            }
        }

        if (count($new_service) > 0) {
            return \Response::json(['response' => 'Subida de imágenes completa'], 200);
        } else {
            return \Response::json(['response' => 'Falla en la subida de imágenes2'], 404);
        }
    }

    public function delete_image($id)
    {
        $image = DepartmentImage::find($id);

        $exists = File::exists(public_path("images/departments/".$image->link_imagen));
        if ($exists) {
            File::delete(public_path("images/departments/".$image->link_imagen));
        }

        if ($image->delete()) 
        {
            \Session::flash('message', 'Imagen Borrada Correctamente');
        } 
        else 
        {
            \Session::flash('message-error', 'La Imagen no puede ser borrada');
        }

        return redirect()->back();
    }


    public function desactivate($id)
    {
        $department = Department::find($id);
        if($department)
        {
            $department->state = "inactivo";
            $department->save();
            Session::flash('message','Se ha desactivado el departamento correctamente');
            return redirect('admin/departments');
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado el departamento.");
            return redirect('admin/departments');
        }
    }

    public function activate($id)
    {
        $department = Department::find($id);
        if($department)
        {
            $department->state = "activo";
            $department->save();
            Session::flash('message','Se ha activado el departamento correctamente');
            return redirect('admin/departments');
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado el departamento.");
            return redirect('admin/departments');
        }
    }

    private function cargar_imagen($file, $imageName = false)
    {
        if ($imageName) 
        {
            $exists = File::exists(public_path("images/departments/".$imageName));
            if ($exists) 
            {
                File::delete(public_path("images/departments/".$imageName));
            }

            $image = explode('.', $imageName);
            $imageName = $image[0].'.'.$file->getClientOriginalExtension();
        } 
        else 
        {
            $imageName = 'Department_'.date('YmdHis', time()).rand().'.'.$file->getClientOriginalExtension();
        }

        $file->move(public_path('images/departments'), $imageName);

        $exists = File::exists(public_path("images/departments/".$imageName));

        if ($exists) 
        {
            return $imageName;
        } 
        else 
        {
            return false;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
