<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\ImageRequest;
use App\Http\Controllers\Controller;
use App\Models\Special;
use App\Models\Department;
use App\Models\Municipality;
use App\Models\InterestSite;
use App\Models\RelationSpecial;
use App\Models\SpecialImage;
use App\Models\SpecialRoute;
use App\Models\SpecialItem;
use App\Models\SpecialDetailItem;
use App\Models\RoutePoint;
use App\Models\SpecialRouteImage;
use Session;
use File;

class SpecialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $specials = Special::all();
        return view('admin.Special.specialList',compact('specials'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $municipalities = Municipality::whereState('activo')->get();
        $types = ['normal'=>'Normal','rutas'=>'Rutas'];
        return view('admin.Special.createEditSpecial',compact('municipalities','types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(),[
            'name'=>'required',
            'keywords'=>'required',
            'description'=>'required',
            ]);
        if($validator->fails())
        {
            foreach($validator->errors()->all() as $error)
            {
                Session::flash('message-error',$error);
            }

            return redirect()->back()->withErrors($validator)->withInput();
        }

        $special = new Special;
        $special->type = $request->type;
        $special->name = $request->name;
        $special->keywords = $request->keywords;
        if($request->outstanding) $special->outstanding = 1;
        else $special->outstanding = 0;
        $special->short_description = $request->short_description;
        $special->description = $request->description;
        $special->recommendations = $request->recommendations;
        $special->text_images = $request->text_images;

        if(isset($request->description_to_site))$special->description_to_site = $request->description_to_site;
        if(isset($request->video)) $special->video = youtube_match($request->video);
        if(isset($request->iframe)) $special->iframe = $request->iframe;
        if($request->card_image)
        {
            $files = $request->file('card_image');

            $card_image = cargar_imagen($files,'special');

            $special->card_image = $card_image;
        }
        if($request->introduction_image)
        {
            $files = $request->file('introduction_image');

            $introduction_image = cargar_imagen($files,'special');

            $special->introduction_image = $introduction_image;
        }
        if($request->description_image)
        {
            $files = $request->file('description_image');

            $description_image = cargar_imagen($files,'special');

            $special->description_image = $description_image;
        }

        if($request->recommendation_image)
        {
            $files = $request->file('recommendation_image');

            $recommendation_image = cargar_imagen($files,'special');

            $special->recommendation_image = $recommendation_image;
        }
        $special->save();

        Session::flash('message', 'Se ha realizado correctamente el registro de información.');
        if($request->typesubmit=="guardartodo")
        {
            return redirect('admin/specials/edit/'.$special->slug);
        }
        else
        return redirect('admin/specials');
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $special = Special::whereSlug($slug)->first();
        $municipalities = Municipality::where('state','activo')->get();
        $sites = InterestSite::where('state','activo')->get();
        $types = ['normal'=>'Normal','rutas'=>'Rutas'];
        return view('admin.Special.createEditSpecial',compact('special','municipalities','sites','types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        \Validator::make($request->all(),[
            'name'=>'required',
            'keywords'=>'required',
            'description'=>'required',
            ])->validate();

        $special = Special::find($id);
        $special->name = $request->name;
        $special->keywords = $request->keywords;
        if($request->outstanding) $special->outstanding = 1;
        else $special->outstanding = 0;
        $special->description = $request->description;
        $special->recommendations = $request->recommendations;
        $special->text_images = $request->text_images;
        if(isset($request->description_to_site))$special->description_to_site = $request->description_to_site;
        if(isset($request->video)) $special->video = youtube_match($request->video);
        if(isset($request->iframe)) $special->iframe = $request->iframe;
        if($request->card_image)
        {
            $files = $request->file('card_image');

            $card_image = cargar_imagen($files,'special',$special->card_image);

            $special->card_image = $card_image;
        }
        if($request->introduction_image)
        {
            $files = $request->file('introduction_image');

            $introduction_image = cargar_imagen($files,'special',$special->introduction_image);

            $special->introduction_image = $introduction_image;
        }
        if($request->description_image)
        {
            $files = $request->file('description_image');

            $description_image = cargar_imagen($files,'special',$special->description_image);

            $special->description_image = $description_image;
        }
        if($request->recommendation_image)
        {
            $files = $request->file('recommendation_image');

            $recommendation_image = cargar_imagen($files,'special',$special->recommendation_image);

            $special->recommendation_image = $recommendation_image;
        }
        $special->save();
        Session::flash('message', 'Se ha realizado correctamente el registro de información.');
        if($request->typesubmit=="guardartodo")
        {
            return redirect('admin/specials/edit/'.$special->slug);
        }
        else
        return redirect('admin/specials');
    }

    public function change(Request $request)
    {
        try
        {
            $special = Special::find($request->cod);
            if($special->outstanding==0)
                $special->outstanding = 1;
            else
                $special->outstanding = 0;
            
            $special->save();
            return 'ok';
        }
        catch(\Exception $e)
        {
            return 'error';
        }
    }

    public function items($slug)
    {
        $special = Special::where('slug',$slug)->first();
        return view('admin.Special.itemsSpecial',compact('special'));
    }

    public function storeItem(Request $request, $id)
    {
        $item = new SpecialItem;
        $item->title = $request->title;
        $item->description = $request->description;
        $item->fk_special = $id;
        $item->save();

        Session::flash('message','Se ha realizado correctamente el regisitro de la información.');
        return redirect()->back();
    }

    public function updateItem(Request $request,$id)
    {
        $item  = SpecialItem::find($id);
        $item->title = $request->title;
        $item->description = $request->description;
        $item->save();
        
        Session::flash('message','Se ha realizado correctamente el regisitro de la información.');
        return redirect()->back();
    }

    public function destroyItem($id)
    {
        SpecialItem::destroy($id);
        Session::flash('message','Se ha eliminado correctamente el item.');
        return redirect()->back();
    }
    public function detailitems($slug)
    {
        $special = Special::where('slug',$slug)->first();
        return view('admin.Special.detailitemsSpecial',compact('special'));
    }

    public function createDetailItems($slug)
    {
        $departments = Department::where('state','activo')->pluck('department_name','id_department')->prepend('');
        $special = Special::where('slug',$slug)->first();
        return view('admin.Special.createEditDetailItem',compact('departments','special'));
    }

    public function storeDetailItems(Request $request, $id)
    {
        \Validator::make($request->all(),['description'=>'required',
                        'fk_municipality'=>'required'])->validate();
        $item = new SpecialDetailItem;
        $item->description = $request->description;
        $item->type_relation = $request->type_relation;
        if($request->type_relation == 'municipality')
        {
            $item->fk_relation = $request->fk_municipality;
        }
        else
        {
            $item->fk_relation = $request->fk_site;
        }
        if($request->image_link)
        {
            $files = $request->file('image_link');
            $image_link = cargar_imagen($files,'detailitem');
            $item->image_link = $image_link;
        }
        $item->fk_special = $id;
        $item->save();

        Session::flash('message','Se ha realizado correctamente el regisitro de la información.');
        return redirect('/admin/specials/detailitems/'.$item->special->slug);
    }

    public function editDetailItems($id)
    {
        $item = SpecialDetailItem::find($id);
        $special = $item->special;
        $departments = Department::where('state','activo')->pluck('department_name','id_department')->prepend('');
        if($item->type_startpoint=='municipality')
        {
            $itemmunicipality = Municipality::find($item->fk_relation);
            $departmentStart = $itemmunicipality->department;
            $municipalitiesStart = $departmentStart->municipalities->pluck('municipality_name','id_municipality');
            $sitesStart = $itemmunicipality->InterestSite->pluck('site_name','id_site');
        }
        else
        {
            $siteStart = InterestSite::find($item->fk_relation);
            $itemmunicipality = $siteStart->municipality;
            $departmentStart = $itemmunicipality->department;
            $municipalitiesStart = $departmentStart->municipalities->pluck('municipality_name','id_municipality');
            $sitesStart = $itemmunicipality->InterestSite->pluck('site_name','id_site');
        }
        return view('admin.Special.createEditDetailItem',compact('item','special','departments','departmentStart','municipalitiesStart','itemmunicipality','sitesStart'));

    }

    public function updateDetailItems(Request $request, $id)
    {
        \Validator::make($request->all(),['description'=>'required',
                        'fk_municipality'=>'required'])->validate();
        $item = SpecialDetailItem::find($id);
        $item->description = $request->description;
        $item->type_relation = $request->type_relation;
        if($request->type_relation == 'municipality')
        {
            $item->fk_relation = $request->fk_municipality;
        }
        else
        {
            $item->fk_relation = $request->fk_site;
        }
        if($request->image_link)
        {
            $files = $request->file('image_link');
            $image_link = cargar_imagen($files,'detailitem',$item->image_link);
            $item->image_link = $image_link;
        }
        $item->save();
        Session::flash('message','Se ha realizado correctamente el regisitro de la información.');
        return redirect('/admin/specials/detailitems/'.$item->special->slug);
    }

    public function destroyDetailItem($id)
    {
        SpecialDetailItem::destroy($id);
        Session::flash('message','Se ha eliminado correctamente el item.');
        return redirect()->back();
    }
    

    public function routes($slug)
    {
        $special = Special::where('slug',$slug)->first();
        return view('admin.Special.Route.routeList',compact('special'));
    }

    public function routesCreate($slug)
    {
        $special = Special::where('slug',$slug)->first();
        $departments = Department::where('state','activo')->pluck('department_name','id_department')->prepend('');
        $levels = ['Fácil'=>'Fácil','Intermedio'=>'Intermedio','Difícil'=>'Difícil'];
        return view('admin.Special.Route.newEditRoute',compact('special','departments','levels'));
    }

    public function storeRoute(Request $request, $slug)
    {
        $validator = \Validator::make($request->all(),[
            'name' => 'required',
            'sentence' => 'required',
            'length' => 'required',
            'keywords' => 'required',
            'url_map' => 'required',
            'highest_point' => 'required',
            'lower_point' => 'required',
            'description' => 'required',
            'recommendation' => 'required',

        ]);
        if($validator->fails())
        {
            foreach($validator->fails()->all() as $error)
            {
                Session::flash('message-error',$error);
            }

            return redirect()->back()->withErrors($validator)->withInput();
        }

        $special = Special::where('slug',$slug)->first();
        $route = new SpecialRoute;
        $route->name = $request->name;
        $route->sentence = $request->sentence;
        $route->length = $request->length;
        $route->difficulty = $request->difficulty;
        $route->keywords = $request->keywords;
        if(isset($request->video)) $route->video = youtube_match($request->video);
        $route->url_map = $request->url_map;
        $route->highest_point = $request->highest_point;
        $route->lower_point = $request->lower_point;
        $route->description = $request->description;
        $route->recommendation = $request->recommendation;
        $route->text_images = $request->text_images;
        if($request->card_image)
        {
            $files = $request->file('card_image');

            $card_image = cargar_imagen($files,'route');

            $route->card_image = $card_image;
        }
        if($request->introduction_image)
        {
            $files = $request->file('introduction_image');

            $introduction_image = cargar_imagen($files,'route');

            $route->introduction_image = $introduction_image;
        }
        if($request->description_image)
        {
            $files = $request->file('description_image');
            $description_image = cargar_imagen($files,'specialroute');
            $route->description_image = $description_image;
        }
        if($request->recommendation_image)
        {
            $files = $request->file('recommendation_image');
            $recommendation_image = cargar_imagen($files,'specialroute');
            $route->recommendation_image = $recommendation_image;
        }
        $route->type_startpoint = $request->type_startpoint;
        if($request->type_startpoint == 'municipality')
            $route->start_point = $request->fk_municipality;
        else
            $route->start_point = $request->fk_site;
        $route->type_endpoint = $request->type_endpoint;
        if($request->type_endpoint == 'municipality')
            $route->end_point = $request->fk_municipality2;
        else
            $route->end_point = $request->fk_site2;

        $route->fk_special = $special->id_special;
        $route->save();

        Session::flash('message','Se ha realizado el registro correctamente.');
        return redirect("admin/specials/routes/$special->slug");
    }

    public function editRoute($slug)
    {
        $route = SpecialRoute::where('slug',$slug)->first();
        $special = $route->special;
        $departments = Department::where('state','activo')->pluck('department_name','id_department')->prepend('');
        if($route->type_startpoint=='municipality')
        {
            $municipalityStart = Municipality::find($route->start_point);
            $departmentStart = $municipalityStart->department;
            $municipalitiesStart = $departmentStart->municipalities->pluck('municipality_name','id_municipality');
            $sitesStart = $municipalityStart->InterestSite->pluck('site_name','id_site');
        }
        else
        {
            $siteStart = InterestSite::find($route->start_point);
            $municipalityStart = $siteStart->municipality;
            $departmentStart = $municipalityStart->department;
            $municipalitiesStart = $departmentStart->municipalities->pluck('municipality_name','id_municipality');
            $sitesStart = $municipalityStart->InterestSite->pluck('site_name','id_site');
        }
        if($route->type_endpoint=='municipality')
        {
            $municipalityEnd = Municipality::find($route->end_point);
            $departmentEnd = $municipalityEnd->department;
            $municipalitiesEnd = $departmentEnd->municipalities->pluck('municipality_name','id_municipality');
            $sitesEnd = $municipalityEnd->InterestSite->pluck('site_name','id_site');
        }
        else
        {
            $siteEnd = InterestSite::find($route->end_point);
            $municipalityEnd = $siteEnd->municipality;
            $departmentEnd = $municipalityEnd->department;
            $municipalitiesEnd = $departmentEnd->municipalities->pluck('municipality_name','id_municipality');
            $sitesEnd = $municipalityEnd->InterestSite->pluck('site_name','id_site');
        }
        $levels = ['Fácil'=>'Fácil','Intermedio'=>'Intermedio','Difícil'=>'Difícil'];
        return view('admin.Special.Route.newEditRoute',compact('route','special','departments','departmentStart','municipalitiesStart','sitesStart','departmentEnd','municipalitiesEnd','sitesEnd','levels'));
    }

    public function updateRoute(Request $request, $id)
    {
        $validator = \Validator::make($request->all(),[
            'name' => 'required',
            'sentence' => 'required',
            'length' => 'required',
            'keywords' => 'required',
            'url_map' => 'required',
            'highest_point' => 'required',
            'lower_point' => 'required',
            'description' => 'required',
            'recommendation' => 'required',
        ]);
        if($validator->fails())
        {
            foreach($validator->errors()->all() as $error)
            {
                Session::flash('message-error',$error);
            }

            return redirect()->back()->withErrors($validator)->withInput();
        }
        $route = SpecialRoute::find($id);
        $route->name = $request->name;
        $route->sentence = $request->sentence;
        $route->length = $request->length;
        $route->difficulty = $request->difficulty;
        $route->keywords = $request->keywords;
        if(isset($request->video)) $route->video = youtube_match($request->video);
        $route->url_map = $request->url_map;
        $route->highest_point = $request->highest_point;
        $route->lower_point = $request->lower_point;
        $route->description = $request->description;
        $route->recommendation = $request->recommendation;
        $route->text_images = $request->text_images;
        if($request->card_image)
        {
            $files = $request->file('card_image');

            $card_image = cargar_imagen($files,'route',$route->card_image);

            $route->card_image = $card_image;
        }
        if($request->introduction_image)
        {
            $files = $request->file('introduction_image');

            $introduction_image = cargar_imagen($files,'route',$route->introduction_image);

            $route->introduction_image = $introduction_image;
        }
        if($request->description_image)
        {
            $files = $request->file('description_image');
            $description_image = cargar_imagen($files,'specialroute',$route->description_image);
            $route->description_image = $description_image;
        }
        if($request->recommendation_image)
        {
            $files = $request->file('recommendation_image');
            $recommendation_image = cargar_imagen($files,'specialroute',$route->recommendation_image);
            $route->recommendation_image = $recommendation_image;
        }
        $route->type_startpoint = $request->type_startpoint;
        if($request->type_startpoint == 'municipality')
            $route->start_point = $request->fk_municipality;
        else
            $route->start_point = $request->fk_site;
        $route->type_endpoint = $request->type_endpoint;
        if($request->type_endpoint == 'municipality')
            $route->end_point = $request->fk_municipality2;
        else
            $route->end_point = $request->fk_site2;
        $route->save();

        $special = $route->special;
        Session::flash('message','Se ha realizado el registro correctamente.');
        return redirect("admin/specials/routes/$special->slug");
    }

    public function imagesRoute($slug)
    {
        $route = SpecialRoute::where('slug',$slug)->first();
        return view('admin.Special.Route.imageRoute', compact('route'));
    }

    public function upload_imagesRoute($id, ImageRequest $request)
    {
        $files = $request->file('file');

        $new_service = [];

        foreach ($files as $file) {
            $imageName = cargar_imagen($file,"special");

            if ($imageName)  
            {
                $new_image = ['link_image' => $imageName, 'fk_route' => $id];
                $service = SpecialRouteImage::create($new_image);
                $new_service[] = $service->toArray();
                unset($new_image);
            }
        }

        if (count($new_service) > 0) {
            return \Response::json(['response' => 'Subida de imágenes completa'], 200);
        } else {
            return \Response::json(['response' => 'Falla en la subida de imágenes'], 404);
        }
    }

    public function delete_imageRoute($id)
    {
        $image = SpecialRouteImage::find($id);

        $exists = File::exists(public_path($image->link_imagen));
        if ($exists) {
            File::delete(public_path($image->link_imagen));
        }

        if ($image->delete()) 
        {
            Session::flash('message', 'Imagen Borrada Correctamente');
        } 
        else 
        {
            Session::flash('message-error', 'La Imagen no puede ser borrada');
        }

        return redirect()->back();
    }

    public function desactivateRoute($slug)
    {
        $route = SpecialRoute::where('slug',$slug)->first();
        $route->state = 'inactivo';
        $route->save();
        Session::flash('message','Se ha desactivado correctamente la ruta.');
        return redirect()->back();
    }

    public function activateRoute($slug)
    {
        $route = SpecialRoute::where('slug',$slug)->first();
        $route->state = 'activo';
        $route->save();
        Session::flash('message','Se ha activado correctamente la ruta.');
        return redirect()->back();
    }

    public function points($slug)
    {
        $route = SpecialRoute::where('slug',$slug)->first();
        return view('admin.Special.Route.listPoint',compact('route'));
    }

    public function createPoint($slug)
    {
        $route = SpecialRoute::where('slug',$slug)->first();
        return view('admin.Special.Route.newEditPoint',compact('route'));
    }

    public function storePoint(Request $request, $slug)
    {
        $validator = \Validator::make($request->all(),[
                'title' =>'required',
                'subtitle' =>'required',
                'description' =>'required',
                ]);
        if($validator->fails())
        {
             foreach($validator->errors()->all() as $error)
            {
                Session::flash('message-error',$error);
            }

            return redirect()->back()->withErrors($validator)->withInput();
        }

        $route = SpecialRoute::where('slug',$slug)->first();

        $point = new RoutePoint;
        $point->title = $request->title;
        $point->subtitle = $request->subtitle;
        $point->description = $request->description;
        $point->fk_route = $route->id_route;
        $point->order =  $route->points->count()+1;
        $point->save();

        Session::flash('message','Se ha realizado el registro correctamente.');
        return redirect("admin/specials/routes/points/$route->slug");
    }

    public function editPoint($id)
    {
        $point = RoutePoint::find($id);
        $route = $point->route;
        return view('admin.Special.Route.newEditPoint',compact('route','point'));
    }
    public function updatePoint(Request $request,$id)
    {
        \Validator::make($request->all(),[
                'title' =>'required',
                'subtitle' =>'required',
                'description' =>'required',])->validate();

        $point = RoutePoint::find($id);
        $point->title = $request->title;
        $point->subtitle = $request->subtitle;
        $point->description = $request->description;
        $point->save();
        Session::flash('message','Se ha realizado la actualización correctamente.');
        return redirect("/admin/specials/routes/points/".$point->route->slug);
    }

    public function deleteItem($id)
    {
        $point = RoutePoint::find($id);
        $new = RoutePoint::count();
        for ($i=$new; $i >= $point->order+1; $i--) 
        { 
            $reorder =  RoutePoint::where('order',$i)->first();
            $reorder->order = $i-1;
            $reorder->save();
        }
        RoutePoint::destroy($id);
        Session::flash('message','Se ha realizado la eliminación correctamente.');
        return redirect()->back();
    }

    public function reorderPoint($order,$new)
    {
        $old = RoutePoint::where('order',$order)->first();
        if($new<$old->order)
        {
            for ($i=$new; $i <= $old->order-1; $i++) 
            { 
                $reorder =  RoutePoint::where('order',$i)->first();
                $reorder->order = $i+1;
                $reorder->save();
            }
            $old->order = $new;
            $old->save();
        }
        else
        {
            for ($i=$new; $i >= $old->order+1; $i--) 
            { 
                $reorder =  RoutePoint::where('order',$i)->first();
                $reorder->order = $i-1;
                $reorder->save();
            }
            $old->order = $new;
            $old->save();
        }
        Session::flash('message','Se ha reorganizado correctamente');
        return redirect("/admin/specials/routes/points/".$old->route->slug);
    }

    public function getRelationalDepartment(Request $request)
    {
        $principalDepartment = Department::find($request->relationalDepartments);
        $municipalities = $principalDepartment->Municipalities;

        return response()->json([
            'municipalities' => $municipalities,
        ]);
    }

    public function getRelationalMunicipality(Request $request)
    {
        $municipality = Municipality::find((int)$request->municipality);
        $interestSites = $municipality->InterestSite;

        return response()->json([
            'interestSites' => $interestSites,
        ]);
    }

    public function getInterestSites(Request $request)
    {
        if(count($request->municipalities)>0)
        {
            $sites = [];
            foreach($request->municipalities as $id)
            {
                $municipality = Municipality::find($id);
                $sites[$municipality->municipality_name] = $municipality->InterestSite->pluck('site_name','id_site');
            }

            return response()->json(['sites'=>$sites]);
        }
    }

    public function images($slug)
    {
        $special = Special::whereSlug($slug)->first();
        return view('admin.Special.imagesSpecial', compact('special'));
    }

    public function upload_images($id, ImageRequest $request)
    {
        $files = $request->file('file');

        $new_service = [];

        foreach ($files as $file) {
            $imageName = cargar_imagen($file,"special");
            if ($imageName)  
            {
                $new_image = ['link_image' => $imageName, 'fk_special' => $id];
                $service = SpecialImage::create($new_image);
                $new_service[] = $service->toArray();
                unset($new_image);
            }
        }

        if (count($new_service) > 0) {
            return \Response::json(['response' => 'Subida de imágenes completa'], 200);
        } else {
            return \Response::json(['response' => 'Falla en la subida de imágenes'], 404);
        }
    }

    public function updateImage(Request $request)
    {
        foreach ($request->description as $id => $value) 
        {
            $image = SpecialImage::find($id);
            $image->description = $value;
            $image->save(); 
        }
        Session::flash('message','Se ha realizado la actualización correctamente');
        return redirect()->back();
    }

    public function delete_image($id)
    {
        $image = SpecialImage::find($id);

        $exists = File::exists(public_path($image->link_imagen));
        if ($exists) {
            File::delete(public_path($image->link_imagen));
        }

        if ($image->delete()) 
        {
            \Session::flash('message', 'Imagen Borrada Correctamente');
        } 
        else 
        {
            \Session::flash('message-error', 'La Imagen no puede ser borrada');
        }

        return redirect()->back();
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
