<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\ImageRequest;
use App\Http\Controllers\Controller;
use App\Models\Establishment;
use App\Models\ServiceEstablishment;
use App\Models\Department;
use App\Models\Room;
use App\Models\Plate;
use App\Models\EstablishmentSchedule;
use App\Models\EstablishmentImage;
use App\Models\Municipality;
use App\Operator;
use Session;
use File;

class EstablishmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($type)
    {
        if($type=="restaurant")
        {
            $establishments = Establishment::whereTypeEstablishment('restaurant')->get();
        }
        else
        {
            $establishments = Establishment::whereTypeEstablishment('hotel')->get();
        }
        $operators = Operator::all();
        return view('admin.Establishment.listEstablishment',compact('establishments','type','operators'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($type)
    {
        $services = ServiceEstablishment::all();
        $cant = (int)round(count($services)/3);
        $departments = Department::where('state','activo')->get()->pluck('department_name','id_department')->prepend('','');
        if($type == "restaurant")
        {
            $kitchenType = [
                'Africana'=>'Africana',
                'Alemana'=>'Alemana',
                'Árabe'=>'Árabe',
                'Argentina'=>'Argentina',
                'Asiática'=>'Asiática',
                'Australiana'=>'Australiana',
                'Balti'=>'Balti',
                'Bar'=>'Bar',
                'Bar de vinos'=>'Bar de vinos',
                'Belga'=>'Belga',
                'Brasilera'=>'Brasilera',
                'Británica'=>'Británica',
                'Café'=>'Café',
                'Criolla'=>'Criolla',
                'Camboyana'=>'Camboyana',
                'Canadiense'=>'Canadiense',
                'Cantonés'=>'Cantonés',
                'Caribeña'=>'Caribeña',
                'Centroamericana'=>'Centroamericana',
                'Cervecería'=>'Cervecería',
                'Chilena'=>'Chilena',
                'China'=>'China',
                'China: Xinjiang'=>'China: Xinjiang',
                'Churrasquería'=>'Churrasquería',
                'Colombiana'=>'Colombiana',
                'Comida de calle'=>'Comida de calle',
                'Comida rápida'=>'Comida rápida',
                'Contemporánea'=>'Contemporánea',
                'Coreana'=>'Coreana',
                'Croata'=>'Croata',
                'Cubana'=>'Cubana',
                'Danesa'=>'Danesa',
                'De Hong Kong'=>'De Hong Kong',
                'De la India'=>'De la India',
                'Del Medio Oriente'=>'Del Medio Oriente',
                'Delicatessen'=>'Delicatessen',
                'Ecuatoriana'=>'Ecuatoriana',
                'Escandinava'=>'Escandinava',
                'Española'=>'Española',
                'Estadounidense'=>'Estadounidense',
                'Europea'=>'Europea',
                'Filipina'=>'Filipina',
                'Francesa'=>'Francesa',
                'Fusión'=>'Fusión',
                'Gastropub'=>'Gastropub',
                'Griega'=>'Griega',
                'Hawaiana'=>'Hawaiana',
                'Indonesia'=>'Indonesia',
                'Internacional'=>'Internacional',
                'Irlandesa'=>'Irlandesa',
                'Israelí'=>'Israelí',
                'Italiana'=>'Italiana',
                'Japonesa'=>'Japonesa',
                'Latina'=>'Latina',
                'Libanesa'=>'Libanesa',
                'Mariscos'=>'Mariscos',
                'Mediterránea'=>'Mediterránea',
                'Mexicana'=>'Mexicana',
                'Pakistaní'=>'Pakistaní',
                'Parrilla'=>'Parrilla',
                'Persa'=>'Persa',
                'Peruana'=>'Peruana',
                'Pizza'=>'Pizza',
                'Portuguesa'=>'Portuguesa',
                'Pub'=>'Pub',
                'Puertorriqueña'=>'Puertorriqueña',
                'Saludable'=>'Saludable',
                'Shanghai'=>'Shanghai',
                'Sichuan'=>'Sichuan',
                'Singapurense'=>'Singapurense',
                'Sopas'=>'Sopas',
                'Sudamericana'=>'Sudamericana',
                'Sueca'=>'Sueca',
                'Suiza'=>'Suiza',
                'Sushi'=>'Sushi',
                'Tailandesa'=>'Tailandesa',
                'Taiwanesa'=>'Taiwanesa',
                'Típica'=>'Típica',
                'Turca'=>'Turca',
                'Vasca'=>'Vasca',
                'Venezolana'=>'Venezolana',
                'Vietnamita'=>'Vietnamita',
            ];
        }
        if($type == 'hotel')
        {
            return view('admin.Establishment.createEditEstablishment',compact('services','type','cant','departments'));
        }
        else
        {   
            $days = ['Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo'];
            return view('admin.Establishment.createEditEstablishment',compact('services','type','cant','departments','kitchenType','days'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$type)
    {
        if($type == "restaurant")
        {
            $validator = \Validator::make($request->all(), [
             'establishment_name' => 'required|max:150',
             'fk_municipality'=>'required',
             'kitchen_type'  => 'required',
             'address'  => 'required|max:150',
             'latitude'  => 'required|max:150',
             'longitude'  => 'required|max:150',
             'phone'  => 'required|max:150',
             'description'  => 'required',
             'category'  => 'required',
            ]); 

            if ($validator->fails()) 
            {
                if(isset($validator->errors()->messages()['latitude']) || isset($validator->errors()->messages()['latitude']))
                {
                    $validator->getMessageBag()->add('address', 'El campo dirección es requerido y debe ser una dirección válida.');
                }
                foreach ($validator->errors()->all() as $error)
                {
                    Session::flash('message-error', $error);
                }

                return redirect()->back()->withErrors($validator)->withInput();
            } 

            $establishment = new Establishment;
            $establishment->type_establishment = $type;
            $establishment->establishment_name = $request->establishment_name;
            $establishment->phone = $request->phone;
            $establishment->address = $request->address;
            $establishment->latitude = $request->latitude;
            $establishment->longitude = $request->longitude;
            $establishment->facebook = $request->facebook;
            $establishment->twitter = $request->twitter;
            $establishment->instagram = $request->instagram;
            if($request->video)
            {
                $establishment->video = youtube_match($request->video); 
            }

            if($request->card_image)
            {
                $files = $request->file('card_image');

                $card_image = cargar_imagen($files,'establishment');

                $establishment->card_image = $card_image;
            }
            if($request->introduction_image)
            {
                $files = $request->file('introduction_image');

                $introduction_image = cargar_imagen($files,'establishment');

                $establishment->introduction_image = $introduction_image;
            }
            $establishment->kitchen_type = $request->kitchen_type;

            $establishment->fk_municipality = $request->fk_municipality;
            $establishment->description = $request->description;
            $establishment->save();
            //servicios
            if(isset($request->service))
            {
                $establishment->Services()->sync($request->service);
            }
             //Creación de platos de establecimiento
            if(isset($request->category))
            {
                foreach($request->category as $id => $category)
                {
                    if($category!="")
                    {
                        foreach ($request->plate[$id] as $idPlate => $Rplate) 
                        {
                            if($Rplate)
                            {
                                $plate = new Plate;
                                $plate->plate = $Rplate;
                                $plate->cost = $request->cost[$id][$idPlate];
                                $plate->category = $category;
                                $plate->fk_establishment = $establishment->id_establishment;
                                $plate->save();
                               
                            }
                            
                        }
                    }
                }
            }


            //Horarios de establecimiento
            $days = ['Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo'];

            foreach($days as $day)
            {
                $schedule = new EstablishmentSchedule;
                $schedule->name = $day;
                $schedule->start = $request->$day['inicio'];
                $schedule->end = $request->$day['fin'];
                if(isset($request->$day['noservice']))
                {
                    $schedule->inactive = true;
                }
                $schedule->fk_establishment = $establishment->id_establishment;
                $schedule->save();
            }

            //Activación de municipios
            $municipality = Municipality::find($request->fk_municipality);
            if($municipality->front_state_restaurant!="activo")
            {
                $municipality->front_state_restaurant = "activo";
                $municipality->save();
            }


            Session::flash('message','Establecimiento registrado correctamente');
            return redirect('admin/establishment/'.$type);

        }//end if type
        elseif($type=="hotel")
        {
            $validator = \Validator::make($request->all(), [
             'establishment_name' => 'required|max:150',
             'fk_municipality'=>'required',
             'address'  => 'required|max:150',
             'latitude'  => 'required|max:150',
             'longitude'  => 'required|max:150',
             'phone'  => 'required|max:150',
             'description'  => 'required',
            ]); 

            if ($validator->fails()) 
            {
                foreach ($validator->errors()->all() as $error)
                {
                    Session::flash('message-error', $error);
                }

                return redirect()->back()->withErrors($validator)->withInput();
            } 

            $establishment = new Establishment;
            $establishment->type_establishment = $type;
            $establishment->establishment_name = $request->establishment_name;
            $establishment->phone = $request->phone;
            $establishment->address = $request->address;
            $establishment->latitude = $request->latitude;
            $establishment->longitude = $request->longitude;
            $establishment->facebook = $request->facebook;
            $establishment->twitter = $request->twitter;
            if(isset($request->url_redireccion)) $establishment->url_redireccion = $request->url_redireccion;
            $establishment->instagram = $request->instagram;
            if($request->video)
            {
                $establishment->video = youtube_match($request->video); 
            }

            if($request->card_image)
            {
                $files = $request->file('card_image');

                $card_image = cargar_imagen($files,'establishment');

                $establishment->card_image = $card_image;
            }
            if($request->introduction_image)
            {
                $files = $request->file('introduction_image');

                $introduction_image = cargar_imagen($files,'establishment');

                $establishment->introduction_image = $introduction_image;
            }
            if($request->card_image)
            {
                $files = $request->file('card_image');

                $card_image = cargar_imagen($files,'establishment',$establishment->card_image);

                $establishment->card_image = $card_image;
            }
            if($request->introduction_image)
            {
                $files = $request->file('introduction_image');

                $introduction_image = cargar_imagen($files,'establishment',$establishment->introduction_image);

                $establishment->introduction_image = $introduction_image;
            }
            $establishment->fk_municipality = $request->fk_municipality;
            $establishment->description = $request->description;
            $establishment->save();
            //servicios
            if(isset($request->service))
            {
                $establishment->Services()->sync($request->service);
            }

            if(isset($request->room) && $request->room!='')
            {
                //Creación de cuartos de establecimiento
                foreach($request->room as $id => $room)
                {
                    if($room!='')
                    {
                        $newRoom = new Room;
                        $newRoom->room = $room;
                        $newRoom->cuantity = $request->cuantity[$id];
                        $newRoom->capacity = $request->capacity[$id];
                        $newRoom->bed = $request->bed[$id];
                        if(isset($request->suite[$id]))
                        {
                            $newRoom->suite = true;
                        }
                        $newRoom->fk_establishment = $establishment->id_establishment;
                        $newRoom->save();
                    }
                }
            }
            
            //Horarios de establecimiento

            if($request->checkin!='')
            {
                $schedule = new EstablishmentSchedule;
                $schedule->name = 'checkin';
                $schedule->start = $request->checkin['inicio'];
                $schedule->end = $request->checkin['fin'];
                $schedule->fk_establishment = $establishment->id_establishment;
                $schedule->save();
            }
            if($request->checkout!='')
            {
                 $schedule = new EstablishmentSchedule;
                $schedule->name = 'checkout';
                $schedule->start = $request->checkout['inicio'];
                $schedule->end = $request->checkout['fin'];
                $schedule->fk_establishment = $establishment->id_establishment;
                $schedule->save();
            }

           
            //Activación de municipios
            $municipality = Municipality::find($request->fk_municipality);
            if($municipality->front_state_hotel!="activo")
            {
                $municipality->front_state_hotel = "activo";
                $municipality->save();
            }


            Session::flash('message','Establecimiento registrado correctamente');
            return redirect('admin/establishment/'.$type);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $establishment = Establishment::where('slug',$slug)->first();
        if($establishment)
        {
            $services = ServiceEstablishment::all();
            $cant = (int)round(count($services)/3);
            $departments = Department::where('state','activo')->get()->pluck('department_name','id_department')->prepend('','');
            $type = $establishment->type_establishment;
            $department = Department::find($establishment->Municipality->fk_department)->id_department;
            $municipality = $establishment->Municipality;
            $municipalities = Department::find($department)->Municipalities->pluck('municipality_name','id_municipality');

            if($type == 'restaurant')
            {
                $days = ['Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo'];
                $kitchenType = [
                                'Africana'=>'Africana',
                                'Alemana'=>'Alemana',
                                'Árabe'=>'Árabe',
                                'Argentina'=>'Argentina',
                                'Asiática'=>'Asiática',
                                'Australiana'=>'Australiana',
                                'Balti'=>'Balti',
                                'Bar'=>'Bar',
                                'Bar de vinos'=>'Bar de vinos',
                                'Belga'=>'Belga',
                                'Brasilera'=>'Brasilera',
                                'Británica'=>'Británica',
                                'Café'=>'Café',
                                'Criolla'=>'Criolla',
                                'Camboyana'=>'Camboyana',
                                'Canadiense'=>'Canadiense',
                                'Cantonés'=>'Cantonés',
                                'Caribeña'=>'Caribeña',
                                'Centroamericana'=>'Centroamericana',
                                'Cervecería'=>'Cervecería',
                                'Chilena'=>'Chilena',
                                'China'=>'China',
                                'China: Xinjiang'=>'China: Xinjiang',
                                'Churrasquería'=>'Churrasquería',
                                'Colombiana'=>'Colombiana',
                                'Comida de calle'=>'Comida de calle',
                                'Comida rápida'=>'Comida rápida',
                                'Contemporánea'=>'Contemporánea',
                                'Coreana'=>'Coreana',
                                'Croata'=>'Croata',
                                'Cubana'=>'Cubana',
                                'Danesa'=>'Danesa',
                                'De Hong Kong'=>'De Hong Kong',
                                'De la India'=>'De la India',
                                'Del Medio Oriente'=>'Del Medio Oriente',
                                'Delicatessen'=>'Delicatessen',
                                'Ecuatoriana'=>'Ecuatoriana',
                                'Escandinava'=>'Escandinava',
                                'Española'=>'Española',
                                'Estadounidense'=>'Estadounidense',
                                'Europea'=>'Europea',
                                'Filipina'=>'Filipina',
                                'Francesa'=>'Francesa',
                                'Fusión'=>'Fusión',
                                'Gastropub'=>'Gastropub',
                                'Griega'=>'Griega',
                                'Hawaiana'=>'Hawaiana',
                                'Indonesia'=>'Indonesia',
                                'Internacional'=>'Internacional',
                                'Irlandesa'=>'Irlandesa',
                                'Israelí'=>'Israelí',
                                'Italiana'=>'Italiana',
                                'Japonesa'=>'Japonesa',
                                'Latina'=>'Latina',
                                'Libanesa'=>'Libanesa',
                                'Mariscos'=>'Mariscos',
                                'Mediterránea'=>'Mediterránea',
                                'Mexicana'=>'Mexicana',
                                'Pakistaní'=>'Pakistaní',
                                'Parrilla'=>'Parrilla',
                                'Persa'=>'Persa',
                                'Peruana'=>'Peruana',
                                'Pizza'=>'Pizza',
                                'Portuguesa'=>'Portuguesa',
                                'Pub'=>'Pub',
                                'Puertorriqueña'=>'Puertorriqueña',
                                'Saludable'=>'Saludable',
                                'Shanghai'=>'Shanghai',
                                'Sichuan'=>'Sichuan',
                                'Singapurense'=>'Singapurense',
                                'Sopas'=>'Sopas',
                                'Sudamericana'=>'Sudamericana',
                                'Sueca'=>'Sueca',
                                'Suiza'=>'Suiza',
                                'Sushi'=>'Sushi',
                                'Tailandesa'=>'Tailandesa',
                                'Taiwanesa'=>'Taiwanesa',
                                'Típica'=>'Típica',
                                'Turca'=>'Turca',
                                'Vasca'=>'Vasca',
                                'Venezolana'=>'Venezolana',
                                'Vietnamita'=>'Vietnamita',
                            ];
                return view('admin.Establishment.createEditEstablishment',compact('establishment','services','type','cant','departments','kitchenType','days','department','municipalities','municipality'));
            }
            else
            {
                return view('admin.Establishment.createEditEstablishment',compact('establishment','services','type','cant','departments','municipalities','municipality','department'));
            }
        }
        else
        {
            Session::flash('message-error','No se ha encontrado el establecimiento.');
            return redirect()->back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $slug
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug)
    {
        $establishment = Establishment::where('slug',$slug)->first();
        if($establishment)
        {
            if($establishment->type_establishment == "restaurant")
            {
                $validator = \Validator::make($request->all(), [
                 'establishment_name' => 'required|max:150',
                 'kitchen_type'  => 'required',
                 'address'  => 'required|max:150',
                 'latitude'  => 'required|max:150',
                 'longitude'  => 'required|max:150',
                 'phone'  => 'required|max:150',
                 'description'  => 'required',
                 'category'  => 'required',
                ]); 

                if ($validator->fails()) 
                {
                    if(isset($validator->errors()->messages()['latitude']) || isset($validator->errors()->messages()['latitude']))
                    {
                        $validator->getMessageBag()->add('address', 'El campo dirección es requerido y debe ser una dirección válida.');
                    }
                    foreach ($validator->errors()->all() as $error)
                    {
                        Session::flash('message-error', $error);
                    }

                    return redirect()->back()->withErrors($validator)->withInput();
                } 

                $establishment->establishment_name = $request->establishment_name;
                $establishment->phone = $request->phone;
                $establishment->address = $request->address;
                $establishment->latitude = $request->latitude;
                $establishment->longitude = $request->longitude;
                $establishment->facebook = $request->facebook;
                $establishment->twitter = $request->twitter;
                $establishment->instagram = $request->instagram;
                if($request->video)
                {
                    $establishment->video = youtube_match($request->video); 
                }
                if($request->card_image)
                {
                    $files = $request->file('card_image');

                    $card_image = cargar_imagen($files,'establishment',$establishment->card_image);

                    $establishment->card_image = $card_image;
                }
                if($request->introduction_image)
                {
                    $files = $request->file('introduction_image');

                    $introduction_image = cargar_imagen($files,'establishment',$establishment->introduction_image);

                    $establishment->introduction_image = $introduction_image;
                }
                $establishment->kitchen_type = $request->kitchen_type;

                $oldId = $establishment->fk_municipality;

                $establishment->fk_municipality = $request->fk_municipality;

                $establishment->description = $request->description;
                $establishment->save();

                //DESACTIVACION DE MUNICIPIO EN CASO DE SER DIFERENTE
                if($oldId != $request->fk_municipality)
                {
                    $oldMunicipality = Municipality::find($oldId);
                     //Desativación del anterior
                    $operatorsOldMunicipality = count($oldMunicipality->Restaurants);
                    if($operatorsOldMunicipality<1)
                    {
                        $oldMunicipality->front_state_restaurant = "inactivo";
                        $oldMunicipality->save();
                    }

                    $municipality = Municipality::find($request->fk_municipality);
                    if($municipality->front_state_restaurant!="activo")
                    {
                        $municipality->front_state_restaurant = "activo";
                        $municipality->save();
                    }
                    
                }

                //FIN DESACTIVACIÓN MUNICIPIO

                //servicios
                if(isset($request->service))
                {
                    $establishment->Services()->sync($request->service);
                }

                if(count($establishment->Plates)>0)
                {
                    foreach($establishment->Plates as $oldPlate)
                    {
                        $oldPlate->delete();
                    }
                }
                
                if(isset($request->category))
                {
                    //Creación de platos de establecimiento
                    foreach($request->category as $id => $category)
                    {
                        if($category!="")
                        {
                            foreach ($request->plate[$id] as $idPlate => $Rplate) 
                            {

                                if($Rplate)
                                {
                                    $plate = new Plate;
                                    $plate->plate = $Rplate;
                                    $plate->cost = $request->cost[$id][$idPlate];
                                    $plate->category = $category;
                                    $plate->fk_establishment = $establishment->id_establishment;
                                    $plate->save();
                                }
                                
                            }
                        }
                    }
                }

                 //Horarios de establecimiento
                $days = ['Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo'];

                foreach($days as $day)
                {
                    $schedule = new EstablishmentSchedule;
                    $schedule->name = $day;
                    $schedule->start = $request->$day['inicio'];
                    $schedule->end = $request->$day['fin'];
                    if(isset($request->$day['noservice']))
                    {
                        $schedule->inactive = true;
                    }
                    $schedule->fk_establishment = $establishment->id_establishment;
                    $schedule->save();
                }


                
                Session::flash('message','Establecimiento actualizado correctamente');
                return redirect('admin/establishment/'.$establishment->type_establishment);

            }//end if type
            elseif($establishment->type_establishment == "hotel")
            {
                $validator = \Validator::make($request->all(), [
             'establishment_name' => 'required|max:150',
             'fk_municipality'=>'required',
             'address'  => 'required|max:150',
             'latitude'  => 'required|max:150',
             'longitude'  => 'required|max:150',
             'phone'  => 'required|max:150',
             'description'  => 'required',
            ]); 

            if ($validator->fails()) 
            {   
                foreach ($validator->errors()->all() as $error)
                {
                    Session::flash('message-error', $error);
                }

                return redirect()->back()->withErrors($validator)->withInput();
            } 

                $establishment->establishment_name = $request->establishment_name;
                $establishment->phone = $request->phone;
                $establishment->address = $request->address;
                $establishment->latitude = $request->latitude;
                $establishment->longitude = $request->longitude;
                $establishment->facebook = $request->facebook;
                $establishment->twitter = $request->twitter;
                $establishment->instagram = $request->instagram;
                if(isset($request->url_redireccion)) $establishment->url_redireccion = $request->url_redireccion;
                if($request->video)
                {
                    $establishment->video = youtube_match($request->video); 
                }
                if($request->card_image)
                {
                    $files = $request->file('card_image');

                    $card_image = cargar_imagen($files,'establishment',$establishment->card_image);

                    $establishment->card_image = $card_image;
                }
                if($request->introduction_image)
                {
                    $files = $request->file('introduction_image');

                    $introduction_image = cargar_imagen($files,'establishment',$establishment->introduction_image);

                    $establishment->introduction_image = $introduction_image;
                }
                $oldId = $establishment->fk_municipality;

                $establishment->fk_municipality = $request->fk_municipality;

                $establishment->description = $request->description;
                $establishment->save();

                //DESACTIVACION DE MUNICIPIO EN CASO DE SER DIFERENTE
                if($oldId != $request->fk_municipality)
                {
                    $oldMunicipality = Municipality::find($oldId);
                     //Desativación del anterior
                    $operatorsOldMunicipality = count($oldMunicipality->Restaurants);
                    if($operatorsOldMunicipality<1)
                    {
                        $oldMunicipality->front_state_hotel  = "inactivo";
                        $oldMunicipality->save();
                    }

                    $municipality = Municipality::find($request->fk_municipality);
                    if($municipality->front_state_hotel !="activo")
                    {
                        $municipality->front_state_hotel  = "activo";
                        $municipality->save();
                    }
                    
                }

                //FIN DESACTIVACIÓN MUNICIPIO

                //servicios
                if(isset($request->service))
                {
                    $establishment->Services()->sync($request->service);
                }

                if(count($establishment->Rooms)>0)
                {
                    foreach($establishment->Rooms as $room)
                    {
                        $room->delete();
                    }  
                }

                if(isset($request->room) && $request->room!='')
                {

                    //Creación de cuartos de establecimiento
                    foreach($request->room as $id => $room)
                    {
                        $newRoom = new Room;
                        $newRoom->room = $room;
                        $newRoom->cuantity = $request->cuantity[$id];
                        $newRoom->capacity = $request->capacity[$id];
                        $newRoom->bed = $request->bed[$id];
                        if(isset($request->suite[$id]))
                        {
                            $newRoom->suite = true;
                        }
                        $newRoom->fk_establishment = $establishment->id_establishment;
                        $newRoom->save();
                    }
                }

                //Horarios de establecimiento
            {
               
            }
            {
                 
            }
                if($request->checkin!='')
                {
                    $schedule = EstablishmentSchedule::where('name','checkin')->where('fk_establishment',$establishment->id_establishment)->first();
                    if($schedule)
                    {
                        $schedule->start = $request->checkin['inicio'];
                        $schedule->end = $request->checkin['fin'];
                        $schedule->save();
                    }
                    else
                    {
                        $schedule = new EstablishmentSchedule;
                        $schedule->name = 'checkin';
                        $schedule->start = $request->checkin['inicio'];
                        $schedule->end = $request->checkin['fin'];
                        $schedule->fk_establishment = $establishment->id_establishment;
                        $schedule->save();
                    }
                }
                if($request->checkout!='')
                {
                    $schedule = EstablishmentSchedule::where('name','checkout')->where('fk_establishment',$establishment->id_establishment)->first();
                    if($schedule)
                    {
                        $schedule->start = $request->checkout['inicio'];
                        $schedule->end = $request->checkout['fin'];
                        $schedule->save();
                    }
                    else
                    {
                        $schedule = new EstablishmentSchedule;
                        $schedule->name = 'checkout';
                        $schedule->start = $request->checkout['inicio'];
                        $schedule->end = $request->checkout['fin'];
                        $schedule->fk_establishment = $establishment->id_establishment;
                        $schedule->save();
                    }
                }


                Session::flash('message','Establecimiento actualizado correctamente');
                return redirect('admin/establishment/'.$establishment->type_establishment);
            
            }
        }
        else
        {
            Session::flash('message-error','No se ha encontrado el establecimiento.');
            return redirect()->back();
        }
    }

    public function change(Request $request)
    {
        try
        {
            $establishment = Establishment::find($request->cod);
            if($establishment->outstanding==0)
                $establishment->outstanding = 1;
            else
                $establishment->outstanding = 0;
            
            $establishment->save();
            return 'ok';
        }
        catch(\Exception $e)
        {
            return 'error';
        }
    }

    public function transfer(Request $request)
    {
        $establishment = Establishment::find($request->establishment);
        if($establishment->internal_state == 'admin')
        {
            $establishment->fk_operator = $request->operator;
            $establishment->internal_state = 'operator';
            $establishment->save();
            Session::flash('message','Se ha realizado la asignación correctamente');
        }
        else
        {
            if(isset($request->administrador))
            {
                $establishment->fk_operator = null;
                $establishment->internal_state = 'admin';
                $establishment->save();
                Session::flash('message','Se ha realizado la asignación correctamente');
            }
            else
                Session::flash('message-error','No se han realizado cambios');
            
        }
        return redirect("admin/establishment/$establishment->type_establishment");
    }
    public function desactivateOperator($id)
    {
        $operator = Operator::find($id);
        $operator->status = 'inactive';
        $operator->save();
        foreach($operator->directService as $services)
        {
            $services->state = 'Desactivado por administrador';
            $services->save();
        }
        foreach($operator->restaurants as $restaurant)
        {
            $restaurant->state = 'Desactivado por administrador';
            $restaurant->save();
        }
        foreach($operator->hotels as $hotel)
        {
            $hotel->state = 'Desactivado por administrador';
            $hotel->save();
        }
        Session::flash('message','Se ha desactivado al operador, sus actividades, hoteles y restaurantes');
        return redirect('admin/operators');
    }

    public function activateOperator($id)
    {
        $operator = Operator::find($id);
        $operator->status = 'active';
        $operator->save();
        foreach($operator->directService as $services)
        {
            $services->state = 'inactivo';
            $services->save();
        }
        foreach($operator->restaurants as $restaurant)
        {
            $restaurant->state = 'inactivo';
            $restaurant->save();
        }
        foreach($operator->hotels as $hotel)
        {
            $hotel->state = 'inactivo';
            $hotel->save();
        }
        Session::flash('message','Se ha activado al operador y sus actividades han quedado inactivas, pero el operador puede activarlas');
        return redirect('admin/operators');
    }


    public function upload_images($id,ImageRequest $request)
    {
        $files = $request->file('file');
        $establishment = Establishment::find($id);
        $new_service = [];
        foreach ($files as $file) {
            $imageName = cargar_imagen($file,$establishment->type_establishment);
            if ($imageName)  
            {
                $new_image = ['link_image' => $imageName, 'fk_establishment' => $id];
                $imageEstablishment = EstablishmentImage::create($new_image);
                $new_service[] = $imageEstablishment->toArray();
                unset($new_image);
            }
        }

        if (count($new_service) > 0) {
            return \Response::json(['response' => 'Subida de imágenes completa'], 200);
        } else {
            return \Response::json(['response' => 'Falla en la subida de imágenes2'], 404);
        }
    }

    public function delete_image($id)
    {
        $image = EstablishmentImage::find($id);

        $exists = File::exists(public_path($image->link_imagen));
        if ($exists) {
            File::delete(public_path($image->link_imagen));
        }

        if ($image->delete()) 
        {
            \Session::flash('message', 'Imagen Borrada Correctamente');
        } 
        else 
        {
            \Session::flash('message-error', 'La Imagen no puede ser borrada');
        }

        return redirect()->back();
    }

    public function desactivate($slug)
    {
        $establishment = Establishment::where('slug',$slug)->first();
        if($establishment)
        {
             $establishment->state = 'inactivo';
            $establishment->save();

            Session::flash('message','Establecimiento desactivado correctamente');
            return redirect('admin/establishment/'.$establishment->type_establishment);
        }
        else
        {
            Session::flash('message-error','No se ha encontrado el establecimiento.');
            return redirect()->back();
        }
    } 

    public function images($slug)
    {
        $establishment = Establishment::where('slug',$slug)->first();
        if($establishment)
        {
            return view('admin.Establishment.imageEstablishment', compact('establishment'));
        }
        else
        {
            Session::flash('message-error','No se ha encontrado el establecimiento.');
            return redirect()->back();
        }
    }

    public function update_images(Request $request)
    {
        foreach ($request->description as $id => $value) 
        {
            $image = EstablishmentImage::find($id);
            $image->description = $value;
            $image->save();
        }
        Session::flash('message','Se ha realizado la actualizacíón correctamente');
        return redirect()->back();
    }

    public function activate($slug)
    {
        $establishment = Establishment::where('slug',$slug)->first();
        if($establishment)
        {
            $establishment->state = 'activo';
            $establishment->save();
            
            Session::flash('message','Establecimiento activado correctamente');
            return redirect('admin/establishment/'.$establishment->type_establishment);
        }
        else
        {
            Session::flash('message-error','No se ha encontrado el establecimiento.');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
