<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Customer;
use Session;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = Customer::all();
        return view('admin.Customer.listCustomer',compact('customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.Customer.createEditCustomer');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
             'nit'=>'required',
             'name'=>'required',
             'contact' => 'required|max:50',
             'phone_contact'  => 'required',
             'email_contact'  => 'required'
            ]); 

        if ($validator->fails()) 
        {
            foreach ($validator->errors()->all() as $error)
            {
                Session::flash('message-error', $error);
            }

            return redirect()->back()->withErrors($validator)->withInput();
        } 
        else 
        {
            $customer = new Customer;
            $customer->nit = $request->nit;
            $customer->name = $request->name;
            $customer->contact = $request->contact;
            $customer->phone_contact = $request->phone_contact;
            $customer->email_contact = $request->email_contact;
            $customer->save();


            Session::flash('message', 'Se ha realizado correctamente el registro de información.');
            return redirect('admin/customers');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $customer = Customer::where('slug',$slug)->first();
        if($customer)
        {
            return view('admin.Customer.createEditCustomer',compact('customer'));
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado el cliente.");
            return redirect('admin/customers');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
             'nit'=>'required',
             'name'=>'required',
             'contact' => 'required|max:50',
             'phone_contact'  => 'required',
             'email_contact'  => 'required'
            ]); 

        if ($validator->fails()) 
        {
            foreach ($validator->errors()->all() as $error)
            {
                Session::flash('message-error', $error);
            }

            return redirect()->back()->withErrors($validator)->withInput();
        } 
        else 
        {
            $customer = Customer::find($id);
            $customer->nit = $request->nit;
            $customer->name = $request->name;
            $customer->contact = $request->contact;
            $customer->phone_contact = $request->phone_contact;
            $customer->email_contact = $request->email_contact;
            $customer->save();


            Session::flash('message', 'Se ha realizado correctamente la actualización de información.');
            return redirect('admin/customers');
        }
    }

    public function desactivate($id)
    {
        $customer = Customer::find($id);
        if($customer)
        {
            $customer->state = "inactivo";
            $customer->save();
            Session::flash('message','Se ha desactivado el cliente correctamente');
            return redirect('admin/customers');
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado el cliente.");
            return redirect('admin/customers');
        }
    }

    public function activate($id)
    {
        $customer = Customer::find($id);
        if($customer)
        {
            $customer->state = "activo";
            $customer->save();
            Session::flash('message','Se ha activado el cliente correctamente');
            return redirect('admin/customers');
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado el cliente.");
            return redirect('admin/customers');
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
