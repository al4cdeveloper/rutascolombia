<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Wallpaper;
use File;
use Session;
class WallpaperController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $wallpapers = Wallpaper::all();
        return view('admin.Wallpaper.listWallpaper',compact('wallpapers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
             'primary_text'=>'required',
             'secundary_text'=>'required',
             'url_redirection'=>'required',
             'link_image'  => 'required',
             'primary_color'=>'required',
             'secundary_color'=>'required',
            ]); 

        if ($validator->fails()) 
        {
            foreach ($validator->errors()->all() as $error)
            {
                Session::flash('message-error', $error);
            }

            return redirect()->back()->withErrors($validator)->withInput();
        } 
        else 
        {
           
            $wall = new Wallpaper;
            $wall->primary_text = $request->primary_text;
            $wall->secundary_text = $request->secundary_text;
            $wall->url_redirection = $request->url_redirection;
            $wall->primary_color = $request->primary_color;
            $wall->secundary_color = $request->secundary_color;

            $files = $request->file('link_image');
            $image = $this->cargar_imagen($files);

            $wall->link_image = $image;
            $wall->save();
            Session::flash('message', 'Se ha realizado correctamente el registro de información.');
            return redirect('admin/wallpapers');
        }
    }

    public function create()
    {
        return view('admin.Wallpaper.newEditWallpaper');
    }

    public function edit($id)
    {
        $wall = Wallpaper::find($id);
        if($wall)
        {
            return view('admin.Wallpaper.newEditWallpaper',compact('wall'));
        }
        else
            return redirect()->back();
    }

    private function cargar_imagen($file, $imageName = false)
    {
        if ($imageName) 
        {
            $exists = File::exists(public_path("wallpapers/".$imageName));
            if ($exists) 
            {
                File::delete(public_path("wallpapers/".$imageName));
            }

            $image = explode('.', $imageName);
            $imageName = $image[0].'.'.$file->getClientOriginalExtension();
        } 
        else 
        {
            $imageName = 'circuit'.date('YmdHis', time()).rand().'.'.$file->getClientOriginalExtension();
        }

        $file->move(public_path('wallpapers'), $imageName);

        $exists = File::exists(public_path("wallpapers/".$imageName));

        if ($exists) 
        {
            return 'wallpapers/'.$imageName;
        } 
        else 
        {
            return false;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
             'primary_text'=>'required',
             'secundary_text'=>'required',
             'url_redirection'=>'required',
             'primary_color'=>'required',
             'secundary_color'=>'required',
            ]); 

        if ($validator->fails()) 
        {
            foreach ($validator->errors()->all() as $error)
            {
                Session::flash('message-error', $error);
            }

            return redirect()->back()->withErrors($validator)->withInput();
        } 
        else 
        {
            $wall = Wallpaper::find($id);
            $wall->primary_text = $request->primary_text;
            $wall->secundary_text = $request->secundary_text;
            $wall->url_redirection = $request->url_redirection;
            $wall->primary_color = $request->primary_color;
            $wall->secundary_color = $request->secundary_color;

            if($request->link_image)
            {
                if($wall->link_image)
                {
                    $exists = File::exists(public_path($wall->link_image));
                    if ($exists) 
                    {
                        File::delete(public_path($wall->link_image));
                    }
                }
                $files = $request->file('link_image');
                $image = $this->cargar_imagen($files);
                $wall->link_image = $image;
            }

            $wall->save();
            Session::flash('message', 'Se ha realizado correctamente la actualización de información.');
            return redirect('admin/wallpapers');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
