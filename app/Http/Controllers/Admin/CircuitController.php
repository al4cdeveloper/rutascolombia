<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
//Este archivo se requiere crear manualmente php artisan make:request ImageRequest
use App\Http\Requests\ImageRequest;

use App\Http\Controllers\Controller;
use App\Models\Circuit;
use App\Models\Department;
use App\Models\Municipality;
use App\Models\RelationCircuit;
use App\Models\InterestSite;
use App\Models\CircuitDetailItem;
use App\Models\CircuitImage;
use Session;
use File;

class CircuitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $circuits = Circuit::all();
        return view('admin.Circuit.listCircuit',compact('circuits'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.Circuit.newEditCircuit');
    }

    public function getDepartment(Request $request)
    {
        $departments = Department::where('id_department','!=',$request->idDepartment)->where('state','activo')->get();
        $municipalities = Municipality::where('fk_department',$request->idDepartment)->where('state','activo')->get();
        $sites = [];
        foreach($municipalities as $municipality)
        {
            $sites[$municipality->municipality_name] = $municipality->InterestSite->pluck('site_name','id_site');
        }
        return response()->json([
            'departments' => $departments,
            'municipalities' => $municipalities,
            'sites'=>$sites,
        ]);
    }

    public function getRelationalDepartment(Request $request)
    {
        $principalDepartment = Department::find($request->principal);
        $municipalities[$principalDepartment->department_name]= $principalDepartment->Municipalities->toArray();

        foreach ($request->relationalDepartments as $department) 
        {
            $thisDepartment = Department::find($department);
            $municipalities[$thisDepartment->department_name] = $thisDepartment->Municipalities->toArray();
        }

        $sites = [];
        foreach($municipalities as $department)
        {
            foreach ($department as $municipality) 
            {

                $thisMunicipality = Municipality::find($municipality['id_municipality']);
                $sites[$thisMunicipality->municipality_name] = $thisMunicipality->InterestSite;
            }
        }
        return response()->json([
            'municipalities' => $municipalities,
            'sites'=>$sites,
        ]);
    }

    public function getMunicipality(Request $request)
    {
        $municipality = Municipality::where('id_municipality',$request->idMunicipality)->first();
        $municipalities = Municipality::where('id_municipality','!=',$request->idMunicipality)->where('state','activo')->where('fk_department',$municipality->fk_department)->get();
        $sites = [];
        foreach($municipalities as $municipalityF)
        {
            $sites[$municipalityF->municipality_name] = $municipalityF->InterestSite->pluck('site_name','id_site');
        }
        $sites[$municipality->municipality_name] = $municipality->InterestSite->pluck('site_name','id_site');
        return response()->json([
            'municipalities' => $municipalities,
            'sites'=>$sites,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
             'name'=>'required',
             'short_description'=>'required',
             'keywords'  => 'required',
             'description'  => 'required',
             'recommendations'  => 'required',
            ])->validate(); 


        $circuit = new Circuit;
       
        $circuit->name = $request->name;
        $circuit->short_description = $request->short_description;
        $circuit->description = $request->description;
        $circuit->keywords = $request->keywords;
        $circuit->recommendations = $request->recommendations;
        $circuit->text_images = $request->text_images;
        $circuit->video = $request->video;
        if($request->card_image)
        {
            $files = $request->file('card_image');

            $card_image = cargar_imagen($files,'special');

            $circuit->card_image = $card_image;
        }
        if($request->introduction_image)
        {
            $files = $request->file('introduction_image');

            $introduction_image = cargar_imagen($files,'circuit');

            $circuit->introduction_image = $introduction_image;
        }
        if($request->description_image)
        {
            $files = $request->file('description_image');

            $description_image = cargar_imagen($files,'circuit');

            $circuit->description_image = $description_image;
        }

        if($request->recommendation_image)
        {
            $files = $request->file('recommendation_image');

            $recommendation_image = cargar_imagen($files,'circuit');

            $circuit->recommendation_image = $recommendation_image;
        }
        if($request->download_file)
        {
            $files = $request->file('download_file');

            $download_file = cargar_imagen($files,'circuit');

            $circuit->download_file = $download_file;
        }

        $circuit->save();
        Session::flash('message', 'Se ha realizado correctamente el registro de información.');
        if($request->typesubmit=="guardartodo")
        {
            return redirect('admin/circuits/edit/'.$circuit->slug);
        }
        else
        return redirect('admin/circuits');  
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $circuit = Circuit::where('slug',$slug)->first();
        // if($circuit->type_relation=='department')
        // {
        //     $departments = Department::where('state','activo')->where('id_department','!=',$circuit->fk_relation)->get();
        // }
        // else
        // {
            $departments = Department::where('state','activo')->get();
        // }
        // if($circuit->type_relation=='municipality')
        // {
        //     $municipalities = Municipality::where('state','activo')->where('id_municipality','!=',$circuit->fk_relation)->get();
        // }
        // else
        // {
            $municipalities = Municipality::where('state','activo')->get();
        // }
        $sites = InterestSite::where('state','activo')->get();
        return view('admin.Circuit.newEditCircuit',compact('circuit','municipalities','departments','sites'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
             'name'=>'required',
             'short_description'=>'required',
             'keywords'  => 'required',
             'description'  => 'required',
             'recommendations'  => 'required',
            ])->validate(); 


        $circuit = Circuit::find($id);
       
        $circuit->name = $request->name;
        $circuit->short_description = $request->short_description;
        $circuit->description = $request->description;
        $circuit->keywords = $request->keywords;
        $circuit->recommendations = $request->recommendations;
        $circuit->text_images = $request->text_images;
        $circuit->video = $request->video;
        if($request->card_image)
        {
            $files = $request->file('card_image');

            $card_image = cargar_imagen($files,'special',$circuit->card_image);

            $circuit->card_image = $card_image;
        }
        if($request->introduction_image)
        {
            $files = $request->file('introduction_image');

            $introduction_image = cargar_imagen($files,'circuit',$circuit->introduction_image);

            $circuit->introduction_image = $introduction_image;
        }
        if($request->description_image)
        {
            $files = $request->file('description_image');

            $description_image = cargar_imagen($files,'circuit',$circuit->description_image);

            $circuit->description_image = $description_image;
        }

        if($request->recommendation_image)
        {
            $files = $request->file('recommendation_image');

            $recommendation_image = cargar_imagen($files,'circuit',$circuit->recommendation_image);

            $circuit->recommendation_image = $recommendation_image;
        }
        if($request->download_file)
        {
            $files = $request->file('download_file');

            $download_file = cargar_imagen($files,'circuit',$circuit->download_file);

            $circuit->download_file = $download_file;
        }

        $circuit->save();
        Session::flash('message', 'Se ha realizado correctamente el registro de información.');
        if($request->typesubmit=="guardartodo")
        {
            return redirect('admin/circuits/edit/'.$circuit->slug);
        }
        else
        return redirect('admin/circuits');  
    }

    public function detailitems($slug)
    {
        $circuit = Circuit::where('slug',$slug)->first();
        return view('admin.Circuit.detailitemsCircuit',compact('circuit'));
    }

    public function createDetailItems($slug)
    {
        $departments = Department::where('state','activo')->pluck('department_name','id_department')->prepend('');
        $circuit = Circuit::where('slug',$slug)->first();
        return view('admin.Circuit.createEditDetailItem',compact('departments','circuit'));
    }

    public function storeDetailItems(Request $request, $id)
    {
        \Validator::make($request->all(),['description'=>'required',
                        'fk_municipality'=>'required'])->validate();
        $item = new CircuitDetailItem;
        $item->description = $request->description;
        $item->type_relation = $request->type_relation;
        if($request->type_relation == 'municipality')
        {
            $item->fk_relation = $request->fk_municipality;
        }
        else
        {
            $item->fk_relation = $request->fk_site;
        }
        if($request->image_link)
        {
            $files = $request->file('image_link');
            $image_link = cargar_imagen($files,'circuitdetailitem');
            $item->image_link = $image_link;
        }
        $item->fk_circuit = $id;
        $item->save();

        Session::flash('message','Se ha realizado correctamente el regisitro de la información.');
        return redirect('/admin/circuits/detailitems/'.$item->circuit->slug);
    }

    public function editDetailItems($id)
    {
        $item = CircuitDetailItem::find($id);
        $circuit = $item->circuit;
        $departments = Department::where('state','activo')->pluck('department_name','id_department')->prepend('');
        if($item->type_startpoint=='municipality')
        {
            $itemmunicipality = Municipality::find($item->fk_relation);
            $departmentStart = $itemmunicipality->department;
            $municipalitiesStart = $departmentStart->municipalities->pluck('municipality_name','id_municipality');
            $sitesStart = $itemmunicipality->InterestSite->pluck('site_name','id_site');
        }
        else
        {
            $siteStart = InterestSite::find($item->fk_relation);
            $itemmunicipality = $siteStart->municipality;
            $departmentStart = $itemmunicipality->department;
            $municipalitiesStart = $departmentStart->municipalities->pluck('municipality_name','id_municipality');
            $sitesStart = $itemmunicipality->InterestSite->pluck('site_name','id_site');
        }
        return view('admin.Circuit.createEditDetailItem',compact('item','circuit','departments','departmentStart','municipalitiesStart','itemmunicipality','sitesStart'));

    }

    public function updateDetailItems(Request $request, $id)
    {
        \Validator::make($request->all(),['description'=>'required',
                        'fk_municipality'=>'required'])->validate();
        $item = CircuitDetailItem::find($id);
        $item->description = $request->description;
        $item->type_relation = $request->type_relation;
        if($request->type_relation == 'municipality')
        {
            $item->fk_relation = $request->fk_municipality;
        }
        else
        {
            $item->fk_relation = $request->fk_site;
        }
        if($request->image_link)
        {
            $files = $request->file('image_link');
            $image_link = cargar_imagen($files,'circuitdetailitem',$item->image_link);
            $item->image_link = $image_link;
        }
        $item->save();
        Session::flash('message','Se ha realizado correctamente el regisitro de la información.');
        return redirect('/admin/circuits/detailitems/'.$item->circuit->slug);
    }

    public function destroyDetailItem($id)
    {
        CircuitDetailItem::destroy($id);
        Session::flash('message','Se ha eliminado correctamente el item.');
        return redirect()->back();
    }

    public function images($slug)
    {
        $circuit = Circuit::where('slug',$slug)->first();
        return view('admin.Circuit.imagesCircuit', compact('circuit'));
    }

    public function upload_images($id, ImageRequest $request)
    {
        $files = $request->file('file');

        $new_service = [];

        foreach ($files as $file) {
            $imageName = cargar_imagen($file,'circuit');

            if ($imageName)  
            {
                $new_image = ['link_image' => $imageName, 'fk_circuit' => $id];
                $service = CircuitImage::create($new_image);
                $new_service[] = $service->toArray();
                unset($new_image);
            }
        }

        if (count($new_service) > 0) {
            return \Response::json(['response' => 'Subida de imágenes completa'], 200);
        } else {
            return \Response::json(['response' => 'Falla en la subida de imágenes2'], 404);
        }
    }

    public function delete_image($id)
    {
        $image = CircuitImage::find($id);

        $exists = File::exists(public_path($image->link_imagen));
        if ($exists) {
            File::delete(public_path($image->link_imagen));
        }

        if ($image->delete()) 
        {
            \Session::flash('message', 'Imagen Borrada Correctamente');
        } 
        else 
        {
            \Session::flash('message-error', 'La Imagen no puede ser borrada');
        }

        return redirect()->back();
    }
   
    public function desactivate($id)
    {
        $circuit = Circuit::find($id);
        if($circuit)
        {
            $circuit->state = "inactivo";
            $circuit->save();
            Session::flash('message','Se ha desactivado el circuito correctamente');
            return redirect('admin/circuits');
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado el circuito.");
            return redirect('admin/circuits');
        }
    }

    public function activate($id)
    {
        $circuit = Circuit::find($id);
        if($circuit)
        {
            $circuit->state = "activo";
            $circuit->save();
            Session::flash('message','Se ha activado el circuito correctamente');
            return redirect('admin/circuits');
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado el circuito.");
            return redirect('admin/circuits');
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
    // Made by Sergio Hernandez 2018 😎
// support sergio.hernandez.goyeneche@gmail.com 
