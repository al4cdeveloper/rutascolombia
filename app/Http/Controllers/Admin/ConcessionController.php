<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Concession;
use App\Models\Department;
use App\Models\Municipality;
use App\Models\InterestSite;
use App\Models\InterestSiteCategory;
use App\Models\Activity;
use App\Models\Toll;
use Validator;
use Session;

class ConcessionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $concessions = Concession::all();
        return view('admin.Concession.listConcession',compact('concessions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.Concession.newEditConcession');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(),['name'=>'required',
                'keywords'=>'required',
                'description'=>'required'])->validate();

        $concession = new Concession;
        if($request->logo)
        {
            $files = $request->file('logo');
            $logo = cargar_imagen($files,'concession');
            $concession->logo = $logo;
        }
        $concession->name = $request->name;
        $concession->acronym = $request->acronym;
        $concession->keywords = $request->keywords;
        $concession->description = $request->description;
        if($request->description_image)
        {
            $files = $request->file('description_image');
            $description_image = cargar_imagen($files,'concession');
            $concession->description_image = $description_image;
        }
        $concession->save();
        Session::flash('message', 'Se ha realizado correctamente el registro de información.');
        if($request->typesubmit=="guardartodo")
        {
            return redirect('admin/concessions/edit/'.$concession->slug);
        }
        else
        return redirect('admin/concessions');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $concession = Concession::where('slug',$slug)->firstOrFail();
        return view('admin.Concession.newEditConcession',compact('concession'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $concession = Concession::find($id);
        if($request->logo)
        {
            $files = $request->file('logo');
            $logo = cargar_imagen($files,'concession',$concession->logo);
            $concession->logo = $logo;
        }
        $concession->name = $request->name;
        $concession->acronym = $request->acronym;
        $concession->keywords = $request->keywords;
        $concession->description = $request->description;
        if($request->description_image)
        {
            $files = $request->file('description_image');
            $description_image = cargar_imagen($files,'concession',$concession->description_image);
            $concession->description_image = $description_image;
        }
        $concession->save();

        Session::flash('message', 'Se ha realizado correctamente la actualización de la información.');
        if($request->typesubmit=="guardartodo")
        {
            return redirect('admin/concessions/edit/'.$concession->slug);
        }
        else
        return redirect('admin/concessions');
    }

    public function desactivate($id)
    {
        $concession = Concession::find($id);
        if($concession)
        {
            $concession->state = "inactivo";
            $concession->save();
            Session::flash('message','Se ha desactivado la concesión correctamente');
            return redirect('admin/concessions');
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado la concesión.");
            return redirect('admin/concessions');
        }
    }

    public function activate($id)
    {
        $concession = Concession::find($id);
        if($concession)
        {
            $concession->state = "activo";
            $concession->save();
            Session::flash('message','Se ha activado la concesión correctamente');
            return redirect('admin/concessions');
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado la concesión.");
            return redirect('admin/concessions');
        }
    }

    public function tolls($slug)
    {
        $concession = Concession::where('slug',$slug)->firstOrFail();
        return view('admin.Concession.listToll',compact('concession'));
    }

    public function createTolls($slug)
    {
        $concession = Concession::where('slug',$slug)->firstOrFail();
        $departments = Department::where('state','activo')->pluck('department_name','id_department')->prepend('');
        $categories = InterestSiteCategory::where('state','activo')->pluck('category_name','id_category')->prepend(['Selecciona'=>null]);
        $activities = Activity::where('state','activo')->pluck('activity_name', 'id_activity');
        $municipalities = Municipality::where('state','activo')->pluck('municipality_name','id_municipality');
        return view('admin.Concession.newEditToll',compact('concession','departments','categories','activities','municipalities'));
    }

    public function storeTolls(Request $request, $slug)
    {
        Validator::make($request->all(),[
            'name'=>'required',
            'fk_site'=>'required'])->validate();

        $concession = Concession::where('slug',$slug)->firstOrFail();
        $toll = new Toll;
        $toll->name = $request->name;
        $toll->fk_site = $request->fk_site;
        $toll->cost_1 = $request->cost_1;
        $toll->cost_2 = $request->cost_2;
        $toll->cost_3 = $request->cost_3;
        $toll->cost_4 = $request->cost_4;
        $toll->cost_5 = $request->cost_5;
        $toll->cost_6 = $request->cost_6;
        $toll->cost_7 = $request->cost_7;
        if($request->link_image)
        {
            $files = $request->file('link_image');
            $link_image = cargar_imagen($files,'toll');
            $toll->link_image = $link_image;
        }
        $toll->fk_concession  = $concession->id_concession;
        $toll->save();
        Session::flash('message','Se ha realizado el registro de la información.');
        return redirect('admin/concessions/tolls/'.$concession->slug);
    }

    public function editTolls($id)
    {
        $toll = Toll::find($id);
        $concession = $toll->concession;
        $departments = Department::where('state','activo')->pluck('department_name','id_department')->prepend('');
        $categories = InterestSiteCategory::where('state','activo')->pluck('category_name','id_category')->prepend(['Selecciona'=>null]);
        $activities = Activity::where('state','activo')->pluck('activity_name', 'id_activity');

        $siteStart = InterestSite::find($toll->fk_site);
        $municipalityStart = $siteStart->municipality;
        $departmentStart = $municipalityStart->department;
        $municipalitiesStart = $departmentStart->municipalities->pluck('municipality_name','id_municipality');
        $sitesStart = $municipalityStart->InterestSite->pluck('site_name','id_site');

        $municipalities = Municipality::where('state','activo')->pluck('municipality_name','id_municipality');

        return view('admin.Concession.newEditToll',compact('toll','municipalityStart','concession','departments','municipalitiesStart','departmentStart','sitesStart','categories','activities','municipalities'));
    }

    public function updateTolls(Request $request, $id)
    {
        Validator::make($request->all(),[
            'name'=>'required',
            'fk_site'=>'required'])->validate();

        $toll = Toll::find($id);
        $concession = $toll->concession;
        $toll->name = $request->name;
        $toll->fk_site = $request->fk_site;
        $toll->cost_1 = $request->cost_1;
        $toll->cost_2 = $request->cost_2;
        $toll->cost_3 = $request->cost_3;
        $toll->cost_4 = $request->cost_4;
        $toll->cost_5 = $request->cost_5;
        $toll->cost_6 = $request->cost_6;
        $toll->cost_7 = $request->cost_7;
        if($request->link_image)
        {
            $files = $request->file('link_image');
            $link_image = cargar_imagen($files,'toll');
            $toll->link_image = $link_image;
        }
        $toll->save();
        Session::flash('message','Se ha realizado el registro de la información.');
        return redirect('admin/concessions/tolls/'.$concession->slug);
    }

    public function deleteTolls($id)
    {
        Toll::destroy($id);
        Session::flash('message','Se ha reliminado el peaje.');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
