<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Page;
use App\Models\PagePart;
use Session;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = Page::all();
        return view('admin.Page.listPage',compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $parts = PagePart::all()->where('state','activo');
        return view('admin.Page.createEditPage',compact('parts'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
             'page_name'=>'required',
             'url'=>'required',
             'keywords'=>'required',
             'description'=>'required',
             'parts'=>'required',
            ]); 

        if ($validator->fails()) 
        {
            foreach ($validator->errors()->all() as $error)
            {
                Session::flash('message-error', $error);
            }

            return redirect()->back()->withErrors($validator)->withInput();
        } 
        else 
        {
            $page = new Page;
            $page->page_name = $request->page_name;
            $page->description = $request->description;
            $page->keywords = $request->keywords;
            $page->url = $request->url;
            $page->save();

            $page->Parts()->sync($request->parts);

            Session::flash('message', 'Se ha realizado correctamente el registro de información.');
            return redirect('admin/pages');
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $page = Page::where('slug',$slug)->first();
        if($page)
        {
            $parts = PagePart::all()->where('state','activo');
            return view('admin.Page.createEditPage',compact('parts','page'));

        }
        else
        {
            Session::flash('message-error', "No se ha encontrado la página.");
            return redirect('admin/pages');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
             'page_name'=>'required',
             'url'=>'required',
             'keywords'=>'required',
             'description'=>'required',
             'parts'=>'required',
            ]); 

        if ($validator->fails()) 
        {
            foreach ($validator->errors()->all() as $error)
            {
                Session::flash('message-error', $error);
            }

            return redirect()->back()->withErrors($validator)->withInput();
        } 
        else 
        {
            $page = Page::find($id);
            $page->page_name = $request->page_name;
            $page->description = $request->description;
            $page->keywords = $request->keywords;
            $page->url = $request->url;
            $page->save();

            $page->Parts()->sync($request->parts);

            Session::flash('message', 'Se ha realizado correctamente la actualización de información.');
            return redirect('admin/pages');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
