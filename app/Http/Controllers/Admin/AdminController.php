<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\ImageRequest;
use App\Http\Controllers\Controller;
use App\Operator;
use App\User;
use App\Models\ServiceOperator;
use App\Models\Schedule;
use App\Models\Service;
use App\Models\ServicePicture;
use App\Models\ServiceItem;
use App\Models\Contact;
use File;
use Session;
class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function operators()
    {
        $operators = Operator::all();
        foreach($operators as $operator)
        {
            $totalReservations = 0;
            $totalServices = 0;
            foreach($operator->directService as $service)
            {
                $totalReservations += count($service->reservations);
                
                foreach ($service->reservations as $thisservice) 
                {
                    if($thisservice->state == "Calificada")
                    {
                        $totalServices ++;
                    }
                }
            }
            array_add($operator,'totalReservations',$totalReservations);
            array_add($operator,'totalServices',$totalServices);            
        }
        // $ch = curl_init();

        // curl_setopt($ch, CURLOPT_POST, 1);
        // curl_setopt($ch, CURLOPT_URL,"https://www.rues.org.co/RNT/ConsultaNumRNT_json");
        // curl_setopt($ch, CURLOPT_URL,"https://www.rues.org.co/RNT/ConsultaNITRNT_json");
        // curl_setopt($ch, CURLOPT_URL,"http://versionanterior.rues.org.co/RUES_Web/Consultas/ConsultaRNT_json");
        // curl_setopt($ch, CURLOPT_POST, 1);
        // curl_setopt($ch, CURLOPT_POSTFIELDS,
        //             "txtRNT=404");
        // curl_setopt($ch, CURLOPT_POSTFIELDS,
                    // "txtNIT=860029002");
        // curl_setopt($ch, CURLOPT_POSTFIELDS,
                    // "strRNT=404");

        // in real life you should use something like:
        // curl_setopt($ch, CURLOPT_POSTFIELDS, 
        //          http_build_query(array('postvar1' => 'value1')));

        // receive server response ...
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // $server_output = curl_exec ($ch);

        // curl_close ($ch);
        // dd(json_decode($server_output, true));
        // further processing ....

        return view('admin.Operator.listOperator',compact('operators'));
    }
    public function operatorServices($id)
    {
        $operator = Operator::find($id);
        $services = $operator->services;
        $days = ['Lunes','Martes','Miercoles','Jueves','Viernes','Sábado','Domingo'];

        foreach($services as $service)
        {
            $detailService = ServiceOperator::find($service->pivot->id_service_operator);
            $horario = Schedule::where('fk_service',$detailService->id_service_operator)->where('state','active')->get();

            if(count($horario)>0)
            {
                array_add($service,'days',$horario);
            }

        }
        // return ['services'=>$services,'days'=>$days];

        return view('admin.Operator.listService',compact('services','days','operator'));
    }

    public function editServiceOperator($id)
    {
        $service = ServiceOperator::find($id);
        $days = ['Lunes','Martes','Miercoles','Jueves','Viernes','Sábado','Domingo'];
        $services = Service::all();
        return view('admin.Operator.editService',compact('services','days','service'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateServiceOperator($id,Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'service'     => 'required|max:150',
            'cost' => 'required',
            'address'    => 'required|max:150',
            'capacity'    => 'required',
            'duration'    => 'required',
            'location'    => 'required',
            'description'    => 'required',
            'requisites'    => 'required'
        ]);


        if ($validator->fails()) 
        {
            return redirect()->back()->withErrors($validator)->withInput();
        } 
        else
        {

            $service = ServiceOperator::find($id);
            $service->cost = $request->cost;
            $service->address = $request->address;
            $service->capacity = $request->capacity;
            $service->duration = $request->duration;
            $service->location = $request->location;
            $service->requisites = $request->requisites;
            $service->description = $request->description;
            $service->save();

            
            \Session::flash('message', 'Se ha realizado la actualización correctamente');

            return redirect('admin/operators/services/'.$service->operador->id_operator);
        }
    }

    public function change(Request $request)
    {
        try
        {
            $operator = Operator::find($request->cod);
            if($operator->outstanding==0)
                $operator->outstanding = 1;
            else
                $operator->outstanding = 0;
            
            $operator->save();
            return 'ok';
        }
        catch(\Exception $e)
        {
            return 'error';
        }
    }


    public function imagesServiceOperator($id)
    {
        $service = ServiceOperator::find($id);
        $images = ServicePicture::where('fk_service',$service->id_service_operator)->get();

        return view('admin.Operator.imageService', compact('images','service'));
    }

    public function itemsOperator($id)
    {
        $items = ServiceItem::all()->where('fk_service',$id);
        $service = ServiceOperator::find($id);
        return view('admin.Operator.listItem',compact('items','service'));
    }

    public function editItemOperator($id)
    {
        $item = ServiceItem::find($id);
        $service = ServiceOperator::where('id_service_operator',$item->fk_service)->first();
        return view('admin.Operator.editItem',compact('item','service'));
    }

    public function updateItemOperator($id,Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'item_name'     => 'required|max:150',
            'cost' => 'required',
            'description'    => 'required',
        ]);


        if ($validator->fails()) 
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        else 
        {
            $item = ServiceItem::find($id);
            $item->item_name = $request->item_name;
            $item->cost = $request->cost;
            $item->description = $request->description;
            $item->fk_service = $request->fk_service;
            $item->save();

            Session::flash('message','Se ha actualizado la infomación correctamente');
            return redirect('admin/operators/items/'.$request->fk_service);
        }
    }

    public function desactivateItem($id)
    {
        $item = ServiceItem::find($id);
        if($item)
        {
            $item->state = "Desactivado por administrador";
            $item->save();
            Session::flash('message','Se ha desactivado el servicio adicional correctamente');
            return redirect('admin/operators/items/'.$item->fk_service);
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado el servicio adicional.");
            return redirect('admin/operators');
        }
    }

    public function activateItem($id)
    {
        $item = ServiceItem::find($id);
        if($item)
        {
            $item->state = "activo";
            $item->save();
            Session::flash('message','Se ha activado el servicio adicional correctamente');
            return redirect('admin/operators/items/'.$item->fk_service);
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado el servicio adicional.");
            return redirect('admin/operators');
        }
    }

    public function desactivate($id)
    {
        $service = ServiceOperator::find($id);
        if($service)
        {
            $service->state = "Desactivado por administrador";
            $service->save();
            Session::flash('message','Se ha desactivado el servicio correctamente');
            return redirect('admin/operators/services/'.$service->operador->id_operator);
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado el servicio.");
            return redirect('admin/operators');
        }
    }

    public function activate($id)
    {
        $service = ServiceOperator::find($id);
        if($service)
        {
            $service->state = "Activo";
            $service->save();
            Session::flash('message','Se ha activado el servicio correctamente');
            return redirect('admin/operators/services/'.$service->operador->id_operator);
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado el servicio.");
            return redirect('admin/operators');
        }
    }

    public function desactivateOperator($id)
    {
        $operator = Operator::find($id);
        $operator->status = 'inactive';
        $operator->save();
        foreach($operator->directService as $services)
        {
            $services->state = 'Desactivado por administrador';
            $services->save();
        }
        foreach($operator->restaurants as $restaurant)
        {
            $restaurant->state = 'Desactivado por administrador';
            $restaurant->save();
        }
        foreach($operator->hotels as $hotel)
        {
            $hotel->state = 'Desactivado por administrador';
            $hotel->save();
        }
        Session::flash('message','Se ha desactivado al operador, sus actividades, hoteles y restaurantes');
        return redirect('admin/operators');
    }

    public function activateOperator($id)
    {
        $operator = Operator::find($id);
        $operator->status = 'active';
        $operator->save();
        foreach($operator->directService as $services)
        {
            $services->state = 'inactivo';
            $services->save();
        }
        foreach($operator->restaurants as $restaurant)
        {
            $restaurant->state = 'inactivo';
            $restaurant->save();
        }
        foreach($operator->hotels as $hotel)
        {
            $hotel->state = 'inactivo';
            $hotel->save();
        }
        Session::flash('message','Se ha activado al operador y sus actividades han quedado inactivas, pero el operador puede activarlas');
        return redirect('admin/operators');
    }



    public function upload_images($id, ImageRequest $request)
    {
        $files = $request->file('file');

        $new_service = [];

        foreach ($files as $file) {
            $imageName = $this->cargar_imagen($file);

            if ($imageName)  
            {
                $new_image = ['link_image' => 'images/services/'.$imageName, 'fk_service' => $id];
                $service = ServicePicture::create($new_image);
                $new_service[] = $service->toArray();
                unset($new_image);
            }
        }

        if (count($new_service) > 0) {
            return \Response::json(['response' => 'Subida de imágenes completa'], 200);
        } else {
            return \Response::json(['response' => 'Falla en la subida de imágenes2'], 404);
        }
    }

    public function delete_image($id)
    {
        $image = ServicePicture::find($id);

        $exists = File::exists(public_path("images/services/".$image->link_imagen));
        if ($exists) {
            File::delete(public_path("images/services/".$image->link_imagen));
        }

        if ($image->delete()) 
        {
            \Session::flash('message', 'Imagen Borrada Correctamente');
        } 
        else 
        {
            \Session::flash('message-error', 'La Imagen no puede ser borrada');
        }

        return redirect()->back();
    }

    private function cargar_imagen($file, $imageName = false)
    {
        if ($imageName) 
        {
            $exists = File::exists(public_path("images/services/".$imageName));
            if ($exists) 
            {
                File::delete(public_path("images/services/".$imageName));
            }

            $image = explode('.', $imageName);
            $imageName = $image[0].'.'.$file->getClientOriginalExtension();
        } 
        else 
        {
            $imageName = 'Service_'.date('YmdHis', time()).rand().'.'.$file->getClientOriginalExtension();
        }

        $file->move(public_path('images/services'), $imageName);

        $exists = File::exists(public_path("images/services/".$imageName));

        if ($exists) 
        {
            return $imageName;
        } 
        else 
        {
            return false;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function users()
    {
        $users = User::all();
        return view('admin.User.listUser',compact('users'));
    }

    public function contact()
    {
        $contacts = Contact::all();
        return view('admin.Contact.listContact',compact('contacts'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
