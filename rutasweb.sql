-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 02-01-2019 a las 22:04:13
-- Versión del servidor: 5.6.35
-- Versión de PHP: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `rutasweb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `activities`
--

CREATE TABLE `activities` (
  `id_activity` int(10) UNSIGNED NOT NULL,
  `activity_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `icon_class` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `activities`
--

INSERT INTO `activities` (`id_activity`, `activity_name`, `image_link`, `title`, `keywords`, `icon_class`, `meta_description`, `slug`, `state`, `created_at`, `updated_at`) VALUES
(1, 'Aventura', 'images/activity/activity_201812032231371344825646.png', 'ELIGE LAS HISTORIAS QUE ESTÁN POR SUCEDER', 'alalalalalallaa', 'fas fa-bezier-curve', '<p>asdas dasd asd asd sad asd</p>', 'aventura', 'activo', '2018-12-04 03:31:37', '2018-12-28 23:17:29'),
(2, 'Ecoturismo', 'images/activity/activity_201812032231401472538081.png', 'ELIGE LAS HISTORIAS QUE ESTÁN POR SUCEDER', 'alalalalalallaa', 'fas fa-crow', '<p>asdas dasd asd asd sad asd</p>', 'aventura-1', 'activo', '2018-12-04 03:31:40', '2018-12-28 23:20:57'),
(3, 'Parques Nacionales', 'images/activity/activity_20181203223141851436773.png', 'ELIGE LAS HISTORIAS QUE ESTÁN POR SUCEDER', 'alalalalalallaa', 'fas fa-flag', '<p>asdas dasd asd asd sad asd</p>', 'aventura-2', 'activo', '2018-12-04 03:31:41', '2018-12-28 23:21:22'),
(4, 'Sol y Playa', 'images/activity/activity_201812032231431952505496.png', 'ELIGE LAS HISTORIAS QUE ESTÁN POR SUCEDER', 'alalalalalallaa', 'fas fa-glass-martini-alt', '<p>asdas dasd asd asd sad asd</p>', 'aventura-3', 'activo', '2018-12-04 03:31:43', '2018-12-28 23:22:38'),
(5, 'Parques temáticos', 'images/activity/activity_20181203223144445325452.png', 'ELIGE LAS HISTORIAS QUE ESTÁN POR SUCEDER', 'alalalalalallaa', 'fas fa-parachute-box', '<p>asdas dasd asd asd sad asd</p>', 'aventura-4', 'activo', '2018-12-04 03:31:44', '2018-12-28 23:15:02'),
(6, 'De compras', 'images/activity/activity_201812032231501820768632.png', 'ELIGE LAS HISTORIAS QUE ESTÁN POR SUCEDER', 'alalalalalallaa', 'fas fa-shopping-bag', '<p>asdas dasd asd asd sad asd</p>', 'aventura-5', 'activo', '2018-12-04 03:31:50', '2018-12-28 23:14:35');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `activity_pivots`
--

CREATE TABLE `activity_pivots` (
  `id_activity_pivot` int(10) UNSIGNED NOT NULL,
  `fk_actity` int(10) UNSIGNED NOT NULL,
  `fk_relation` int(11) NOT NULL,
  `type_relation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `activity_pivots`
--

INSERT INTO `activity_pivots` (`id_activity_pivot`, `fk_actity`, `fk_relation`, `type_relation`, `created_at`, `updated_at`) VALUES
(2, 1, 2, 'municipality', NULL, NULL),
(3, 2, 2, 'municipality', NULL, NULL),
(4, 4, 2, 'municipality', NULL, NULL),
(5, 1, 1, 'municipality', NULL, NULL),
(6, 2, 1, 'municipality', NULL, NULL),
(7, 3, 1, 'municipality', NULL, NULL),
(8, 5, 1, 'municipality', NULL, NULL),
(9, 6, 1, 'municipality', NULL, NULL),
(10, 2, 5, 'municipality', NULL, NULL),
(11, 3, 5, 'municipality', NULL, NULL),
(13, 2, 7, 'municipality', NULL, NULL),
(14, 1, 6, 'interest_site', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'al4cdeveloper', 'dev@tupaseo.travel', '$2y$10$Q8tO81UTNhUwBTHh.7Qcx.5jCmzcmCXiA4/ajctk63oaJ1zAAcg6y', 'gubCHkVIRhxSykjlqUVLym7vxv1M6cGgPHPHePfWSFtbEmIBTE51wwi2bann', '2018-12-04 03:21:35', '2018-12-04 03:21:35');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_password_resets`
--

CREATE TABLE `admin_password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `circuits`
--

CREATE TABLE `circuits` (
  `id_circuit` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `short_description` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `description_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `recommendations` text COLLATE utf8_unicode_ci,
  `recommendation_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `text_images` text COLLATE utf8_unicode_ci,
  `card_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `introduction_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `download_file` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `circuits`
--

INSERT INTO `circuits` (`id_circuit`, `name`, `keywords`, `short_description`, `description`, `description_image`, `recommendations`, `recommendation_image`, `text_images`, `card_image`, `introduction_image`, `video`, `download_file`, `slug`, `state`, `created_at`, `updated_at`) VALUES
(1, 'wweb bien mamona', 'asdasd', '123', '<p>asdasd asd</p>', NULL, '<p>asd asdasd&nbsp;</p>', NULL, NULL, NULL, NULL, NULL, NULL, 'web', 'activo', '2018-12-06 09:53:10', '2018-12-06 10:29:56');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `circuit_detail_items`
--

CREATE TABLE `circuit_detail_items` (
  `id_detail_item` int(10) UNSIGNED NOT NULL,
  `image_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `fk_circuit` int(10) UNSIGNED NOT NULL,
  `type_relation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fk_relation` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `circuit_detail_items`
--

INSERT INTO `circuit_detail_items` (`id_detail_item`, `image_link`, `description`, `fk_circuit`, `type_relation`, `fk_relation`, `created_at`, `updated_at`) VALUES
(1, '', '<p>dsfbghjk</p>', 1, 'municipality', 1, '2018-12-06 22:12:13', '2018-12-06 22:12:13');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `circuit_images`
--

CREATE TABLE `circuit_images` (
  `id_circuit_image` int(10) UNSIGNED NOT NULL,
  `link_image` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `fk_circuit` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `circuit_images`
--

INSERT INTO `circuit_images` (`id_circuit_image`, `link_image`, `fk_circuit`, `created_at`, `updated_at`) VALUES
(1, 'images/circuit/circuit_20181206191105972983273.png', 1, '2018-12-07 00:11:05', '2018-12-07 00:11:05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `concessions`
--

CREATE TABLE `concessions` (
  `id_concession` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `acronym` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(191) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `concessions`
--

INSERT INTO `concessions` (`id_concession`, `name`, `logo`, `acronym`, `keywords`, `description_image`, `description`, `slug`, `state`, `created_at`, `updated_at`) VALUES
(1, 'AGENCIA NACIONAL DE INFRAESTRUCTURA', 'images/concession/concession_201812141902481456883220.png', NULL, 'ANI,RUTASCOLOBMIA', NULL, '<p>Agencia Nacional de Infraestructura (ANI), tendr&aacute; por objeto planear, coordinar, estructurar, contratar, ejecutar, administrar y evaluar proyectos de concesiones y otras formas de Asociaci&oacute;n P&uacute;blico Privada APP, para el dise&ntilde;o, construcci&oacute;n, mantenimiento, operaci&oacute;n, administraci&oacute;n y/o explotaci&oacute;n de la infraestructura p&uacute;blica de transporte en todos sus modos y de los servicios conexos o relacionados y el desarrollo de proyectos de asociaci&oacute;n p&uacute;blico privada para otro tipo infraestructura p&uacute;blica cuando as&iacute; lo determine expresamente el Gobierno Nacional respecto de infraestructuras semejantes a las enunciadas en este art&iacute;culo, dentro del respeto a las normas que regulan la distribuci&oacute;n de funciones y competencias y su asignaci&oacute;n.</p>', 'agencia-nacional-de-infraestructura', 'activo', '2018-12-05 09:32:18', '2018-12-15 00:02:48');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacts`
--

CREATE TABLE `contacts` (
  `id_contact` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `customers`
--

CREATE TABLE `customers` (
  `id_customer` int(10) UNSIGNED NOT NULL,
  `nit` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone_contact` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_contact` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departments`
--

CREATE TABLE `departments` (
  `id_department` int(10) UNSIGNED NOT NULL,
  `department_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `short_description` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `multimedia_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `latitude` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `longitude` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description_image` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `text_images` text COLLATE utf8_unicode_ci,
  `title_extra_description` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `extra_description` text COLLATE utf8_unicode_ci,
  `video_youtube` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type_last_user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fk_last_edition` int(11) NOT NULL,
  `fk_region` int(10) UNSIGNED NOT NULL,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `departments`
--

INSERT INTO `departments` (`id_department`, `department_name`, `description`, `short_description`, `multimedia_type`, `latitude`, `longitude`, `description_image`, `text_images`, `title_extra_description`, `extra_description`, `video_youtube`, `slug`, `type_last_user`, `link_image`, `fk_last_edition`, `fk_region`, `keywords`, `state`, `created_at`, `updated_at`) VALUES
(1, 'Bogotá D.C', '<p>Descripci&oacute;n de municipio bogot&aacute;&nbsp;Descripci&oacute;n de municipio bogot&aacute; bDescripci&oacute;n de municipio bogot&aacute;</p>', 'Descripción de municipio bogotá', 'images', '4.710988599999999', '-74.072092', 'images/department/department_20181221211804604571969.png', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>', '\"EX VERO MEDIOCREM\"', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed a lorem quis neque interdum consequat ut sed sem. Duis quis tempor nunc. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>', '8xUG5AJeaDQ', 'bogota-d-c', 'admin', 'images/departments/Department_201812032240401136621206.png', 1, 1, 'Bogotá,departamento', 'activo', '2018-12-04 03:40:40', '2018-12-22 02:18:04'),
(2, 'Cundinamarca', '<p>Descripci&oacute;n de municipio CundinamarcaDescripci&oacute;n de municipio CundinamarcaDescripci&oacute;n de municipio CundinamarcaDescripci&oacute;n de municipio CundinamarcaDescripci&oacute;n de municipio Cundinamarca</p>', 'Descripción de municipio Cundinamarca', 'images', '5.026002999999999', '-74.03001219999999', NULL, NULL, NULL, NULL, NULL, 'cundinamarca', 'admin', 'images/departments/Department_20181203224116289111439.png', 1, 1, 'asdas dasd as d,asd asd asd', 'activo', '2018-12-04 03:41:16', '2018-12-04 03:41:16');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `department_images`
--

CREATE TABLE `department_images` (
  `id_department_image` int(10) UNSIGNED NOT NULL,
  `link_image` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `fk_department` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `department_images`
--

INSERT INTO `department_images` (`id_department_image`, `link_image`, `fk_department`, `created_at`, `updated_at`) VALUES
(1, 'images/departments/Department_201812212120001018853089.jpeg', 1, '2018-12-22 02:20:00', '2018-12-22 02:20:00'),
(2, 'images/departments/Department_20181221212000414556585.jpeg', 1, '2018-12-22 02:20:00', '2018-12-22 02:20:00'),
(3, 'images/departments/Department_20181221212001584560658.jpeg', 1, '2018-12-22 02:20:01', '2018-12-22 02:20:01'),
(4, 'images/departments/Department_20181221212001706315636.jpeg', 1, '2018-12-22 02:20:01', '2018-12-22 02:20:01');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `establishments`
--

CREATE TABLE `establishments` (
  `id_establishment` int(10) UNSIGNED NOT NULL,
  `type_establishment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `establishment_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` int(11) NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `latitude` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `longitude` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instagram` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kitchen_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fk_operator` int(11) DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `url_redireccion` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `card_image` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `introduction_image` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `internal_state` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'admin',
  `fk_municipality` int(10) UNSIGNED NOT NULL,
  `outstanding` double NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `establishments`
--

INSERT INTO `establishments` (`id_establishment`, `type_establishment`, `establishment_name`, `phone`, `address`, `latitude`, `longitude`, `facebook`, `twitter`, `instagram`, `website`, `video`, `kitchen_type`, `slug`, `fk_operator`, `description`, `url_redireccion`, `card_image`, `introduction_image`, `state`, `internal_state`, `fk_municipality`, `outstanding`, `created_at`, `updated_at`) VALUES
(1, 'hotel', 'Hotel no mames', 2147483647, 'cra 147 # 142 50, cra 142#134-33', '4.7517403', '-74.11751449999997', NULL, NULL, NULL, NULL, '', NULL, 'hotel-mamon', 1, '<p>asfdasd asdasdasd&nbsp;</p>', NULL, 'images/establishment/establishment_20181217181128655314912.png', 'images/establishment/establishment_201812171619511220139565.png', 'activo', 'admin', 1, 1, '2018-12-11 22:57:45', '2018-12-19 01:14:30'),
(2, 'restaurant', 'Restaurante pro', 123123, 'almeidas', '4.97088', '-73.37990000000002', NULL, NULL, NULL, NULL, NULL, 'Venezolana', 'restaurante-pro', 1, '<p>asda sdasd asd asd asd&nbsp;</p>', NULL, NULL, NULL, 'activo', 'admin', 2, 1, '2018-12-11 22:58:55', '2018-12-17 21:23:01'),
(3, 'hotel', 'Hotel mamon', 2147483647, 'cra 147 # 142 50, cra 142#134-33', '4.7517403', '-74.11751449999997', NULL, NULL, NULL, NULL, NULL, NULL, 'hotel-mamon-2', NULL, '<p>asfdasd asdasdasd&nbsp;</p>', NULL, NULL, NULL, 'activo', 'admin', 1, 0, '2018-12-11 22:57:45', '2018-12-15 11:16:51'),
(4, 'restaurant', 'Restaurante pro', 123123, 'almeidas', '4.97088', '-73.37990000000002', NULL, NULL, NULL, NULL, NULL, 'Venezolana', 'restaurante-pro-2', NULL, '<p>asda sdasd asd asd asd&nbsp;</p>', NULL, NULL, NULL, 'activo', 'admin', 2, 0, '2018-12-11 22:58:55', '2018-12-11 22:58:55'),
(5, 'restaurant', 'Restaurante pro', 123123, 'almeidas', '4.97088', '-73.37990000000002', NULL, NULL, NULL, NULL, NULL, 'Venezolana', 'restaurante-pro-3', NULL, '<p>asda sdasd asd asd asd&nbsp;</p>', NULL, NULL, NULL, 'activo', 'admin', 1, 0, '2018-12-11 22:58:55', '2018-12-11 22:58:55'),
(6, 'hotel', 'Hotel mamon', 2147483647, 'cra 147 # 142 50, cra 142#134-33', '4.7517403', '-74.11751449999997', NULL, NULL, NULL, NULL, NULL, NULL, 'hotel-mamon-3', 1, '<p>asfdasd asdasdasd&nbsp;</p>', NULL, NULL, NULL, 'activo', 'operator', 2, 0, '2018-12-11 22:57:45', '2018-12-12 00:56:52');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `establishment_images`
--

CREATE TABLE `establishment_images` (
  `id_establishment_image` int(10) UNSIGNED NOT NULL,
  `link_image` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `fk_establishment` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `establishment_images`
--

INSERT INTO `establishment_images` (`id_establishment_image`, `link_image`, `description`, `fk_establishment`, `created_at`, `updated_at`) VALUES
(5, 'images/hotel/hotel_20181218163426202449746.jpg', 'Disfrute de la ubicación céntrica y del elegante ambiente del hotel Hilton Bogota. Ubicado en el centro del distrito financiero, el hotel se encuentra cerca la Zona Rosa, uno de los vecindarios más prestigiosos de la ciudad y de la Zona G, que ofrece entretenimiento y restaurantes.', 1, '2018-12-18 21:34:26', '2018-12-18 21:36:10'),
(6, 'images/hotel/hotel_201812181634261063977765.jpg', 'Descanse en una habitación moderna con WiFi, escritorio y comodidades.', 1, '2018-12-18 21:34:26', '2018-12-18 21:36:10'),
(7, 'images/hotel/hotel_201812181634271315661890.jpg', 'Elija una suite para disfrutar de un ambiente sofisticado y espacio adicional en la sala de estar con un sofá.', 1, '2018-12-18 21:34:27', '2018-12-18 21:36:10'),
(8, 'images/hotel/hotel_20181218163427618726101.jpg', 'Hospédese en una habitación o una suite Executive para tener acceso a los privilegios exclusivos de la sala de estar ejecutiva.', 1, '2018-12-18 21:34:27', '2018-12-18 21:36:10'),
(9, 'images/hotel/hotel_20181218163435222987351.jpg', 'Llegue al hotel desde el Aeropuerto Internacional El Dorado en solo 25 minutos.', 1, '2018-12-18 21:34:35', '2018-12-18 21:36:10'),
(10, 'images/hotel/hotel_201812181634351376076751.jpg', 'Modernas instalaciones para reuniones con 930 metros cuadrados/10.000 pies cuadrados de espacio para eventos; WiFi en todo el hotel.', 1, '2018-12-18 21:34:35', '2018-12-18 21:36:10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `establishment_schedules`
--

CREATE TABLE `establishment_schedules` (
  `id_establishment_schedule` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `start` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `end` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  `fk_establishment` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `establishment_schedules`
--

INSERT INTO `establishment_schedules` (`id_establishment_schedule`, `name`, `start`, `end`, `inactive`, `fk_establishment`, `created_at`, `updated_at`) VALUES
(1, 'checkin', '12:57:45', '12:57:45', 0, 1, '2018-12-11 22:57:45', '2018-12-11 22:57:45'),
(2, 'checkout', '12:57:45', '12:57:45', 0, 1, '2018-12-11 22:57:45', '2018-12-11 22:57:45'),
(3, 'Lunes', '13:58:45', '13:58:45', 0, 2, '2018-12-11 22:58:55', '2018-12-11 22:58:55'),
(4, 'Martes', '13:58:45', '13:58:45', 0, 2, '2018-12-11 22:58:55', '2018-12-11 22:58:55'),
(5, 'Miercoles', '13:58:45', '13:58:45', 0, 2, '2018-12-11 22:58:55', '2018-12-11 22:58:55'),
(6, 'Jueves', '13:58:45', '13:58:45', 0, 2, '2018-12-11 22:58:55', '2018-12-11 22:58:55'),
(7, 'Viernes', '13:58:45', '13:58:45', 0, 2, '2018-12-11 22:58:55', '2018-12-11 22:58:55'),
(8, 'Sabado', '13:58:45', '13:58:45', 0, 2, '2018-12-11 22:58:55', '2018-12-11 22:58:55'),
(9, 'Domingo', '13:58:45', '13:58:45', 0, 2, '2018-12-11 22:58:55', '2018-12-11 22:58:55');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fairs`
--

CREATE TABLE `fairs` (
  `id_fair` int(10) UNSIGNED NOT NULL,
  `fair_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `fk_municipality` int(10) UNSIGNED NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `multimedia_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `link_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `outstanding` tinyint(1) NOT NULL DEFAULT '0',
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `fairs`
--

INSERT INTO `fairs` (`id_fair`, `fair_name`, `start_date`, `end_date`, `fk_municipality`, `state`, `multimedia_type`, `keywords`, `description`, `link_image`, `outstanding`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Evento de prueba', '2018-12-07', '2018-12-01', 1, 'activo', 'images', 'palabras,clave,separadas,por,comas', '<p>asdas dasd asd asd asda sd asd asd asd asd asd asd asd&nbsp;</p>', 'images/fair/fair_20181220180753617273963.png', 1, 'evento-de-prueba', '2018-12-20 23:07:53', '2019-01-02 22:21:26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fair_images`
--

CREATE TABLE `fair_images` (
  `id_fair_image` int(10) UNSIGNED NOT NULL,
  `link_image` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `fk_fair` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `fair_images`
--

INSERT INTO `fair_images` (`id_fair_image`, `link_image`, `fk_fair`, `created_at`, `updated_at`) VALUES
(1, 'images/fairs/Fair_20181221010008622435515.jpeg', 1, '2018-12-21 06:00:09', '2018-12-21 06:00:09'),
(2, 'images/fairs/Fair_201812210100091638055379.jpeg', 1, '2018-12-21 06:00:09', '2018-12-21 06:00:09'),
(3, 'images/fairs/Fair_20181221010011610457698.jpeg', 1, '2018-12-21 06:00:11', '2018-12-21 06:00:11'),
(4, 'images/fairs/Fair_201812210100111110499775.jpeg', 1, '2018-12-21 06:00:11', '2018-12-21 06:00:11');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `history_regions`
--

CREATE TABLE `history_regions` (
  `id_history_region` int(10) UNSIGNED NOT NULL,
  `description_to_approve` text COLLATE utf8_unicode_ci NOT NULL,
  `fk_region` int(10) UNSIGNED NOT NULL,
  `fk_reporter` int(10) UNSIGNED NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `approved_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `interest_sites`
--

CREATE TABLE `interest_sites` (
  `id_site` int(10) UNSIGNED NOT NULL,
  `site_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fk_category` int(10) UNSIGNED NOT NULL,
  `fk_municipality` int(10) UNSIGNED NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `web` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instagram` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `youtube` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img_360` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video_youtube` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `iframe` text COLLATE utf8_unicode_ci,
  `multimedia_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `latitude` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `longitude` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link_icon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `interest_sites`
--

INSERT INTO `interest_sites` (`id_site`, `site_name`, `fk_category`, `fk_municipality`, `address`, `phone`, `web`, `facebook`, `twitter`, `instagram`, `youtube`, `img_360`, `video_youtube`, `iframe`, `multimedia_type`, `latitude`, `longitude`, `slug`, `link_image`, `link_icon`, `description`, `keywords`, `state`, `created_at`, `updated_at`) VALUES
(1, 'Portal del norte', 2, 1, 'Bogotá, Cundinamarca', '320 3962358', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'images', '4.75423', '-74.04616', 'portal-del-norte', 'images/interestsites/site20181203225035419083475.png', NULL, '<p>El Portal del Norte es una de las estaciones de cabecera o terminales del Sistema de Transporte M&aacute;sivo de Bogot&aacute; TransMilenio. Hace parte de la primera fase del sistema, la cual fue inaugurada en el a&ntilde;o 2000.</p>', 'pORTAL,NORTE', 'activo', '2018-12-04 03:50:35', '2018-12-04 03:50:35'),
(2, 'portal suba', 2, 1, 'cra 147 # 142 50, cra 142#134-33', '3203233082', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '4.7463884', '-74.0947723', 'portal-suba', NULL, NULL, '<p>hrtehtjyuiiohuiuygt</p>', 'tyrtyubnm.', 'activo', '2018-12-04 23:35:36', '2018-12-21 02:28:00'),
(3, 'Portal suba', 2, 1, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '123123', '123123', 'portal-suba-1', NULL, NULL, NULL, '', 'activo', '2018-12-04 23:35:53', '2018-12-04 23:35:53'),
(4, 'Peaje de los andes', 1, 1, '123123', '123123', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '4.8298496', '-74.0352798', 'peaje-de-los-andes', NULL, NULL, '<p>asd asd asd&nbsp;</p>', 'asdas dasd asd a', 'activo', '2018-12-05 04:53:45', '2018-12-15 09:02:35'),
(5, 'Peaje Aburrá E', 1, 1, '234234 23423', '21234234', NULL, NULL, NULL, NULL, NULL, NULL, 'https://www.youtube.com/watch?v=7cHGapaqCqU', NULL, '', '4.6613479', '-74.0593758', 'peaje-aburra-e', NULL, NULL, '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa voluptates commodi eveniet amet facilis rerum veritatis. Dolor dolore iusto cumque, voluptatem dolores similique. Repellat repellendus, vitae illo odit eaque distinctio?</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa voluptates commodi eveniet amet facilis rerum veritatis. Dolor dolore iusto cumque, voluptatem dolores similique. Repellat repellendus, vitae illo odit eaque distinctio?</p>\r\n\r\n<p>&nbsp;</p>', 'asda sda,sd as das,dasd as', 'activo', '2018-12-06 01:44:55', '2018-12-21 02:05:49'),
(6, 'Salitre màgico', 3, 1, 'cra 147 # 142 50, cra 142#134-33', '3203233082', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 'images', '4.6648196', '-74.0919759', 'salitre-magico', 'images/interestsites/site20181226175643861310801.png', NULL, '<p>asdasd asd asda</p>', 'Salitre,magico,en rutas', 'activo', '2018-12-26 22:56:43', '2018-12-26 22:56:43');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `interest_site_categories`
--

CREATE TABLE `interest_site_categories` (
  `id_category` int(10) UNSIGNED NOT NULL,
  `category_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `default_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `default_icon` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `interest_site_categories`
--

INSERT INTO `interest_site_categories` (`id_category`, `category_name`, `default_image`, `default_icon`, `state`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Peajes', 'images/interestsitescategory/category201812032225311647562090.png', 'images/interestsitescategory/category20181203222531617342798.png', 'activo', 'peajes', '2018-12-04 03:25:31', '2018-12-04 03:25:31'),
(2, 'portales de transmilenio', 'images/interestsitescategory/category201812032245412143720765.png', 'images/interestsitescategory/category201812032245411642149770.png', 'activo', 'portales-de-transmilenio', '2018-12-04 03:45:41', '2018-12-04 03:45:41'),
(3, 'Parques de diversiones', 'images/interestsitescategory/category20181226175450132340092.png', 'images/interestsitescategory/category201812261754502038895583.png', 'activo', 'parques-de-diversiones', '2018-12-26 22:54:50', '2018-12-26 22:54:50');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `interest_site_images`
--

CREATE TABLE `interest_site_images` (
  `id_site_image` int(10) UNSIGNED NOT NULL,
  `link_image` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `fk_site` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `interest_site_images`
--

INSERT INTO `interest_site_images` (`id_site_image`, `link_image`, `fk_site`, `created_at`, `updated_at`) VALUES
(1, 'images/interestsites/site201812202105071245846690.jpeg', 5, '2018-12-21 02:05:07', '2018-12-21 02:05:07'),
(2, 'images/interestsites/site20181220210507569687665.jpeg', 5, '2018-12-21 02:05:07', '2018-12-21 02:05:07'),
(3, 'images/interestsites/site20181220210508771825007.jpeg', 5, '2018-12-21 02:05:08', '2018-12-21 02:05:08'),
(4, 'images/interestsites/site201812202105081778521947.jpeg', 5, '2018-12-21 02:05:08', '2018-12-21 02:05:08');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(114, '2014_10_12_000000_create_users_table', 1),
(115, '2014_10_12_100000_create_password_resets_table', 1),
(116, '2018_01_22_154619_create_admins_table', 1),
(117, '2018_01_22_154620_create_admin_password_resets_table', 1),
(118, '2018_01_22_195432_create_regions_table', 1),
(119, '2018_01_22_200727_create_reporters_table', 1),
(120, '2018_01_22_200728_create_reporter_password_resets_table', 1),
(121, '2018_01_22_203511_create_history_regions_table', 1),
(122, '2018_01_23_181617_create_region_images_table', 1),
(123, '2018_01_25_172538_create_departments_table', 1),
(124, '2018_01_25_204838_create_department_images_table', 1),
(125, '2018_01_25_212046_create_municipalities_table', 1),
(126, '2018_01_29_170933_create_municipality_images_table', 1),
(127, '2018_01_29_180040_create_interest_site_categories_table', 1),
(128, '2018_01_29_214958_create_interest_sites_table', 1),
(129, '2018_02_01_175959_create_interest_site_images_table', 1),
(130, '2018_02_05_213347_create_circuits_table', 1),
(131, '2018_02_05_213625_create_relation_circuits_table', 1),
(132, '2018_02_06_211404_create_fairs_table', 1),
(133, '2018_02_07_210812_create_fair_images_table', 1),
(134, '2018_02_08_172030_create_portable_documents_table', 1),
(135, '2018_02_08_174141_create_videos_table', 1),
(136, '2018_02_08_204645_create_user_password_resets_table', 1),
(137, '2018_02_08_211010_create_operators_table', 1),
(138, '2018_02_08_211011_create_operator_password_resets_table', 1),
(139, '2018_02_09_165451_create_service_categories_table', 1),
(140, '2018_02_09_165823_create_services_table', 1),
(141, '2018_02_09_165903_create_service_operators_table', 1),
(142, '2018_02_09_170002_create_contacts_table', 1),
(143, '2018_02_09_170049_create_schedules_table', 1),
(144, '2018_02_09_170158_create_reservations_table', 1),
(145, '2018_02_09_170744_create_service_pictures_table', 1),
(146, '2018_02_09_175954_create_polls_table', 1),
(147, '2018_02_09_203947_create_service_items_table', 1),
(148, '2018_02_12_212803_create_wallpapers_table', 1),
(149, '2018_03_07_161448_create_customers_table', 1),
(150, '2018_03_09_164310_create_page_parts_table', 1),
(151, '2018_03_09_175043_create_pages_table', 1),
(152, '2018_03_09_175907_create_page_part_pages_table', 1),
(153, '2018_03_12_170357_create_patterns_table', 1),
(154, '2018_03_12_182258_create_pattern_parts_table', 1),
(155, '2018_09_10_205131_create_specials_table', 1),
(156, '2018_09_14_172421_create_special_images_table', 1),
(157, '2018_09_17_211952_create_establishments_table', 1),
(158, '2018_09_19_174530_create_service_establishments_table', 1),
(159, '2018_09_19_174923_create_service_establishment_pivots_table', 1),
(160, '2018_09_19_175045_create_plates_table', 1),
(161, '2018_09_19_175415_create_establishment_schedules_table', 1),
(162, '2018_09_19_175533_create_rooms_table', 1),
(163, '2018_09_20_155445_create_establishment_images_table', 1),
(164, '2018_10_29_175850_create_activities_table', 1),
(165, '2018_10_29_214330_create_activity_pivots_table', 1),
(166, '2018_11_21_221335_create_special_items_table', 1),
(167, '2018_11_21_221700_create_special_detail_items_table', 1),
(168, '2018_11_22_181952_create_special_routes_table', 1),
(169, '2018_11_27_173839_create_route_points_table', 1),
(170, '2018_12_03_210010_create_routes_table', 1),
(172, '2018_12_03_210250_create_real_route_points_table', 2),
(175, '2018_12_05_003615_create_concessions_table', 3),
(176, '2018_12_05_174138_create_tolls_table', 4),
(177, '2018_12_06_013554_create_route_images_table', 5),
(178, '2018_12_06_030604_create_circuits_table', 6),
(180, '2018_12_06_053201_create_circuit_detail_items_table', 7),
(181, '2018_12_06_182111_create_circuit_images_table', 8),
(183, '2018_12_07_023303_create_municipality_keydatas_table', 9),
(184, '2018_12_07_030419_create_site_keydatas_table', 10),
(185, '2018_12_12_190624_create_special_route_images_table', 11);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `municipalities`
--

CREATE TABLE `municipalities` (
  `id_municipality` int(10) UNSIGNED NOT NULL,
  `municipality_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `multimedia_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `latitude` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `longitude` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type_last_user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fk_last_edition` int(11) NOT NULL,
  `fk_department` int(10) UNSIGNED NOT NULL,
  `city` tinyint(1) NOT NULL,
  `mayor_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mayor_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mayor_description` text COLLATE utf8_unicode_ci,
  `logo_partner` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flag` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shield` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `show_additional_data` tinyint(1) NOT NULL DEFAULT '0',
  `img_360` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video_youtube` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `iframe` text COLLATE utf8_unicode_ci,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `outstanding` tinyint(1) NOT NULL DEFAULT '0',
  `front_state` varchar(191) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'inactivo',
  `front_state_hotel` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'inactivo',
  `front_state_restaurant` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'inactivo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `municipalities`
--

INSERT INTO `municipalities` (`id_municipality`, `municipality_name`, `description`, `multimedia_type`, `latitude`, `longitude`, `slug`, `type_last_user`, `link_image`, `fk_last_edition`, `fk_department`, `city`, `mayor_name`, `mayor_image`, `mayor_description`, `logo_partner`, `flag`, `shield`, `show_additional_data`, `img_360`, `video_youtube`, `iframe`, `keywords`, `state`, `outstanding`, `front_state`, `front_state_hotel`, `front_state_restaurant`, `created_at`, `updated_at`) VALUES
(1, 'Bogotá', '<p>dfgioasdacfshgdjhb koaisd arsydgh ioasdtayfsv dgbaoi&ntilde;sd p9atsftydvg ajbsdoiasgd ftavygsjdkoansd aysdvg asdasiudfavhsbjkdj uiabsjd a</p>', 'images', '4.710988599999999', '-74.072092', 'bogota', 'admin', 'images/municipalities/Municipality_201812032241591363939759.png', 1, 1, 1, 'hOLIII', NULL, NULL, NULL, NULL, NULL, 1, 'images/municipality/municipality_20181207023202962334543.png', '', NULL, 'gfhjkasdasd asd asd,as dasd asd a,sda sdasd asd', 'activo', 1, 'activo', 'activo', 'inactivo', '2018-12-04 03:42:00', '2018-12-26 23:50:26'),
(2, 'Almeidas', '<p>asda sdas dasd asd asd asd&nbsp;</p>', 'images', '4.97088', '-73.37990000000002', 'almeidas', 'admin', 'images/municipalities/Municipality_201812111734162076384959.png', 1, 2, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, '', NULL, 'municipio,Almeidas', 'activo', 1, 'inactivo', 'inactivo', 'activo', '2018-12-11 22:34:17', '2018-12-26 23:50:05'),
(3, 'sibaté', '<p>asd asd asd</p>', 'images', '4.485011', '-74.25933399999997', 'sibate', 'admin', 'images/municipalities/Municipality_201812130655131969746671.png', 1, 2, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 'asdas das,d asd', 'activo', 0, 'inactivo', 'inactivo', 'inactivo', '2018-12-13 11:55:14', '2018-12-13 11:55:14'),
(4, 'suesca', '<p>asd asdasd asd asd</p>', 'images', '5.103929', '-73.798632', 'suesca', 'admin', 'images/municipalities/Municipality_20181213065616254103101.png', 1, 2, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 'adasd asd asd asd', 'activo', 1, 'inactivo', 'inactivo', 'inactivo', '2018-12-13 11:56:17', '2018-12-22 02:48:00'),
(5, 'cota', '<p>asd asd a</p>', 'images', '4.809271', '-74.10312099999999', 'cota', 'admin', 'images/municipalities/Municipality_201812130659381371517766.png', 1, 2, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, '', NULL, '1231 2312 3', 'activo', 0, 'inactivo', 'inactivo', 'inactivo', '2018-12-13 11:59:38', '2018-12-26 23:50:47'),
(6, 'Manta', '<p>adasd asd&nbsp;</p>', 'images', '5.009443999999999', '-73.539987', 'manta', 'admin', 'images/municipalities/Municipality_201812131729241354733957.png', 1, 2, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, '', NULL, 'asd asd,asd', 'activo', 0, 'activo', 'inactivo', 'inactivo', '2018-12-13 22:29:24', '2018-12-26 23:51:02'),
(7, 'Sesquilé', '<p>sf sfsdf sdf sdf&nbsp;</p>', 'images', '5.045268', '-73.79692499999999', 'sesquile', 'admin', 'images/municipalities/Municipality_20181213173047139915207.png', 1, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, '', NULL, '234e2 423 4', 'activo', 1, 'inactivo', 'inactivo', 'inactivo', '2018-12-13 22:30:47', '2018-12-26 23:51:23');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `municipality_images`
--

CREATE TABLE `municipality_images` (
  `id_municipality_image` int(10) UNSIGNED NOT NULL,
  `link_image` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `fk_municipality` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `municipality_keydatas`
--

CREATE TABLE `municipality_keydatas` (
  `id_keydata` int(10) UNSIGNED NOT NULL,
  `keydata_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keydata_value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fk_municipality` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `municipality_keydatas`
--

INSERT INTO `municipality_keydatas` (`id_keydata`, `keydata_name`, `keydata_value`, `category`, `fk_municipality`, `created_at`, `updated_at`) VALUES
(1, '45678', '5678', 'Tiempo Estimado de Vuelo', 1, '2018-12-07 08:03:03', '2018-12-07 08:03:03'),
(2, '60.000 habitantes', '(Proyección DANE 2019)', 'Datos Útiles', 2, '2018-12-19 05:35:29', '2018-12-19 05:35:29'),
(3, 'Tipo de Clima', 'Cálido - 34 ºC', 'Datos Útiles', 2, '2018-12-19 05:35:29', '2018-12-19 05:35:29'),
(4, 'Humedad', '38 ºC', 'Datos Útiles', 2, '2018-12-19 05:35:29', '2018-12-19 05:35:29'),
(5, 'Bogotá', '100 km', 'Distancia Estimada Terrestre', 2, '2018-12-19 05:35:29', '2018-12-19 05:35:29'),
(6, 'Tunja', '120 km', 'Distancia Estimada Terrestre', 2, '2018-12-19 05:35:29', '2018-12-19 05:35:29'),
(7, 'Girardot', '300 km', 'Distancia Estimada Terrestre', 2, '2018-12-19 05:35:29', '2018-12-19 05:35:29'),
(8, 'Bogotá', 'Sin datos', 'Tiempo Estimado de Vuelo', 2, '2018-12-19 05:35:29', '2018-12-19 05:35:29'),
(9, 'Bogotá', 'Sin datos', 'Tiempo Estimado de Vuelo', 2, '2018-12-19 05:35:29', '2018-12-19 05:35:29'),
(10, 'Bogotá', 'Sin datos', 'Tiempo Estimado de Vuelo', 2, '2018-12-19 05:35:29', '2018-12-19 05:35:29'),
(11, 'Terminal de Transportes de Santa Marta', 'Km 18 vía Tobia	(1) 430 3297', 'Información de Contacto', 2, '2018-12-19 05:35:29', '2018-12-19 05:35:29');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `operators`
--

CREATE TABLE `operators` (
  `id_operator` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `national_register` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_personal` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nit_rut` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `service_category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `web` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instagram` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `days_for_reservation` int(11) NOT NULL DEFAULT '2',
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'operator',
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `service_panel` tinyint(1) NOT NULL DEFAULT '0',
  `cuantity_services` int(11) NOT NULL DEFAULT '3',
  `slug` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `outstanding` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `operators`
--

INSERT INTO `operators` (`id_operator`, `name`, `national_register`, `contact_personal`, `contact_phone`, `nit_rut`, `email`, `service_category`, `web`, `facebook`, `twitter`, `instagram`, `address`, `days_for_reservation`, `avatar`, `role`, `status`, `service_panel`, `cuantity_services`, `slug`, `description`, `password`, `outstanding`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Sergio Hernandez', '123123', 'chechito', '23423432', '123123123', 'sergio.hernandez.goyeneche222@gmail.com', '1', 'no hay :c', NULL, NULL, NULL, NULL, 2, NULL, 'operator', 'active', 1, 3, 'sergio-hernandez', '<p>ad asdasd asd asd asd qweqeasd asd 32234sfsefw dsg sht hrth rjgfujf gyj wsywtw23ERF SDF SF SDFQ wef saf df sdf</p>', '$2y$10$6SrcMddPjgu1OtXuYWPAwei53JqQmt1OyoeAt5zfp3hvo2i9vNB1m', 1, 'QagLZ9yIVuwpqECSrnevHVq9sQnLvcGwWpbJQMJcX2aghFBWG3YHHuT7hvsX', '2018-12-11 23:01:22', '2018-12-20 21:53:49'),
(2, 'Sergio Hernandez', '12312312', '3203233082', '3203233082', '', 'gillocaffu-9364@yopmail.com', '1', '123', NULL, NULL, NULL, NULL, 2, NULL, 'operator', 'active', 1, 3, 'sergio-hernandez-1', NULL, '$2y$10$t9h1VUd5YQE9uLfi77PnAejh603oexwgBoTD5IssKIp7PNftAU1ki', 0, NULL, '2018-12-18 03:41:05', '2018-12-18 03:44:26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `operator_password_resets`
--

CREATE TABLE `operator_password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pages`
--

CREATE TABLE `pages` (
  `id_page` int(10) UNSIGNED NOT NULL,
  `page_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `page_parts`
--

CREATE TABLE `page_parts` (
  `id_part` int(10) UNSIGNED NOT NULL,
  `part_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `page_part_pages`
--

CREATE TABLE `page_part_pages` (
  `id_part_page` int(10) UNSIGNED NOT NULL,
  `fk_page` int(10) UNSIGNED NOT NULL,
  `fk_part` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `patterns`
--

CREATE TABLE `patterns` (
  `id_pattern` int(10) UNSIGNED NOT NULL,
  `fk_customer` int(10) UNSIGNED NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `multimedia_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `clicks` int(11) DEFAULT NULL,
  `redirection` int(11) DEFAULT NULL,
  `publication_day` date NOT NULL,
  `publication_finish` date NOT NULL,
  `pattern` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pattern_parts`
--

CREATE TABLE `pattern_parts` (
  `id_pattern_part` int(10) UNSIGNED NOT NULL,
  `fk_pattern` int(10) UNSIGNED NOT NULL,
  `fk_pagepart` int(10) UNSIGNED NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `clicks` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plates`
--

CREATE TABLE `plates` (
  `id_plate` int(10) UNSIGNED NOT NULL,
  `plate` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cost` int(11) NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fk_establishment` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `polls`
--

CREATE TABLE `polls` (
  `id_poll` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` int(11) NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `activities` text COLLATE utf8_unicode_ci NOT NULL,
  `observations` text COLLATE utf8_unicode_ci,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `portable_documents`
--

CREATE TABLE `portable_documents` (
  `id_pdf` int(10) UNSIGNED NOT NULL,
  `link_pdf` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `type_relation` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `fk_relation` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `real_route_points`
--

CREATE TABLE `real_route_points` (
  `id_point` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `kilometer` tinyint(4) NOT NULL,
  `type_relation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fk_relation` int(11) NOT NULL,
  `fk_route` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `real_route_points`
--

INSERT INTO `real_route_points` (`id_point`, `title`, `description`, `kilometer`, `type_relation`, `fk_relation`, `fk_route`, `created_at`, `updated_at`) VALUES
(5, 'Peaje andes', '<p>Peaje Andes.</p>\r\n\r\n<p>Valor $10000.</p>', 10, 'site', 4, 2, '2018-12-05 04:53:59', '2018-12-05 05:18:06');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `regions`
--

CREATE TABLE `regions` (
  `id_region` int(10) UNSIGNED NOT NULL,
  `region_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sentence` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `dark_color` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `light_color` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `multimedia_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type_last_user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fk_last_edition` int(11) NOT NULL,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `regions`
--

INSERT INTO `regions` (`id_region`, `region_name`, `sentence`, `description`, `dark_color`, `light_color`, `multimedia_type`, `slug`, `type_last_user`, `link_image`, `fk_last_edition`, `keywords`, `state`, `created_at`, `updated_at`) VALUES
(1, 'Centro', '', '<p>Rghasdjnasdhgasjhkjd iaousgdhabj ksldopiauysutvd gjasd&nbsp;</p>', '#009984', '#c0e1d7', 'images', 'centro', 'admin', 'images/regions/Region_20181203223943807741725.png', 1, 'region centro', 'activo', '2018-12-04 03:39:43', '2018-12-04 03:39:43'),
(2, 'Costa Caribe', 'Frase de prueba', '<h3>Regi&oacute;n Caribe,</h3>\r\n\r\n<p>&nbsp;Est&aacute; ubicada en la parte norte de Colombia y Am&eacute;rica del Sur. Limita al norte con el mar caribe, al que debe su nombre, al este con Venezuela,&nbsp;al sur con la regi&oacute;n Andina&nbsp;y al oeste con la&nbsp;<a href=\"https://es.wikipedia.org/wiki/Regi%C3%B3n_del_Pac%C3%ADfico_(Colombia)\" title=\"Región del Pacífico (Colombia)\">r</a>egi&oacute;n del Pac&iacute;fico. Sus principales centros urbanos son:</p>\r\n\r\n<p>Recuerda visitar:</p>\r\n\r\n<ul>\r\n	<li>Barranquilla</li>\r\n	<li>Cartagena de Indias</li>\r\n	<li>Soldead.</li>\r\n	<li>Santa Marta</li>\r\n	<li>Valledupar</li>\r\n	<li>Monter&iacute;a</li>\r\n	<li>Sincelejo</li>\r\n	<li>Valledupar</li>\r\n</ul>', '', '', 'images', 'costa-caribe', 'admin', '', 1, 'costa,caribe,', 'activo', NULL, NULL),
(3, 'Amazonia', 'Vive el intercambio cultural con las comunidades indígenas', '<div class=\"col-md-6 col-sm-6\">\r\n<h3>La Amazonia, territorio biodiverso</h3>\r\n\r\n<p>Esta regi&oacute;n de Colombia fascina a los visitantes por la diversidad de su flora y fauna y por la posibilidad de contactar con tribus ind&iacute;genas que conservan sus tradiciones ancestrales.</p>\r\n\r\n<p>En medio de la selva encontrar&aacute; excelentes y c&oacute;modas opciones para alojarse, as&iacute; como un buen n&uacute;mero de actividades para vivir una aventura nueva cada d&iacute;a.</p>\r\n\r\n<p>Recuerda visitar:</p>\r\n\r\n<ul>\r\n	<li>PNN Amacayacu (Amazonas)</li>\r\n	<li>Cascadas del Fin del Mundo (Putumayo)</li>\r\n	<li>Cerros del Mavecure (Guain&iacute;a)</li>\r\n</ul>\r\n</div>', '', '', 'images', 'amazonia', 'admin', '', 1, 'amazonia,no,mames', 'activo', NULL, NULL),
(4, 'Antioquia y Eje Cafetero', 'Encantadores paisajes ideales para los deportes de aventura', '<div class=\"col-md-6 col-sm-6\">\r\n<h3>Antioquia y Eje Cafetero, destinos imperdibles</h3>\r\n\r\n<p>Aqu&iacute; encontrar&aacute; una gran variedad de sitios tur&iacute;sticos, como parques de atracciones, haciendas cafeteras y espacios dedicados al turismo de aventura.</p>\r\n\r\n<p>Recuerda visitar:</p>\r\n\r\n<ul>\r\n	<li>Guatap&eacute; (Antioquia)</li>\r\n	<li>SFF Ot&uacute;n Quimbaya(Risaralda)</li>\r\n	<li>Ecoparque La Monta&ntilde;a (Caldas)</li>\r\n	<li>Calarc&aacute; (Quind&iacute;o)</li>\r\n</ul>\r\n</div>', '', '', 'images', 'antioquia-y-eje-cafetero', 'admin', '', 1, 'antioquia y ,eje, cafetero', 'activo', NULL, NULL),
(5, 'Costa Pacífica', 'Admire las maravillosas zonas de reserva natural', '<div class=\"col-md-6 col-sm-6\">\r\n<h3>La Costa Pac&iacute;fica, reserva de vida silvestre</h3>\r\n\r\n<p>Visitar la Costa Pac&iacute;fica es admirar el escenario representativo de playas coralinas y selvas paradis&iacute;acas en Colombia. Es un destino tur&iacute;stico para quienes desean hacer turismo contemplativo y de aventura responsable, potenciado por la c&aacute;lida acogida de su gente.</p>\r\n\r\n<p>Recuerda visitar:</p>\r\n\r\n<ul>\r\n	<li>Bah&iacute;a Solano (Choc&oacute;)</li>\r\n	<li>Juanchaco y Ladrilleros (Valle del Cauca)</li>\r\n	<li>Tumaco (Nari&ntilde;o)</li>\r\n</ul>\r\n</div>', '', '', 'images', 'costa-pacifica', 'admin', '', 1, 'costa,pacifica', 'activo', NULL, NULL),
(6, 'Llanos Orientales', 'Inmensa llanura para vivir la adrenalina extrema', '<div class=\"col-md-6 col-sm-6\">\r\n<h3>Los Llanos Orientales, inspirador y &uacute;nico</h3>\r\n\r\n<p>En esta regi&oacute;n podr&aacute; apreciar los m&aacute;s bellos atardeceres mientras se relaja gracias al agradable clima fresco que se siente en esta zona de Colombia.</p>\r\n\r\n<p>Recuerda visitar:</p>\r\n\r\n<ul>\r\n	<li>Sierra de la Macarena (Meta)</li>\r\n	<li>Cuevas Vampiro (Casanare)</li>\r\n	<li>Tame (Arauca)</li>\r\n	<li>Pozos Naturales (Guaviare)</li>\r\n</ul>\r\n</div>', '', '', 'images', 'llanos-orientales', 'admin', '', 1, 'llanos, orientales,', 'activo', NULL, NULL),
(7, 'Suroccidente', '', '', '', '', 'images', 'suroccidente', 'admin', '', 1, 'suroccidente, no maes,', 'activo', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `region_images`
--

CREATE TABLE `region_images` (
  `id_region_image` int(10) UNSIGNED NOT NULL,
  `link_image` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `fk_region` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `region_images`
--

INSERT INTO `region_images` (`id_region_image`, `link_image`, `fk_region`, `created_at`, `updated_at`) VALUES
(1, 'images/regions/Region_201812271737361179276291.jpeg', 1, '2018-12-27 22:37:36', '2018-12-27 22:37:36'),
(2, 'images/regions/Region_201812271737361870592550.jpeg', 1, '2018-12-27 22:37:36', '2018-12-27 22:37:36'),
(3, 'images/regions/Region_201812271737371616054680.jpeg', 1, '2018-12-27 22:37:37', '2018-12-27 22:37:37'),
(4, 'images/regions/Region_20181227173737824471508.jpeg', 1, '2018-12-27 22:37:37', '2018-12-27 22:37:37');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reporters`
--

CREATE TABLE `reporters` (
  `id_reporter` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rol` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reporter_password_resets`
--

CREATE TABLE `reporter_password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reservations`
--

CREATE TABLE `reservations` (
  `id_reservation` int(10) UNSIGNED NOT NULL,
  `fk_service` int(10) UNSIGNED NOT NULL,
  `fk_user` int(10) UNSIGNED NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cuantity` int(11) NOT NULL,
  `comment` text COLLATE utf8_unicode_ci,
  `calification` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rooms`
--

CREATE TABLE `rooms` (
  `id_room` int(10) UNSIGNED NOT NULL,
  `room` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cuantity` int(11) NOT NULL,
  `capacity` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bed` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `suite` tinyint(1) NOT NULL,
  `fk_establishment` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `rooms`
--

INSERT INTO `rooms` (`id_room`, `room`, `cuantity`, `capacity`, `bed`, `suite`, `fk_establishment`, `created_at`, `updated_at`) VALUES
(13, 'HABITACION', 12, 'TRES ADULTOS', 'cama king', 1, 1, '2018-12-19 01:14:30', '2018-12-19 01:14:30'),
(14, 'HABITACION', 2, 'TRES ADULTOS', 'cama king camarote', 0, 1, '2018-12-19 01:14:30', '2018-12-19 01:14:30'),
(15, 'HABITACION', 3, 'niños', 'camarotes', 0, 1, '2018-12-19 01:14:30', '2018-12-19 01:14:30');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `routes`
--

CREATE TABLE `routes` (
  `id_route` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sentence` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_map` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `description_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `recommendation` text COLLATE utf8_unicode_ci NOT NULL,
  `recommendation_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `card_image` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `introduction_image` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `images` text COLLATE utf8_unicode_ci,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `routes`
--

INSERT INTO `routes` (`id_route`, `name`, `sentence`, `keywords`, `file_map`, `description`, `description_image`, `recommendation`, `recommendation_image`, `card_image`, `introduction_image`, `images`, `state`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'BOGOTÁ - SANTA MARTA', 'RECORRE COLOMBIA', 'rutaa,bogotá,santamarta,rutascolombia', NULL, '<div class=\"aboutus\">\r\n<p>Lo hecho en las seis temporadas que lleva en el World Tour tienen a Quintana como uno de los preferidos de la afici&oacute;n y quien podr&iacute;a escalar posiciones en caso de lograr la corona en el Tour de Francia, carrera en la que se ha subido al podio en tres ocasiones.<br />\r\n<br />\r\nLo hecho en las seis temporadas que lleva en el World Tour tienen a Quintana como uno de los preferidos de la afici&oacute;n y quien podr&iacute;a escalar posiciones en caso de lograr la corona en el Tour de Francia, carrera en la que se ha subido al podio en tres ocasiones.</p>\r\n</div>\r\n\r\n<div class=\"row\">\r\n<div class=\"col-sm-6\">\r\n<ul>\r\n	<li>Jacks of all. Experts in all.</li>\r\n	<li>It&#39;s good to virtually meet you.</li>\r\n	<li>A crew of creative doers.</li>\r\n	<li>Let&#39;s create something great.</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"col-sm-6\">\r\n<ul>\r\n	<li>Jacks of all. Experts in all.</li>\r\n	<li>It&#39;s good to virtually meet you.</li>\r\n	<li>A crew of creative doers.</li>\r\n	<li>Let&#39;s create something great.</li>\r\n</ul>\r\n</div>\r\n</div>', NULL, '<p>Ludus albucius <strong>adversarium eam eu</strong>. Sit eu reque tation aliquip. Quo no dolorum albucius lucilius, hinc eligendi ut sed. Ex nam quot ferri suscipit, mea ne legere alterum repudiandae. Ei pri quaerendum intellegebat, ut vel consequuntur voluptatibus. Et volumus sententiae adversarium duo.</p>\r\n\r\n<h4>Mel at vide soluta</h4>\r\n\r\n<p>Ad cum movet fierent assueverit, mei stet legere ne. Mel at vide soluta, ut utamur antiopam inciderint sed. Ut iriure perpetua voluptaria has, vim postea denique in, <strong>mollis pertinax elaboraret</strong> sed in. Per no vidit timeam, quis omittam sed at. Ludus albucius adversarium eam eu. Sit eu reque tation aliquip. Quo no dolorum albucius lucilius, hinc eligendi ut sed. Ex nam quot ferri suscipit, mea ne legere alterum repudiandae.</p>\r\n\r\n<h4>Don&#39;t forget</h4>\r\n\r\n<p>Ad cum movet fierent assueverit, mei stet legere ne. Mel at vide soluta, ut <a href=\"#\">utamur antiopam inciderint</a> sed. Ut iriure perpetua voluptaria has, vim postea denique in, mollis pertinax elaboraret sed in. Per no vidit timeam, quis omittam sed at.</p>', NULL, NULL, NULL, NULL, 'activo', 'bogota-santamarta', '2018-12-04 05:36:52', '2018-12-04 05:39:22'),
(2, 'BOGOTÁ - MEDELLIN', 'RECORRE COLOMBIA', 'bogotá,medallo', NULL, '<div class=\"aboutus\">\r\n                            <p class=\"aboutus-text\">Lo hecho en las seis temporadas que lleva en el World Tour tienen a Quintana como uno de los preferidos de la afición y quien podría escalar posiciones en caso de lograr la corona en el Tour de Francia, carrera en la que se ha subido al podio en tres ocasiones.<br>\r\n                            <br>\r\n                            Lo hecho en las seis temporadas que lleva en el World Tour tienen a Quintana como uno de los preferidos de la afición y quien podría escalar posiciones en caso de lograr la corona en el Tour de Francia, carrera en la que se ha subido al podio en tres ocasiones.</p>\r\n                        </div>\r\n\r\n                        <div class=\"row\">\r\n                            <div class=\"col-sm-6\">\r\n                                <ul class=\"list-unstyled g-font-size-13 mb-0\">\r\n                                    <li class=\"d-flex mb-3\">\r\n                                        <i class=\"g-color-primary g-mt-5 g-mr-10 icon-check\"></i>\r\n                                        Jacks of all. Experts in all.\r\n                                    </li>\r\n                                    <li class=\"d-flex mb-3\">\r\n                                        <i class=\"g-color-primary g-mt-5 g-mr-10 icon-check\"></i>\r\n                                        It\'s good to virtually meet you.\r\n                                    </li>\r\n                                    <li class=\"d-flex mb-3\">\r\n                                        <i class=\"g-color-primary g-mt-5 g-mr-10 icon-check\"></i>\r\n                                        A crew of creative doers.\r\n                                    </li>\r\n                                    <li class=\"d-flex mb-3\">\r\n                                        <i class=\"g-color-primary g-mt-5 g-mr-10 icon-check\"></i>\r\n                                        Let\'s create something great.\r\n                                    </li>\r\n                                </ul>\r\n                            </div>\r\n\r\n                            <div class=\"col-sm-6\">\r\n                                <ul class=\"list-unstyled g-font-size-13 mb-0\">\r\n                                    <li class=\"d-flex mb-3\">\r\n                                        <i class=\"g-color-primary g-mt-5 g-mr-10 icon-check\"></i>\r\n                                        Jacks of all. Experts in all.\r\n                                    </li>\r\n                                    <li class=\"d-flex mb-3\">\r\n                                        <i class=\"g-color-primary g-mt-5 g-mr-10 icon-check\"></i>\r\n                                        It\'s good to virtually meet you.\r\n                                    </li>\r\n                                    <li class=\"d-flex mb-3\">\r\n                                        <i class=\"g-color-primary g-mt-5 g-mr-10 icon-check\"></i>\r\n                                        A crew of creative doers.\r\n                                    </li>\r\n                                    <li class=\"d-flex mb-3\">\r\n                                        <i class=\"g-color-primary g-mt-5 g-mr-10 icon-check\"></i>\r\n                                        Let\'s create something great.\r\n                                    </li>\r\n                                </ul>\r\n                            </div>\r\n                        </div>', NULL, '<p>Ludus albucius <strong>adversarium eam eu</strong>. Sit eu reque tation aliquip. Quo no dolorum albucius lucilius, hinc eligendi ut sed. Ex nam quot ferri suscipit, mea ne legere alterum repudiandae. Ei pri quaerendum intellegebat, ut vel consequuntur voluptatibus. Et volumus sententiae adversarium duo.</p>\r\n\r\n<h4>Mel at vide soluta</h4>\r\n\r\n<p>Ad cum movet fierent assueverit, mei stet legere ne. Mel at vide soluta, ut utamur antiopam inciderint sed. Ut iriure perpetua voluptaria has, vim postea denique in, <strong>mollis pertinax elaboraret</strong> sed in. Per no vidit timeam, quis omittam sed at. Ludus albucius adversarium eam eu. Sit eu reque tation aliquip. Quo no dolorum albucius lucilius, hinc eligendi ut sed. Ex nam quot ferri suscipit, mea ne legere alterum repudiandae.</p>\r\n\r\n<h4>Don&#39;t forget</h4>\r\n\r\n<p>Ad cum movet fierent assueverit, mei stet legere ne. Mel at vide soluta, ut <a href=\"#\">utamur antiopam inciderint</a> sed. Ut iriure perpetua voluptaria has, vim postea denique in, mollis pertinax elaboraret sed in. Per no vidit timeam, quis omittam sed at.</p>', NULL, 'images/route/route_201812140622271843589258.png', 'images/route/route_20181214062228752482907.png', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>', 'activo', 'bogota-medellin', '2018-12-04 05:41:37', '2018-12-15 02:08:29');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `route_images`
--

CREATE TABLE `route_images` (
  `id_route_image` int(10) UNSIGNED NOT NULL,
  `link_image` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `fk_route` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `route_images`
--

INSERT INTO `route_images` (`id_route_image`, `link_image`, `fk_route`, `created_at`, `updated_at`) VALUES
(2, 'images/routes/route201812060258211324670070.png', 2, '2018-12-06 07:58:21', '2018-12-06 07:58:21'),
(3, 'images/routes/route201812060258211962638714.jpg', 2, '2018-12-06 07:58:21', '2018-12-06 07:58:21');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `route_points`
--

CREATE TABLE `route_points` (
  `id_point` int(10) UNSIGNED NOT NULL,
  `subtitle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `order` tinyint(4) NOT NULL,
  `fk_route` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `route_points`
--

INSERT INTO `route_points` (`id_point`, `subtitle`, `title`, `description`, `order`, `fk_route`, `created_at`, `updated_at`) VALUES
(1, 'Lorem ipsum', 'MEETING POINT', '<p>Winter purslane courgette pumpkin quandong komatsuna fennel green bean cucumber watercress. Pea sprouts wattle seed rutabaga okra yarrow cress avocado grape radish bush tomato ricebean black-eyed pea maize eggplant.</p>', 1, 1, '2018-12-12 10:31:03', '2018-12-12 10:31:03'),
(2, 'Lorem ipsum', 'MEETING POINT', '<p>Winter purslane courgette pumpkin quandong komatsuna fennel green bean cucumber watercress. Pea sprouts wattle seed rutabaga okra yarrow cress avocado grape radish bush tomato ricebean black-eyed pea maize eggplant.</p>', 2, 1, '2018-12-12 10:31:14', '2018-12-12 10:31:14'),
(3, 'Lorem ipsum', 'MEETING POINT', '<p>Winter purslane courgette pumpkin quandong komatsuna fennel green bean cucumber watercress. Pea sprouts wattle seed rutabaga okra yarrow cress avocado grape radish bush tomato ricebean black-eyed pea maize eggplant.</p>', 3, 1, '2018-12-12 10:31:15', '2018-12-12 10:31:15'),
(4, 'Lorem ipsum', 'MEETING POINT', '<p>Winter purslane courgette pumpkin quandong komatsuna fennel green bean cucumber watercress. Pea sprouts wattle seed rutabaga okra yarrow cress avocado grape radish bush tomato ricebean black-eyed pea maize eggplant.</p>', 4, 1, '2018-12-12 10:31:16', '2018-12-12 10:31:16'),
(5, 'Lorem ipsum', 'MEETING POINT', '<p>Winter purslane courgette pumpkin quandong komatsuna fennel green bean cucumber watercress. Pea sprouts wattle seed rutabaga okra yarrow cress avocado grape radish bush tomato ricebean black-eyed pea maize eggplant.</p>', 5, 1, '2018-12-12 10:31:17', '2018-12-12 10:31:17'),
(6, 'Lorem ipsum', 'MEETING POINT', '<p>Winter purslane courgette pumpkin quandong komatsuna fennel green bean cucumber watercress. Pea sprouts wattle seed rutabaga okra yarrow cress avocado grape radish bush tomato ricebean black-eyed pea maize eggplant.</p>', 6, 1, '2018-12-12 10:31:22', '2018-12-12 10:31:22');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `schedules`
--

CREATE TABLE `schedules` (
  `id_schedule` int(10) UNSIGNED NOT NULL,
  `fk_service` int(10) UNSIGNED NOT NULL,
  `day` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hora_inicio` time DEFAULT NULL,
  `hora_receso` time DEFAULT NULL,
  `tiempo_receso` int(11) DEFAULT NULL,
  `hora_final` time DEFAULT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `schedules`
--

INSERT INTO `schedules` (`id_schedule`, `fk_service`, `day`, `hora_inicio`, `hora_receso`, `tiempo_receso`, `hora_final`, `state`, `created_at`, `updated_at`) VALUES
(1, 2, 'Lunes', NULL, NULL, NULL, NULL, 'active', '2018-12-11 23:44:20', '2018-12-11 23:44:20'),
(2, 3, 'Lunes', '08:00:00', '12:00:00', 0, '18:00:00', 'active', '2018-12-11 23:49:07', '2018-12-11 23:51:20'),
(3, 4, 'Lunes', '08:00:00', '12:00:00', 0, '18:00:00', 'active', '2018-12-18 03:44:26', '2018-12-18 03:44:40');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `services`
--

CREATE TABLE `services` (
  `id_service` int(10) UNSIGNED NOT NULL,
  `service` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type_service` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fk_service_category` int(10) UNSIGNED NOT NULL,
  `slug` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `services`
--

INSERT INTO `services` (`id_service`, `service`, `type_service`, `description`, `image`, `fk_service_category`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Paracaidismo', '', '', NULL, 1, 'paracaidismo', '2018-12-11 23:02:48', '2018-12-11 23:02:48'),
(2, 'nomames', '', '', NULL, 1, 'nomames', '2018-12-18 03:43:15', '2018-12-18 03:43:15');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `service_categories`
--

CREATE TABLE `service_categories` (
  `id_category` int(10) UNSIGNED NOT NULL,
  `service_category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `service_categories`
--

INSERT INTO `service_categories` (`id_category`, `service_category`, `created_at`, `updated_at`) VALUES
(1, 'Aire', '2018-12-11 23:02:26', '2018-12-11 23:02:26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `service_establishments`
--

CREATE TABLE `service_establishments` (
  `id_service_establishment` int(10) UNSIGNED NOT NULL,
  `service_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `service_establishments`
--

INSERT INTO `service_establishments` (`id_service_establishment`, `service_name`, `created_at`, `updated_at`) VALUES
(1, 'Wifi', NULL, NULL),
(2, 'Plasma Tv', NULL, NULL),
(3, 'Carta de vinos y licores', NULL, NULL),
(4, 'Eventos especiales', NULL, NULL),
(5, 'Cenas V.I.P.', NULL, NULL),
(6, 'Salón de espera', NULL, NULL),
(7, 'Música en vivo', NULL, NULL),
(8, 'Música ambiental', NULL, NULL),
(9, 'Acceso para sillas de ruedas', NULL, NULL),
(10, 'Opciones sin gluten', NULL, NULL),
(11, 'Parqueadero', NULL, NULL),
(12, 'Valet parking', NULL, NULL),
(13, 'Traductor', NULL, NULL),
(14, 'Caja de seguridad', NULL, NULL),
(15, 'Catering para eventos', NULL, NULL),
(16, 'Pet Friendly', NULL, NULL),
(17, 'Entrega a domicilio', NULL, NULL),
(18, 'Acepta tarjetas de crédito', NULL, NULL),
(19, 'Opciones veganas', NULL, NULL),
(20, 'Apto para vegetarianos', NULL, NULL),
(21, 'No se permiten mascotas', NULL, NULL),
(22, 'Zona de fumadores', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `service_establishment_pivots`
--

CREATE TABLE `service_establishment_pivots` (
  `id_service_establishment_pivot` int(10) UNSIGNED NOT NULL,
  `fk_service` int(10) UNSIGNED NOT NULL,
  `fk_establishment` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `service_establishment_pivots`
--

INSERT INTO `service_establishment_pivots` (`id_service_establishment_pivot`, `fk_service`, `fk_establishment`, `created_at`, `updated_at`) VALUES
(1, 10, 1, NULL, NULL),
(2, 12, 1, NULL, NULL),
(3, 13, 1, NULL, NULL),
(4, 4, 2, NULL, NULL),
(5, 9, 2, NULL, NULL),
(6, 13, 2, NULL, NULL),
(7, 14, 2, NULL, NULL),
(8, 21, 2, NULL, NULL),
(9, 3, 1, NULL, NULL),
(10, 5, 1, NULL, NULL),
(11, 7, 1, NULL, NULL),
(12, 18, 1, NULL, NULL),
(13, 19, 1, NULL, NULL),
(14, 20, 1, NULL, NULL),
(15, 21, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `service_items`
--

CREATE TABLE `service_items` (
  `id_service_item` int(10) UNSIGNED NOT NULL,
  `item_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cost` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `fk_service` int(10) UNSIGNED NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `service_operators`
--

CREATE TABLE `service_operators` (
  `id_service_operator` int(10) UNSIGNED NOT NULL,
  `fk_service` int(10) UNSIGNED NOT NULL,
  `fk_operator` int(10) UNSIGNED NOT NULL,
  `service_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `latitude` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `longitude` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `capacity` int(11) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `video` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `requisites` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `policies` text COLLATE utf8_unicode_ci NOT NULL,
  `fk_municipality` int(10) UNSIGNED NOT NULL,
  `fk_interest_site` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Inhabilitado',
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `service_operators`
--

INSERT INTO `service_operators` (`id_service_operator`, `fk_service`, `fk_operator`, `service_name`, `address`, `latitude`, `longitude`, `capacity`, `duration`, `video`, `requisites`, `description`, `policies`, `fk_municipality`, `fk_interest_site`, `state`, `slug`, `created_at`, `updated_at`) VALUES
(2, 1, 1, '24234234', 'cra 147 # 142 50, cra 142#134-33', '4.7517403', '-74.11751449999997', 30, 1, '', '<p>SDF SDF&nbsp;</p>', '<p>SFSDF SDF</p>', '<p>SDF SDF</p>', 2, NULL, 'Activo', 'cra-147-142-50-cra-142-134-33', '2018-12-11 23:44:20', '2018-12-11 23:44:20'),
(3, 1, 1, '24234234', 'cra 147 # 142 50, cra 142#134-33', '4.7517403', '-74.11751449999997', 30, 1, '', '<p>SDF SDF&nbsp;</p>', '<p>SFSDF SDF</p>', '<p>SDF SDF</p>', 1, NULL, 'Activo', 'cra-147-142-50-cra-142-134-33-1', '2018-12-11 23:49:07', '2018-12-11 23:51:20'),
(4, 2, 2, '15 bewdfw dw', 'cra 147 # 142 50, cra 142#134-33', '4.570868', '-74.29733299999998', 20, 2, NULL, '<p>a sda sd</p>', '<p>asd asd asd asd asd as d</p>', '<p>as d asdasd&nbsp;</p>', 2, NULL, 'Activo', 'cra-147-142-50-cra-142-134-33-2', '2018-12-18 03:44:26', '2018-12-18 03:44:40');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `service_pictures`
--

CREATE TABLE `service_pictures` (
  `id_picture` int(10) UNSIGNED NOT NULL,
  `link_image` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `fk_service` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `service_pictures`
--

INSERT INTO `service_pictures` (`id_picture`, `link_image`, `fk_service`, `created_at`, `updated_at`) VALUES
(1, 'images/services/Service_20181220182039225602712.jpeg', 2, '2018-12-20 23:20:39', '2018-12-20 23:20:39'),
(2, 'images/services/Service_20181220182039549822813.jpeg', 2, '2018-12-20 23:20:39', '2018-12-20 23:20:39'),
(3, 'images/services/Service_201812201820401437681586.jpeg', 2, '2018-12-20 23:20:40', '2018-12-20 23:20:40'),
(4, 'images/services/Service_201812201820401979581585.jpeg', 2, '2018-12-20 23:20:40', '2018-12-20 23:20:40');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `site_keydatas`
--

CREATE TABLE `site_keydatas` (
  `id_keydata` int(10) UNSIGNED NOT NULL,
  `keydata_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keydata_value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fk_site` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `specials`
--

CREATE TABLE `specials` (
  `id_special` int(10) UNSIGNED NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `short_description` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `recommendations` text COLLATE utf8_unicode_ci,
  `card_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `introduction_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `recommendation_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_to_site` text COLLATE utf8_unicode_ci,
  `text_images` text COLLATE utf8_unicode_ci,
  `video` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `iframe` text COLLATE utf8_unicode_ci,
  `download_file` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `outstanding` tinyint(1) NOT NULL DEFAULT '0',
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `specials`
--

INSERT INTO `specials` (`id_special`, `type`, `name`, `keywords`, `short_description`, `description`, `recommendations`, `card_image`, `introduction_image`, `description_image`, `recommendation_image`, `description_to_site`, `text_images`, `video`, `iframe`, `download_file`, `slug`, `outstanding`, `state`, `created_at`, `updated_at`) VALUES
(1, 'normal', 'web', '123', '123', '<p>123</p>', '<p>123</p>', NULL, NULL, NULL, NULL, '<p>123</p>', NULL, NULL, NULL, NULL, 'web', 0, 'activo', '2018-12-06 08:41:44', '2018-12-07 23:32:52'),
(2, 'normal', 'INFORMACIÓN IMPORTANTE', 'INFORMACIÓN IMPORTANTE', 'INFORMACIÓN IMPORTANTE', '<div class=\"col-md-4 col-sm-6 col-xs-12\">\r\n<div class=\"aboutus\">\r\n<p>Lo hecho en las seis temporadas que lleva en el World Tour tienen a Quintana como uno de los preferidos de la afici&oacute;n y quien podr&iacute;a escalar posiciones en caso de lograr la corona en el Tour de Francia, carrera en la que se ha subido al podio en tres ocasiones.<br />\r\n<br />\r\nLorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex, appareat similique an usu.</p>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-md-5 col-sm-6 col-xs-12\">\r\n<div class=\"bicifeature\">\r\n<h3>+120 Premium city tours</h3>\r\n\r\n<p>Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor.</p>\r\n</div>\r\n\r\n<div class=\"bicifeature\">\r\n<h3>+120 Premium city tours</h3>\r\n\r\n<p>Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor.</p>\r\n</div>\r\n</div>', '<p>Ludus albucius <strong>adversarium eam eu</strong>. Sit eu reque tation aliquip. Quo no dolorum albucius lucilius, hinc eligendi ut sed. Ex nam quot ferri suscipit, mea ne legere alterum repudiandae. Ei pri quaerendum intellegebat, ut vel consequuntur voluptatibus. Et volumus sententiae adversarium duo.</p>\r\n\r\n<h4>Mel at vide soluta</h4>\r\n\r\n<p>Ad cum movet fierent assueverit, mei stet legere ne. Mel at vide soluta, ut utamur antiopam inciderint sed. Ut iriure perpetua voluptaria has, vim postea denique in, <strong>mollis pertinax elaboraret</strong> sed in. Per no vidit timeam, quis omittam sed at. Ludus albucius adversarium eam eu. Sit eu reque tation aliquip. Quo no dolorum albucius lucilius, hinc eligendi ut sed. Ex nam quot ferri suscipit, mea ne legere alterum repudiandae.</p>\r\n\r\n<h4>Don&#39;t forget</h4>\r\n\r\n<p>Ad cum movet fierent assueverit, mei stet legere ne. Mel at vide soluta, ut <a href=\"#\">utamur antiopam inciderint</a> sed. Ut iriure perpetua voluptaria has, vim postea denique in, mollis pertinax elaboraret sed in. Per no vidit timeam, quis omittam sed at.</p>', 'images/special/special_201812071831162110778123.jpg', 'images/special/special_20181207183116304974139.jpg', 'images/special/special_201812071831162123379230.png', 'images/special/special_201812071831161019617133.jpg', NULL, '<p>asdas dasd asd asd asd</p>', 'tvgpLZZgqOA', NULL, NULL, 'informacion-importante', 1, 'activo', '2018-12-07 23:31:16', '2018-12-12 04:06:39'),
(3, 'rutas', 'COLOMBIA EN BICI', 'COLOMBIA EN BICI,COLOMBIA', 'UNA INNOVADORA FORMA DE RECORRER COLOMBIA', '<p>Aqu&iacute; va una introducci&oacute;n al especial.</p>\r\n\r\n<p>Lo hecho en las seis temporadas que lleva en el World Tour tienen a Quintana como uno de los preferidos de la afici&oacute;n y quien podr&iacute;a escalar posiciones en caso de lograr la corona en el Tour de Francia, carrera en la que se ha subido al podio en tres ocasiones, recordando lo hecho en el 2012 cuando fue segundo por detr&aacute;s de Froome, adem&aacute;s de ganar la camiseta de la clasificaci&oacute;n de los j&oacute;venes y la de la monta&ntilde;a.</p>', NULL, 'images/special/special_20181207183945770847836.png', 'images/special/special_201812071839451402406240.png', 'images/special/special_201812120543291909070871.png', NULL, NULL, NULL, NULL, NULL, NULL, 'colombia-en-bici', 1, 'activo', '2018-12-07 23:39:45', '2018-12-12 10:43:29');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `special_detail_items`
--

CREATE TABLE `special_detail_items` (
  `id_detail_item` int(10) UNSIGNED NOT NULL,
  `image_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `fk_special` int(10) UNSIGNED NOT NULL,
  `type_relation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fk_relation` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `special_detail_items`
--

INSERT INTO `special_detail_items` (`id_detail_item`, `image_link`, `description`, `fk_special`, `type_relation`, `fk_relation`, `created_at`, `updated_at`) VALUES
(1, '', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos in eveniet tempore asperiores totam laboriosam. Iusto placeat aliquid ad qui voluptates quaerat, temporibus aut et minima recusandae, explicabo, cumque doloribus!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos in eveniet tempore asperiores totam laboriosam. Iusto placeat aliquid ad qui voluptates quaerat, temporibus aut et minima recusandae, explicabo, cumque doloribus!</p>', 2, 'site', 1, '2018-12-07 23:32:13', '2018-12-07 23:32:13'),
(2, '', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos in eveniet tempore asperiores totam laboriosam. Iusto placeat aliquid ad qui voluptates quaerat, temporibus aut et minima recusandae, explicabo, cumque doloribus!</p>', 2, 'site', 2, '2018-12-07 23:32:23', '2018-12-07 23:32:23'),
(3, '', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos in eveniet tempore asperiores totam laboriosam. Iusto placeat aliquid ad qui voluptates quaerat, temporibus aut et minima recusandae, explicabo, cumque doloribus!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos in eveniet tempore asperiores totam laboriosam. Iusto placeat aliquid ad qui voluptates quaerat, temporibus aut et minima recusandae, explicabo, cumque doloribus!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos in eveniet tempore asperiores totam laboriosam. Iusto placeat aliquid ad qui voluptates quaerat, temporibus aut et minima recusandae, explicabo, cumque doloribus!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos in eveniet tempore asperiores totam laboriosam. Iusto placeat aliquid ad qui voluptates quaerat, temporibus aut et minima recusandae, explicabo, cumque doloribus!</p>', 2, 'site', 4, '2018-12-07 23:32:37', '2018-12-07 23:32:37'),
(4, 'images/detailitem/detailitem_201812111735051142506068.jpg', '<p>adasd asd asd as</p>', 2, 'municipality', 2, '2018-12-11 22:35:06', '2018-12-11 22:35:06');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `special_images`
--

CREATE TABLE `special_images` (
  `id_special_image` int(10) UNSIGNED NOT NULL,
  `link_image` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `fk_special` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `special_images`
--

INSERT INTO `special_images` (`id_special_image`, `link_image`, `description`, `fk_special`, `created_at`, `updated_at`) VALUES
(1, 'images/special/special_20181206041133305350398.png', '', 1, '2018-12-06 09:11:33', '2018-12-06 09:11:33'),
(2, 'images/special/special_20181206041133370293110.jpg', '', 1, '2018-12-06 09:11:33', '2018-12-06 09:11:33'),
(7, 'images/special/special_201812112352152141024855.jpeg', '', 2, '2018-12-12 04:52:15', '2018-12-12 04:52:15'),
(8, 'images/special/special_20181211235215213630463.jpeg', '', 2, '2018-12-12 04:52:15', '2018-12-12 04:52:15'),
(9, 'images/special/special_20181211235216523036432.jpeg', '', 2, '2018-12-12 04:52:16', '2018-12-12 04:52:16'),
(10, 'images/special/special_20181211235216572637127.jpeg', '', 2, '2018-12-12 04:52:16', '2018-12-12 04:52:16');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `special_items`
--

CREATE TABLE `special_items` (
  `id_item` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `fk_special` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `special_items`
--

INSERT INTO `special_items` (`id_item`, `title`, `description`, `fk_special`, `created_at`, `updated_at`) VALUES
(1, 'item 1', 'Información referente al ítem 1. Lo hecho en las seis temporadas que lleva en el World Tour tienen a Quintana como uno de los preferidos de la afición', 3, '2018-12-07 23:40:23', '2018-12-07 23:40:23'),
(2, 'ITEM 2', 'Información referente al ítem 1. Lo hecho en las seis temporadas que lleva en el World Tour tienen a Quintana como uno de los preferidos de la afición', 3, '2018-12-07 23:40:30', '2018-12-07 23:40:30'),
(3, 'ITEM 3', 'Información referente al ítem 1. Lo hecho en las seis temporadas que lleva en el World Tour tienen a Quintana como uno de los preferidos de la afición', 3, '2018-12-07 23:40:37', '2018-12-07 23:40:37');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `special_routes`
--

CREATE TABLE `special_routes` (
  `id_route` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sentence` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `length` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `difficulty` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `video` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_map` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `highest_point` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `lower_point` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `description_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `card_image` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `introduction_image` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `recommendation` text COLLATE utf8_unicode_ci NOT NULL,
  `text_images` text COLLATE utf8_unicode_ci,
  `recommendation_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type_startpoint` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `start_point` int(11) NOT NULL,
  `type_endpoint` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `end_point` int(11) NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fk_special` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `special_routes`
--

INSERT INTO `special_routes` (`id_route`, `name`, `sentence`, `length`, `difficulty`, `keywords`, `video`, `url_map`, `highest_point`, `lower_point`, `description`, `description_image`, `card_image`, `introduction_image`, `recommendation`, `text_images`, `recommendation_image`, `type_startpoint`, `start_point`, `type_endpoint`, `end_point`, `state`, `slug`, `fk_special`, `created_at`, `updated_at`) VALUES
(1, 'RUTA EN BICI NO. 1', 'BiciAventura en Antioquia', '181,72', 'Difícil', 'pa,aspdaspd asd,a sdas,d asd', NULL, 'https://es.wikiloc.com/wikiloc/spatialArtifacts.do?event=view&id=21591335&measures=on&title=off&near=off&images=off&maptype=H', '3.360', '1.350', '<div class=\"bicifeature\">\r\n<h3>+120 Premium city tours</h3>\r\n\r\n<p>Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor.</p>\r\n</div>\r\n\r\n<div class=\"bicifeature\">\r\n<h3>+120 Premium city tours</h3>\r\n\r\n<p>Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor.</p>\r\n</div>', 'images/specialroute/specialroute_20181212052945851462110.png', 'images/special/special_20181212060223536628951.png', 'images/route/route_201812122129391969006171.png', '<p>Ludus albucius <strong>adversarium eam eu</strong>. Sit eu reque tation aliquip. Quo no dolorum albucius lucilius, hinc eligendi ut sed. Ex nam quot ferri suscipit, mea ne legere alterum repudiandae. Ei pri quaerendum intellegebat, ut vel consequuntur voluptatibus. Et volumus sententiae adversarium duo.</p>\r\n\r\n<h4>Mel at vide soluta</h4>\r\n\r\n<p>Ad cum movet fierent assueverit, mei stet legere ne. Mel at vide soluta, ut utamur antiopam inciderint sed. Ut iriure perpetua voluptaria has, vim postea denique in, <strong>mollis pertinax elaboraret</strong> sed in. Per no vidit timeam, quis omittam sed at. Ludus albucius adversarium eam eu. Sit eu reque tation aliquip. Quo no dolorum albucius lucilius, hinc eligendi ut sed. Ex nam quot ferri suscipit, mea ne legere alterum repudiandae.</p>\r\n\r\n<h4>Don&#39;t forget</h4>\r\n\r\n<p>Ad cum movet fierent assueverit, mei stet legere ne. Mel at vide soluta, ut <a href=\"#\">utamur antiopam inciderint</a> sed. Ut iriure perpetua voluptaria has, vim postea denique in, mollis pertinax elaboraret sed in. Per no vidit timeam, quis omittam sed at.</p>', NULL, 'images/specialroute/specialroute_20181212052945387472724.jpg', 'site', 1, 'municipality', 2, 'activo', 'ruta-en-bici-no-1', 3, '2018-12-12 10:29:45', '2018-12-13 02:39:25');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `special_route_images`
--

CREATE TABLE `special_route_images` (
  `id_route_image` int(10) UNSIGNED NOT NULL,
  `link_image` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `fk_route` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `special_route_images`
--

INSERT INTO `special_route_images` (`id_route_image`, `link_image`, `description`, `fk_route`, `created_at`, `updated_at`) VALUES
(9, 'images/special/special_20181212204117856458485.jpeg', '', 1, '2018-12-13 01:41:17', '2018-12-13 01:41:17'),
(10, 'images/special/special_201812122041171631958959.jpeg', '', 1, '2018-12-13 01:41:17', '2018-12-13 01:41:17'),
(11, 'images/special/special_201812122041181329568899.jpeg', '', 1, '2018-12-13 01:41:18', '2018-12-13 01:41:18'),
(12, 'images/special/special_20181212204118671223488.jpeg', '', 1, '2018-12-13 01:41:18', '2018-12-13 01:41:18');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tolls`
--

CREATE TABLE `tolls` (
  `id_toll` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cost_1` int(11) DEFAULT NULL,
  `cost_2` int(11) DEFAULT NULL,
  `cost_3` int(11) DEFAULT NULL,
  `cost_4` int(11) DEFAULT NULL,
  `cost_5` int(11) DEFAULT NULL,
  `cost_6` int(11) DEFAULT NULL,
  `cost_7` int(11) DEFAULT NULL,
  `fk_site` int(10) UNSIGNED NOT NULL,
  `fk_concession` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `tolls`
--

INSERT INTO `tolls` (`id_toll`, `name`, `link_image`, `cost_1`, `cost_2`, `cost_3`, `cost_4`, `cost_5`, `cost_6`, `cost_7`, `fk_site`, `fk_concession`, `created_at`, `updated_at`) VALUES
(1, 'peaje-aburra-e', NULL, 20000, 20000, 20000, 20000, NULL, 20000, 19996, 5, 1, '2018-12-06 03:53:00', '2018-12-06 03:53:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'user',
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_change` tinyint(1) NOT NULL DEFAULT '1',
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_password_resets`
--

CREATE TABLE `user_password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `videos`
--

CREATE TABLE `videos` (
  `id_video` int(10) UNSIGNED NOT NULL,
  `link_video` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `type_relation` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `fk_relation` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `videos`
--

INSERT INTO `videos` (`id_video`, `link_video`, `type_relation`, `fk_relation`, `created_at`, `updated_at`) VALUES
(1, 'xp6706wVdCI', 'fair', 1, '2018-12-21 05:59:27', '2018-12-21 05:59:27'),
(2, 'EPVgHkKQX4k', 'fair', 1, '2018-12-21 05:59:52', '2018-12-21 05:59:52');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wallpapers`
--

CREATE TABLE `wallpapers` (
  `id_wallpaper` int(10) UNSIGNED NOT NULL,
  `link_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `primary_text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `primary_color` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `secundary_text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `secundary_color` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url_redirection` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `wallpapers`
--

INSERT INTO `wallpapers` (`id_wallpaper`, `link_image`, `primary_text`, `primary_color`, `secundary_text`, `secundary_color`, `url_redirection`, `state`, `created_at`, `updated_at`) VALUES
(1, 'wallpapers/circuit201812272018481211831790.jpg', 'PULMÓN DEL MUNDO', '#0540ff', 'Visitar este lugar es ideal para respirar aire puro', '#1647f5', '/home/region/amazonas', 'activo', '2018-12-28 01:18:48', '2018-12-28 01:18:48');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`id_activity`);

--
-- Indices de la tabla `activity_pivots`
--
ALTER TABLE `activity_pivots`
  ADD PRIMARY KEY (`id_activity_pivot`),
  ADD KEY `activity_pivots_fk_actity_foreign` (`fk_actity`);

--
-- Indices de la tabla `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indices de la tabla `admin_password_resets`
--
ALTER TABLE `admin_password_resets`
  ADD KEY `admin_password_resets_email_index` (`email`),
  ADD KEY `admin_password_resets_token_index` (`token`);

--
-- Indices de la tabla `circuits`
--
ALTER TABLE `circuits`
  ADD PRIMARY KEY (`id_circuit`);

--
-- Indices de la tabla `circuit_detail_items`
--
ALTER TABLE `circuit_detail_items`
  ADD PRIMARY KEY (`id_detail_item`),
  ADD KEY `circuit_detail_items_fk_circuit_foreign` (`fk_circuit`);

--
-- Indices de la tabla `circuit_images`
--
ALTER TABLE `circuit_images`
  ADD PRIMARY KEY (`id_circuit_image`),
  ADD KEY `circuit_images_fk_circuit_foreign` (`fk_circuit`);

--
-- Indices de la tabla `concessions`
--
ALTER TABLE `concessions`
  ADD PRIMARY KEY (`id_concession`);

--
-- Indices de la tabla `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id_contact`);

--
-- Indices de la tabla `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id_customer`);

--
-- Indices de la tabla `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id_department`),
  ADD KEY `departments_fk_region_foreign` (`fk_region`);

--
-- Indices de la tabla `department_images`
--
ALTER TABLE `department_images`
  ADD PRIMARY KEY (`id_department_image`),
  ADD KEY `department_images_fk_department_foreign` (`fk_department`);

--
-- Indices de la tabla `establishments`
--
ALTER TABLE `establishments`
  ADD PRIMARY KEY (`id_establishment`),
  ADD KEY `establishments_fk_municipality_foreign` (`fk_municipality`);

--
-- Indices de la tabla `establishment_images`
--
ALTER TABLE `establishment_images`
  ADD PRIMARY KEY (`id_establishment_image`),
  ADD KEY `establishment_images_fk_establishment_foreign` (`fk_establishment`);

--
-- Indices de la tabla `establishment_schedules`
--
ALTER TABLE `establishment_schedules`
  ADD PRIMARY KEY (`id_establishment_schedule`),
  ADD KEY `establishment_schedules_fk_establishment_foreign` (`fk_establishment`);

--
-- Indices de la tabla `fairs`
--
ALTER TABLE `fairs`
  ADD PRIMARY KEY (`id_fair`),
  ADD KEY `fairs_fk_municipality_foreign` (`fk_municipality`);

--
-- Indices de la tabla `fair_images`
--
ALTER TABLE `fair_images`
  ADD PRIMARY KEY (`id_fair_image`),
  ADD KEY `fair_images_fk_fair_foreign` (`fk_fair`);

--
-- Indices de la tabla `history_regions`
--
ALTER TABLE `history_regions`
  ADD PRIMARY KEY (`id_history_region`),
  ADD KEY `history_regions_fk_region_foreign` (`fk_region`),
  ADD KEY `history_regions_fk_reporter_foreign` (`fk_reporter`);

--
-- Indices de la tabla `interest_sites`
--
ALTER TABLE `interest_sites`
  ADD PRIMARY KEY (`id_site`),
  ADD KEY `interest_sites_fk_category_foreign` (`fk_category`),
  ADD KEY `interest_sites_fk_municipality_foreign` (`fk_municipality`);

--
-- Indices de la tabla `interest_site_categories`
--
ALTER TABLE `interest_site_categories`
  ADD PRIMARY KEY (`id_category`);

--
-- Indices de la tabla `interest_site_images`
--
ALTER TABLE `interest_site_images`
  ADD PRIMARY KEY (`id_site_image`),
  ADD KEY `interest_site_images_fk_site_foreign` (`fk_site`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `municipalities`
--
ALTER TABLE `municipalities`
  ADD PRIMARY KEY (`id_municipality`),
  ADD KEY `municipalities_fk_department_foreign` (`fk_department`);

--
-- Indices de la tabla `municipality_images`
--
ALTER TABLE `municipality_images`
  ADD PRIMARY KEY (`id_municipality_image`),
  ADD KEY `municipality_images_fk_municipality_foreign` (`fk_municipality`);

--
-- Indices de la tabla `municipality_keydatas`
--
ALTER TABLE `municipality_keydatas`
  ADD PRIMARY KEY (`id_keydata`),
  ADD KEY `municipality_keydatas_fk_municipality_foreign` (`fk_municipality`);

--
-- Indices de la tabla `operators`
--
ALTER TABLE `operators`
  ADD PRIMARY KEY (`id_operator`),
  ADD UNIQUE KEY `operators_email_unique` (`email`);

--
-- Indices de la tabla `operator_password_resets`
--
ALTER TABLE `operator_password_resets`
  ADD KEY `operator_password_resets_email_index` (`email`),
  ADD KEY `operator_password_resets_token_index` (`token`);

--
-- Indices de la tabla `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id_page`);

--
-- Indices de la tabla `page_parts`
--
ALTER TABLE `page_parts`
  ADD PRIMARY KEY (`id_part`);

--
-- Indices de la tabla `page_part_pages`
--
ALTER TABLE `page_part_pages`
  ADD PRIMARY KEY (`id_part_page`),
  ADD KEY `page_part_pages_fk_page_foreign` (`fk_page`),
  ADD KEY `page_part_pages_fk_part_foreign` (`fk_part`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `patterns`
--
ALTER TABLE `patterns`
  ADD PRIMARY KEY (`id_pattern`),
  ADD KEY `patterns_fk_customer_foreign` (`fk_customer`);

--
-- Indices de la tabla `pattern_parts`
--
ALTER TABLE `pattern_parts`
  ADD PRIMARY KEY (`id_pattern_part`),
  ADD KEY `pattern_parts_fk_pattern_foreign` (`fk_pattern`),
  ADD KEY `pattern_parts_fk_pagepart_foreign` (`fk_pagepart`);

--
-- Indices de la tabla `plates`
--
ALTER TABLE `plates`
  ADD PRIMARY KEY (`id_plate`),
  ADD KEY `plates_fk_establishment_foreign` (`fk_establishment`);

--
-- Indices de la tabla `polls`
--
ALTER TABLE `polls`
  ADD PRIMARY KEY (`id_poll`);

--
-- Indices de la tabla `portable_documents`
--
ALTER TABLE `portable_documents`
  ADD PRIMARY KEY (`id_pdf`);

--
-- Indices de la tabla `real_route_points`
--
ALTER TABLE `real_route_points`
  ADD PRIMARY KEY (`id_point`),
  ADD KEY `real_route_points_fk_route_foreign` (`fk_route`);

--
-- Indices de la tabla `regions`
--
ALTER TABLE `regions`
  ADD PRIMARY KEY (`id_region`);

--
-- Indices de la tabla `region_images`
--
ALTER TABLE `region_images`
  ADD PRIMARY KEY (`id_region_image`),
  ADD KEY `region_images_fk_region_foreign` (`fk_region`);

--
-- Indices de la tabla `reporters`
--
ALTER TABLE `reporters`
  ADD PRIMARY KEY (`id_reporter`),
  ADD UNIQUE KEY `reporters_email_unique` (`email`);

--
-- Indices de la tabla `reporter_password_resets`
--
ALTER TABLE `reporter_password_resets`
  ADD KEY `reporter_password_resets_email_index` (`email`),
  ADD KEY `reporter_password_resets_token_index` (`token`);

--
-- Indices de la tabla `reservations`
--
ALTER TABLE `reservations`
  ADD PRIMARY KEY (`id_reservation`),
  ADD KEY `reservations_fk_service_foreign` (`fk_service`),
  ADD KEY `reservations_fk_user_foreign` (`fk_user`);

--
-- Indices de la tabla `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id_room`),
  ADD KEY `rooms_fk_establishment_foreign` (`fk_establishment`);

--
-- Indices de la tabla `routes`
--
ALTER TABLE `routes`
  ADD PRIMARY KEY (`id_route`);

--
-- Indices de la tabla `route_images`
--
ALTER TABLE `route_images`
  ADD PRIMARY KEY (`id_route_image`),
  ADD KEY `route_images_fk_route_foreign` (`fk_route`);

--
-- Indices de la tabla `route_points`
--
ALTER TABLE `route_points`
  ADD PRIMARY KEY (`id_point`),
  ADD KEY `route_points_fk_route_foreign` (`fk_route`);

--
-- Indices de la tabla `schedules`
--
ALTER TABLE `schedules`
  ADD PRIMARY KEY (`id_schedule`),
  ADD KEY `schedules_fk_service_foreign` (`fk_service`);

--
-- Indices de la tabla `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id_service`),
  ADD KEY `services_fk_service_category_foreign` (`fk_service_category`);

--
-- Indices de la tabla `service_categories`
--
ALTER TABLE `service_categories`
  ADD PRIMARY KEY (`id_category`);

--
-- Indices de la tabla `service_establishments`
--
ALTER TABLE `service_establishments`
  ADD PRIMARY KEY (`id_service_establishment`);

--
-- Indices de la tabla `service_establishment_pivots`
--
ALTER TABLE `service_establishment_pivots`
  ADD PRIMARY KEY (`id_service_establishment_pivot`),
  ADD KEY `service_establishment_pivots_fk_service_foreign` (`fk_service`),
  ADD KEY `service_establishment_pivots_fk_establishment_foreign` (`fk_establishment`);

--
-- Indices de la tabla `service_items`
--
ALTER TABLE `service_items`
  ADD PRIMARY KEY (`id_service_item`),
  ADD KEY `service_items_fk_service_foreign` (`fk_service`);

--
-- Indices de la tabla `service_operators`
--
ALTER TABLE `service_operators`
  ADD PRIMARY KEY (`id_service_operator`),
  ADD KEY `service_operators_fk_service_foreign` (`fk_service`),
  ADD KEY `service_operators_fk_operator_foreign` (`fk_operator`),
  ADD KEY `service_operators_fk_municipality_foreign` (`fk_municipality`);

--
-- Indices de la tabla `service_pictures`
--
ALTER TABLE `service_pictures`
  ADD PRIMARY KEY (`id_picture`),
  ADD KEY `service_pictures_fk_service_foreign` (`fk_service`);

--
-- Indices de la tabla `site_keydatas`
--
ALTER TABLE `site_keydatas`
  ADD PRIMARY KEY (`id_keydata`),
  ADD KEY `site_keydatas_fk_site_foreign` (`fk_site`);

--
-- Indices de la tabla `specials`
--
ALTER TABLE `specials`
  ADD PRIMARY KEY (`id_special`);

--
-- Indices de la tabla `special_detail_items`
--
ALTER TABLE `special_detail_items`
  ADD PRIMARY KEY (`id_detail_item`),
  ADD KEY `special_detail_items_fk_special_foreign` (`fk_special`);

--
-- Indices de la tabla `special_images`
--
ALTER TABLE `special_images`
  ADD PRIMARY KEY (`id_special_image`),
  ADD KEY `special_images_fk_special_foreign` (`fk_special`);

--
-- Indices de la tabla `special_items`
--
ALTER TABLE `special_items`
  ADD PRIMARY KEY (`id_item`),
  ADD KEY `special_items_fk_special_foreign` (`fk_special`);

--
-- Indices de la tabla `special_routes`
--
ALTER TABLE `special_routes`
  ADD PRIMARY KEY (`id_route`),
  ADD KEY `special_routes_fk_special_foreign` (`fk_special`);

--
-- Indices de la tabla `special_route_images`
--
ALTER TABLE `special_route_images`
  ADD PRIMARY KEY (`id_route_image`),
  ADD KEY `special_route_images_fk_route_foreign` (`fk_route`);

--
-- Indices de la tabla `tolls`
--
ALTER TABLE `tolls`
  ADD PRIMARY KEY (`id_toll`),
  ADD KEY `tolls_fk_site_foreign` (`fk_site`),
  ADD KEY `tolls_fk_concession_foreign` (`fk_concession`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indices de la tabla `user_password_resets`
--
ALTER TABLE `user_password_resets`
  ADD KEY `user_password_resets_email_index` (`email`),
  ADD KEY `user_password_resets_token_index` (`token`);

--
-- Indices de la tabla `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id_video`);

--
-- Indices de la tabla `wallpapers`
--
ALTER TABLE `wallpapers`
  ADD PRIMARY KEY (`id_wallpaper`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `activities`
--
ALTER TABLE `activities`
  MODIFY `id_activity` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `activity_pivots`
--
ALTER TABLE `activity_pivots`
  MODIFY `id_activity_pivot` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT de la tabla `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `circuits`
--
ALTER TABLE `circuits`
  MODIFY `id_circuit` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `circuit_detail_items`
--
ALTER TABLE `circuit_detail_items`
  MODIFY `id_detail_item` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `circuit_images`
--
ALTER TABLE `circuit_images`
  MODIFY `id_circuit_image` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `concessions`
--
ALTER TABLE `concessions`
  MODIFY `id_concession` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id_contact` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `customers`
--
ALTER TABLE `customers`
  MODIFY `id_customer` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `departments`
--
ALTER TABLE `departments`
  MODIFY `id_department` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `department_images`
--
ALTER TABLE `department_images`
  MODIFY `id_department_image` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `establishments`
--
ALTER TABLE `establishments`
  MODIFY `id_establishment` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `establishment_images`
--
ALTER TABLE `establishment_images`
  MODIFY `id_establishment_image` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `establishment_schedules`
--
ALTER TABLE `establishment_schedules`
  MODIFY `id_establishment_schedule` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `fairs`
--
ALTER TABLE `fairs`
  MODIFY `id_fair` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `fair_images`
--
ALTER TABLE `fair_images`
  MODIFY `id_fair_image` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `history_regions`
--
ALTER TABLE `history_regions`
  MODIFY `id_history_region` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `interest_sites`
--
ALTER TABLE `interest_sites`
  MODIFY `id_site` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `interest_site_categories`
--
ALTER TABLE `interest_site_categories`
  MODIFY `id_category` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `interest_site_images`
--
ALTER TABLE `interest_site_images`
  MODIFY `id_site_image` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=186;
--
-- AUTO_INCREMENT de la tabla `municipalities`
--
ALTER TABLE `municipalities`
  MODIFY `id_municipality` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `municipality_images`
--
ALTER TABLE `municipality_images`
  MODIFY `id_municipality_image` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `municipality_keydatas`
--
ALTER TABLE `municipality_keydatas`
  MODIFY `id_keydata` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT de la tabla `operators`
--
ALTER TABLE `operators`
  MODIFY `id_operator` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `pages`
--
ALTER TABLE `pages`
  MODIFY `id_page` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `page_parts`
--
ALTER TABLE `page_parts`
  MODIFY `id_part` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `page_part_pages`
--
ALTER TABLE `page_part_pages`
  MODIFY `id_part_page` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `patterns`
--
ALTER TABLE `patterns`
  MODIFY `id_pattern` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `pattern_parts`
--
ALTER TABLE `pattern_parts`
  MODIFY `id_pattern_part` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `plates`
--
ALTER TABLE `plates`
  MODIFY `id_plate` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `polls`
--
ALTER TABLE `polls`
  MODIFY `id_poll` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `portable_documents`
--
ALTER TABLE `portable_documents`
  MODIFY `id_pdf` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `real_route_points`
--
ALTER TABLE `real_route_points`
  MODIFY `id_point` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `regions`
--
ALTER TABLE `regions`
  MODIFY `id_region` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `region_images`
--
ALTER TABLE `region_images`
  MODIFY `id_region_image` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `reporters`
--
ALTER TABLE `reporters`
  MODIFY `id_reporter` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `reservations`
--
ALTER TABLE `reservations`
  MODIFY `id_reservation` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id_room` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT de la tabla `routes`
--
ALTER TABLE `routes`
  MODIFY `id_route` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `route_images`
--
ALTER TABLE `route_images`
  MODIFY `id_route_image` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `route_points`
--
ALTER TABLE `route_points`
  MODIFY `id_point` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `schedules`
--
ALTER TABLE `schedules`
  MODIFY `id_schedule` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `services`
--
ALTER TABLE `services`
  MODIFY `id_service` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `service_categories`
--
ALTER TABLE `service_categories`
  MODIFY `id_category` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `service_establishments`
--
ALTER TABLE `service_establishments`
  MODIFY `id_service_establishment` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT de la tabla `service_establishment_pivots`
--
ALTER TABLE `service_establishment_pivots`
  MODIFY `id_service_establishment_pivot` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT de la tabla `service_items`
--
ALTER TABLE `service_items`
  MODIFY `id_service_item` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `service_operators`
--
ALTER TABLE `service_operators`
  MODIFY `id_service_operator` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `service_pictures`
--
ALTER TABLE `service_pictures`
  MODIFY `id_picture` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `site_keydatas`
--
ALTER TABLE `site_keydatas`
  MODIFY `id_keydata` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `specials`
--
ALTER TABLE `specials`
  MODIFY `id_special` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `special_detail_items`
--
ALTER TABLE `special_detail_items`
  MODIFY `id_detail_item` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `special_images`
--
ALTER TABLE `special_images`
  MODIFY `id_special_image` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `special_items`
--
ALTER TABLE `special_items`
  MODIFY `id_item` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `special_routes`
--
ALTER TABLE `special_routes`
  MODIFY `id_route` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `special_route_images`
--
ALTER TABLE `special_route_images`
  MODIFY `id_route_image` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT de la tabla `tolls`
--
ALTER TABLE `tolls`
  MODIFY `id_toll` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `videos`
--
ALTER TABLE `videos`
  MODIFY `id_video` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `wallpapers`
--
ALTER TABLE `wallpapers`
  MODIFY `id_wallpaper` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `activity_pivots`
--
ALTER TABLE `activity_pivots`
  ADD CONSTRAINT `activity_pivots_fk_actity_foreign` FOREIGN KEY (`fk_actity`) REFERENCES `activities` (`id_activity`);

--
-- Filtros para la tabla `circuit_detail_items`
--
ALTER TABLE `circuit_detail_items`
  ADD CONSTRAINT `circuit_detail_items_fk_circuit_foreign` FOREIGN KEY (`fk_circuit`) REFERENCES `circuits` (`id_circuit`);

--
-- Filtros para la tabla `circuit_images`
--
ALTER TABLE `circuit_images`
  ADD CONSTRAINT `circuit_images_fk_circuit_foreign` FOREIGN KEY (`fk_circuit`) REFERENCES `circuits` (`id_circuit`);

--
-- Filtros para la tabla `departments`
--
ALTER TABLE `departments`
  ADD CONSTRAINT `departments_fk_region_foreign` FOREIGN KEY (`fk_region`) REFERENCES `regions` (`id_region`);

--
-- Filtros para la tabla `department_images`
--
ALTER TABLE `department_images`
  ADD CONSTRAINT `department_images_fk_department_foreign` FOREIGN KEY (`fk_department`) REFERENCES `departments` (`id_department`);

--
-- Filtros para la tabla `establishments`
--
ALTER TABLE `establishments`
  ADD CONSTRAINT `establishments_fk_municipality_foreign` FOREIGN KEY (`fk_municipality`) REFERENCES `municipalities` (`id_municipality`);

--
-- Filtros para la tabla `establishment_images`
--
ALTER TABLE `establishment_images`
  ADD CONSTRAINT `establishment_images_fk_establishment_foreign` FOREIGN KEY (`fk_establishment`) REFERENCES `establishments` (`id_establishment`);

--
-- Filtros para la tabla `establishment_schedules`
--
ALTER TABLE `establishment_schedules`
  ADD CONSTRAINT `establishment_schedules_fk_establishment_foreign` FOREIGN KEY (`fk_establishment`) REFERENCES `establishments` (`id_establishment`);

--
-- Filtros para la tabla `fairs`
--
ALTER TABLE `fairs`
  ADD CONSTRAINT `fairs_fk_municipality_foreign` FOREIGN KEY (`fk_municipality`) REFERENCES `municipalities` (`id_municipality`);

--
-- Filtros para la tabla `fair_images`
--
ALTER TABLE `fair_images`
  ADD CONSTRAINT `fair_images_fk_fair_foreign` FOREIGN KEY (`fk_fair`) REFERENCES `fairs` (`id_fair`);

--
-- Filtros para la tabla `history_regions`
--
ALTER TABLE `history_regions`
  ADD CONSTRAINT `history_regions_fk_region_foreign` FOREIGN KEY (`fk_region`) REFERENCES `regions` (`id_region`),
  ADD CONSTRAINT `history_regions_fk_reporter_foreign` FOREIGN KEY (`fk_reporter`) REFERENCES `reporters` (`id_reporter`);

--
-- Filtros para la tabla `interest_sites`
--
ALTER TABLE `interest_sites`
  ADD CONSTRAINT `interest_sites_fk_category_foreign` FOREIGN KEY (`fk_category`) REFERENCES `interest_site_categories` (`id_category`),
  ADD CONSTRAINT `interest_sites_fk_municipality_foreign` FOREIGN KEY (`fk_municipality`) REFERENCES `municipalities` (`id_municipality`);

--
-- Filtros para la tabla `interest_site_images`
--
ALTER TABLE `interest_site_images`
  ADD CONSTRAINT `interest_site_images_fk_site_foreign` FOREIGN KEY (`fk_site`) REFERENCES `interest_sites` (`id_site`);

--
-- Filtros para la tabla `municipalities`
--
ALTER TABLE `municipalities`
  ADD CONSTRAINT `municipalities_fk_department_foreign` FOREIGN KEY (`fk_department`) REFERENCES `departments` (`id_department`);

--
-- Filtros para la tabla `municipality_images`
--
ALTER TABLE `municipality_images`
  ADD CONSTRAINT `municipality_images_fk_municipality_foreign` FOREIGN KEY (`fk_municipality`) REFERENCES `municipalities` (`id_municipality`);

--
-- Filtros para la tabla `municipality_keydatas`
--
ALTER TABLE `municipality_keydatas`
  ADD CONSTRAINT `municipality_keydatas_fk_municipality_foreign` FOREIGN KEY (`fk_municipality`) REFERENCES `municipalities` (`id_municipality`);

--
-- Filtros para la tabla `page_part_pages`
--
ALTER TABLE `page_part_pages`
  ADD CONSTRAINT `page_part_pages_fk_page_foreign` FOREIGN KEY (`fk_page`) REFERENCES `pages` (`id_page`),
  ADD CONSTRAINT `page_part_pages_fk_part_foreign` FOREIGN KEY (`fk_part`) REFERENCES `page_parts` (`id_part`);

--
-- Filtros para la tabla `patterns`
--
ALTER TABLE `patterns`
  ADD CONSTRAINT `patterns_fk_customer_foreign` FOREIGN KEY (`fk_customer`) REFERENCES `customers` (`id_customer`);

--
-- Filtros para la tabla `pattern_parts`
--
ALTER TABLE `pattern_parts`
  ADD CONSTRAINT `pattern_parts_fk_pagepart_foreign` FOREIGN KEY (`fk_pagepart`) REFERENCES `page_part_pages` (`id_part_page`),
  ADD CONSTRAINT `pattern_parts_fk_pattern_foreign` FOREIGN KEY (`fk_pattern`) REFERENCES `patterns` (`id_pattern`);

--
-- Filtros para la tabla `plates`
--
ALTER TABLE `plates`
  ADD CONSTRAINT `plates_fk_establishment_foreign` FOREIGN KEY (`fk_establishment`) REFERENCES `establishments` (`id_establishment`);

--
-- Filtros para la tabla `real_route_points`
--
ALTER TABLE `real_route_points`
  ADD CONSTRAINT `real_route_points_fk_route_foreign` FOREIGN KEY (`fk_route`) REFERENCES `routes` (`id_route`);

--
-- Filtros para la tabla `region_images`
--
ALTER TABLE `region_images`
  ADD CONSTRAINT `region_images_fk_region_foreign` FOREIGN KEY (`fk_region`) REFERENCES `regions` (`id_region`);

--
-- Filtros para la tabla `reservations`
--
ALTER TABLE `reservations`
  ADD CONSTRAINT `reservations_fk_service_foreign` FOREIGN KEY (`fk_service`) REFERENCES `service_operators` (`id_service_operator`),
  ADD CONSTRAINT `reservations_fk_user_foreign` FOREIGN KEY (`fk_user`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `rooms`
--
ALTER TABLE `rooms`
  ADD CONSTRAINT `rooms_fk_establishment_foreign` FOREIGN KEY (`fk_establishment`) REFERENCES `establishments` (`id_establishment`);

--
-- Filtros para la tabla `route_images`
--
ALTER TABLE `route_images`
  ADD CONSTRAINT `route_images_fk_route_foreign` FOREIGN KEY (`fk_route`) REFERENCES `routes` (`id_route`);

--
-- Filtros para la tabla `route_points`
--
ALTER TABLE `route_points`
  ADD CONSTRAINT `route_points_fk_route_foreign` FOREIGN KEY (`fk_route`) REFERENCES `special_routes` (`id_route`);

--
-- Filtros para la tabla `schedules`
--
ALTER TABLE `schedules`
  ADD CONSTRAINT `schedules_fk_service_foreign` FOREIGN KEY (`fk_service`) REFERENCES `service_operators` (`id_service_operator`);

--
-- Filtros para la tabla `services`
--
ALTER TABLE `services`
  ADD CONSTRAINT `services_fk_service_category_foreign` FOREIGN KEY (`fk_service_category`) REFERENCES `service_categories` (`id_category`);

--
-- Filtros para la tabla `service_establishment_pivots`
--
ALTER TABLE `service_establishment_pivots`
  ADD CONSTRAINT `service_establishment_pivots_fk_establishment_foreign` FOREIGN KEY (`fk_establishment`) REFERENCES `establishments` (`id_establishment`),
  ADD CONSTRAINT `service_establishment_pivots_fk_service_foreign` FOREIGN KEY (`fk_service`) REFERENCES `service_establishments` (`id_service_establishment`);

--
-- Filtros para la tabla `service_items`
--
ALTER TABLE `service_items`
  ADD CONSTRAINT `service_items_fk_service_foreign` FOREIGN KEY (`fk_service`) REFERENCES `service_operators` (`id_service_operator`);

--
-- Filtros para la tabla `service_operators`
--
ALTER TABLE `service_operators`
  ADD CONSTRAINT `service_operators_fk_municipality_foreign` FOREIGN KEY (`fk_municipality`) REFERENCES `municipalities` (`id_municipality`),
  ADD CONSTRAINT `service_operators_fk_operator_foreign` FOREIGN KEY (`fk_operator`) REFERENCES `operators` (`id_operator`),
  ADD CONSTRAINT `service_operators_fk_service_foreign` FOREIGN KEY (`fk_service`) REFERENCES `services` (`id_service`);

--
-- Filtros para la tabla `service_pictures`
--
ALTER TABLE `service_pictures`
  ADD CONSTRAINT `service_pictures_fk_service_foreign` FOREIGN KEY (`fk_service`) REFERENCES `service_operators` (`id_service_operator`);

--
-- Filtros para la tabla `site_keydatas`
--
ALTER TABLE `site_keydatas`
  ADD CONSTRAINT `site_keydatas_fk_site_foreign` FOREIGN KEY (`fk_site`) REFERENCES `interest_sites` (`id_site`);

--
-- Filtros para la tabla `special_detail_items`
--
ALTER TABLE `special_detail_items`
  ADD CONSTRAINT `special_detail_items_fk_special_foreign` FOREIGN KEY (`fk_special`) REFERENCES `specials` (`id_special`);

--
-- Filtros para la tabla `special_images`
--
ALTER TABLE `special_images`
  ADD CONSTRAINT `special_images_fk_special_foreign` FOREIGN KEY (`fk_special`) REFERENCES `specials` (`id_special`);

--
-- Filtros para la tabla `special_items`
--
ALTER TABLE `special_items`
  ADD CONSTRAINT `special_items_fk_special_foreign` FOREIGN KEY (`fk_special`) REFERENCES `specials` (`id_special`);

--
-- Filtros para la tabla `special_routes`
--
ALTER TABLE `special_routes`
  ADD CONSTRAINT `special_routes_fk_special_foreign` FOREIGN KEY (`fk_special`) REFERENCES `specials` (`id_special`);

--
-- Filtros para la tabla `special_route_images`
--
ALTER TABLE `special_route_images`
  ADD CONSTRAINT `special_route_images_fk_route_foreign` FOREIGN KEY (`fk_route`) REFERENCES `special_routes` (`id_route`);

--
-- Filtros para la tabla `tolls`
--
ALTER TABLE `tolls`
  ADD CONSTRAINT `tolls_fk_concession_foreign` FOREIGN KEY (`fk_concession`) REFERENCES `concessions` (`id_concession`),
  ADD CONSTRAINT `tolls_fk_site_foreign` FOREIGN KEY (`fk_site`) REFERENCES `interest_sites` (`id_site`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
