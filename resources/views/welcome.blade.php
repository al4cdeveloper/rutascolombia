@extends('layouts.frontheader')
    @section('title','Inicio')

    @section('additionalStyle')
        <link rel="stylesheet" href="{{asset('front/css/fontello/css/all-fontello.min.css')}}">
        <style type="text/css">
            @import url(http://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700);

            body
            {
                overflow-x: hidden!important;
            }
        </style>

    
        <link rel="stylesheet" href="{{asset('front/layerslider/css/layerslider.css')}}">
        <link rel="stylesheet" href="{{asset('front/css/personalstyle.css')}}">

        <link rel="stylesheet" href="https://cdn.pannellum.org/2.4/pannellum.css"/>
        <script type="text/javascript" src="https://cdn.pannellum.org/2.4/pannellum.js"></script>
         <!-- REVOLUTION SLIDER CSS -->
        <link rel="stylesheet" type="text/css" href="{{asset('front/rev-slider-files/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('front/rev-slider-files/fonts/font-awesome/css/font-awesome.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('front/rev-slider-files/css/settings.css')}}">
    
        <link rel="stylesheet" type="text/css" href="{{asset('plugins/slick/slick.css')}}"/>
        <link rel="stylesheet" type="text/css" href="{{asset('plugins/slick/slick-theme.css')}}"/>
        <link rel="stylesheet" href="{{asset('front/css/cardRegions.css')}}">
    @endsection


    @section('main')
        <div class="app">
          <router-view></router-view>
        </div>
    @endsection

@section('additionalScript')

    <!-- Specific scripts -->
    <script src="{{asset('front/layerslider/js/greensock.js')}}"></script>
    <script src="{{asset('front/layerslider/js/layerslider.transitions.js')}}"></script>
    <script src="{{asset('front/layerslider/js/layerslider.kreaturamedia.jquery.js')}}"></script>
    <link href="{{asset('front/css/slider-pro.css')}}" rel="stylesheet">
    
    <script src="{{asset('front/js/jquery.sliderPro.min.js')}}"></script>
    
    <script type="text/javascript" src="{{asset('front/rev-slider-files/js/jquery.themepunch.tools.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('front/rev-slider-files/js/jquery.themepunch.revolution.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('front/rev-slider-files/js/extensions/revolution.extension.actions.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('front/rev-slider-files/js/extensions/revolution.extension.carousel.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('front/rev-slider-files/js/extensions/revolution.extension.kenburn.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('front/rev-slider-files/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('front/rev-slider-files/js/extensions/revolution.extension.migration.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('front/rev-slider-files/js/extensions/revolution.extension.navigation.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('front/rev-slider-files/js/extensions/revolution.extension.parallax.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('front/rev-slider-files/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('front/rev-slider-files/js/extensions/revolution.extension.video.min.js')}}"></script>

     <script src="{{asset('front/js/snap.svg-min.js')}}" type="text/javascript"></script>
     <script type="text/javascript" src="{{asset('plugins/slick/slick.min.js')}}"></script>
    <script src="{{asset('front/js/jquery.browser.min.js')}}" type="text/javascript"></script>

     <script src="{{asset('front/js/colombia.js')}}" type="text/javascript"></script>
    <!-- Map -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBXtArR0GzW4Y_EezVpjGdvjrlwsQBcayw" ></script>
    <script type="text/javascript" src="{{asset('front/js/infobox.js')}}"></script>
     <!--<script type="text/javascript" src="{{asset('front/js/map_home.js')}}"></script>-->

     <script>
        var mapObject;
        function setpoints(object,typeIcon,zoom)
        {
            var
            markers = [];

            var funciona;
            var mapOptions = {
                zoom: zoom,
                center: new google.maps.LatLng(48.865633, 2.321236),
                mapTypeId: google.maps.MapTypeId.ROADMAP,

                mapTypeControl: false,
                mapTypeControlOptions: {
                    style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
                    position: google.maps.ControlPosition.LEFT_CENTER
                },
                panControl: false,
                panControlOptions: {
                    position: google.maps.ControlPosition.TOP_RIGHT
                },
                zoomControl: true,
                zoomControlOptions: {
                    style: google.maps.ZoomControlStyle.LARGE,
                    position: google.maps.ControlPosition.TOP_LEFT
                },
                scrollwheel: false,
                scaleControl: false,
                scaleControlOptions: {
                    position: google.maps.ControlPosition.TOP_LEFT
                },
                streetViewControl: true,
                streetViewControlOptions: {
                    position: google.maps.ControlPosition.LEFT_TOP
                },
                styles: [
                             {
                    "featureType": "landscape",
                    "stylers": [
                        {
                            "hue": "#FFBB00"
                        },
                        {
                            "saturation": 43.400000000000006
                        },
                        {
                            "lightness": 37.599999999999994
                        },
                        {
                            "gamma": 1
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "stylers": [
                        {
                            "hue": "#FFC200"
                        },
                        {
                            "saturation": -61.8
                        },
                        {
                            "lightness": 45.599999999999994
                        },
                        {
                            "gamma": 1
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "stylers": [
                        {
                            "hue": "#FF0300"
                        },
                        {
                            "saturation": -100
                        },
                        {
                            "lightness": 51.19999999999999
                        },
                        {
                            "gamma": 1
                        }
                    ]
                },
                {
                    "featureType": "road.local",
                    "stylers": [
                        {
                            "hue": "#FF0300"
                        },
                        {
                            "saturation": -100
                        },
                        {
                            "lightness": 52
                        },
                        {
                            "gamma": 1
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "stylers": [
                        {
                            "hue": "#0078FF"
                        },
                        {
                            "saturation": -13.200000000000003
                        },
                        {
                            "lightness": 2.4000000000000057
                        },
                        {
                            "gamma": 1
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "stylers": [
                        {
                            "hue": "#00FF6A"
                        },
                        {
                            "saturation": -1.0989010989011234
                        },
                        {
                            "lightness": 11.200000000000017
                        },
                        {
                            "gamma": 1
                        }
                    ]
                }
                ]
            };
            var marker;
            mapObject = new google.maps.Map(document.getElementById('map'), mapOptions);
            object.forEach(function (item) {
                if(typeIcon != 'individual')
                {
                    if(typeIcon == 'municipality' || typeIcon == 'hotel' || typeIcon=='restaurant'|| typeIcon =='site')
                    {
                         marker = new google.maps.Marker({
                                        position: new google.maps.LatLng(item.latitude, item.longitude),
                                        map: mapObject,
                                        icon: '/front/img/pins/'+typeIcon+'.png',
                                    });
                    }
                    else
                    {
                        marker = new google.maps.Marker({
                                        position: new google.maps.LatLng(item.latitude, item.longitude),
                                        map: mapObject,
                                        icon: '/front/img/pins/default.png',
                                    });
                    }
                }
                else
                {
                    marker = new google.maps.Marker({
                                        position: new google.maps.LatLng(item.latitude, item.longitude),
                                        map: mapObject,
                                        icon: '/front/img/pins/'+item.type_relation+'.png',
                                    });

                }
                
               
                mapObject.setCenter(new google.maps.LatLng(item.latitude, item.longitude));
                google.maps.event.addListener(marker, 'click', (function () {
                  closeInfoBox();
                if(typeIcon != 'individual')
                {
                    if(typeIcon == 'municipality')
                    {
                        getInfoBox(item).open(mapObject, this);
                    }
                    else if(typeIcon == 'site')
                    {
                        getInfoBoxSite(item).open(mapObject, this);
                    }
                    else if(typeIcon == 'hotel' || typeIcon=='restaurant')
                    {
                        getInfoBoxEstablishment(item).open(mapObject, this);
                    }
                    else
                    {
                        getInfoBoxService(item).open(mapObject, this);
                    }
                }
                else
                {
                    getInfoBoxCustom(item).open(mapObject, this);
                }


                 }));
            })

        }


    function calculateAndDisplayRoute(initial,end) 
    {
        var directionsService = new google.maps.DirectionsService;
        var directionsDisplay = new google.maps.DirectionsRenderer({
          map: mapObject,
          draggable: true,
          panel: document.getElementById('right-panel')
        });

         directionsDisplay.addListener('directions_changed', function() {
          computeTotalDistance(directionsDisplay.getDirections());
        });

        /*
    Código funcional de solo ubicación
    Documentación; https://developers.google.com/maps/documentation/javascript/examples/directions-draggable

    function calculateAndDisplayRoute(initial,end) 
      {
        var directionsDisplay = new google.maps.DirectionsRenderer;
        var directionsService = new google.maps.DirectionsService;
console.log(directionsDisplay);
        directionsDisplay.setMap(mapObject);
        var selectedMode = document.getElementById('mode').value;
        console.log(initial.latitude)
        console.log(end.latitude)
        console.log(initial.longitude)
        console.log(end.longitude)
        directionsService.route({
            origin: new google.maps.LatLng(initial.latitude, initial.longitude),
            destination: new google.maps.LatLng(end.latitude, end.longitude),  // Ocean Beach.
          // Note that Javascript allows us to access the constant
          // using square brackets and a string value as its
          // "property."
          travelMode: google.maps.TravelMode[selectedMode]
        }, function(response, status) {
          if (status == 'OK') {
            directionsDisplay.setDirections(response);
          } else {
            window.alert('Directions request failed due to ' + status);
          }
        });
      }


        */

        directionsService.route({
          origin: initial.name,
          destination: end.name,
          travelMode: 'DRIVING',
          avoidTolls: true
        }, function(response, status) {
          if (status === 'OK') {
            directionsDisplay.setDirections(response);
          } else {
            alert('Could not display directions due to: ' + status);
          }
        });

    }


      function computeTotalDistance(result) {
        var total = 0;
        var myroute = result.routes[0];
        for (var i = 0; i < myroute.legs.length; i++) {
          total += myroute.legs[i].distance.value;
        }
        total = total / 1000;
        document.getElementById('total').innerHTML = total + ' km';
      }


   




        function closeInfoBox() {
            $('div.infoBox').remove();
        };

        function getInfoBox(item) {
            return new InfoBox({
                content:
                '<div class="marker_info" id="marker_info">' +
                '<img src="/' + item.link_image + '" alt="Image" width="280" height="auto"/>' +
                '<h3>'+ item.municipality_name +'</h3>' +
                '<span>'+ item.description.substr(0,110) +'</span>' +
                '<div class="marker_tools">' +
                '<form action="http://maps.google.com/maps" method="get" target="_blank" style="display:inline-block""><input name="saddr" value="'+ '" type="hidden"><input type="hidden" name="daddr" value="'+ item.latitude +',' +item.longitude +'"><button type="submit" value="Get directions" class="btn_infobox_get_directions">Como llegar</button></form>' +
                    //'<a href="tel://'+  +'" class="btn_infobox_phone">'+ item.phone +'</a>' +
                    '</div>' +
                    //'<a href="'+ item.url_point + '" class="btn_infobox">Details</a>' +
                '</div>',
                disableAutoPan: false,
                maxWidth: 0,
                pixelOffset: new google.maps.Size(10, 125),
                closeBoxMargin: '5px -20px 2px 2px',
                closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif",
                isHidden: false,
                alignBottom: true,
                pane: 'floatPane',
                enableEventPropagation: true
            });
        };

        function getInfoBoxSite(item) {
            return new InfoBox({
                content:
                '<div class="marker_info" id="marker_info">' +
                '<img src="/' + item.link_image + '" alt="Image" width="280" height="auto"/>' +
                '<h3>'+ item.site_name +'</h3>' +
                '<span>'+ item.description.substr(0,110) +'</span>' +
                '<div class="marker_tools">' +
                '<form action="http://maps.google.com/maps" method="get" target="_blank" style="display:inline-block""><input name="saddr" value="'+ '" type="hidden"><input type="hidden" name="daddr" value="'+ item.latitude +',' +item.longitude +'"><button type="submit" value="Get directions" class="btn_infobox_get_directions">Como llegar</button></form>' +
                    //'<a href="tel://'+  +'" class="btn_infobox_phone">'+ item.phone +'</a>' +
                    '</div>' +
                    //'<a href="'+ item.url_point + '" class="btn_infobox">Details</a>' +
                '</div>',
                disableAutoPan: false,
                maxWidth: 0,
                pixelOffset: new google.maps.Size(10, 125),
                closeBoxMargin: '5px -20px 2px 2px',
                closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif",
                isHidden: false,
                alignBottom: true,
                pane: 'floatPane',
                enableEventPropagation: true
            });
        };

        function getInfoBoxService(item) {
            return new InfoBox({
                content:
                '<div class="marker_info" id="marker_info">' +
                '<img src="/' + item.image_link + '" alt="Image" width="280" height="auto"/>' +
                '<h3>'+ item.service_name +'</h3>' +
                '<span>'+ item.description.substr(0,110) +'</span>' +
                '<div class="marker_tools">' +
                '<form action="http://maps.google.com/maps" method="get" target="_blank" style="display:inline-block""><input name="saddr" value="'+ '" type="hidden"><input type="hidden" name="daddr" value="'+ item.latitude +',' +item.longitude +'"><button type="submit" value="Get directions" class="btn_infobox_get_directions">Como llegar</button></form>' +
                    //'<a href="tel://'+  +'" class="btn_infobox_phone">'+ item.phone +'</a>' +
                    '</div>' +
                    //'<a href="'+ item.url_point + '" class="btn_infobox">Details</a>' +
                '</div>',
                disableAutoPan: false,
                maxWidth: 0,
                pixelOffset: new google.maps.Size(10, 125),
                closeBoxMargin: '5px -20px 2px 2px',
                closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif",
                isHidden: false,
                alignBottom: true,
                pane: 'floatPane',
                enableEventPropagation: true
            });
        };

        function getInfoBoxEstablishment(item) {
            return new InfoBox({
                content:
                '<div class="marker_info" id="marker_info">' +
                '<img src="/' + item.link_image + '" alt="Image" width="280" height="auto"/>' +
                '<h3>'+ item.establishment_name +'</h3>' +
                '<span>'+ item.description.substr(0,110) +'</span>' +
                '<div class="marker_tools">' +
                '<form action="http://maps.google.com/maps" method="get" target="_blank" style="display:inline-block""><input name="saddr" value="'+ '" type="hidden"><input type="hidden" name="daddr" value="'+ item.latitude +',' +item.longitude +'"><button type="submit" value="Get directions" class="btn_infobox_get_directions">Como llegar</button></form>' +
                    //'<a href="tel://'+  +'" class="btn_infobox_phone">'+ item.phone +'</a>' +
                    '</div>' +
                    //'<a href="'+ item.url_point + '" class="btn_infobox">Details</a>' +
                '</div>',
                disableAutoPan: false,
                maxWidth: 0,
                pixelOffset: new google.maps.Size(10, 125),
                closeBoxMargin: '5px -20px 2px 2px',
                closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif",
                isHidden: false,
                alignBottom: true,
                pane: 'floatPane',
                enableEventPropagation: true
            });
        };

        function getInfoBoxCustom(item) {
            return new InfoBox({
                content:
                '<div class="marker_info" id="marker_info">' +
                '<img src="/' + item.image + '" alt="Image" width="280" height="auto"/>' +
                '<h3>'+ item.name +'</h3>' +
                '<span>'+ item.description.substr(0,110) +'</span>' +
                '<div class="marker_tools">' +
                '<form action="http://maps.google.com/maps" method="get" target="_blank" style="display:inline-block""><input name="saddr" value="'+ '" type="hidden"><input type="hidden" name="daddr" value="'+ item.latitude +',' +item.longitude +'"><button type="submit" value="Get directions" class="btn_infobox_get_directions">Como llegar</button></form>' +
                    //'<a href="tel://'+  +'" class="btn_infobox_phone">'+ item.phone +'</a>' +
                    '</div>' +
                    //'<a href="'+ item.url_point + '" class="btn_infobox">Details</a>' +
                '</div>',
                disableAutoPan: false,
                maxWidth: 0,
                pixelOffset: new google.maps.Size(10, 125),
                closeBoxMargin: '5px -20px 2px 2px',
                closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif",
                isHidden: false,
                alignBottom: true,
                pane: 'floatPane',
                enableEventPropagation: true
            });
        };


        function create360Image(holi)
        {
            pannellum.viewer('panorama', {
                "type": "equirectangular",
                "panorama": holi.img_360,
                "pitch": 2.3,
                "yaw": -135.4,
                "hfov": 120,
                "autoRotate": -2,
            });
        }
     </script>
@endsection
    