<!DOCTYPE html>

<html lang="es">

<head>
    <!-- Basic Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- Most Important Meta -->
    <meta name="robots" content="index, follow">
    <meta name="description" content="RutasColombia es una plataforma digital que conecta a los viajeros que buscan realizar actividades de aventura en Colombia">
    <meta name="keywords" content="turismo, paseo, pasear, actividades de aventura, turismo de naturaleza, deportes de alto riesgo, deportes extremos, colombia, escapadas, fin de semana, lugares para pasear, RutasColombia, tu paseo">
    <meta name="author" content="RutasColombia.Com">
    <meta name="creator" content="Euclides Rafael Alsina">
    <meta name="creator" content="Sergio Hernández Goyeneche">
    <title>@yield('title') | Guía de Rutas por Colombia</title> 

     
    
    <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="{{asset('icon/favicon.png')}}">
        <link rel="apple-touch-icon" href="{{asset('icon/icon57.png')}}" sizes="57x57">
        <link rel="apple-touch-icno" href="{{asset('icon/icon72.png')}}" sizes="72x72">
        <link rel="apple-touch-icon" href="{{asset('icon/icon76.png')}}" sizes="76x76">
        <link rel="apple-touch-icon" href="{{asset('icon/icon114.png')}}" sizes="114x114">
        <link rel="apple-touch-icon" href="{{asset('icon/icon120.png')}}" sizes="120x120">
        <link rel="apple-touch-icon" href="{{asset('icon/icon144.png')}}" sizes="144x144">
        <link rel="apple-touch-icon" href="{{asset('icon/icon152.png')}}" sizes="152x152">
        <link rel="apple-touch-icon" href="{{asset('icon/icon180.png')}}" sizes="180x180">
    <!-- END Icons -->
    
    <!-- Google web fonts -->
    <link href="https://fonts.googleapis.com/css?family=Gochi+Hand|Lato:300,400|Montserrat:400,400i,700,700i" rel="stylesheet">

    <!-- BASE CSS -->
    <link href="{{asset('front/css/base.css')}}" rel="stylesheet">

    <!-- REVOLUTION SLIDER CSS -->
    <link rel="stylesheet" type="text/css" href="{{asset('front/rev-slider-files/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('front/rev-slider-files/fonts/font-awesome/css/font-awesome.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('front/rev-slider-files/css/settings.css')}}">

    <!-- REVOLUTION LAYERS STYLES -->
    <style>
        .tp-caption.News-Title,
        .News-Title {
            color: rgba(255, 255, 255, 1.00);
            font-size: 70px;
            line-height: 60px;
            font-weight: 700;
            font-style: normal;
            text-decoration: none;
            background-color: transparent;
            border-color: transparent;
            border-style: none;
            border-width: 0px;
            border-radius: 0 0 0 0px
        }

        .tp-caption.News-Subtitle,
        .News-Subtitle {
            color: rgba(255, 255, 255, 1.00);
            font-size: 15px;
            line-height: 24px;
            font-weight: 300;
            font-style: normal;
            font-family: Roboto Slab;
            text-decoration: none;
            background-color: rgba(255, 255, 255, 0);
            border-color: transparent;
            border-style: none;
            border-width: 0px;
            border-radius: 0 0 0 0px
        }

        .tp-caption.News-Subtitle:hover,
        .News-Subtitle:hover {
            color: rgba(255, 255, 255, 0.65);
            text-decoration: none;
            background-color: rgba(255, 255, 255, 0);
            border-color: transparent;
            border-style: solid;
            border-width: 0px;
            border-radius: 0 0 0px 0
        }
    </style>

    <style type="text/css">
        @import url(http://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700);
    </style>

    <style type="text/css">
        .hermes.tp-bullets {}

        .hermes .tp-bullet {
            overflow: hidden;
            border-radius: 50%;
            width: 16px;
            height: 16px;
            background-color: rgba(0, 0, 0, 0);
            box-shadow: inset 0 0 0 2px rgb(255, 255, 255);
            -webkit-transition: background 0.3s ease;
            transition: background 0.3s ease;
            position: absolute
        }

        .hermes .tp-bullet:hover {
            background-color: rgba(0, 0, 0, 0.21)
        }

        .hermes .tp-bullet:after {
            content: ' ';
            position: absolute;
            bottom: 0;
            height: 0;
            left: 0;
            width: 100%;
            background-color: rgb(255, 255, 255);
            box-shadow: 0 0 1px rgb(255, 255, 255);
            -webkit-transition: height 0.3s ease;
            transition: height 0.3s ease
        }

        .hermes .tp-bullet.selected:after {
            height: 100%
        }
    </style>
    @yield('additionalStyle')
    <!-- TWITTER OG -->
    <meta name="twitter:creator" content="@" />
    <meta name="twitter:site" content="@" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:title" content="rutascolombia.com" />
    <meta name="twitter:description" content="RutasColombia es una plataforma digital que conecta a los operadores turísticos con viajeros que buscan realizar actividades de aventura en Colombia" />
    <meta name="twitter:image" content="http://www.rutascolombia.com/img/brand/logo.png" />

    <!-- FACEBOOK OG -->
    <meta property="fb:app_id" content="" /> 
    <meta property="og:type"   content="article" /> 
    <meta property="og:url"    content="http://www.rutascolombia.com/" /> 
    <meta property="og:title"  content="rutascolombia.com" /> 
    <meta property="og:image"  content="http://www.rutascolombia.com/img/brand/logo.png" />
    <meta property="og:description" content="RutasColombia es una plataforma digital que conecta a los operadores turísticos con viajeros que buscan realizar actividades de aventura en Colombia" />
    <meta name="author" content="rutascolombia.com">

    <!-- Marcado JSON-LD generado por el Asistente para el marcado de datos estructurados de Google. -->
    <script type="application/ld+json">
        {
          "@context" : "http://schema.org",
          "@type" : "LocalBusiness",
          "name" : "rutascolombia.com",
          "image" : "img/brand/logo.png",
          "telephone" : "+57 (1) 3121540",
          "email" : "info@rutascolombia.com",
          "address" : {
            "@type" : "PostalAddress",
            "streetAddress" : "Cra. 9 # 72-81 Oficina 504",
            "addressLocality" : "Bogotá D.C.",
            "addressCountry" : "Colombia"
          },
          "openingHoursSpecification" : {
            "@type" : "OpeningHoursSpecification",
            "dayOfWeek" : {
              "@type" : "DayOfWeek",
              "name" : "8:30 a.m. - 5:30 p.m."
            }
          },
          "url" : "http://www.rutascolombia.com/",
          "review" : {
            "@type" : "Review",
            "author" : {
              "@type" : "Person",
              "name" : "rutascolombia.com"
            },
            "reviewBody" : "RutasColombia es una plataforma digital que conecta a los operadores turísticos con viajeros que buscan realizar actividades de aventura en Colombia"
          }
        }
    </script>
</head>
<body >

	<div id="preloader">
        <div class="sk-spinner sk-spinner-wave" id="spinner">
            <div class="sk-rect1"></div>
            <div class="sk-rect2"></div>
            <div class="sk-rect3"></div>
            <div class="sk-rect4"></div>
            <div class="sk-rect5"></div>
        </div>
    </div>
    <!-- End Preload -->

    <div class="layer"></div>
    <!-- Mobile menu overlay mask -->

    <!-- Header================================================== -->

    <div class="app">   
        <Navbar></Navbar>
    </div><!-- End Header -->

    @yield('additionalPreMain')

    @yield('main')

    @include('layouts.footer')


    <!-- Common scripts -->
    <script src="{{asset('front/js/jquery-2.2.4.min.js')}}"></script>
    <script src="{{asset('front/js/common_scripts_min.js')}}"></script>
    <script src="{{asset('front/js/functions.js')}}"></script>
    <script src="{{asset('/js/vue-app.js')}}"></script>
    
    @yield('additionalScript')
    


</body>

</html>