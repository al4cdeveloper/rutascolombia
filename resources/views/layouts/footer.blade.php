    <!-- Footer -->
    <footer>

        
    	<div class="container">
    			<div class="row">
    				<div class="col-md-3 col-sm-6">
    					<h3>¿Necesitas ayuda?</h3>
    					<a href="tel://+573165287931" id="phone">+57 316 528 7931</a>
    					<a href="mailto:info@rutascolombia.com" id="email_footer">info@rutascolombia.com</a>
    				</div>
    				<div class="col-md-2 col-sm-6">
    					<h3>Nosotros</h3>
    					<ul>
    						<li><a href="#">Qué es Tu Paseo</a>
    						</li>
    						<li><a href="#">Blog</a>
    						</li>
    						<li><a href="#">Contacto</a>
    						</li>
    						
    					</ul>
    				</div>
    				<div class="col-md-3 col-sm-6">
    					<h3>Más información</h3>
    					<ul>
    						<li><a href="pstregister.html">¿Eres operador turístico?</a>
    						</li>
    						<li><a href="#">Preguntas frecuentes</a>
    						</li>
    						<li><a href="#">Terminos y Condiciones</a>
    						</li>
    					</ul>
    				</div>
    				<div class="col-md-4 col-sm-6">
    					<p><img src="{{asset('front/img/slides/footerapps.png')}}" width="231" height="30" alt="Image" data-retina="true" class="img-responsive">
    					</p>
    					<strong>Transacciones seguras gracias a</strong>
    					<p><img src="{{asset('front/img/payments.png')}}" width="231" height="30" alt="Image" data-retina="true" class="img-responsive">
    					</p>
    					
    				</div>
    			</div>
    			<!-- End row -->
    			<div class="row">
    				<div class="col-md-12">
    					<div id="social_footer">
    						<ul>
    							<li><a href="#"><i class="icon-facebook"></i></a>
    							</li>
    							<li><a href="#"><i class="icon-google"></i></a>
    							</li>
    							<li><a href="#"><i class="icon-instagram"></i></a>
    							</li>
    							<li><a href="#"><i class="icon-youtube-play"></i></a>
    							</li>
    						</ul>
    						<p>© Puntos Suspensivos Editores 2018</p>
    					</div>
    				</div>
    			</div>
    			<!-- End row -->
    	</div> <!-- End container -->
	</footer> <!-- End footer -->

	<div id="toTop"></div><!-- Back to top button -->
