@extends('operator.layout.auth')

@section('title', 'Dashboard')


@section('content')
    <div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>Panel administrativo</h1>
                    </div>
                </div>
                {{-- <div class="col-sm-6 hidden-xs">
                    <div class="header-section">
                        <ul class="breadcrumb breadcrumb-top">
                            <li>Category</li>
                            <li><a href="">Page</a></li>
                        </ul>
                    </div>
                </div> --}}
            </div>
        </div>
        <!-- END Page Header -->

        <!-- Example Block -->
        <div class="block">
            
            <!-- Example Content -->
            <h3>Panel administrativo  <small>Por favor seleccione una de las opciones del lado izquierdo.</small></h3>
            <!-- END Example Content -->
        </div>
        <!-- END Example Block -->
    </div>
@endsection
