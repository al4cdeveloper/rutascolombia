@extends('operator.layout.auth')

@section('title', 'Preferencias')


@section('content')
<div id="page-content">
     <div class="content-header">
          <div class="row">
              <form>
                  <div class="col-sm-12">
                      <div class="header-section">
                          <h1>Perfil</h1>
                      </div>
                      
                  </div>
              </form>
          </div>
      </div>
      <!-- END Page Header -->

      <!-- Example Block -->
      <div class="block" id="service">
          <div class="row">
            <!-- Form Validation Content -->
                    <div class="col-md-12">
                      <h2>Editar información</h2>
                      <small>A continuación por favor ingresar la información que desees cambiar y luego clickea en el botón "Guardar".</small>
                    </div>
                    <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                        <!-- Form Validation Form -->
                        <form id="form-validation" action="{{url('operator/updatepreferences')}}" method="post" class="form-horizontal form-bordered">
                          {{csrf_field()}}
                            <div class="form-group">
                                <label class="col-md-6 control-label" for="name">Ingrese la cantidad de días con los que deben agendar los clientes <span class="text-danger">*</span></label>
                                <div class="col-md-6">
                                    <input type="number" id="name" name="days_for_reservation" class="form-control" placeholder="Ingrese la cantidad de días con los que deben agendar los clientes" required value="{{$user->days_for_reservation}}" max="10" min="1">
                                </div>
                            </div>
                            <div class="form-group form-actions">
                                <div class="col-md-8 col-md-offset-3">
                                    <button type="submit" class="btn btn-effect-ripple btn-primary">Enviar</button>
                                    <a href="{{url('operator/home')}}" class="btn btn-effect-ripple btn-danger">Cancelar</a>
                                </div>
                            </div>
                        </form>
                    </div>
            </div>
        </div> 
</div>
@endsection
