@extends('operator.layout.auth')

@section('title', 'Perfil')

{{-- @section('aditionalStyle')
    <link rel="stylesheet" type="text/css" href="{{asset('vue/css/app.css')}}">
@endsection --}}
@section('aditionalStyle')
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
@endsection

@section('content')
    <div id="page-content">

     <!-- Page Header -->
      <div class="content-header">
          <div class="row">
              <form>
                  <div class="col-sm-12">
                      <div class="header-section">
                          <h1>Perfil</h1>
                      </div>
                      
                  </div>
              </form>
          </div>
      </div>
      <!-- END Page Header -->

      <!-- Example Block -->
      <div class="block" id="service">
          <div class="row">
            <!-- Form Validation Content -->
                    <div class="col-md-12">
                      <h2>Editar información</h2>
                      <small>A continuación por favor ingresar la información que desees cambiar y luego clickea en el botón "Guardar".</small>
                    </div>
            {!!Form::model($user,['url'=>['operator/update'],'method'=>'POST', 'class'=> 'form-horizontal', 'enctype' => 'multipart/form-data', 'id' => 'userForm', 'novalidate'])!!}
                    <div class="col-sm-12">
                      <div class="">
                        <label class="col-md-3 control-label" for="name">Imagen de perfil <span class="text-danger">*</span></label>
                        
                        <div class="kv-avatar center-block text-center" style="width:200px">
                            <input id="avatar-2" name="file" type="file" class="file-loading">
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-12">
                        <label class="col-md-3 control-label" for="name">Nombre <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                           {!!Form::text('name', null, ['class'=>'form-control', 'placeholder' => 'Nombre o Razón social', 'required'])!!}
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <label class="col-md-3 control-label" for="national_register">Registro nacional de turismo <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                          {!!Form::text('national_register', null, ['class'=>'form-control', 'placeholder' => 'Registro Nacional de Turismo', 'disabled'])!!}
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <label class="col-md-3 control-label" for="nit_rut">NIT o RUT <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                          {!!Form::text('nit_rut', null, ['class'=>'form-control', 'placeholder' => 'Nit o RUT', 'disabled'])!!}
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <label class="col-md-3 control-label" for="contact_personal">Persona de contacto<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                          {!!Form::text('contact_personal', null, ['class'=>'form-control', 'placeholder' => 'Persona de contacto', 'required'])!!}
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <label class="col-md-3 control-label" for="contact_phone">Teléfono de contacto<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                          {!!Form::number('contact_phone', null, ['class'=>'form-control', 'placeholder' => 'Teléfono de contacto', 'required'])!!}
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <label class="col-md-3 control-label" for="email">Email <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                          {!!Form::email('email', null, ['class'=>'form-control', 'placeholder' => 'Ingresa tu email', 'required'])!!}
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <label class="col-md-3 control-label" for="web">Facebook </label>
                        <div class="col-md-6">
                            {!!Form::text('facebook', null, ['class'=>'form-control', 'placeholder' => 'URL de facebook', 'required'])!!}
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <label class="col-md-3 control-label" for="web">Twitter </label>
                        <div class="col-md-6">
                            {!!Form::text('twitter', null, ['class'=>'form-control', 'placeholder' => 'URL de twitter', 'required'])!!}
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <label class="col-md-3 control-label" for="web">Instagram </label>
                        <div class="col-md-6">
                            {!!Form::text('instagram', null, ['class'=>'form-control', 'placeholder' => 'URL de instagram', 'required'])!!}
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <label class="col-md-3 control-label" for="web">Página web <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            {!!Form::text('web', null, ['class'=>'form-control', 'placeholder' => 'Página web', 'required'])!!}
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <label class="col-md-3 control-label" for="web">Dirección </label>
                        <div class="col-md-6">
                            {!!Form::text('address', null, ['class'=>'form-control', 'placeholder' => 'Dirección', 'required'])!!}
                        </div>
                    </div>
                    <div class="col-sm-12">
                          <label for="duration">Descripción</label>
                          <div class="form-group">
                              {!! Form::textarea('description',null,['class'=>'ckeditor']) !!}
                          <span class="label label-danger">{{$errors->first('description') }}</span>
                          </div>
                    </div>
                    <div class="col-sm-12">
                        <h3>Cambiar contraseña</h3>
                            <button class="btn btn-danger" type="button" onclick="changepass()" id="cambiar">Cambiar contraseña</button>
                        <div id="changePass" class="hidden">
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label for="password">Password<span class="kv-reqd">*</span></label>
                                <input type="password" class="form-control" id="password" name="password" >
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label for="password_confirmation">Confirmar password<span class="kv-reqd">*</span></label>
                                <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" >
                              </div>
                            </div>
                        </div>
                      </div>
                    <div class="form-group form-actions col-sm-12" align="center">
                        <div class="col-md-8 col-md-offset-3">
                            <button type="submit" class="btn btn-effect-ripple btn-primary">Enviar</button>
                            <a href="{{url('operator/home')}}" class="btn btn-effect-ripple btn-danger">Cancelar</a>
                        </div>
                    </div>
              {!!Form::close()!!}
          </div>
      </div>
      <!-- END Example Block -->
        
  </div>

@endsection
@section('aditionalScript')

    <script src="{{asset('auth-panel/js/plugins/ckeditor/ckeditor.js')}}"></script>

    <script src="{{asset('auth-panel/js/pages/formsComponents.js')}}"></script>
    <script>$(function(){ FormsComponents.init(); });</script>
<script type="text/javascript">
  function changepass()
  {
      $("#changePass").removeClass('hidden');
      $("#cambiar").attr('onclick','cancel()');
      $("#cambiar").text('Cancelar');
      $("#password").attr('required','required')
      $("#password_confirmation").attr('required','required')
  }
function cancel()
  {
      $("#changePass").addClass('hidden');
      $("#cambiar").attr('onclick','changepass()');
      $("#cambiar").text('Cambiar contraseña');
      $("#password").removeAttr('required')
  }
$(document).ready(function()
{
  $("#avatar-2").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        showBrowse: false,
        browseOnZoneClick: true,
        removeLabel: '',
        removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-avatar-errors-2',
        msgErrorClass: 'alert alert-block alert-danger',
        defaultPreviewContent: '<img src="@if($user->avatar) {{asset($user->avatar)}} @else {{asset('img/no-profile-image.png')}} @endif" alt="Tu avatar" style="width:160px"><h6 class="text-muted">Click para cambiar</h6>',
        layoutTemplates: {main2: '{preview} ' + ' {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "gif"]
    });
});
</script>
@endsection