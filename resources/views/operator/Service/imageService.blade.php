@extends('operator.layout.auth')

@section('title', 'Imágenes de servicios')


@section('content')
    <div id="page-content">

     <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <form>
                    <div class="col-sm-12">
                        <div class="header-section">
                    <a href="{{url('operator/services/')}}" class="btn btn-effect-ripple btn-primary pull-right">Guardar</a>
                            <h1>Imágenes de servicio</h1>
                        </div>
                        
                    </div>
                </form>
            </div>
        </div>
        <!-- END Page Header -->
     <!-- Example Block -->
        <div class="block">
            {!!Form::open(['url'=>['operator/service/upload_images',$service->id_service_operator],'method'=>'PUT', 'class'=> 'form-horizontal dropzone', 'enctype' => 'multipart/form-data', 'id' => 'my-dropzone'])!!}
                <div class="panel-body">
                    <div class="dropzone-previews"></div>
                    <div class="dz-message" style="height:100px;">
                        Sube las imágenes aquí
                    </div>
                </div>
                {!! Form::close() !!}
        </div>

        <div class="block">
            <h2>Listado de Imágenes Existentes</h2>
            <p>La primera imagen que cargues será la elegida para poner en la parte superior del detalle de tu servicio, por lo cual te recomendamos que los tamaños de la misma sean de 1900 pixeles por 470 pixeles.</p>
            <div class="panel-body">
                <div class="gallery">
                    {!!Form::open(['url'=>['operator/service/update_images'], 'method'=>'POST','class'=>'form-horizontal'])!!}
                    <div class="row">
                    @foreach($service->images as $image)
                       <div class="col-sm-4 col-lg-3" >
                            <div class="media-items animation-fadeInQuick2" data-category="image">
                                <div class="media-items-options text-right">
                                    <a href="{{asset($image->link_image)}}" class="btn btn-xs btn-info" data-toggle="lightbox-image">Ver</a>
                                    <a href="{{url('operator/service/delete_image/'.$image->id_picture)}}" class="btn btn-xs btn-danger" onclick='return confirm("¿Desea eliminar éste registro?");'><i class="fa fa-times"></i></a>
                                </div>
                                <div class="media-items-content">
                                    <img src="{{asset($image->link_image)}}" class="img-responsive">
                                </div>
                                <div>
                                    <div class="form-group">
                                    <label for="description">Descripción</label>
                                    <textarea name="description[{{$image->id_picture}}]" id="description" class="form-control" rows="3" style="width: 88%;margin: 12px">{{$image->description}}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    </div>
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-info">Guardar descripciones</button>
                    </div>
                    {!!Form::close()!!}
                </div>
            </div>
        </div>
@endsection
@section('aditionalScript')
<script src="{{asset('plugins/dropzone/dist/min/dropzone.min.js')}}"></script>

<script type="text/javascript">
    Dropzone.options.myDropzone = {
        autoProcessQueue: true,
        uploadMultiple: true,
        maxFiles: 4,
        maxFilesize: 2, // MB
        acceptedFiles: '.jpg, .jpeg, .png, .bmp',

        init: function() {
            var submitBtn = document.querySelector("#submit");
            myDropzone = this;

            this.on("complete", function(file) {
                myDropzone.removeFile(file);
                if (this.getQueuedFiles().length == 0 && this.getUploadingFiles().length == 0) {
                    location.reload();
                }
            });

            this.on("success", function() {
                myDropzone.processQueue.bind(myDropzone);
            });
        }
    };

</script>