@extends('operator.layout.auth')

@section('title', 'Servicios')

@section('content')
    <div id="page-content">

     <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-12">
                    <div class="header-section">
                        @if(isset($service))
                        <h1>Editar servicio</h1>
                        @else
                        <h1>Nuevo servicio</h1>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="block">
            <!-- Labels on top Form Title -->
            <div class="block-title">
                <h2>Ingrese la información solicitada</h2>
            </div>
            <!-- END Labels on top Form Title -->

            <!-- Labels on top Form Content -->
            @if(isset($service))
            {!!Form::model($service,['route'=>['operator.services.update',$service->id_service_operator],'method'=>'PUT', 'class'=> 'form-bordered', 'enctype' => 'multipart/form-data','novalidate'])!!}
            @else
            {!!Form::open(['route'=>'operator.services.store', 'method'=>'POST', 'class'=> 'form-bordered', 'enctype' => 'multipart/form-data','novalidate'])!!}
            @endif
                <div class="form-group">
                    <label for="example-nf-email">Servicio</label>
                    <select id="example-chosen" name="service" class="select-chosen" data-placeholder="Seleccione el servicio.." required>
                        <option></option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
                        @foreach($services as $listService)
                          <option value="{{$listService->id_service}}" @if(isset($service)) @if($service->PrincipalService->service == $listService->service) selected @endif @endif>{{$listService->service}}</option>
                        @endforeach
                    </select>
                    <span class="label label-danger">{{$errors->first('service') }}</span>

                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="costo">Nombre del servicio</label>
                        {!!Form::text('service_name', null, ['class'=>'form-control', 'placeholder' => 'Ej: Parapente + fotos + vídeo', 'required'])!!}
                        <span class="label label-danger">{{$errors->first('service_name') }}</span>

                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="col-md-5 ">
                        <label class="col-md-4 control-label" for="example-progress-username">Dirección</label>
                        <div class="col-md-8">
                            {{Form::text('address',null,['class'=>'form-control','placeholder'=>'Ingrese dirección del servicio','id'=>'address'])}}
                            <span class="label label-danger">{{$errors->first('address') }}</span>
                        </div>
                        <small>Por favor ingrese su dirección y revise en el mapa que la ubicación contrada sea la correcta. <br> En caso de no serla por favor ingrese una dirección más exacta (Una opción puede ser poner el nombre de la ciudad después de la dirección).</small>
                        <div id="errors" style="color:red;"></div>
                    </div>
                    <div class="col-md-5 col-md-offset-1">
                        <div id="map" style="height: 300px;width: 300px;"></div>
                    </div>

                    {!!Form::hidden('latitude',null,['id'=>'latitude'])!!}
                    {!!Form::hidden('longitude',null,['id'=>'longitude'])!!}
                </div>
                <div class="col-sm-12">
                    <div class="col-sm-6">
                        <div class="form-group">

                            {!!Form::label('days', 'Días de funcionamiento')!!}
                            <select name="days[]" class="select-chosen" data-placeholder="Selecciona los días de servicio" multiple required>
                                @foreach($days as $day)
                                    <option value="{{$day}}" @if(isset($service))

                                        @foreach($service->Schedule as $daySave)

                                                @if($daySave->day == $day) selected @endif 
                                        @endforeach

                                    @endif>{{$day}}</option>
                                @endforeach
                            </select>
                            <span class="label label-danger">{{$errors->first('days') }}</span>

                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="capacity">Capacidad de personas que se pueden atender por servicio o por hora de servicio</label>
                            {!!Form::number('capacity', null, ['class'=>'form-control', 'placeholder' => 'Ingrese capacidad de personas', 'required'])!!}
                            <span class="label label-danger">{{$errors->first('capacity') }}</span>
                        </div>

                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="duration">Hora(s) de duración de cada servicio</label>
                            {!!Form::number('duration', null, ['class'=>'form-control', 'placeholder' => 'Ingrese horas de duración del servicio', 'required'])!!}
                            <span class="label label-danger">{{$errors->first('duration') }}</span>
                        </div>
                    </div>
                    @if(!isset($service))
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="location">Departamento</label>
                            {!!Form::select('departments', $departments, null, ['class'=>'select-chosen','id'=>'departments'])!!}
                        </div>
                    </div>
                    @else
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="location">Departamento</label>
                                <select name="departments" id="departments" class="select-chosen">
                                    @foreach($departments as $id => $departmentF)
                                        <option value="{{$id}}" @if($id == $department) selected @endif>{{$departmentF}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="col-sm-12" id="exactSite">
                    <div class="col-sm-6 @if(!isset($service)) hidden @endif " id="municipalityDiv">
                        <div class="form-group">
                            <label for="location">Municipio</label>
                            <select name="fk_municipality" id="municipalities" class="select-chosen">
                                @if(isset($service))
                                    @if($service->type_relation=="Municipality")
                                        @foreach($municipalities as $id => $municipalityF)
                                            <option value="{{$id}}" @if($service->fk_location==$id) selected @endif>{{$municipalityF}}</option>    
                                        @endforeach
                                    @else
                                        @foreach($municipalities as $id => $municipalityF)
                                            <option value="{{$id}}" @if($service->fk_location==$id) selected @endif>{{$municipalityF}}</option>    
                                        @endforeach
                                    @endif
                                @endif
                            </select>
                        </div>
                    </div>
                     <div class="col-sm-6" >
                        <div class="form-group @if(isset($service) && $service->type_relation!="InterestSite") hidden @endif" id="siteDiv">
                            <label for="location">Sitio de interés</label>
                            <select name="fk_site" id="interesSite" class="select-chosen">
                                @if(isset($service))
                                    @if($service->type_relation=="InterestSite")
                                        @foreach($municipality->InterestSite as $site)
                                            <option value="{{$site->id_site}}" @if($service->fk_location==$site->id_site) selected @endif>{{$site->site_name}}</option>    
                                        @endforeach
                                    @else
                                        @if(count($municipality->InterestSite)>0)
                                            <option value=""></option>
                                            @foreach($municipality->InterestSite as $site)
                                                <option value="{{$site->id_site}}" >{{$site->site_name}}</option>    
                                            @endforeach
                                        @endif
                                    @endif
                                @endif
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="costo">Vídeo de Youtube<small>Debe agregar un vídeo de su servicio</small></label>
                    {!!Form::text('video', null, ['class'=>'form-control', 'placeholder' => 'Inserte link de vídeo de youtube (ej:https://www.youtube.com/watch?v=b_RJJF_GzeQ)'])!!}
                    <span class="label label-danger">{{$errors->first('video') }}</span>
                </div>
                <div class="form-group">
                    <label for="duration">Descripción</label>
                    <div class="form-group">
                        {!! Form::textarea('description',null,['class'=>'ckeditor']) !!}
                        <span class="label label-danger">{{$errors->first('description') }}</span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="duration">Recomendaciones</label>
                    <div class="form-group">
                        {!! Form::textarea('requisites',null,['class'=>'ckeditor']) !!}
                        <span class="label label-danger">{{$errors->first('requisites') }}</span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="duration">Políticas</label>
                    <div class="form-group">
                        {!! Form::textarea('policies',null,['class'=>'ckeditor']) !!}
                        <span class="label label-danger">{{$errors->first('policies') }}</span>
                    </div>
                </div>
                
                 {!!Form::hidden('type_relation', null, ['id'=>'type'])!!}
                <div class="form-group form-actions" align="center">
                    <button type="submit" class="btn btn-effect-ripple btn-primary">Guardar</button>
                    <a href="{{url('operator/services')}}" class="btn btn-effect-ripple btn-danger">Cancelar</a>
                </div>
            {!!Form::close()!!}
            <!-- END Labels on top Form Content -->
        </div>
    </div>
@endsection
@section('aditionalScript')
    
    <script src="{{asset('auth-panel/js/plugins/ckeditor/ckeditor.js')}}"></script>

    <script src="{{asset('auth-panel/js/pages/formsComponents.js')}}"></script>
    <script>$(function(){ FormsComponents.init(); });</script>

    <script>
        $("#departments").change(function()
        {   
            token = '{{csrf_token()}}' 
            $.ajax({
                url: '/api/getmunicipalities',
                type: 'POST',
                headers:{'X-CSRF-TOKEN':token},
                dataType: 'JSON',
                data: {department: $(this).val()},
                complete:function(transport)
                {
                    response = transport.responseJSON;
                    $('#municipalities').empty();
                    $("#municipalityDiv").removeClass('hidden');
                    $("#interesSite").empty();
                    $("#siteDiv").addClass('hidden');
                    $("#municipalities").append('<option value=""></option>').trigger("chosen:updated");

                    for(x in response)
                    {
                        $("#municipalities").append('<option value="'+x+'">'+response[x]+'</option>').trigger("chosen:updated");
                    }
                }
            })
        })
        $('#municipalities').change(function(event) 
        {
            token = '{{csrf_token()}}';
            $.ajax({
                url: '/api/getinterestsites',
                type: 'POST',
                dataType: 'JSON',
                data: {municipality: $(this).val()},
                headers:{'X-CSRF-TOKEN':token},
                complete:function (transport)
                {
                    response = transport.responseJSON;
                    if(response['number']>0)
                    {
                        $("#siteDiv").removeClass('hidden');
                        $("#interesSite").empty();
                        $("#interesSite").append('<option value=""></option>').trigger("chosen:updated");
                        for(x in response['sites'])
                        {
                            $("#interesSite").append('<option value="'+x+'">'+response['sites'][x]+'</option>').trigger("chosen:updated");
                        }
                    }
                    else
                    {
                        $("#siteDiv").addClass('hidden');
                        $("#interesSite").empty();
                    }
                }
            });
            $('#type').val('Municipality');
        });

        $("#address").on('blur',function()
        {
            if($("#latitude").val() == "" || $("#longitude").val()=="")
            {
                $("#address").val('');
            }
        });
        @if($errors->first('address'))
            $("#address").val('');
        @endif

        @if(!isset($service))
        $("#departments").val('');
        $("#address").val('');

        @endif

        $("#interesSite").change(function(event) {
            $('#type').val('InterestSite');
        });
        var controladorTiempo = "";
            var markers = [];
           function initMap() {
                var map = new google.maps.Map(document.getElementById('map'), {
                  zoom: 10,
                  center: {lat: 4.570868, lng: -74.29733299999998}
                });
                var geocoder = new google.maps.Geocoder();
                $("#address").on("keyup", function() {
                    clearTimeout(controladorTiempo);
                    controladorTiempo = setTimeout(geocodeAddress(geocoder, map), 2000);
                });
               
              }
        function geocodeAddress(geocoder, resultsMap) 
        {
            var address = document.getElementById('address').value+",Colombia" ;
            $('#errors').empty();
            clearMarkers();
            if(address!= "")
            {
                geocoder.geocode({'address': address}, function(results, status) {
                  if (status === 'OK') {
                    clearMarkers();
                    resultsMap.setCenter(results[0].geometry.location);
                    var marker = new google.maps.Marker({
                      map: resultsMap,
                      position: results[0].geometry.location
                    });
                     markers.push(marker);
                    $('#errors').empty();

                    $("#latitude").val(results[0].geometry.location.lat());
                    $("#longitude").val(results[0].geometry.location.lng());
                    console.log(results[0].geometry.location.lat() + " " + results[0].geometry.location.lng());
                  } else {
                    $('#errors').empty();
                     $("#latitude").val('');
                    $("#longitude").val('');
                    $('#errors').append('No se puede encontrar su localización, intentelo nuevamente');
                  }
                });
            }
            
          }
          // Sets the map on all markers in the array.
      function setMapOnAll(map) {
        for (var i = 0; i < markers.length; i++) {
          markers[i].setMap(map);
        }
      }

      // Removes the markers from the map, but keeps them in the array.
      function clearMarkers() {
        setMapOnAll(null);
      }

      // Shows any markers currently in the array.
      function showMarkers() {
        setMapOnAll(map);
      }

      // Deletes all markers in the array by removing references to them.
      function deleteMarkers() {
        clearMarkers();
        markers = [];
      }
        </script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBXtArR0GzW4Y_EezVpjGdvjrlwsQBcayw&callback=initMap"
        async defer></script>
@endsection