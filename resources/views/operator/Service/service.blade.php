@extends('operator.layout.auth')

@section('title', 'Servicios')
{{-- 
@section('aditionalStyle')
    <link rel="stylesheet" type="text/css" href="{{asset('vue/css/app.css')}}">
@endsection
 --}}
@section('content')
    <div id="page-content">

     <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-12">
                    <div class="header-section">
                        @if(count($services) < Auth::user()->cuantity_services)
                        <div class="pull-right">
                            <a href="{{route('operator.services.create')}}" class="btn btn-primary"> Crear nuevo servicio</a>
                        </div>
                        @else
                        <div class="pull-right">
                            Haz excedido la cantidad de servicios permitidos. <br>
                            Para más información,  <a href="{{url('/contact?name='.Auth::user()->name.'&email='.Auth::user()->email.'&phone='.Auth::user()->contact_phone)}}" target="_blank">solicita contacto con soporte</a>.
                        </div>
                        @endif
                        <h1>Servicios</h1>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Page Header -->

        <!-- Example Block -->
        <div class="block" id="service">
            
            <table id="datatable" class="table" role="grid" aria-describedby="datatable_info">
                <thead>
                    <th>Servicio</th>
                    <th>Nombre servicio</th>
                    <th>Costo</th>
                    <th>Tipo de servicio</th>
                    <th>Lugar</th>
                    <th>Capacidad</th>
                    <!-- <th>Desde</th> -->
                    <!-- <th>Hasta</th> -->
                    <th>Días de operación</th>
                    <th>Estado</th>
                    <th>Horarios</th>
                    <th style="width: 120px;">
                            &nbsp;
                        </th>
                </thead>
                <tbody>
                    @foreach($services as $service)
                    <tr >
                        <td>{{$service->PrincipalService->service}}</td>
                        <td>{{$service->service_name}}</td>
                        <td>{{$service->cost}}</td>
                        <td>{{$service->PrincipalService->type_service}}</td>
                        <td>{{$service->Municipality->municipality_name}}</td>
                        <td>{{$service->capacity}}</td>
                        <td>
                            @if(isset($service->days))
                                <ul>
                                @foreach($service->days as $day)
                                    <li>{{$day->day}}</li>
                                @endforeach
                                </ul>
                            @else
                            No definidos
                            @endif
                        </td>
                        <td>@if($service->state=='Inhabilitado') <label data-toggle="tooltip" data-placement="top" title="Para poder activar  el servicio es necesario que indiques toda la información del mismo (horarios)">No disponible</label> @else {{$service->state}} @endif</td>
                        <td>
                            @if($service->fk_municipality != "")
                            <a href="{{url('operator/shedule/'.$service->slug)}}" class="btn btn-primary"> <i class="fa fa-calendar" aria-hidden="true"></i></a>
                            @else
                            <button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Para poder activar  el servicio es necesario que indiques toda la información del mismo">No disponible</button>
                            @endif
                            {{-- <router-link  :to="{name:'schedule', params:{id:service.id_service}}" ></router-link> --}}
                        </td>
                        <td >
                            <a href="{{route('operator.services.edit',$service->id_service_operator)}}" class="btn btn-warning" data-toggle="tooltip" title="Editar servicio" {{-- data-target="#editService{{$service->id_service_operator}}" --}}><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                            <a href="{{ url('operator/serviceimages', $service->id_service_operator) }}" data-toggle="tooltip" title="Administrar imágenes" class="btn btn-info"><i class="fa fa-photo" aria-hidden="true"></i></a>
                            <a href="{{ url('operator/items', $service->id_service_operator) }}" data-toggle="tooltip" title="Administrar servicios adicionales" class="btn btn-success"><i class="fa fa-puzzle-piece"></i></a>
                            @if($service->state=="Activo")
                            <a href="{{url('operator/service/desactivate/'.$service->slug)}}" data-toggle="tooltip" title="Desactivar servicio" class="btn btn-effect-ripple btn-danger"><i class="fa fa-times"></i></a>
                            @elseif($service->state!="Desactivado por administrador")
                            <a href="{{url('operator/service/activate/'.$service->slug)}}" data-toggle="tooltip" title="Activar servicio" class="btn btn-effect-ripple btn-success"><i class="fa fa-check"></i></a>
                            @endif
                        </td>

                       {{--  <td width="10px">
                            <a href="#" class="btn btn-danger btn-sm" >Desactivar</a> 
                        </td> --}}
                    </tr>

                    <div class="modal fade" id="editService{{$service->id_service_operator}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <form method="post" action="{{url('operator/updateservice/'.$service->id_service_operator)}}">

                                {{csrf_field()}}
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Editar servicio</h4>
                              </div>
                              <div class="modal-body">
                                <div class="modal-body form-horizontal">
                                    <div class="form-group ">
                                        <label for="service" class="col-md-4 control-label">Servicio</label>
                                        <div class="col-md-6">
                                            <input id="service" type="text" class="form-control" name="service" value="{{$service->service}}" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="cost" class="col-md-4 control-label">Costo</label>
                                        <div class="col-md-6">
                                            <input id="cost" type="text" class="form-control" name="cost" value="{{$service->cost}}" required>
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="address" class="col-md-4 control-label">Direccion</label>
                                        <div class="col-md-6">
                                            <input id="address" type="text" class="form-control" name="address" value="{{$service->address}}" required>
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="days" class="col-md-4 control-label">Días de funcionamiento</label>
                                        <div class="col-md-6">
                                            <select name="days[]" class="select-chosen" data-placeholder="Selecciona los días de servicio" multiple required>
                                                @foreach($days as $day)
                                                    <option value="{{$day}}" @if(isset($service->days))

                                                        @foreach($service->days as $daySave)
                                                                @if($daySave->day == $day) selected @endif 
                                                        @endforeach

                                                    @endif>{{$day}}</option>
                                                @endforeach
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="capacity" class="col-md-4 control-label">Capacidad de personas</label>
                                        <div class="col-md-6">
                                            <input id="capacity" type="number" class="form-control" name="capacity" value="{{$service->capacity}}" required >
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="capacity" class="col-md-4 control-label">Duración del servicio en horas</label>
                                        <div class="col-md-6">
                                            <input id="duration" type="number" class="form-control" name="duration" value="{{$service->duration}}" required >
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="location" class="col-md-4 control-label">Lugar/Ciudad</label>
                                        <div class="col-md-6">
                                            <input id="location" type="text" class="form-control" name="location" value="{{$service->location}}" required>
                                        </div>
                                    </div>
                                  
                                    <div class="form-group ">
                                        <label for="requisites" class="col-md-4 control-label">Recomendaciones</label>
                                        <div class="col-md-6">
                                            <textarea class="form-control" name="requisites" id="requisites">{{$service->requisites}}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="description" class="col-md-4 control-label">Descripcion</label>
                                        <div class="col-md-6">
                                            <textarea class="form-control" name="description" id="description"> {{$service->description}}</textarea>
                                        </div>
                                    </div>
{{--                                     <div class="form-group " v-if="fillService.state != 'Inhabilitado'">
                                        <label for="state" class="col-md-4 control-label">Estado</label>
                                        <div class="col-md-6">
                                            <textarea class="form-control" name="state" id="state" v-model="fillService.state"></textarea>
                                        </div>
                                    </div> --}}
                                </div>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                <button type="submit" class="btn btn-primary">Guardar</button>
                               </div>
                            </form>
                        </div>
                      </div>
                    </div>
                    @endforeach
                </tbody>
            </table>

        </div>
        <!-- END Example Block -->
    
    {{-- <div class="col-sm-6 hidden-xs">
                    <div class="header-section">
                        <ul class="breadcrumb breadcrumb-top">
                            <li>Category</li>
                            <li><a href="">Page</a></li>
                        </ul>
                    </div>
                </div> --}}
    </div>
@endsection
{{-- @section('aditionalScript')
        <script src="{{asset('auth-panel/js/pages/compCalendar.js')}}"></script>
        <script src="{{asset('vue/js/Service.js')}}"></script>

        <script type="text/javascript">
            $(document).ready(function() {

                // page is now ready, initialize the calendar...

                $('#calendar').fullCalendar({
                    // put your options and callbacks here
                })

            });
        </script>
@endsection --}}