@extends('admin.layout.auth')

@section('title', 'Listado de circuitos')


@section('content')
    <div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>Circuitos</h1>
                    </div>
                </div>
            </div>
        </div>
            <div class="block full">
                <div class="block-title">
                    <h2>Circuitos</h2>
                    <div class="pull-right" style="padding: 5px;"><a href="{{url('admin/circuits/create')}}" class="btn btn-primary btn-sm"><i class="far fa-plus-square fa-lg"></i></a></div>
                </div>
                <div class="table-responsive">
                    <table id="example-datatable" class="table table-striped table-bordered table-vcenter">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th style="width: 120px;">Estado</th>
                                <th class="text-center" style="width: 75px;"><i class="fas fa-sync-alt"></i></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($circuits as $circuit)
                            <tr>
                                <td>{{$circuit->name}}</td>
                                <td>@if($circuit->state=="activo")
                                    <a class="label label-info">{{$circuit->state}}</a>
                                    @else
                                    <span class="label label-danger">{{$circuit->state}}</span>
                                    @endif
                                </td>
                                <td class="text-center">

                                    <a href="{{url('admin/circuits/edit/'.$circuit->slug)}}" data-toggle="tooltip" title="Editar circuito" class="btn btn-effect-ripple btn-xs btn-warning"><i class="fa fa-edit"></i></a>
                                    @if($circuit->state=="activo")
                                    <a href="{{url('admin/circuits/desactivate/'.$circuit->id_circuit)}}" data-toggle="tooltip" title="Desactivar circuito" class="btn btn-effect-ripple btn-xs btn-danger"><i class="fa fa-times"></i></a>
                                    @else
                                    <a href="{{url('admin/circuits/activate/'.$circuit->id_circuit)}}" data-toggle="tooltip" title="Activar circuito" class="btn btn-effect-ripple btn-xs btn-success"><i class="fas fa-check"></i></a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
    </div>
@endsection

@section('aditionalScript')
<!-- Load and execute javascript code used only in this page -->
        <script src="{{asset('auth-panel/js/pages/uiTables.js')}}"></script>
        <script>$(function(){ UiTables.init(); });</script>
@endsection