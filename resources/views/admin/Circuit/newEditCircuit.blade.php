@extends('admin.layout.auth')

@if(isset($circuit))
    @section('title', 'Editar circuito')
@else
    @section('title', 'Crear circuito ')
@endif

@section('aditionalStyle')
    <link href="{{asset('plugins/kartik-v-bootstrap-fileinput/css/fileinput.css')}}" media="all" rel="stylesheet" type="text/css"/>
    <style>
.kv-avatar .krajee-default.file-preview-frame,.kv-avatar .krajee-default.file-preview-frame:hover {
    margin: 0;
    padding: 0;
    border: none;
    box-shadow: none;
    text-align: center;
}
.kv-avatar {
    display: inline-block;
}
.kv-avatar .file-input {
    display: table-cell;
    width: 213px;
}
.kv-reqd {
    color: red;
    font-family: monospace;
    font-weight: normal;
}
</style>
@endsection

@section('content')
<div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>Circuito</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="block">
            <!-- Labels on top Form Title -->
            <div class="block-title">
                <h2>Ingrese la información solicitada</h2>
                 @if(isset($circuit))
                    <div class="col-sm-6 col-sm-offset-6">
                        <div class="pull-right" style="padding: 5px;">
                                <a href="{{url('admin/circuits/images/'.$circuit->slug)}}" class="btn btn-primary btn-sm">Administrar multimedia  <i class="fa fa-images fa-lg"></i></a><br>
                                <a href="{{url('admin/circuits/detailitems/'.$circuit->slug)}}" class="btn btn-primary btn-sm">Administrar items detallados  <i class="fas fa-list-ul"></i></a>
                        </div>
                    </div> 
                @else 
                    <div class="col-sm-6 col-sm-offset-6">
                        <p>Luego de crear el circuito se activarán las opciones de administración de multimedia e items.</p>
                    </div>
                @endif
            </div>
			
            @if(isset($circuit))
            {!!Form::model($circuit,['url'=>['admin/circuits/update',$circuit->id_circuit],'method'=>'POST', 'class'=> 'form-horizontal', 'enctype' => 'multipart/form-data', 'id' => 'userForm', 'novalidate'])!!}
            @else
            {!!Form::open(['url'=>'admin/circuits/store', 'method'=>'POST', 'class'=> 'form-horizontal nuevo', 'enctype' => 'multipart/form-data', 'id' => 'userForm', 'novalidate'])!!}
            @endif
            <div class="col-sm-12">
                <div class="col-sm-4 text-center col-sm-offset-2">
                    <div class="kv-avatar">
                        <div class="form-group">
                        <label for="costo">Imagen de tarjeta <small>(Opcional)</small></label>
                            <div class="file-loading">

                                {!!Form::file('card_image',['id'=>'avatar-1'])!!}
                            </div>
                            <span class="label label-danger">{{$errors->first('card_image') }}</span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 text-center">
                    <div class="kv-avatar">
                        <div class="form-group">
                        <label for="costo">Imagen de introducción <small>(Opcional)</small></label>
                            <div class="file-loading">

                                {!!Form::file('introduction_image',['id'=>'avatar-2'])!!}
                            </div>
                            <span class="label label-danger">{{$errors->first('introduction_image') }}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="costo">Nombre del circuito</label>
                    {!!Form::text('name', null, ['class'=>'form-control', 'placeholder' => 'Inserte nombre del circuito', 'required'])!!}
                    <span class="label label-danger">{{$errors->first('name') }}</span>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="costo">Descripción corta</label>
                    {!!Form::text('short_description', null, ['class'=>'form-control', 'placeholder' => 'Inserte una descripción corta', 'required'])!!}
                    <span class="label label-danger">{{$errors->first('short_description') }}</span>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                <label >Palabras clave <small>Separadas por comas (,)</small></label>
                    {!!Form::text('keywords',null,['class'=>'form-control input-tags', 'placeholder'=>'Palabras clave'])!!}
                    <span class="label label-danger">{{$errors->first('keywords') }}</span>
                </div>
            </div>
            <div class="col-sm-12 rutas">
                <div class="form-group">
                    <label for="costo">Vídeo (Opcional)</label>
                    {!!Form::text('video', null, ['class'=>'form-control', 'placeholder' => 'Inserte url de video en caso de tenerlo', 'required'])!!}
                    <span class="label label-danger">{{$errors->first('video') }}</span>
                </div>
            </div>
            <div class="col-sm-12 form-group">
                <label for="duration">Descripción</label>
                <div class="form-group">
                    {!! Form::textarea('description',null,['class'=>'ckeditor']) !!}
                    <span class="label label-danger">{{$errors->first('description') }}</span>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="col-sm-4 text-center col-sm-offset-4">
                    <div class="kv-avatar">
                        <div class="form-group">
                        <label for="costo">Imagen de Descripción <small>(Opcional)</small></label>
                            <div class="file-loading">

                                {!!Form::file('description_image',['id'=>'avatar-3'])!!}
                            </div>
                            <span class="label label-danger">{{$errors->first('description_image') }}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 rutas">
                <label for="duration">Recomendaciones</label>
                <div class="form-group">
                    {!! Form::textarea('recommendations',null,['class'=>'ckeditor']) !!}
                    <span class="label label-danger">{{$errors->first('recommendations') }}</span>
                </div>
            </div>
            <div class="col-sm-12 rutas">
                <div class="col-sm-4 text-center col-sm-offset-4">
                    <div class="kv-avatar">
                        <div class="form-group">
                        <label for="costo">Imagen de recomendaciones <small>(Opcional)</small></label>
                            <div class="file-loading">

                                {!!Form::file('recommendation_image',['id'=>'avatar-4'])!!}
                            </div>
                            <span class="label label-danger">{{$errors->first('recommendation_image') }}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 rutas">
                <label for="duration">Texto de imágenes</label>
                <div class="form-group">
                    {!! Form::textarea('text_images',null,['class'=>'ckeditor']) !!}
                    <span class="label label-danger">{{$errors->first('text_images') }}</span>
                </div>
            </div>
            <div class="col-sm-12 rutas">
                <label for="duration">Archivo de descarga (opcional)</label>
                <div class="form-group">
                    {!!Form::file('download_file',['id'=>'avatar-3'])!!}
                    <span class="label label-danger">{{$errors->first('download_file') }}</span>
                </div>
            </div>
            <div class="form-group form-actions" align="center">
                <button type="submit" class="btn btn-effect-ripple btn-primary" name="typesubmit" value="guardarysalir">Guardar y salir</button>
                <button type="submit" class="btn btn-effect-ripple btn-info" name="typesubmit" value="guardartodo">Guardar todo</button>
                <a href="{{url('admin/circuits')}}" class="btn btn-effect-ripple btn-danger">Cancelar</a>
            </div>
            {!!Form::close()!!}
        </div>
</div>
@endsection

@section('aditionalScript')

    <script src="{{asset('auth-panel/js/plugins/ckeditor/ckeditor.js')}}"></script>

    <script src="{{asset('auth-panel/js/pages/formsComponents.js')}}"></script>
    <script>$(function(){ FormsComponents.init(); });</script>

     <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/plugins/sortable.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/fileinput.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/locales/fr.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/locales/es.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/themes/explorer-fa/theme.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/themes/fa/theme.js')}}" type="text/javascript"></script>

    <script>
        $(document).on('ready', function() {
            $("#input-20").fileinput({
                browseClass: "btn btn-primary btn-block",
                browseLabel: "Seleccionar imagen",
                showRemove: false,
                showUpload: false

            });
        });

         $("#avatar-1").fileinput({
            overwriteInitial: true,
            maxFileSize: 1500,
            showClose: false,
            showCaption: false,
            browseLabel: '',
            removeLabel: '',
            browseIcon: '<i class="fab fa-searchengin"></i>',
            removeIcon: '<i class="far fa-times-circle"></i>',
            viewIcon:'<i class="far fa-times-circle"></i>',
            removeTitle: 'Cancel or reset changes',
            elErrorContainer: '#kv-avatar-errors-1',
            msgErrorClass: 'alert alert-block alert-danger',
            @if(isset($circuit))
            defaultPreviewContent: '<img src="{{asset($circuit->card_image)}}" alt="Imagen de tarjeta" width="50%">',
                @if($circuit->card_image)
                defaultPreviewContent: '<img src="{{asset($circuit->card_image)}}" alt="Imagen de introducción" width="50%">',
                @else
                defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Imagen de introducción" width="50%">',
                @endif
            @else
            defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Imagen de tarjeta" width="50%">',
            @endif
            layoutTemplates: {main2: '{preview} ' + ' {remove} {browse}'},
            allowedFileExtensions: ["jpg", "png", "gif"]
        });
        $("#avatar-2").fileinput({
            overwriteInitial: true,
            maxFileSize: 1500,
            showClose: false,
            showCaption: false,
            browseLabel: '',
            removeLabel: '',
            browseIcon: '<i class="fab fa-searchengin"></i>',
            removeIcon: '<i class="far fa-times-circle"></i>',
            viewIcon:'<i class="far fa-times-circle"></i>',
            removeTitle: 'Cancel or reset changes',
            elErrorContainer: '#kv-avatar-errors-1',
            msgErrorClass: 'alert alert-block alert-danger',
            @if(isset($circuit))
                @if($circuit->introduction_image)
                defaultPreviewContent: '<img src="{{asset($circuit->introduction_image)}}" alt="Imagen de introducción" width="50%">',
                @else
                defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Imagen de introducción" width="50%">',
                @endif
            @else
            defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Imagen de introducción" width="50%">',
            @endif
            layoutTemplates: {main2: '{preview} ' + ' {remove} {browse}'},
            allowedFileExtensions: ["jpg", "png", "gif"]
        });
        $("#avatar-3").fileinput({
            overwriteInitial: true,
            maxFileSize: 1500,
            showClose: false,
            showCaption: false,
            browseLabel: '',
            removeLabel: '',
            browseIcon: '<i class="fab fa-searchengin"></i>',
            removeIcon: '<i class="far fa-times-circle"></i>',
            viewIcon:'<i class="far fa-times-circle"></i>',
            removeTitle: 'Cancel or reset changes',
            elErrorContainer: '#kv-avatar-errors-1',
            msgErrorClass: 'alert alert-block alert-danger',
            @if(isset($circuit))
            defaultPreviewContent: '<img src="{{asset($circuit->description_image)}}" alt="Imagen de descripción" width="50%">',
                @if($circuit->description_image)
                defaultPreviewContent: '<img src="{{asset($circuit->description_image)}}" alt="Imagen de descripción" width="50%">',
                @else
                defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Imagen de descripción" width="50%">',
                @endif
            @else
            defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Imagen de descripción" width="50%">',
            @endif
            layoutTemplates: {main2: '{preview} ' + ' {remove} {browse}'},
            allowedFileExtensions: ["jpg", "png", "gif"]
        });
        $("#avatar-4").fileinput({
            overwriteInitial: true,
            maxFileSize: 1500,
            showClose: false,
            showCaption: false,
            browseLabel: '',
            removeLabel: '',
            browseIcon: '<i class="fab fa-searchengin"></i>',
            removeIcon: '<i class="far fa-times-circle"></i>',
            viewIcon:'<i class="far fa-times-circle"></i>',
            removeTitle: 'Cancel or reset changes',
            elErrorContainer: '#kv-avatar-errors-1',
            msgErrorClass: 'alert alert-block alert-danger',
            @if(isset($circuit))
            defaultPreviewContent: '<img src="{{asset($circuit->recommendation_image)}}" alt="Imagen de recomendación" width="50%">',
                @if($circuit->recommendation_image)
                defaultPreviewContent: '<img src="{{asset($circuit->recommendation_image)}}" alt="Imagen de recomendación" width="50%">',
                @else
                defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Imagen de recomendación" width="50%">',
                @endif
            @else
            defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Imagen de recomendación" width="50%">',
            @endif
            layoutTemplates: {main2: '{preview} ' + ' {remove} {browse}'},
            allowedFileExtensions: ["jpg", "png", "gif"]
        });

    </script>
@endsection