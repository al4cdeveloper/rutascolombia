@extends('admin.layout.auth')

@if(isset($department))
    @section('title', 'Actualizar departamento')
@else
    @section('title', 'Crear departamento')
@endif
@section('aditionalStyle')
    <link href="{{asset('plugins/kartik-v-bootstrap-fileinput/css/fileinput.css')}}" media="all" rel="stylesheet" type="text/css"/>
    <style>
.kv-avatar .krajee-default.file-preview-frame,.kv-avatar .krajee-default.file-preview-frame:hover {
    margin: 0;
    padding: 0;
    border: none;
    box-shadow: none;
    text-align: center;
}
.kv-avatar {
    display: inline-block;
}
.kv-avatar .file-input {
    display: table-cell;
    width: 213px;
}
.kv-reqd {
    color: red;
    font-family: monospace;
    font-weight: normal;
}
</style>
@endsection
@section('content')
<div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>Departamentos</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="block">
            <!-- Labels on top Form Title -->
            <div class="block-title">
                <h2>Ingrese la información solicitada</h2>
            </div>
            @if(isset($department))
			{!!Form::model($department,['url'=>['admin/departments/update',$department->id_department],'method'=>'POST', 'class'=> 'form-horizontal', 'enctype' => 'multipart/form-data', 'id' => 'userForm', 'novalidate'])!!}
			@else
			{!!Form::open(['url'=>'admin/departments/store', 'method'=>'POST', 'class'=> 'form-horizontal nuevo', 'enctype' => 'multipart/form-data', 'id' => 'userForm', 'novalidate'])!!}
			@endif
            <div class="col-sm-12">
                <div class="col-sm-4 text-center col-sm-offset-4">
                    <div class="kv-avatar">
                        <div class="form-group">
                        <label for="costo">Imagen principal del departamento</label>
                            <div class="file-loading">

                                {!!Form::file('link_image',['id'=>'avatar-1'])!!}
                            </div>
                            <span class="label label-danger">{{$errors->first('link_image') }}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="costo">Región</label>
                    {!!Form::select('fk_region', $regions, null, ['class'=>'select-chosen'])!!}
                </div>
            </div>
			<div class="col-sm-12">
                <div class="form-group">
                    <label for="costo">Nombre</label>
                    {!!Form::text('department_name', null, ['class'=>'form-control', 'placeholder' => 'Inserte nombre del departamento', 'required','id'=>'department_name'])!!}
                    <span class="label label-danger">{{$errors->first('department_name') }}</span>
                </div>
            </div>
             <div class="form-group col-sm-12">
                <div id="errors" style="color:red;"></div>
                <div class="col-md-4 col-md-offset-4">
                        <div id="map" style="height: 300px;width: 300px;"></div>
                    </div>

                    {!!Form::text('latitude',null,['id'=>'latitude'])!!}
                    {!!Form::text('longitude',null,['id'=>'longitude'])!!}
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                <label >Palabras clave <small>Separadas por comas (,)</small></label>
                    {!!Form::text('keywords',null,['class'=>'form-control input-tags', 'placeholder'=>'Palabras clave'])!!}
                    <span class="label label-danger">{{$errors->first('keywords') }}</span>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="costo">Descripción corta</label>
                    {!!Form::text('short_description', null, ['class'=>'form-control', 'placeholder' => 'Inserte breve descripción del departamento', 'required'])!!}
                    <span class="label label-danger">{{$errors->first('short_description') }}</span>
                </div>
            </div>
            <div class="col-sm-12 rutas">
                <div class="form-group">
                    <label for="costo">Vídeo (Opcional)</label>
                    {!!Form::text('video_youtube', null, ['class'=>'form-control', 'placeholder' => 'Inserte url de video en de youtube', 'required'])!!}
                    <span class="label label-danger">{{$errors->first('video_youtube') }}</span>
                </div>
            </div>
            <div class="col-sm-12 form-group">
                <label for="duration">Descripción</label>
                <div class="form-group">
                    {!! Form::textarea('description',null,['class'=>'ckeditor']) !!}
                    <span class="label label-danger">{{$errors->first('description') }}</span>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="col-sm-4 text-center col-sm-offset-4">
                    <div class="kv-avatar">
                        <div class="form-group">
                        <label for="costo">Imagen de Descripción <small>(Opcional)</small></label>
                            <div class="file-loading">

                                {!!Form::file('description_image',['id'=>'avatar-3'])!!}
                            </div>
                            <span class="label label-danger">{{$errors->first('description_image') }}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 rutas">
                <label for="duration">Texto de imágenes  (opcional)</label>
                <div class="form-group">
                    {!! Form::textarea('text_images',null,['class'=>'ckeditor']) !!}
                    <span class="label label-danger">{{$errors->first('text_images') }}</span>
                </div>
            </div>
            <div class="col-sm-12 rutas">
                <div class="form-group">
                    <label for="costo">Titulo de descripción extra (Opcional)</label>
                    {!!Form::text('title_extra_description', null, ['class'=>'form-control', 'placeholder' => 'Inserte url de video en de youtube', 'required'])!!}
                    <span class="label label-danger">{{$errors->first('title_extra_description') }}</span>
                </div>
            </div>
            <div class="col-sm-12 rutas">
                <label for="duration">descripción extra (opcional)</label>
                <div class="form-group">
                    {!! Form::textarea('extra_description',null,['class'=>'ckeditor']) !!}
                    <span class="label label-danger">{{$errors->first('extra_description') }}</span>
                </div>
            </div>
            <div class="form-group form-actions" align="center">
                <button type="submit" class="btn btn-effect-ripple btn-primary">Guardar</button>
                <a href="{{url('admin/departments')}}" class="btn btn-effect-ripple btn-danger">Cancelar</a>
            </div>
            {!!Form::close()!!}
        </div>
</div>
@endsection

@section('aditionalScript')

    <script src="{{asset('auth-panel/js/plugins/ckeditor/ckeditor.js')}}"></script>

    <script src="{{asset('auth-panel/js/pages/formsComponents.js')}}"></script>
    <script>$(function(){ FormsComponents.init(); });</script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/plugins/sortable.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/fileinput.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/locales/fr.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/locales/es.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/themes/explorer-fa/theme.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/themes/fa/theme.js')}}" type="text/javascript"></script>

    <script>
    
    $("#avatar-1").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        browseLabel: '',
        removeLabel: '',
        browseIcon: '<i class="fab fa-searchengin"></i>',
        removeIcon: '<i class="far fa-times-circle"></i>',
        viewIcon:'<i class="far fa-times-circle"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        @if(isset($department))
        defaultPreviewContent: '<img src="{{asset($department->link_image)}}" alt="Avatar por defecto" width="50%">',
        @else
        defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Avatar por defecto" width="50%">',
        @endif
        layoutTemplates: {main2: '{preview} ' + ' {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "gif"]
    });

    @if(!isset($department))
        $("#department_name").val('');
    @endif

    var controladorTiempo = "";
            var markers = [];
           function initMap() {
                @if(isset($department))
                var map = new google.maps.Map(document.getElementById('map'), {
                  zoom: 7,
                  center: {lat: 4.570868, lng: -74.29733299999998}
                });
                var geocoder = new google.maps.Geocoder();
                $("#department_name").on("keyup", function() {
                    clearTimeout(controladorTiempo);
                    controladorTiempo = setTimeout(geocodeAddress(geocoder, map), 2000);
                });

                var myLatlng = new google.maps.LatLng({{$department->latitude}},{{$department->longitude}});
                var marker = new google.maps.Marker({
                    position: myLatlng,
                    title:"Hello World!"
                });

                // To add the marker to the map, call setMap();
                marker.setMap(map);
                @else

                 var map = new google.maps.Map(document.getElementById('map'), {
                  zoom: 7,
                  center: {lat: 4.570868, lng: -74.29733299999998}
                });
                var geocoder = new google.maps.Geocoder();
                $("#department_name").on("keyup", function() {
                    clearTimeout(controladorTiempo);
                    controladorTiempo = setTimeout(geocodeAddress(geocoder, map), 2000);
                });

                @endif

                    
              }

        function geocodeAddress(geocoder, resultsMap) 
        {
            var address = document.getElementById('department_name').value+",Colombia" ;
            $('#errors').empty();
            clearMarkers();
            if(address!= "")
            {
                geocoder.geocode({'address': address}, function(results, status) {
                  if (status === 'OK') {
                    clearMarkers();
                    resultsMap.setCenter(results[0].geometry.location);
                    var marker = new google.maps.Marker({
                      map: resultsMap,
                      position: results[0].geometry.location
                    });
                     markers.push(marker);
                    $('#errors').empty();

                    $("#latitude").val(results[0].geometry.location.lat());
                    $("#longitude").val(results[0].geometry.location.lng());
                    console.log(results[0].geometry.location.lat() + " " + results[0].geometry.location.lng());
                  } else {
                    $('#errors').empty();
                     $("#latitude").val('');
                    $("#longitude").val('');
                    $('#errors').append('No se puede encontrar su localización, intentelo nuevamente');
                  }
                });
            }
            
          }
          // Sets the map on all markers in the array.
      function setMapOnAll(map) {
        for (var i = 0; i < markers.length; i++) {
          markers[i].setMap(map);
        }
      }

      // Removes the markers from the map, but keeps them in the array.
      function clearMarkers() {
        setMapOnAll(null);
      }

      // Shows any markers currently in the array.
      function showMarkers() {
        setMapOnAll(map);
      }

      // Deletes all markers in the array by removing references to them.
      function deleteMarkers() {
        clearMarkers();
        markers = [];
      }

      $("#avatar-3").fileinput({
            overwriteInitial: true,
            maxFileSize: 1500,
            showClose: false,
            showCaption: false,
            browseLabel: '',
            removeLabel: '',
            browseIcon: '<i class="fab fa-searchengin"></i>',
            removeIcon: '<i class="far fa-times-circle"></i>',
            viewIcon:'<i class="far fa-times-circle"></i>',
            removeTitle: 'Cancel or reset changes',
            elErrorContainer: '#kv-avatar-errors-1',
            msgErrorClass: 'alert alert-block alert-danger',
            @if(isset($department))
            defaultPreviewContent: '<img src="{{asset($department->description_image)}}" alt="Imagen de descripción" width="50%">',
                @if($department->description_image)
                defaultPreviewContent: '<img src="{{asset($department->description_image)}}" alt="Imagen de descripción" width="50%">',
                @else
                defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Imagen de descripción" width="50%">',
                @endif
            @else
            defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Imagen de descripción" width="50%">',
            @endif
            layoutTemplates: {main2: '{preview} ' + ' {remove} {browse}'},
            allowedFileExtensions: ["jpg", "png", "gif"]
        });
        </script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBXtArR0GzW4Y_EezVpjGdvjrlwsQBcayw&callback=initMap"
        async defer></script>
</script>
@endsection