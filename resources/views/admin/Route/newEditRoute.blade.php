@extends('admin.layout.auth')

@if(isset($route))
    @section('title', 'Editar ruta')
@else
    @section('title', 'Crear ruta ')
@endif

@section('aditionalStyle')
    <link href="{{asset('plugins/kartik-v-bootstrap-fileinput/css/fileinput.css')}}" media="all" rel="stylesheet" type="text/css"/>
    <style>
.kv-avatar .krajee-default.file-preview-frame,.kv-avatar .krajee-default.file-preview-frame:hover {
    margin: 0;
    padding: 0;
    border: none;
    box-shadow: none;
    text-align: center;
}
.kv-avatar {
    display: inline-block;
}
.kv-avatar .file-input {
    display: table-cell;
    width: 213px;
}
.kv-reqd {
    color: red;
    font-family: monospace;
    font-weight: normal;
}
</style>
@endsection

@section('content')

<div id="page-content">
         <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>Ruta</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="block">
            <!-- Labels on top Form Title -->
            <div class="block-title">
                <h2>Ingrese la información solicitada</h2>
            </div>
            
            @if(isset($route))
            {!!Form::model($route,['url'=>['admin/routes/update',$route->id_route],'method'=>'POST', 'class'=> 'form-horizontal', 'enctype' => 'multipart/form-data', 'id' => 'userForm', 'novalidate'])!!}
            @else
            {!!Form::open(['url'=>'admin/routes/store', 'method'=>'POST', 'class'=> 'form-horizontal nuevo', 'enctype' => 'multipart/form-data', 'id' => 'userForm', 'novalidate'])!!}
            @endif
             <div class="col-sm-12">
                <div class="col-sm-4 text-center col-sm-offset-2">
                    <div class="kv-avatar">
                        <div class="form-group">
                        <label for="costo">Imagen de tarjeta <small>(Opcional)</small></label>
                            <div class="file-loading">

                                {!!Form::file('card_image',['id'=>'avatar-3'])!!}
                            </div>
                            <span class="label label-danger">{{$errors->first('card_image') }}</span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 text-center">
                    <div class="kv-avatar">
                        <div class="form-group">
                        <label for="costo">Imagen de introducción <small>(Opcional)</small></label>
                            <div class="file-loading">

                                {!!Form::file('introduction_image',['id'=>'avatar-4'])!!}
                            </div>
                            <span class="label label-danger">{{$errors->first('introduction_image') }}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="costo">Nombre</label>
                    {!!Form::text('name', null, ['class'=>'form-control', 'placeholder' => 'Inserte nombre de ruta', 'required'])!!}
                    <span class="label label-danger">{{$errors->first('name') }}</span>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="costo">Descripción corta</label>
                    {!!Form::text('sentence', null, ['class'=>'form-control', 'placeholder' => 'Inserte descripción corta', 'required'])!!}
                    <span class="label label-danger">{{$errors->first('sentence') }}</span>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="costo">Archivo de descarga <small>(Opcional)</small></label>
                        {!!Form::file('file_map', null, ['class'=>'form-control', 'id'=>'file_map'])!!}
                        <span class="label label-danger">{{$errors->first('file_map') }}</span>
                    </div>
                </div>
                @if(isset($route))
                <div class="col-md-4">
                    @if($route->file_map)
                    <div class="form-group">
                        <label href="{{url($route->file_map)}}" target="_blank"> <i class="fas fa-file"></i>   Ver archivo cargado recientemente  </label>
                    </div>
                    @endif
                </div>
                @endif
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                <label >Palabras clave <small>Separadas por comas (,)</small></label>
                    {!!Form::text('keywords',null,['class'=>'form-control input-tags', 'placeholder'=>'Palabras clave'])!!}
                    <span class="label label-danger">{{$errors->first('keywords') }}</span>
                </div>
            </div>
            <div class="col-sm-12 form-group">
                <label for="duration">Descripción</label>
                <div class="form-group">
                    {!! Form::textarea('description',null,['class'=>'ckeditor']) !!}
                    <span class="label label-danger">{{$errors->first('description') }}</span>
                </div>
            </div>
            <div class="col-sm-12 rutas">
                <div class="col-sm-4 text-center col-sm-offset-4">
                    <div class="kv-avatar">
                        <div class="form-group">
                        <label for="costo">Imagen de descripción <small>(Opcional)</small></label>
                            <div class="file-loading">

                                {!!Form::file('description_image',['id'=>'avatar-1'])!!}
                            </div>
                            <span class="label label-danger">{{$errors->first('description_image') }}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 form-group">
                <label for="duration">Recomendaciones</label>
                <div class="form-group">
                    {!! Form::textarea('recommendation',null,['class'=>'ckeditor']) !!}
                    <span class="label label-danger">{{$errors->first('recommendation') }}</span>
                </div>
            </div>
            <div class="col-sm-12 rutas">
                <div class="col-sm-4 text-center col-sm-offset-4">
                    <div class="kv-avatar">
                        <div class="form-group">
                        <label for="costo">Imagen de recomendaciones <small>(Opcional)</small></label>
                            <div class="file-loading">

                                {!!Form::file('recommendation_image',['id'=>'avatar-2'])!!}
                            </div>
                            <span class="label label-danger">{{$errors->first('recommendation_image') }}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 form-group">
                <label for="duration">Texto de imágenes</label>
                <div class="form-group">
                    {!! Form::textarea('images',null,['class'=>'ckeditor']) !!}
                    <span class="label label-danger">{{$errors->first('images') }}</span>
                </div>
            </div>
            <div class="form-group form-actions" align="center">
                <button type="submit" class="btn btn-effect-ripple btn-primary">Guardar</button>
                <a href="{{url('admin/routes')}}" class="btn btn-effect-ripple btn-danger">Cancelar</a>
            </div>
            {!!Form::close()!!}
        </div>
</div>
@endsection

@section('aditionalScript')

    <script src="{{asset('auth-panel/js/plugins/ckeditor/ckeditor.js')}}"></script>

    <script src="{{asset('auth-panel/js/pages/formsComponents.js')}}"></script>
    <script>$(function(){ FormsComponents.init(); });</script>

     <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/plugins/sortable.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/fileinput.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/locales/fr.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/locales/es.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/themes/explorer-fa/theme.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/themes/fa/theme.js')}}" type="text/javascript"></script>

    <script>
        $("#avatar-1").fileinput({
            overwriteInitial: true,
            maxFileSize: 1500,
            showClose: false,
            showCaption: false,
            browseLabel: '',
            removeLabel: '',
            browseIcon: '<i class="fab fa-searchengin"></i>',
            removeIcon: '<i class="far fa-times-circle"></i>',
            viewIcon:'<i class="far fa-times-circle"></i>',
            removeTitle: 'Cancel or reset changes',
            elErrorContainer: '#kv-avatar-errors-1',
            msgErrorClass: 'alert alert-block alert-danger',
            @if(isset($route))
            defaultPreviewContent: '<img src="{{asset($route->description_image)}}" alt="Imagen de descripción" width="50%">',
                @if($route->description_image)
                defaultPreviewContent: '<img src="{{asset($route->description_image)}}" alt="Imagen de descripción" width="50%">',
                @else
                defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Imagen de descripción" width="50%">',
                @endif
            @else
            defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Imagen de descripción" width="50%">',
            @endif
            layoutTemplates: {main2: '{preview} ' + ' {remove} {browse}'},
            allowedFileExtensions: ["jpg", "png", "gif"]
        });
        $("#avatar-2").fileinput({
            overwriteInitial: true,
            maxFileSize: 1500,
            showClose: false,
            showCaption: false,
            browseLabel: '',
            removeLabel: '',
            browseIcon: '<i class="fab fa-searchengin"></i>',
            removeIcon: '<i class="far fa-times-circle"></i>',
            viewIcon:'<i class="far fa-times-circle"></i>',
            removeTitle: 'Cancel or reset changes',
            elErrorContainer: '#kv-avatar-errors-1',
            msgErrorClass: 'alert alert-block alert-danger',
            @if(isset($route))
            defaultPreviewContent: '<img src="{{asset($route->recommendation_image)}}" alt="Imagen de recomendación" width="50%">',
                @if($route->recommendation_image)
                defaultPreviewContent: '<img src="{{asset($route->recommendation_image)}}" alt="Imagen de recomendación" width="50%">',
                @else
                defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Imagen de recomendación" width="50%">',
                @endif
            @else
            defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Imagen de recomendación" width="50%">',
            @endif
            layoutTemplates: {main2: '{preview} ' + ' {remove} {browse}'},
            allowedFileExtensions: ["jpg", "png", "gif"]
        });
        $("#avatar-3").fileinput({
            overwriteInitial: true,
            maxFileSize: 1500,
            showClose: false,
            showCaption: false,
            browseLabel: '',
            removeLabel: '',
            browseIcon: '<i class="fab fa-searchengin"></i>',
            removeIcon: '<i class="far fa-times-circle"></i>',
            viewIcon:'<i class="far fa-times-circle"></i>',
            removeTitle: 'Cancel or reset changes',
            elErrorContainer: '#kv-avatar-errors-1',
            msgErrorClass: 'alert alert-block alert-danger',
            @if(isset($route))
            defaultPreviewContent: '<img src="{{asset($route->card_image)}}" alt="Imagen de tarjeta" width="50%">',
                @if($route->card_image)
                defaultPreviewContent: '<img src="{{asset($route->card_image)}}" alt="Imagen de tarjeta" width="50%">',
                @else
                defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Imagen de tarjeta" width="50%">',
                @endif
            @else
            defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Imagen de tarjeta" width="50%">',
            @endif
            layoutTemplates: {main2: '{preview} ' + ' {remove} {browse}'},
            allowedFileExtensions: ["jpg", "png", "gif"]
        });
          $("#avatar-4").fileinput({
            overwriteInitial: true,
            maxFileSize: 1500,
            showClose: false,
            showCaption: false,
            browseLabel: '',
            removeLabel: '',
            browseIcon: '<i class="fab fa-searchengin"></i>',
            removeIcon: '<i class="far fa-times-circle"></i>',
            viewIcon:'<i class="far fa-times-circle"></i>',
            removeTitle: 'Cancel or reset changes',
            elErrorContainer: '#kv-avatar-errors-1',
            msgErrorClass: 'alert alert-block alert-danger',
            @if(isset($route))
                @if($route->introduction_image)
                defaultPreviewContent: '<img src="{{asset($route->introduction_image)}}" alt="Imagen de introducción" width="50%">',
                @else
                defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Imagen de introducción" width="50%">',
                @endif
            @else
            defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Imagen de introducción" width="50%">',
            @endif
            layoutTemplates: {main2: '{preview} ' + ' {remove} {browse}'},
            allowedFileExtensions: ["jpg", "png", "gif"]
        });
        
    </script>
@endsection
