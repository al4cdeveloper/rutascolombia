@extends('admin.layout.auth')

@section('title', 'Listado de puntos')


@section('content')
    <div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-12">
                    <div class="header-section">
                        <h1>Puntos de la ruta {{$route->name}}</h1>
                        <div class="pull-right" style="padding: 5px;"><a href="{{url('admin/routes')}}" class="btn btn-primary btn-sm">Volver al lista de rutas</a></div>
                    </div>
                </div>
            </div>
        </div>
            <div class="block full">
                <div class="block-title">
                    <h2>Items</h2>
                    <div class="pull-right" style="padding: 5px;">
                        <a href="{{url('admin/routes/points/new/'.$route->slug)}}" class="btn btn-primary btn-sm"><i class="far fa-plus-square fa-lg"></i></a>
                    </div>
                </div>
                <div class="table-responsive">
                    <table id="example-datatable" class="table table-striped table-bordered table-vcenter">
                        <thead>
                            <tr>
                                <th style="width: 120px;">Kilometro</th>
                                <th class="text-center">Titulo</th>
                                <th>Lugar relacionado</th>
                                <th class="text-center" style="width: 75px;"><i class="fas fa-sync-alt"></i></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($route->points as $item)
                                <tr>
                                    <td>
                                        {{$item->kilometer}} km
                                    </td>
                                    <td>{{$item->title}}</td>
                                    <td>{{$item->type_relation == 'municipality' ? $item->municipality->municipality_name : $item->site->site_name}}</td>
                                    <td>
                                        <a href="{{url('admin/routes/points/edit/'.$item->id_point)}}" data-toggle="tooltip" title="Editar punto" class="btn btn-effect-ripple btn-xs btn-warning"><i class="fa fa-edit"></i></a>
                                        
                                        <a href="{{url('admin/routes/points/delete/'.$item->id_point)}}" data-toggle="tooltip" title="Eliminar item" class="btn btn-effect-ripple btn-xs btn-danger"><i class="fa fa-times"></i></a></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
@endsection

@section('aditionalScript')
<!-- Load and execute javascript code used only in this page -->
        <script src="{{asset('auth-panel/js/pages/uiTables.js')}}"></script>
        <script>$(function(){ UiTables.init(); });</script>
@endsection