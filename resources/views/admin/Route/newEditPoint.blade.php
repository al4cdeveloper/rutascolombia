@extends('admin.layout.auth')

@if(isset($point))
    @section('title', 'Editar punto')
@else
    @section('title', 'Crear punto')
@endif

@section('additionalStyle')
    <link rel="stylesheet" href="{{asset('plugins/dist/sweetalert2.min.css')}}">
@endsection

@section('content')
<div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-12">
                    <div class="header-section">
                        <h1>Ruta de {{$route->name}}</h1>
                        <div class="pull-right" style="padding: 5px;"><a href="{{url('admin/routes/points/'.$route->slug)}}" class="btn btn-primary btn-sm">Volver a lista de rutas</a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="block">
            <!-- Labels on top Form Title -->
            <div class="block-title">
                <h2>Ingrese la información solicitada</h2>
            </div>
			
            @if(isset($point))
            {!!Form::model($point,['url'=>['admin/routes/points/update',$point->id_point],'method'=>'POST', 'class'=> 'form-horizontal', 'enctype' => 'multipart/form-data', 'id' => 'userForm', 'novalidate'])!!}
            @else
            {!!Form::open(['url'=>"admin/routes/points/store/$route->slug", 'method'=>'POST', 'class'=> 'form-horizontal nuevo', 'enctype' => 'multipart/form-data', 'id' => 'userForm', 'novalidate'])!!}
            @endif
            <div class="col-sm-12">
                <div class="col-sm-7">
                    <div class="form-group">
                        <label for="costo">Titulo</label>
                        {!!Form::text('title', null, ['class'=>'form-control', 'placeholder' => 'Inserte titulo', 'required'])!!}
                        <span class="label label-danger">{{$errors->first('title') }}</span>
                    </div>
                </div>
                <div class="col-sm-4 col-sm-offset-1">
                    <div class="form-group">
                        <label for="costo">Kilometro</label>
                        {!!Form::number('kilometer', null, ['class'=>'form-control', 'placeholder' => 'ej: 7,2', 'required'])!!}
                        <span class="label label-danger">{{$errors->first('kilometer') }}</span>
                    </div>
                </div>
            </div>
            <input type="hidden" name="type_relation" id="type_relation" @if(isset($point)) value="{{$point->type_relation}}" @endif>
            <div class="col-sm-12" id="departmentdiv" >
                <div class="col-md-12">
                <h4 class="col-md-4 ">Lugar o municipio relacionado</h4>
                <div class="col-md-4 pull-right">
                    <a href="#modal-large" class="btn btn-effect-ripple btn-default" data-toggle="modal">Crear Sitio de interés</a>
                </div>
                <!-- Large Modal -->
                <div id="modal-large" class="modal" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h3 class="modal-title"><strong>Crear sitio de interes</strong></h3>
                            </div>
                            <div class="modal-body">
                                <div class="form-group col-sm-12">
                                    <div class="col-sm-5 col-xs-12">
                                        <div class="form-group">
                                            <label for="costo">Categoria de sitio de interés *</label>
                                            {!!Form::select('fk_category', $categories, null, ['class'=>'select-chosen','id'=>'category'])!!}
                                        </div>
                                    </div>
                                    <div class="col-sm-5 col-sm-offset-1 col-xs-12">
                                        <div class="form-group">
                                            <label for="costo">Actividades <small>opcional</small></label>
                                            <select name="activities[]" id="activities" class="select-chosen" multiple="">
                                                    @foreach($activities as  $key=> $activity)
                                                        <option value="{{$key}}" 
                                                        @if(isset($site)) @foreach($site->Activities as $activityPivot) 
                                                        @if($activityPivot->id_activity == $key) 
                                                        selected @endif @endforeach @endif>{{$activity}}</option>
                                                    @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-sm-12">
                                    <div class="col-sm-5 col-xs-12">
                                        <div class="form-group">
                                            <label for="costo">Municipio al que pertenece *</label>
                                            {!!Form::select('fk_municipality', $municipalities, null, ['class'=>'select-chosen','id'=>'municipalitySite'])!!}
                                        </div>
                                    </div>
                                    <div class="col-sm-5 col-sm-offset-1 col-xs-12">
                                        <div class="form-group">
                                            <label for="costo">Nombre *</label>
                                            {!!Form::text('site_name', null, ['class'=>'form-control', 'placeholder' => 'Inserte nombre del sitio de interés', 'required','id'=>'site_name'])!!}
                                            <span class="label label-danger">{{$errors->first('site_name') }}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-sm-12">
                                    <div class="col-sm-5 col-xs-12">
                                        <div class="form-group">
                                            <label for="costo">Latitud *</label>
                                            {!!Form::text('latitude', null, ['class'=>'form-control', 'placeholder' => 'Ingrese latitud', 'id'=>'latitude','required'])!!}
                                            <span class="label label-danger">{{$errors->first('latitude') }}</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-5 col-sm-offset-1 col-xs-12">
                                        <div class="form-group">
                                            <label for="costo">Longitud *</label>
                                            {!!Form::text('longitude', null, ['class'=>'form-control', 'placeholder' => 'Ingrese longitud','id'=>'longitude' ,'required'])!!}
                                            <span class="label label-danger">{{$errors->first('longitude') }}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 form-group" id="description">
                                    <label for="duration">Descripción</label>
                                    <div class="form-group">
                                        {!! Form::textarea('description',null,['class'=>'ckeditor']) !!}
                                        <span class="label label-danger">{{$errors->first('description') }}</span>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <div class="modal-footer">
                                <button type="button" onclick="register()" class="btn btn-effect-ripple btn-primary">Registrar</button>
                                <button type="button" class="btn btn-effect-ripple btn-danger" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END Large Modal -->
                </div>
                @if(!isset($point))
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="location">Departamento</label>
                        {!!Form::select('departments', $departments, null, ['class'=>'select-chosen','onchange'=>'changeDepartment()','id'=>'relation_departaments'])!!}
                    </div>
                </div>
                @else
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="location">Departamento</label>
                            <select name="departments" onchange="changeDepartment();" id="relation_departaments" class="select-chosen">
                                @foreach($departments as $id => $departmentF)
                                    <option value="{{$id}}" @if($id == $departmentStart->id_department) selected @endif>{{$departmentF}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                @endif
                <div class="col-sm-4 @if(!isset($point)) hidden @endif " id="municipalityDiv">
                    <div class="form-group">
                        <label for="location">Municipio</label>
                        <select name="fk_municipality" id="municipalities" onchange="changeMunicipality()" class="select-chosen">
                            @if(isset($point))
                                @if($point->type_relation=="municipality")
                                    @foreach($municipalitiesStart as $id => $municipalityF)
                                        <option value="{{$id}}" @if($point->fk_relation==$id) selected @endif>{{$municipalityF}}</option>    
                                    @endforeach
                                @else
                                    @foreach($municipalitiesStart as $id => $municipalityF)
                                        <option value="{{$id}}">{{$municipalityF}}</option>    
                                    @endforeach
                                @endif
                            @endif
                        </select>
                    </div>
                </div>
                <div class="col-sm-4" >
                    <div class="form-group @if(!isset($point)) hidden @endif" id="siteDiv">
                        <label for="location">Sitio de interés</label>
                        <select name="fk_site" id="interesSite" class="select-chosen">
                            @if(isset($point))
                                @if($point->type_relation=="site")
                                    <option></option>
                                    @foreach($sitesStart as $id => $site)
                                        <option value="{{$id}}" @if($point->fk_relation==$id) selected @endif>{{$site}}</option>    
                                    @endforeach
                                @else
                                    @if(count($sitesStart)>0)
                                        <option value=""></option>
                                        @foreach($sitesStart as $id => $site)
                                            <option value="{{$id}}" >{{$site}}</option>    
                                        @endforeach
                                    @endif
                                @endif
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 form-group">
                <label for="duration">Descripción</label>
                <div class="form-group">
                    {!! Form::textarea('description',null,['class'=>'ckeditor']) !!}
                    <span class="label label-danger">{{$errors->first('description') }}</span>
                </div>
            </div>
            <div class="form-group form-actions" align="center">
                <button type="submit" class="btn btn-effect-ripple btn-primary">Guardar</button>
                <a href="{{url('admin/routes')}}" class="btn btn-effect-ripple btn-danger">Cancelar</a>
            </div>
            {!!Form::close()!!}
        </div>
</div>
@endsection

@section('aditionalScript')

    <script src="{{asset('auth-panel/js/plugins/ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('plugins/sweetalert2/dist/sweetalert2.all.min.js')}}"></script>
    <script src="{{asset('auth-panel/js/pages/formsComponents.js')}}"></script>
    <script>$(function(){ FormsComponents.init(); });</script>

    <script>
        function changeDepartment()
        {
            $("#type_relation").val('municipality');
            $("#siteDiv").addClass('hidden');
            relationalDepartments = $('#relation_departaments').val();
            var token = "{{csrf_token()}}";
            var route="/admin/specials/routes/getreladepartment";            
            $.ajax({
                url: route,
                headers: {'X-CSRF-TOKEN': token},
                type: 'POST',
                dataType: 'json',
                data:{relationalDepartments},
                complete: function(transport)
                {
                    data = transport.responseJSON;
                    municipalities = data['municipalities'];
                    // //MUNICIPIOS
                    $("#municipalityDiv").removeClass('hidden');
                    $('#municipalities option').remove();
                    
                    $("#municipalities").append('<option></option>').trigger("chosen:updated");

                    for(x in municipalities)
                    {
                        $("#municipalities").append('<option value="'+municipalities[x].id_municipality+'" id="municipality'+municipalities[x].id_municipality+'">'+municipalities[x].municipality_name+'</option>').trigger("chosen:updated");
                    }
                }
              });
        };
        function changeMunicipality()
        {
            municipality = $('#municipalities').val();
            var token = "{{csrf_token()}}";
            var route="/admin/specials/routes/getrelamunicipality";            
            $.ajax({
                url: route,
                headers: {'X-CSRF-TOKEN': token},
                type: 'get',
                dataType: 'json',
                data:{municipality},
                complete: function(transport)
                {
                    data = transport.responseJSON;
                    interestsites = data['interestSites'];
                    if(interestsites.length >0)
                    {
                        $("#siteDiv").removeClass('hidden');
                        $('#interesSite option').remove();

                        for(x in interestsites)
                        {
                            $("#interesSite").append('<option value="'+interestsites[x].id_site+'">'+interestsites[x].site_name+'</option>').trigger("chosen:updated");
                        }
                    }
                    else{             
                        $("#siteDiv").addClass('hidden');
                    }
                }
              });
        };
        $("#interesSite").change(function()
        {
            if($(this).val()!='')
            {
                $("#type_relation").val('site');
            }
            else
            {
                $("#type_relation").val('municipality');
            }
        }); 

        function register()
        {
            activities = $('#activities').val();
            category = $('#category').val();
            municipality = $('#municipalitySite').val();
            site_name = $('#site_name').val();
            latitude = $('#latitude').val();
            longitude = $('#longitude').val();
            description = $('#siteDescription').val();
            if(category != '' && municipality != '' && site_name != '' && latitude != '' && longitude != '')
            {
                $.ajax({
                    url:'/admin/interestsite/register',
                    headers:{'X-CSRF-TOKEN':'{{csrf_token()}}'},
                    type:'POST',
                    data:{activities:activities,category:category,municipality:municipality,site_name:site_name,latitude:latitude,longitude:longitude,description:description},
                    complete:function(transport)
                    {
                        response = transport.responseJSON;
                        if(response.status == 'ok')
                        {
                            $('#relation_departaments').val(response.department.id_department).trigger("chosen:updated");
                            changeDepartment();
                        setTimeout(function() {
                            $('#municipalities').val(municipality).trigger("chosen:updated");
                            changeMunicipality();
                        }, 1000);
                            swal({
                                  type: 'success',
                                  title: 'Se ha registrado correctamente el sitio de interes.',
                                });
                            $('#modal-large').modal('hide');
                            $("#municipality"+municipality).attr('selected', 'selected');
                            $('#activities').val('');
                            $('#category').val('');
                            $('#municipalitySite').val('');
                            $('#site_name').val('');
                            $('#latitude').val('');
                            $('#longitude').val('');
                            $('#siteDescription').val('');
                        }
                        else
                        {
                            swal({
                              type: 'error',
                              title: 'Ha ocurrdio un error, por favor inténtelo nuevamente.',
                            });
                        }
                    }
                })
            }
            else
            {
                //swal('Error!','Por favor diligencia los campos marcados con *.','error');
                swal({
                  type: 'error',
                  title: 'Por favor diligencia los campos marcados con *.',
                });
            }
        }
    </script>

@endsection