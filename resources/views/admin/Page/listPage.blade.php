@extends('admin.layout.auth')

@section('title', 'Listado de páginas')


@section('content')
    <div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>Páginas</h1>
                    </div>
                </div>
            </div>
        </div>
            <div class="block full">
                <div class="block-title">
                    <h2>Páginas</h2>
                    <div class="pull-right" style="padding: 5px;"><a href="{{url('admin/pages/create')}}" class="btn btn-primary btn-sm"><i class="far fa-plus-square fa-lg"></i></a></div>
                </div>
                <div class="table-responsive">
                    <table id="example-datatable" class="table table-striped table-bordered table-vcenter">
                        <thead>
                            <tr>
                                <th class="text-center">Pagina</th>
                                <th>URL</th>
                                <th>Descripción</th>
                                <th>Secciones</th>
                                <th class="text-center" style="width: 75px;"><i class="fas fa-sync-alt"></i></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($pages as $page)
                            <tr>
                                <td>{{$page->page_name}}</td>
                                <td>{{$page->url}}</td>
                                <td>{{$page->description}}</td>
                                <td>
                                    <ul>
                                    @foreach($page->Parts as $part)
                                        <li>{{$part->part_name}}</li>
                                    @endforeach
                                    </ul>
                                </td>
                                <td class="text-center">

                                    <a href="{{url('admin/pages/edit/'.$page->slug)}}" data-toggle="tooltip" title="Editar pagina" class="btn btn-effect-ripple btn-xs btn-warning"><i class="fa fa-edit"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
    </div>
@endsection

@section('aditionalScript')
<!-- Load and execute javascript code used only in this page -->
        <script src="{{asset('auth-panel/js/pages/uiTables.js')}}"></script>
        <script>$(function(){ UiTables.init(); });</script>
@endsection