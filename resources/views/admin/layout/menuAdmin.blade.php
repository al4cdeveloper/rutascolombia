<div id="sidebar-scroll" style="margin-top: 15%">
    <!-- Sidebar Content -->
    <div class="sidebar-content sidebar-light">
        <!-- Sidebar Navigation -->
        <ul class="sidebar-nav">
            <li @if(Request::path()=="admin/home") class="active" @endif>
                <a href="{{url('admin/home')}}"><i class="gi gi-compass sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Dashboard</span></a>
            </li>
            {{-- <li class="sidebar-separator">
                <i class="fa fa-ellipsis-h"></i>
            </li> --}}
            <li @if(substr(Request::path(), 0,13)=="admin/regions") class="active" @endif>
                <a href="#" class="sidebar-nav-menu">
                    {{-- Flecha que indica select --}}
                    <i class="fa fa-chevron-left sidebar-nav-indicator sidebar-nav-mini-hide icon-mt"></i> 
                    {{-- Icono de la izquierda --}}
                    <i class="fas fa-globe sidebar-nav-icon" ></i>
                    <span class="sidebar-nav-mini-hide ">Regiones</span></a>
                <ul>
                    <li @if(Request::path()=="admin/regions") class="active" @endif>
                        <a href="{{url('admin/regions/')}}">Lista de regiones</a>
                    </li>
                    <li @if(Request::path()=="admin/regions/create") class="active" @endif>
                        <a href="{{url('admin/regions/create')}}">Crear región</a>
                    </li>
                </ul>
            </li>
            <li @if(substr(Request::path(), 0,17)=="admin/departments") class="active" @endif>
                <a href="#" class="sidebar-nav-menu">
                    {{-- Flecha que indica select --}}
                    <i class="fa fa-chevron-left sidebar-nav-indicator sidebar-nav-mini-hide icon-mt"></i> 
                    {{-- Icono de la izquierda --}}
                    <i class="fab fa-deviantart sidebar-nav-icon" ></i>
                    <span class="sidebar-nav-mini-hide">Departamentos</span></a>
                <ul>
                    <li @if(Request::path()=="admin/departments") class="active" @endif>
                        <a href="{{url('admin/departments/')}}">Lista de departamentos</a>
                    </li>
                    <li @if(Request::path()=="admin/departments/create") class="active" @endif>
                        <a href="{{url('admin/departments/create')}}">Crear departamento</a>
                    </li>
                </ul>
            </li>
            <li @if(substr(Request::path(), 0,14)=="admin/whattodo") class="active" @endif>
                <a href="{{url('admin/whattodo')}}"><i class="fas fa-binoculars sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">¿Que hacer? </span></a>
            </li>

            <li @if(substr(Request::path(), 0,20)=="admin/municipalities") class="active" @endif>
                <a href="#" class="sidebar-nav-menu">
                    {{-- Flecha que indica select --}}
                    <i class="fa fa-chevron-left sidebar-nav-indicator sidebar-nav-mini-hide icon-mt"></i> 
                    {{-- Icono de la izquierda --}}
                    <i class="fab fa-cloudsmith sidebar-nav-icon"></i>
                    <span class="sidebar-nav-mini-hide">Municipios</span></a>
                <ul>
                    <li @if(Request::path()=="admin/municipalities") class="active" @endif>
                        <a href="{{url('admin/municipalities/')}}">Lista de municipios</a>
                    </li>
                    <li @if(Request::path()=="admin/municipalities/create") class="active" @endif>
                        <a href="{{url('admin/municipalities/create')}}">Crear municipio</a>
                    </li>
                </ul>
            </li>
            <li @if(substr(Request::path(), 0,19)=="admin/interestsites") class="active" @endif>
                <a href="#" class="sidebar-nav-menu">
                    {{-- Flecha que indica select --}}
                    <i class="fa fa-chevron-left sidebar-nav-indicator sidebar-nav-mini-hide icon-mt"></i> 
                    {{-- Icono de la izquierda --}}
                    <i class="fab fa-connectdevelop sidebar-nav-icon"></i>
                    <span class="sidebar-nav-mini-hide">Sitios de interés</span></a>
                <ul>
                    <li @if(Request::path()=="admin/interestsites/category") class="active" @endif>
                        <a href="{{url('admin/interestsites/category')}}">Categorias</a>
                    </li>
                    <li @if(Request::path()=="admin/interestsites") class="active" @endif>
                        <a href="{{url('admin/interestsites/')}}">Sitios de interes</a>
                    </li>
                </ul>
            </li>
            
            <!--<li @if(substr(Request::path(), 0,19)=="admin/interestsites") class="active" @endif>
                <a href="#" class="sidebar-nav-menu">
                    {{-- Flecha que indica select --}}
                    <i class="fa fa-chevron-left sidebar-nav-indicator sidebar-nav-mini-hide icon-mt"></i> 
                    {{-- Icono de la izquierda --}}
                    <i class="fab fa-connectdevelop"></i>
                    <span class="sidebar-nav-mini-hide">Sitios de interés</span></a>
                <ul>
                    <li @if(Request::path()=="admin/interestsites/category") class="active" @endif>
                        <a href="{{url('admin/interestsites/category')}}">Categorias</a>
                    </li>
                    <li @if(Request::path()=="admin/interestsites") class="active" @endif>
                        <a href="{{url('admin/interestsites/')}}">Sitios de interes</a>
                    </li>
                </ul>
            </li>-->
            <li @if(substr(Request::path(), 0,14)=="admin/circuits") class="active" @endif>
                <a href="{{url('admin/circuits')}}"><i class="fa fa-subway sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Circuitos</span></a>
            </li>
            <li @if(substr(Request::path(), 0,12)=="admin/routes") class="active" @endif>
                <a href="{{url('admin/routes')}}"><i class="fa fa-road sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Rutas</span></a>
            </li>
            <li @if(substr(Request::path(), 0,11)=="admin/fairs") class="active" @endif>
                <a href="{{url('admin/fairs')}}"><i class="fab fa-first-order sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Ferias y fiestas</span></a>
            </li>
            <li @if(substr(Request::path(), 0,17)=="admin/concessions") class="active" @endif>
                <a href="{{url('admin/concessions')}}"><i class="fas fa-store-alt sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Concesiones</span></a>
            </li>
            <li class="sidebar-separator">
                <i class="fas fa-ellipsis-h"></i>
            </li>
            <li @if(substr(Request::path(), 0,14)=="admin/specials") class="active" @endif>
                <a href="{{url('admin/specials')}}"><i class="far fa-star sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Especiales</span></a>
            </li>
            <li class="sidebar-separator">
                <i class="fas fa-ellipsis-h"></i>
            </li>
            <li @if(substr(Request::path(), 0,25)=="admin/establishment/hotel") class="active" @endif>    
                <a href="{{url('admin/establishment/hotel')}}"><i class="fa fa-h-square sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Hoteles</span></a>
            </li>
            <li @if(substr(Request::path(), 0,30)=="admin/establishment/restaurant") class="active" @endif>
                <a href="{{url('admin/establishment/restaurant')}}"><i class="fas fa-utensils sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Restaurantes</span></a>
            </li>
            <li class="sidebar-separator">
                <i class="fas fa-ellipsis-h"></i>
            </li>
            <li @if(substr(Request::path(), 0,16)=="admin/activities") class="active" @endif>
                <a href="#" class="sidebar-nav-menu">
                    {{-- Flecha que indica select --}}
                    <i class="fa fa-chevron-left sidebar-nav-indicator sidebar-nav-mini-hide icon-mt"></i> 
                    {{-- Icono de la izquierda --}}
                    <i class="fas fa-leaf sidebar-nav-icon"></i>
                    <span class="sidebar-nav-mini-hide">Actividades operador</span></a>
                <ul>
                    <li @if(Request::path()=="admin/activities/servicecategory") class="active" @endif>
                        <a href="{{url('admin/activities/servicecategory')}}">Categoria</a>
                    </li>
                    <li @if(Request::path()=="admin/activities/service") class="active" @endif>
                        <a href="{{url('admin/activities/service')}}">Servicios</a>
                    </li>
                </ul>
            </li>
            <li class="sidebar-separator">
                <i class="fa fa-ellipsis-h"></i>
            </li>

            <li @if(substr(Request::path(), 0,15)=="admin/pageparts") class="active" @endif>
                <a href="{{url('admin/pageparts')}}"><i class="fas fa-puzzle-piece sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Secciones de página</span></a>
            </li>
            <li @if(substr(Request::path(), 0,11)=="admin/pages") class="active" @endif>
                <a href="{{url('admin/pages')}}"><i class="far fa-file-alt sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Páginas</span></a>
            </li>
            <li @if(substr(Request::path(), 0,15)=="admin/customers") class="active" @endif>
                <a href="{{url('admin/customers')}}"><i class="far fa-address-book sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Clientes</span></a>
            </li>
            <li @if(substr(Request::path(), 0,15)=="admin/patterns") class="active" @endif>
                <a href="{{url('admin/patterns')}}"><i class="fab fa-hubspot sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Publicidad</span></a>
            </li>

            <li class="sidebar-separator">
                <i class="fas fa-ellipsis-h"></i>
            </li> 
            <li @if(substr(Request::path(), 0,16)=="admin/wallpapers") class="active" @endif>
                <a href="{{url('admin/wallpapers')}}"><i class="fab fa-mix sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Wallpapers inicio</span></a>
            </li>
            <li class="sidebar-separator">
                <i class="fas fa-ellipsis-h"></i>
            </li> 
            <li @if(substr(Request::path(), 0,15)=="admin/operators") class="active" @endif>
                <a href="{{url('admin/operators')}}"><i class="fas fa-id-card sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Operadores</span></a>
            </li>
            <li @if(substr(Request::path(), 0,11)=="admin/users") class="active" @endif>
                <a href="{{url('admin/users')}}"><i class="fas fa-users sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Usuarios</span></a>
            </li>
            {{-- 
            <li class="sidebar-separator">
                <i class="fas fa-ellipsis-h"></i>
            </li>
            <li @if(substr(Request::path(), 0,13)=="admin/contact") class="active" @endif>
                <a href="{{url('admin/contact')}}"><i class="fas fa-comment-alt sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Contacto <STRONG>FALTAAA!!!</STRONG></span></a>
            </li>

            
            <li @if(substr(Request::path(), 0,14)=="admin/circuits") class="active" @endif>
                <a href="{{url('admin/services')}}"><i class="fa fa-subway sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Servicios</span></a>
            </li> --}}
        </ul>
        <!-- END Sidebar Navigation -->
    </div>
    <!-- END Sidebar Content -->
</div>
