@extends('admin.layout.auth')

@if(isset($activity))
    @section('title', 'Actualizar categoría de actividades')
@else
    @section('title', 'Crear categoría de actividades')
@endif

@section('aditionalStyle')
    <link href="{{asset('plugins/kartik-v-bootstrap-fileinput/css/fileinput.css')}}" media="all" rel="stylesheet" type="text/css"/>
    <style>
.kv-avatar .krajee-default.file-preview-frame,.kv-avatar .krajee-default.file-preview-frame:hover {
    margin: 0;
    padding: 0;
    border: none;
    box-shadow: none;
    text-align: center;
}
.kv-avatar {
    display: inline-block;
}
.kv-avatar .file-input {
    display: table-cell;
    width: 213px;
}
.kv-reqd {
    color: red;
    font-family: monospace;
    font-weight: normal;
}
</style>

<link rel="stylesheet" href="{{asset('front/css/fontello/css/all-fontello.min.css')}}">
@endsection

@section('content')
<div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>Categoría de Actividades</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="block">
            <!-- Labels on top Form Title -->
            <div class="block-title">
                <h2>Ingrese la información solicitada</h2>
            </div>
            @if(isset($activity))
			{!!Form::model($activity,['url'=>['admin/whattodo/update',$activity->id_activity],'method'=>'POST', 'class'=> 'form-horizontal', 'enctype' => 'multipart/form-data', 'id' => 'userForm', 'novalidate'])!!}
			@else
			{!!Form::open(['url'=>'admin/whattodo/store', 'method'=>'POST', 'class'=> 'form-horizontal nuevo', 'enctype' => 'multipart/form-data', 'id' => 'userForm', 'novalidate'])!!}
			@endif
			<div class="col-sm-12">
                <div class="form-group">
                    <label>Nombre de categoria</label>
                    {!!Form::text('activity_name', null, ['class'=>'form-control', 'placeholder' => 'Inserte nombre de categoría', 'required'])!!}
                    <span class="label label-danger">{{$errors->first('activity_name') }}</span>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Titulo de página</label>
                    {!!Form::text('title', null, ['class'=>'form-control', 'placeholder' => 'Inserte nombre de categoría', 'required'])!!}
                    <span class="label label-danger">{{$errors->first('title') }}</span>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="col-md-8">
                    <div class="form-group">
                    <label >Palabras clave <small>Separadas por comas (,)</small></label>
                        {!!Form::text('keywords',null,['class'=>'form-control input-tags', 'placeholder'=>'Palabras clave'])!!}
                        <span class="label label-danger">{{$errors->first('keywords') }}</span>
                    </div>
                </div>
                <div class="col-sm-2 col-sm-offset-1">
                    <div class="form-group">
                        <!--<li class="dropdown" style="list-style: none">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    Icono
                                </a>

                                <ul class="dropdown-menu" role="menu" style="min-width: 130px!important;">
                                    <li>
                                        @foreach($icons as $icon)
                                        <ul> <button type="button" onclick="seleccion('{{$icon}}')" style="background: none; border: none;font-size: 30px;"><i class="{{$icon}}"></i></button></ul>
                                        @endforeach
                                    </li>
                                </ul>
                                <div id="result" style="    font-size: 25px;">
                                    
                                </div>
                            </li>-->
                        <label >Clase de icono <small>Font awesome</small></label>
                        {!!Form::text('icon_class',null,['class'=>'form-control', 'placeholder'=>'clase'])!!}
                        <span class="label label-danger">{{$errors->first('icon_class') }}</span>
                    </div>
                </div>
            </div>

            <div class="col-sm-12 form-group">
                <label for="duration">Meta Descripción</label>
                <div class="form-group">
                    {!! Form::textarea('meta_description',null,['class'=>'ckeditor']) !!}
                    <span class="label label-danger">{{$errors->first('meta_description') }}</span>
                </div>
            </div>
            <div class="col-sm-12">
                 <div class="col-sm-4 text-center col-sm-offset-4">
                    <div class="kv-avatar">
                        <div class="form-group">
                        <label>Imágen de portada</label>
                            <div class="file-loading">

                                {!!Form::file('image_link',['id'=>'avatar-1'])!!}
                            </div>
                            <span class="label label-danger">{{$errors->first('image_link') }}</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group form-actions" align="center">
                <button type="submit" class="btn btn-effect-ripple btn-primary">Guardar</button>
                <a href="{{url('admin/whattodo')}}" class="btn btn-effect-ripple btn-danger">Cancelar</a>
            </div>
            {!!Form::close()!!}
        </div>
</div>
@endsection

@section('aditionalScript')

    <script src="{{asset('auth-panel/js/plugins/ckeditor/ckeditor.js')}}"></script>

    <script src="{{asset('auth-panel/js/pages/formsComponents.js')}}"></script>
    <script>$(function(){ FormsComponents.init(); });</script>

    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/plugins/sortable.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/fileinput.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/locales/fr.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/locales/es.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/themes/explorer-fa/theme.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/themes/fa/theme.js')}}" type="text/javascript"></script>

    <script>
         @if(isset($activity))
        jQuery(document).ready(function($) {
            $("#result").append('<i class="{{$activity->icon_class}}"></i>');
            
        });
        @endif
        function seleccion(variable)
        {
            $("#result").empty();
            $("#result").append('<i class="'+variable+'"></i>');
            $("#icon_class").val(variable);

        }

    $("#avatar-1").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        browseLabel: '',
        removeLabel: '',
        browseIcon: '<i class="fab fa-searchengin"></i>',
        removeIcon: '<i class="far fa-times-circle"></i>',
        viewIcon:'<i class="far fa-times-circle"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        @if(isset($activity))
        defaultPreviewContent: '<img src="{{asset($activity->image_link)}}" alt="Avatar por defecto" width="50%">',
        @else
        defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Avatar por defecto" width="50%">',
        @endif
        layoutTemplates: {main2: '{preview} ' + ' {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "gif"]
    });
</script>
@endsection