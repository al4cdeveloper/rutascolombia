@extends('admin.layout.auth')

@if(isset($toll))
    @section('title', 'Editar peaje')
@else
    @section('title', 'Crear peaje')
@endif

@section('additionalStyle')
    <link rel="stylesheet" href="{{asset('plugins/dist/sweetalert2.min.css')}}">
    <link href="{{asset('plugins/kartik-v-bootstrap-fileinput/css/fileinput.css')}}" media="all" rel="stylesheet" type="text/css"/>
    <style>
.kv-avatar .krajee-default.file-preview-frame,.kv-avatar .krajee-default.file-preview-frame:hover {
    margin: 0;
    padding: 0;
    border: none;
    box-shadow: none;
    text-align: center;
}
.kv-avatar {
    display: inline-block;
}
.kv-avatar .file-input {
    display: table-cell;
    width: 213px;
}
.kv-reqd {
    color: red;
    font-family: monospace;
    font-weight: normal;
}
</style>
@endsection

@section('content')
<div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-12">
                    <div class="header-section">
                        <h1>Peajes de la concesión {{$concession->name}}</h1>
                        <div class="pull-right" style="padding: 5px;"><a href="{{url('admin/concessions/tolls/'.$concession->slug)}}" class="btn btn-primary btn-sm">Volver a lista de peajes</a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="block">
            <!-- Labels on top Form Title -->
            <div class="block-title">
                <h2>Ingrese la información solicitada</h2>
            </div>
			
            @if(isset($toll))
            {!!Form::model($toll,['url'=>['admin/concessions/tolls/update',$toll->id_toll],'method'=>'POST', 'class'=> 'form-horizontal', 'enctype' => 'multipart/form-data', 'id' => 'userForm', 'novalidate'])!!}
            @else
            {!!Form::open(['url'=>"admin/concessions/tolls/store/$concession->slug", 'method'=>'POST', 'class'=> 'form-horizontal nuevo', 'enctype' => 'multipart/form-data', 'id' => 'userForm', 'novalidate'])!!}
            @endif
             <div class="col-sm-12">
                <div class="col-sm-4 text-center col-sm-offset-4">
                    <div class="kv-avatar">
                        <div class="form-group">
                        <label for="costo">Imagen de peaje</label>
                            <div class="file-loading">

                                {!!Form::file('link_image',['id'=>'avatar-1'])!!}
                            </div>
                            <span class="label label-danger">{{$errors->first('link_image') }}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="costo">Nombre</label>
                    {!!Form::text('name', null, ['class'=>'form-control', 'placeholder' => 'Inserte costo de categoria', 'required'])!!}
                    <span class="label label-danger">{{$errors->first('name') }}</span>
                </div>
            </div>
            <div class="col-sm-12" id="departmentdiv" >
                <div class="col-md-12">
                <h4 class="col-md-4 ">Sitio de interés relacionado</h4>
                <div class="col-md-4 pull-right">
                    <a href="#modal-large" class="btn btn-effect-ripple btn-default" data-toggle="modal">Crear Sitio de interés</a>
                </div>
                <!-- Large Modal -->
                <div id="modal-large" class="modal" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h3 class="modal-title"><strong>Crear sitio de interes</strong></h3>
                            </div>
                            <div class="modal-body">
                                <div class="form-group col-sm-12">
                                    <div class="col-sm-5 col-xs-12">
                                        <div class="form-group">
                                            <label for="costo">Categoria de sitio de interés *</label>
                                            {!!Form::select('fk_category', $categories, null, ['class'=>'select-chosen','id'=>'category'])!!}
                                        </div>
                                    </div>
                                    <div class="col-sm-5 col-sm-offset-1 col-xs-12">
                                        <div class="form-group">
                                            <label for="costo">Actividades <small>opcional</small></label>
                                            <select name="activities[]" id="activities" class="select-chosen" multiple="">
                                                    @foreach($activities as  $key=> $activity)
                                                        <option value="{{$key}}" 
                                                        @if(isset($site)) @foreach($site->Activities as $activityPivot) 
                                                        @if($activityPivot->id_activity == $key) 
                                                        selected @endif @endforeach @endif>{{$activity}}</option>
                                                    @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-sm-12">
                                    <div class="col-sm-5 col-xs-12">
                                        <div class="form-group">
                                            <label for="costo">Municipio al que pertenece *</label>
                                            {!!Form::select('fk_municipality', $municipalities, null, ['class'=>'select-chosen','id'=>'municipalitySite'])!!}
                                        </div>
                                    </div>
                                    <div class="col-sm-5 col-sm-offset-1 col-xs-12">
                                        <div class="form-group">
                                            <label for="costo">Nombre *</label>
                                            {!!Form::text('site_name', null, ['class'=>'form-control', 'placeholder' => 'Inserte nombre del sitio de interés', 'required','id'=>'site_name'])!!}
                                            <span class="label label-danger">{{$errors->first('site_name') }}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-sm-12">
                                    <div class="col-sm-5 col-xs-12">
                                        <div class="form-group">
                                            <label for="costo">Latitud *</label>
                                            {!!Form::text('latitude', null, ['class'=>'form-control', 'placeholder' => 'Ingrese latitud', 'id'=>'latitude','required'])!!}
                                            <span class="label label-danger">{{$errors->first('latitude') }}</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-5 col-sm-offset-1 col-xs-12">
                                        <div class="form-group">
                                            <label for="costo">Longitud *</label>
                                            {!!Form::text('longitude', null, ['class'=>'form-control', 'placeholder' => 'Ingrese longitud','id'=>'longitude' ,'required'])!!}
                                            <span class="label label-danger">{{$errors->first('longitude') }}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 form-group" id="description">
                                    <label for="duration">Descripción</label>
                                    <div class="form-group">
                                        {!! Form::textarea('description',null,['class'=>'ckeditor']) !!}
                                        <span class="label label-danger">{{$errors->first('description') }}</span>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <div class="modal-footer">
                                <button type="button" onclick="register()" class="btn btn-effect-ripple btn-primary">Registrar</button>
                                <button type="button" class="btn btn-effect-ripple btn-danger" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END Large Modal -->
                </div>
                @if(!isset($toll))
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="location">Departamento</label>
                        {!!Form::select('departments', $departments, null, ['class'=>'select-chosen','onchange'=>'changeDepartment()','id'=>'relation_departaments'])!!}
                    </div>
                </div>
                @else
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="location">Departamento</label>
                            <select name="departments" onchange="changeDepartment();" id="relation_departaments" class="select-chosen">
                                @foreach($departments as $id => $departmentF)
                                    <option value="{{$id}}" @if($id == $departmentStart->id_department) selected @endif>{{$departmentF}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                @endif
                <div class="col-sm-4 @if(!isset($toll)) hidden @endif " id="municipalityDiv">
                    <div class="form-group">
                        <label for="location">Municipio</label>
                        <select name="fk_municipality" id="municipalities" onchange="changeMunicipality()" class="select-chosen">
                            @if(isset($toll))
                                @foreach($municipalitiesStart as $id => $municipalityF)
                                    <option value="{{$id}}" @if($id == $municipalityStart->id_municipality) selected @endif>{{$municipalityF}}</option>    
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="col-sm-4" >
                    <div class="form-group @if(!isset($toll)) hidden @endif" id="siteDiv">
                        <label for="location">Sitio de interés</label>
                        <select name="fk_site" id="interesSite" class="select-chosen">
                            @if(isset($toll))
                                <option></option>
                                @foreach($sitesStart as $id => $site)
                                    <option value="{{$id}}" @if($toll->fk_site==$id) selected @endif>{{$site}}</option>    
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 form-group">
                <div class="col-sm-5 ">
                    <label for="duration">Costo categoría I</label>
                    <div class="form-group">
                       {!!Form::number('cost_1', null, ['class'=>'form-control', 'placeholder' => 'Inserte costo de categoria', 'required'])!!}
                        <span class="label label-danger">{{$errors->first('cost_1') }}</span>
                    </div>
                </div>
                <div class="col-sm-offset-1 col-sm-5 ">
                    <label for="duration">Costo categoría II</label>
                    <div class="form-group">
                       {!!Form::number('cost_2', null, ['class'=>'form-control', 'placeholder' => 'Inserte costo de categoria', 'required'])!!}
                        <span class="label label-danger">{{$errors->first('cost_2') }}</span>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 form-group">
                <div class="col-sm-5 ">
                    <label for="duration">Costo categoría III</label>
                    <div class="form-group">
                       {!!Form::number('cost_3', null, ['class'=>'form-control', 'placeholder' => 'Inserte costo de categoria', 'required'])!!}
                        <span class="label label-danger">{{$errors->first('cost_3') }}</span>
                    </div>
                </div>
                <div class="col-sm-offset-1 col-sm-5">
                    <label for="duration">Costo categoría IV</label>
                    <div class="form-group">
                       {!!Form::number('cost_4', null, ['class'=>'form-control', 'placeholder' => 'Inserte costo de categoria', 'required'])!!}
                        <span class="label label-danger">{{$errors->first('cost_4') }}</span>
                    </div>
                </div>
            </div> 
            <div class="col-sm-12 form-group">
                <div class="col-sm-5 ">
                    <label for="duration">Costo categoría V</label>
                    <div class="form-group">
                       {!!Form::number('cost_5', null, ['class'=>'form-control', 'placeholder' => 'Inserte costo de categoria', 'required'])!!}
                        <span class="label label-danger">{{$errors->first('cost_5') }}</span>
                    </div>
                </div>
                <div class="col-sm-offset-1 col-sm-5">
                    <label for="duration">Costo categoría VI</label>
                    <div class="form-group">
                       {!!Form::number('cost_6', null, ['class'=>'form-control', 'placeholder' => 'Inserte costo de categoria', 'required'])!!}
                        <span class="label label-danger">{{$errors->first('cost_6') }}</span>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 form-group">
                <div class="col-sm-6 col-sm-offset-3 ">
                    <label for="duration">Costo categoría VII</label>
                    <div class="form-group">
                       {!!Form::number('cost_7', null, ['class'=>'form-control', 'placeholder' => 'Inserte costo de categoria', 'required'])!!}
                        <span class="label label-danger">{{$errors->first('cost_7') }}</span>
                    </div>
                </div>
            </div>
            <div class="form-group form-actions" align="center">
                <button type="submit" class="btn btn-effect-ripple btn-primary">Guardar</button>
                <a href="{{url('admin/concessions/tolls/'.$concession->slug)}}" class="btn btn-effect-ripple btn-danger">Cancelar</a>
            </div>
            {!!Form::close()!!}
        </div>
</div>
@endsection

@section('aditionalScript')

    <script src="{{asset('auth-panel/js/plugins/ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('plugins/sweetalert2/dist/sweetalert2.all.min.js')}}"></script>
    <script src="{{asset('auth-panel/js/pages/formsComponents.js')}}"></script>
    <script>$(function(){ FormsComponents.init(); });</script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/plugins/sortable.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/fileinput.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/locales/fr.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/locales/es.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/themes/explorer-fa/theme.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/themes/fa/theme.js')}}" type="text/javascript"></script>
    <script>
        $("#avatar-1").fileinput({
            overwriteInitial: true,
            maxFileSize: 1500,
            showClose: false,
            showCaption: false,
            browseLabel: '',
            removeLabel: '',
            browseIcon: '<i class="fab fa-searchengin"></i>',
            removeIcon: '<i class="far fa-times-circle"></i>',
            viewIcon:'<i class="far fa-times-circle"></i>',
            removeTitle: 'Cancel or reset changes',
            elErrorContainer: '#kv-avatar-errors-1',
            msgErrorClass: 'alert alert-block alert-danger',
            @if(isset($toll))
                @if($toll->link_image)
                defaultPreviewContent: '<img src="{{asset($toll->link_image)}}" alt="Avatar por defecto" width="50%">',
                @else
                defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Avatar por defecto" width="50%">',
                @endif
            @else
            defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Avatar por defecto" width="50%">',
            @endif
            layoutTemplates: {main2: '{preview} ' + ' {remove} {browse}'},
            allowedFileExtensions: ["jpg", "png", "gif"]
        });
        function changeDepartment()
        {
            $("#siteDiv").addClass('hidden');
            relationalDepartments = $('#relation_departaments').val();
            var token = "{{csrf_token()}}";
            var route="/admin/specials/routes/getreladepartment";            
            $.ajax({
                url: route,
                headers: {'X-CSRF-TOKEN': token},
                type: 'POST',
                dataType: 'json',
                data:{relationalDepartments},
                complete: function(transport)
                {
                    data = transport.responseJSON;
                    municipalities = data['municipalities'];
                    // //MUNICIPIOS
                    $("#municipalityDiv").removeClass('hidden');
                    $('#municipalities option').remove();
                    
                    $("#municipalities").append('<option></option>').trigger("chosen:updated");

                    for(x in municipalities)
                    {
                        $("#municipalities").append('<option value="'+municipalities[x].id_municipality+'" id="municipality'+municipalities[x].id_municipality+'">'+municipalities[x].municipality_name+'</option>').trigger("chosen:updated");
                    }
                }
              });
        };
        function changeMunicipality()
        {
            municipality = $('#municipalities').val();
            var token = "{{csrf_token()}}";
            var route="/admin/specials/routes/getrelamunicipality";            
            $.ajax({
                url: route,
                headers: {'X-CSRF-TOKEN': token},
                type: 'get',
                dataType: 'json',
                data:{municipality},
                complete: function(transport)
                {
                    data = transport.responseJSON;
                    interestsites = data['interestSites'];
                    if(interestsites.length >0)
                    {
                        $("#siteDiv").removeClass('hidden');
                        $('#interesSite option').remove();

                        for(x in interestsites)
                        {
                            $("#interesSite").append('<option value="'+interestsites[x].id_site+'">'+interestsites[x].site_name+'</option>').trigger("chosen:updated");
                        }
                    }
                    else{             
                        $("#siteDiv").addClass('hidden');
                    }
                }
              });
        };

        function register()
        {
            activities = $('#activities').val();
            category = $('#category').val();
            municipality = $('#municipalitySite').val();
            site_name = $('#site_name').val();
            latitude = $('#latitude').val();
            longitude = $('#longitude').val();
            description = $('#siteDescription').val();
            if(category != '' && municipality != '' && site_name != '' && latitude != '' && longitude != '')
            {
                $.ajax({
                    url:'/admin/interestsite/register',
                    headers:{'X-CSRF-TOKEN':'{{csrf_token()}}'},
                    type:'POST',
                    data:{activities:activities,category:category,municipality:municipality,site_name:site_name,latitude:latitude,longitude:longitude,description:description},
                    complete:function(transport)
                    {
                        response = transport.responseJSON;
                        if(response.status == 'ok')
                        {
                            $('#relation_departaments').val(response.department.id_department).trigger("chosen:updated");
                            changeDepartment();
                        setTimeout(function() {
                            $('#municipalities').val(municipality).trigger("chosen:updated");
                            changeMunicipality();
                        }, 1000);
                            swal({
                                  type: 'success',
                                  title: 'Se ha registrado correctamente el sitio de interes.',
                                });
                            $('#modal-large').modal('hide');
                            $("#municipality"+municipality).attr('selected', 'selected');
                            $('#activities').val('');
                            $('#category').val('');
                            $('#municipalitySite').val('');
                            $('#site_name').val('');
                            $('#latitude').val('');
                            $('#longitude').val('');
                            $('#siteDescription').val('');
                        }
                        else
                        {
                            swal({
                              type: 'error',
                              title: 'Ha ocurrdio un error, por favor inténtelo nuevamente.',
                            });
                        }
                    }
                })
            }
            else
            {
                //swal('Error!','Por favor diligencia los campos marcados con *.','error');
                swal({
                  type: 'error',
                  title: 'Por favor diligencia los campos marcados con *.',
                });
            }
        }
    </script>

@endsection