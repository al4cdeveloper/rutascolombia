@extends('admin.layout.auth')

@if(isset($concession))
    @section('title', 'Actualizar concesión')
@else
    @section('title', 'Crear concesión')
@endif

@section('content')
<div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>Concesión</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="block">
            <!-- Labels on top Form Title -->
            <div class="block-title">
                <h2>Ingrese la información solicitada</h2>
                @if(isset($concession))
                    <div class="col-sm-6 col-sm-offset-6">
                        <div class="pull-right" style="padding: 5px;">
                            <a href="{{url('admin/concessions/tolls/'.$concession->slug)}}" class="btn btn-primary btn-sm">Administrar peajes   <i class="fas fa-monument"></i></a>
                        </div>
                    </div> 
                @else
                    <div class="col-sm-6 col-sm-offset-6">
                        <p>Luego de crear la concesión se activarán las opciones de administración de peajes.</p>
                    </div>
                @endif
            </div>
            @if(isset($concession))
			{!!Form::model($concession,['url'=>['admin/concessions/update',$concession->id_concession],'method'=>'POST', 'class'=> 'form-horizontal', 'enctype' => 'multipart/form-data', 'novalidate'])!!}
			@else
			{!!Form::open(['url'=>'admin/concessions/store', 'method'=>'POST', 'class'=> 'form-horizontal nuevo', 'enctype' => 'multipart/form-data', 'novalidate'])!!}
			@endif
             <div class="col-sm-12">
                <div class="col-sm-4 text-center col-sm-offset-4">
                    <div class="kv-avatar">
                        <div class="form-group">
                        <label for="costo">Logo <small>(opcional)</small></label>
                            <div class="file-loading">

                                {!!Form::file('logo',['id'=>'avatar-1'])!!}
                            </div>
                            <span class="label label-danger">{{$errors->first('logo') }}</span>
                        </div>
                    </div>
                </div>
            </div>
			<div class="col-sm-12">
                <div class="col-sm-8">
                    <div class="form-group">
                        <label for="costo">Nombre</label>
                        {!!Form::text('name', null, ['class'=>'form-control', 'placeholder' => 'Inserte nombre de concesión', 'required'])!!}
                        <span class="label label-danger">{{$errors->first('name') }}</span>
                    </div>
                </div>
                <div class="col-sm-3 col-sm-offset-1">
                    <div class="form-group">
                        <label for="costo">Siglas <small>(opcional)</small></label>
                        {!!Form::text('acronym', null, ['class'=>'form-control', 'placeholder' => 'Inserte nombre de cliente', 'required'])!!}
                        <span class="label label-danger">{{$errors->first('acronym') }}</span>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                <label >Palabras clave <small>Separadas por comas (,)</small></label>
                    {!!Form::text('keywords',null,['class'=>'form-control input-tags', 'placeholder'=>'Palabras clave'])!!}
                    <span class="label label-danger">{{$errors->first('keywords') }}</span>
                </div>
            </div>
            <div class="col-sm-12 form-group">
                <label for="duration">Descripción</label>
                <div class="form-group">
                    {!! Form::textarea('description',null,['class'=>'ckeditor']) !!}
                    <span class="label label-danger">{{$errors->first('description') }}</span>
                </div>
            </div>
             <div class="col-sm-12">
                <div class="col-sm-4 text-center col-sm-offset-4">
                    <div class="kv-avatar">
                        <div class="form-group">
                        <label for="costo">Imagen de Descripción <small>(Opcional)</small></label>
                            <div class="file-loading">

                                {!!Form::file('description_image',['id'=>'avatar-2'])!!}
                            </div>
                            <span class="label label-danger">{{$errors->first('description_image') }}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group form-actions" align="center">
                <button type="submit" class="btn btn-effect-ripple btn-primary" name="typesubmit" value="guardarysalir">Guardar y salir</button>
                <button type="submit" class="btn btn-effect-ripple btn-info" name="typesubmit" value="guardartodo">Guardar todo</button>
                <a href="{{url('admin/concessions')}}" class="btn btn-effect-ripple btn-danger">Cancelar</a>
            </div>
            {!!Form::close()!!}
        </div>
</div>
@endsection

@section('aditionalScript')

    <script src="{{asset('auth-panel/js/plugins/ckeditor/ckeditor.js')}}"></script>

    <script src="{{asset('auth-panel/js/pages/formsComponents.js')}}"></script>
    <script>$(function(){ FormsComponents.init(); });</script>

     <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/plugins/sortable.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/fileinput.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/locales/fr.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/locales/es.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/themes/explorer-fa/theme.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/themes/fa/theme.js')}}" type="text/javascript"></script>

    <script>
    $("#avatar-1").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        browseLabel: '',
        removeLabel: '',
        browseIcon: '<i class="fab fa-searchengin"></i>',
        removeIcon: '<i class="far fa-times-circle"></i>',
        viewIcon:'<i class="far fa-times-circle"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        @if(isset($concession))
            @if($concession->logo)
            defaultPreviewContent: '<img src="{{asset($concession->logo)}}" alt="Avatar por defecto" width="50%">',
            @else
            defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Avatar por defecto" width="50%">',
            @endif
        @else
        defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Avatar por defecto" width="50%">',
        @endif
        layoutTemplates: {main2: '{preview} ' + ' {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "gif"]
    });
    $("#avatar-2").fileinput({
            overwriteInitial: true,
            maxFileSize: 1500,
            showClose: false,
            showCaption: false,
            browseLabel: '',
            removeLabel: '',
            browseIcon: '<i class="fab fa-searchengin"></i>',
            removeIcon: '<i class="far fa-times-circle"></i>',
            viewIcon:'<i class="far fa-times-circle"></i>',
            removeTitle: 'Cancel or reset changes',
            elErrorContainer: '#kv-avatar-errors-1',
            msgErrorClass: 'alert alert-block alert-danger',
            @if(isset($concession))
            defaultPreviewContent: '<img src="{{asset($concession->description_image)}}" alt="Imagen de descripción" width="50%">',
                @if($concession->description_image)
                defaultPreviewContent: '<img src="{{asset($concession->description_image)}}" alt="Imagen de descripción" width="50%">',
                @else
                defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Imagen de descripción" width="50%">',
                @endif
            @else
            defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Imagen de descripción" width="50%">',
            @endif
            layoutTemplates: {main2: '{preview} ' + ' {remove} {browse}'},
            allowedFileExtensions: ["jpg", "png", "gif"]
        });
</script>
@endsection