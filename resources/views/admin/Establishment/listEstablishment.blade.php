@extends('admin.layout.auth')

@section('title', ($type=='hotel') ? "Lista de Hoteles" : "Lista de Restaurantes")
@section('additionalStyle')
    <link rel="stylesheet" href="{{asset('plugins/dist/sweetalert2.min.css')}}">
@endsection


@section('content')
    <div id="page-content">

     <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-12">
                    <div class="header-section">
                        <div class="pull-right">
                            <a href="{{url("admin/establishment/$type/create")}}" class="btn btn-primary"> Crear nuevo {{($type=='hotel') ?  "Hotel" : "Restaurante"}}</a>
                        </div>
                        <h1>Lista de @if($type=='hotel') Hoteles @else Restaurantes @endif</h1>
                    </div>
                </div>
                
            </div>
        </div>
        <div class="block full">
            <table id="example-datatable" class="table" role="grid" aria-describedby="datatable_info">
                <thead>
                    <th>Nombre</th>
                    <th>Telefóno</th>
                    <th>Dirección</th>
                    <th>Municipio</th>
                    <th>Administrado por</th>
                    <th>Destacado</th>
                    <th>Estado</th>
                    <th></th>
                </thead>
                <tbody>
                    @foreach($establishments as $establishment)
                    <tr >
                        <td>{{$establishment->establishment_name}}</td>
                        <td>{{$establishment->phone}}</td>
                        <td>{{$establishment->address}}</td>
                        <td>{{$establishment->Municipality->municipality_name}}</td>
                        <td>
                            @if($establishment->internal_state == 'admin')
                                administrador
                            @else
                                <a href="{{url('admin/operators?operator='.$establishment->Operator->id_operator)}}">{{$establishment->Operator->name}}</a>
                            @endif
                        </td>
                        <td> <label class="switch switch-warning" onclick="change({{$establishment->id_establishment}})"><input type="checkbox" @if($establishment->outstanding) checked @endif><span></span></label></td>
                        <td>
                            {{$establishment->state}}
                        </td>
                        <td >
                            <a href="{{url('admin/establishment/edit',$establishment->slug)}}" class="btn btn-warning btn-xs" data-toggle="tooltip" title="Editar establecimiento"><i class="far fa-edit"></i></a>
                            <a href="{{ url('admin/establishment/images', $establishment->slug) }}" data-toggle="tooltip" title="Administrar imágenes" class="btn btn-info btn-xs"><i class="far fa-images"></i></a>
                            <button type="button" onclick="transferShow('{{$establishment->internal_state}}',{{$establishment->id_establishment}})" data-toggle="tooltip" title="Transferir" class="btn btn-success btn-xs"><i class="fas fa-exchange-alt"></i></button>
                            @if($establishment->state=="activo")
                            <a href="{{url('admin/establishment/desactivate/'.$establishment->slug)}}" data-toggle="tooltip" title="Desactivar establecimiento" class="btn btn-effect-ripple btn-danger btn-xs"><i class="fa fa-times"></i></a>
                            @else
                            <a href="{{url('admin/establishment/activate/'.$establishment->slug)}}" data-toggle="tooltip" title="Activar establecimiento" class="btn btn-effect-ripple btn-success btn-xs"><i class="fa fa-check"></i></a>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

            <div class="modal fade" id="transfer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form action="{{url('admin/establishment/transfer')}}" method="post">
                        {{csrf_field()}}
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Transferencia de establecimiento</h4>
                          </div>
                          <div class="modal-body">
                            <div id="admin" style="display:none">
                                <div class="col-md-4">
                                    Transferir al operador:
                                </div>
                                <div>
                                    <select name="operator" id="" class='select-chosen col-md-5'>
                                        @foreach($operators as $operator)
                                            <option value="{{$operator->id_operator}}">{{$operator->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div id="operator" style="display:none">
                                <label class="csscheckbox csscheckbox-primary">
                                    <input type="checkbox" name="administrador" value="1"><span></span> Regresar a administrar por administrador.
                                </label> 
                            </div>
                          </div>
                          {{Form::hidden('establishment',null,['id'=>'establishment_id'])}}
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                    </form>
                </div>
              </div>
            </div>
        </div>
    </div>
@endsection
@section('aditionalScript')
<!-- Load and execute javascript code used only in this page -->
        <script src="{{asset('auth-panel/js/pages/uiTables.js')}}"></script>
    <script src="{{asset('plugins/sweetalert2/dist/sweetalert2.all.min.js')}}"></script>

        <script>$(function(){ UiTables.init(); });</script>

        <script>
            function transferShow(state, id)
            {
                $("#transfer").modal('show');
                $("#establishment_id").val(id);
                if(state=="admin")
                {
                    $("#admin").css('display','inline');
                }
                else
                {
                    $("#operator").css('display','inline');
                }
            }
        </script>
        <script>
        
        var funcionando = true;

        function change(id)
        {
            var cod = id;
            var token = '{{csrf_token()}}';


            if(funcionando)
            {
                funcionando=false;

                setTimeout(function() {
                    $.ajax(
                    {
                        url:'/admin/establishment/change',
                        headers:{'X-CSRF-TOKEN':token},
                        type:'POST',
                        dataType:'json',
                        data:{cod},
                        complete:function(transport)
                        {
                            response = transport.responseText;
                            if(response == 'ok')
                            {
                                swal({
                                  type: 'success',
                                  title: 'Se ha cambiado el estado del establecimiento.',
                                  timer: 1500
                                })
                            }
                            else
                            {
                                swal({
                                  type: 'error',
                                  title: 'Se ha presentado un error, inténtalo más tarde.',
                                  timer: 1500
                                })
                            }
                            funcionando=true;
                        }
                    }); 
                }, 500);
                

            }
              
        }
    </script>
@endsection