@extends('admin.layout.auth')

@section('title', 'Listado de regiones')


@section('content')
    <div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>Regiones</h1>
                    </div>
                </div>
                {{-- <div class="col-sm-6 hidden-xs">
                    <div class="header-section">
                        <ul class="breadcrumb breadcrumb-top">
                            <li>Category</li>
                            <li><a href="">Page</a></li>
                        </ul>
                    </div>
                </div> --}}
            </div>
        </div>
        <!-- END Page Header -->

        {{-- <div class="block"> --}}
            {{-- <div class="row"> --}}
                <div class="block full">
                    <div class="block-title">
                        <h2>Regiones</h2>
                        <div class="pull-right" style="padding: 5px;"><a href="{{url('admin/regions/create')}}" class="btn btn-primary btn-sm"><i class="far fa-plus-square fa-lg"></i></a></div>
                    </div>
                    <div class="table-responsive">
                        <table id="example-datatable" class="table table-striped table-bordered table-vcenter">
                            <thead>
                                <tr>
                                    <th class="text-center" style="width: 50px;">Región</th>
                                    <th>Palabras clave</th>
                                    <th>Ultima edición</th>
                                    <th style="width: 120px;">Estado</th>
                                    <th class="text-center" style="width: 75px;"><i class="fas fa-sync-alt"></i></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($regions as $region)
                                <tr>
                                    <td>{{$region->region_name}}</td>
                                    <td>{{$region->keywords}}</td>
                                    <td>{{$region->last_edition}}</td>
                                    <td>@if($region->state=="activo")
                                        <a class="label label-info">{{$region->state}}</a>
                                        @else
                                        <span class="label label-danger">{{$region->state}}</span>
                                        @endif
                                    </td>
                                    <td class="text-center">

                                        <a href="{{url('admin/regions/edit/'.$region->slug)}}" data-toggle="tooltip" title="Editar región" class="btn btn-effect-ripple btn-xs btn-warning"><i class="fa fa-edit"></i></a>
                                        <a href="{{url('admin/regions/images/'.$region->slug)}}" data-toggle="tooltip" title="Administrar multimedia" class="btn btn-effect-ripple btn-xs btn-info"><i class="fa fa-images"></i></a>
                                        @if($region->state=="activo")
                                        <a href="{{url('admin/regions/desactivate/'.$region->id_region)}}" data-toggle="tooltip" title="Desactivar región" class="btn btn-effect-ripple btn-xs btn-danger"><i class="fa fa-times"></i></a>
                                        @else
                                        <a href="{{url('admin/regions/activate/'.$region->id_region)}}" data-toggle="tooltip" title="Activar región" class="btn btn-effect-ripple btn-xs btn-success"><i class="fas fa-check"></i></a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END Datatables Block -->
            {{-- </div> --}}
        {{-- </div> --}}
    </div>
@endsection

@section('aditionalScript')
<!-- Load and execute javascript code used only in this page -->
        <script src="{{asset('auth-panel/js/pages/uiTables.js')}}"></script>
        <script>$(function(){ UiTables.init(); });</script>
@endsection