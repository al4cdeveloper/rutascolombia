@extends('admin.layout.auth')

@if(isset($pattern))
    @section('title', 'Actualizar publicidad')
@else
    @section('title', 'Crear publicidad')
@endif
@section('aditionalStyle')
    <link href="{{asset('plugins/kartik-v-bootstrap-fileinput/css/fileinput.css')}}" media="all" rel="stylesheet" type="text/css"/>
    <style>
.kv-avatar .krajee-default.file-preview-frame,.kv-avatar .krajee-default.file-preview-frame:hover {
    margin: 0;
    padding: 0;
    border: none;
    box-shadow: none;
    text-align: center;
}
.kv-avatar {
    display: inline-block;
}
.kv-avatar .file-input {
    display: table-cell;
    width: 213px;
}
.kv-reqd {
    color: red;
    font-family: monospace;
    font-weight: normal;
}
</style>
@endsection
@section('content')
<div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>Publicidad</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="block">
            <!-- Labels on top Form Title -->
            <div class="block-title">
                <h2>Ingrese la información solicitada</h2>
            </div>
            @if(isset($pattern))
			{!!Form::model($pattern,['url'=>['admin/patterns/update',$pattern->id_pattern],'method'=>'POST', 'class'=> 'form-horizontal', 'enctype' => 'multipart/form-data', 'id' => 'userForm', 'novalidate'])!!}
			@else
			{!!Form::open(['url'=>'admin/patterns/store', 'method'=>'POST', 'class'=> 'form-horizontal nuevo', 'enctype' => 'multipart/form-data', 'id' => 'userForm', 'novalidate'])!!}
			@endif
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="costo">Cliente</label>
                    {!!Form::select('fk_customer', $customers, null, ['class'=>'select-chosen'])!!}
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="costo">Tipo de multimedia</label>
                    {!!Form::select('multimedia_type', $multimediaType, null, ['class'=>'select-chosen','id'=>'multimedia_type'])!!}
                    <span class="label label-danger">{{$errors->first('multimedia_type') }}</span>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="costo">Tipo de publicidad</label>
                    {!!Form::select('type', $type, null, ['class'=>'select-chosen','id'=>'type_publicity'])!!}
                    <span class="label label-danger">{{$errors->first('type') }}</span>
                </div>
            </div>
            
            <!-- AJAX -->

            <div class="col-sm-12" id="patterndiv" @if(!isset($pattern)) style="display: none" @endif>
                <div class="form-group">
                    <label for="costo">Lugares de publicidad: </label>
                    <select name="page_parts[]" id="page_parts" class="select-chosen" multiple="">
                        @if(isset($pattern))
                            @foreach($pages as $page)
                                <optgroup label="{{$page->page_name}}">
                                    @foreach($page['parts']->where('type',$pattern->type) as $part)
                                         <option value="{{$part->pivot->id_part_page}}" 
                                            @foreach($pattern->Parts as $patternPart) 
                                            @if($patternPart->id_part_page == $part->pivot->id_part_page && $patternPart->pivot->state == "activo") 
                                            selected @endif @endforeach >{{$part->part_name}}</option>
                                    @endforeach
                                </optgroup>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>


			<div class="col-sm-12" id="redirection">
                <div class="form-group">
                    <label for="costo">URL De redirección <small>(Opcional)</small></label>
                    {!!Form::text('redirection', null, ['class'=>'form-control', 'placeholder' => 'Inserte nombre de sección', 'required'])!!}
                    <span class="label label-danger">{{$errors->first('redirection') }}</span>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group col-sm-5 col-xs-12">
                    <label for="costo">Fecha de inicio</label>
                    {!!Form::text('publication_day',date("Y-m-d"),['class'=>'form-control input-datepicker','data-date-format'=>'yyyy-mm-dd','placeholder'=>'yyyy-mm-dd','id'=>'publication_day'])!!}
                    <span class="label label-danger">{{$errors->first('publication_day') }}</span>

                </div>
                <div class="col-xs-12 col-sm-5 col-md-offset-1 col-xs-12">
                    <label for="costo">Fecha de fin</label>
                    {!!Form::text('publication_finish',null,['class'=>'form-control input-datepicker','data-date-format'=>'yyyy-mm-dd','placeholder'=>'yyyy-mm-dd','id'=>'publication_finish'])!!}
                    <span class="label label-danger">{{$errors->first('publication_finish') }}</span>
                </div>
            </div>
            <div class="col-sm-12" id="pattern">
                @if(isset($pattern))
                    @if($pattern->multimedia_type=="Imagen")
                    <div class="col-md-4 col-md-offset-4 text-center" id="imagen">
                        <div class="kv-avatar">
                            <div class="form-group">
                            <label for="costo">Imagen publicitaria</label>
                                <div class="file-loading">

                                    {!!Form::file('pattern',['id'=>'avatar-1'])!!}
                                </div>
                                <span class="label label-danger">{{$errors->first('pattern') }}</span>
                            </div>
                        </div>
                    </div>
                    @elseif($pattern->multimedia_type=="Video")
                     <div class="col-sm-12 hidden" id="video">
                        <label for="costo">Url de Vídeo publicitario</label>
                        {!!Form::text('pattern', "https://www.youtube.com/embed/$pattern->pattern", ['class'=>'form-control', 'placeholder' => 'Inserte URL de vídeo (YOUTUBE)', 'required','id'=>'pattern_video'])!!}
                        <span class="label label-danger">{{$errors->first('pattern') }}</span>
                    </div>
                    <div class="col-md-12" id="preview">
                        <div class="col-md-4 col-md-offset-4">
                            <iframe width="100%" height="200" src="https://www.youtube.com/embed/{{$pattern->pattern}}?rel=0&controls=1&modestbranding=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                    </div>
                    @else
                    <div class="col-sm-12 hidden" id="codigo">
                        <label for="costo">Código publicitario</label>
                        {!!Form::textarea('pattern', null, ['class'=>'form-control', 'placeholder' => 'Inserte codigo publicitario', 'required','rows'=>'7','id'=>'pattern_code'])!!}
                        <span class="label label-danger">{{$errors->first('pattern') }}</span>
                    </div>
                    @endif
                @endif
            </div>
            <div class="form-group form-actions" align="center">
                 <button type="submit" class="btn btn-effect-ripple btn-primary" name="typesubmit" value="guardarysalir">Guardar y salir</button>
                <button type="submit" class="btn btn-effect-ripple btn-info" name="typesubmit" value="guardartodo">Guardar todo</button>
                <a href="{{url('admin/patterns')}}" class="btn btn-effect-ripple btn-danger">Cancelar</a>
            </div>
            {!!Form::close()!!}
        </div>
</div>
@endsection

@section('aditionalScript')

    <script src="{{asset('auth-panel/js/plugins/ckeditor/ckeditor.js')}}"></script>

    <script src="{{asset('auth-panel/js/pages/formsComponents.js')}}"></script>
    <script>$(function(){ FormsComponents.init(); });</script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/plugins/sortable.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/fileinput.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/locales/fr.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/locales/es.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/themes/explorer-fa/theme.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/themes/fa/theme.js')}}" type="text/javascript"></script>

    <script>
    $('#multimedia_type').change(function()
    {
        if($('#multimedia_type option:selected').val()=="Video")
        {
            $('#pattern').empty();
           
            html = '<div class="col-sm-12" id="video"><label for="costo">Url de Vídeo publicitario</label>{!!Form::text('pattern', null, ['class'=>'form-control', 'placeholder' => 'Inserte URL de vídeo (YOUTUBE)', 'required','id'=>'pattern_video'])!!}<span class="label label-danger">{{$errors->first('pattern') }}</span></div>';

            $('#pattern').append(html);
                

            $("#redirection").addClass('hidden');
            $('#type_publicity option[value=Estatica]').attr('selected','selected');
            $('#type_publicity option[value=Carrusel]').attr('disabled','disabled').trigger("chosen:updated");
        }
        else if($('#multimedia_type option:selected').val()=="Codigo")
        {

            $('#pattern').empty();

            html= '<div class="col-sm-12" id="codigo"><label for="costo">Código publicitario</label>{!!Form::textarea('pattern', null, ['class'=>'form-control', 'placeholder' => 'Inserte codigo publicitario', 'required','rows'=>'7','id'=>'pattern_code'])!!}<span class="label label-danger">{{$errors->first('pattern') }}</span></div>';
            $('#pattern').append(html);
            
            $("#redirection").addClass('hidden');

            $('#type_publicity option[value=Estatica]').attr('selected','selected');
            $('#type_publicity option[value=Carrusel]').attr('disabled','disabled').trigger("chosen:updated");
        }
        else if($('#multimedia_type option:selected').val()=="Imagen")
        {
            $('#pattern').empty();
            html = '<div class="col-md-4 col-md-offset-4 text-center" id="imagen"><div class="kv-avatar"><div class="form-group"><label for="costo">Imagen publicitaria</label><div class="file-loading">{!!Form::file('pattern',['id'=>'avatar-1'])!!}</div><span class="label label-danger">{{$errors->first('pattern') }}</span></div></div></div>'; 
            $('#pattern').append(html);
            $("#avatar-1").fileinput({
                overwriteInitial: true,
                maxFileSize: 1500,
                showClose: false,
                showCaption: false,
                browseLabel: '',
                removeLabel: '',
                browseIcon: '<i class="fab fa-searchengin"></i>',
                removeIcon: '<i class="far fa-times-circle"></i>',
                viewIcon:'<i class="far fa-times-circle"></i>',
                removeTitle: 'Cancel or reset changes',
                elErrorContainer: '#kv-avatar-errors-1',
                msgErrorClass: 'alert alert-block alert-danger',
                @if(isset($pattern) && $pattern->multimedia_type=="Imagen")
                defaultPreviewContent: '<img src="{{asset($pattern->pattern)}}" width="50%">',
                @else
                defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" width="50%">',
                @endif
                layoutTemplates: {main2: '{preview} ' + ' {remove} {browse}'},
                allowedFileExtensions: ["jpg", "png", "gif"]
            });
            $("#redirection").removeClass('hidden');
            $("#imagen").removeClass('hidden');
            $('#type_publicity option[value=Estatica]').attr('selected','selected');
            $('#type_publicity option[value=Carrusel]').removeAttr('disabled').trigger("chosen:updated");
            $("#type_publicity").removeAttr('disabled').trigger("chosen:updated");
        }
        var token = "{{csrf_token()}}";
        var multimediaType =$('#multimedia_type option:selected').val();
        var typePublicity =$('#type_publicity option:selected').val();
        $.ajax({
            url: '/api/getsectionpart',
            type: 'POST',
            headers: {'X-CSRF-TOKEN': token},
            dataType: 'JSON',
            data: {typePublicity,multimediaType},
            complete: function(transport)
            {
                $('#page_parts optgroup').remove();
                $('#page_parts option').remove();

                var response = transport.responseJSON;
                $('#patterndiv').css('display','inline');
                for(x in response)
                {
                    $("#page_parts").append('<optgroup label="'+x+'" id="page'+x+'"></optgroup>').trigger("chosen:updated");
                    for(y in response[x])
                    {
                        $("#page"+x).append('<option value="'+response[x][y].pivot.id_part_page+'">'+response[x][y].part_name+'</option>')
                    }
                    $("#page_parts").trigger("chosen:updated");
                }
            }
        })
    });

    $("#type_publicity").change(function(event) 
    {
        var token = "{{csrf_token()}}";
        var multimediaType =$('#multimedia_type option:selected').val();
        var typePublicity =$('#type_publicity option:selected').val();
        console.log(typePublicity);
        $.ajax({
            url: '/api/getsectionpart',
            type: 'POST',
            headers: {'X-CSRF-TOKEN': token},
            dataType: 'JSON',
            data: {typePublicity,multimediaType},
            complete: function(transport)
            {
                $('#page_parts optgroup').remove();
                $('#page_parts option').remove();

                var response = transport.responseJSON;
                $('#patterndiv').css('display','inline');
                for(x in response)
                {
                    $("#page_parts").append('<optgroup label="'+x+'" id="page'+x+'"></optgroup>').trigger("chosen:updated");
                    for(y in response[x])
                    {
                        $("#page"+x).append('<option value="'+response[x][y].pivot.id_part_page+'">'+response[x][y].part_name+'</option>')
                    }
                    $("#page_parts").trigger("chosen:updated");
                }
            }
        })
    });

    $(document).ready(function()
    {
        if($('#multimedia_type option:selected').val()=="Video")
        {
            $("#redirection").addClass('hidden');
            $('#type_publicity option[value=Estatica]').attr('selected','selected');
            $('#type_publicity option[value=Carrusel]').attr('disabled','disabled').trigger("chosen:updated");
        }
        else if($('#multimedia_type option:selected').val()=="Codigo")
        {
            $("#imagen").addClass('hidden');
            $("#video").addClass('hidden');
            $("#redirection").addClass('hidden');
            $("#codigo").removeClass('hidden');
            $('#type_publicity option[value=Estatica]').attr('selected','selected');
            $('#type_publicity option[value=Carrusel]').attr('disabled','disabled').trigger("chosen:updated");
        }
        else if($('#multimedia_type option:selected').val()=="Imagen")
        {
            $('#type_publicity option[value=Carrusel]').removeAttr('disabled').trigger("chosen:updated");
            $("#redirection").removeClass('hidden');
            $("#imagen").removeClass('hidden');
            $("#type_publicity").removeAttr('disabled').trigger("chosen:updated");
        }
        var inicio=document.getElementById("publication_day").value;
        //var fin=document.getElementById("datepicker2").value;

        var start=new Date(inicio);
        start.setFullYear(start.getFullYear()+1);
        var startf = start.toISOString().slice(0,10).replace(/-/g,"-");
        document.getElementById("publication_finish").value= startf;
    });
    $("#avatar-1").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        browseLabel: '',
        removeLabel: '',
        browseIcon: '<i class="fab fa-searchengin"></i>',
        removeIcon: '<i class="far fa-times-circle"></i>',
        viewIcon:'<i class="far fa-times-circle"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        @if(isset($pattern) && $pattern->multimedia_type=="Imagen")
        defaultPreviewContent: '<img src="{{asset($pattern->pattern)}}" width="50%">',
        @else
        defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" width="50%">',
        @endif
        layoutTemplates: {main2: '{preview} ' + ' {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "gif"]
    });
    @if(isset($pattern) && $pattern->multimedia_type=="Imagen")
        $('#multimedia_type').change(function()
        {

            $('#pattern_video').val('');
            $('#pattern_code').val(''); 
            
        });
    @endif
    </script>
@endsection