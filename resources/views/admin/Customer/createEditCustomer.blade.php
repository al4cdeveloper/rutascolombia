@extends('admin.layout.auth')

@if(isset($customer))
    @section('title', 'Actualizar cliente')
@else
    @section('title', 'Crear cliente')
@endif

@section('content')
<div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>Cliente</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="block">
            <!-- Labels on top Form Title -->
            <div class="block-title">
                <h2>Ingrese la información solicitada</h2>
            </div>
            @if(isset($customer))
			{!!Form::model($customer,['url'=>['admin/customers/update',$customer->id_customer],'method'=>'POST', 'class'=> 'form-horizontal', 'enctype' => 'multipart/form-data', 'id' => 'userForm', 'novalidate'])!!}
			@else
			{!!Form::open(['url'=>'admin/customers/store', 'method'=>'POST', 'class'=> 'form-horizontal nuevo', 'enctype' => 'multipart/form-data', 'id' => 'userForm', 'novalidate'])!!}
			@endif
			<div class="col-sm-12">
                <div class="form-group">
                    <label for="costo">NIT</label>
                    {!!Form::text('nit', null, ['class'=>'form-control', 'placeholder' => 'Inserte NIT', 'required'])!!}
                    <span class="label label-danger">{{$errors->first('nit') }}</span>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="costo">Nombre</label>
                    {!!Form::text('name', null, ['class'=>'form-control', 'placeholder' => 'Inserte nombre de cliente', 'required'])!!}
                    <span class="label label-danger">{{$errors->first('name') }}</span>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="costo">Persona de contacto</label>
                    {!!Form::text('contact', null, ['class'=>'form-control', 'placeholder' => 'Inserte nombre de persona de contacto', 'required'])!!}
                    <span class="label label-danger">{{$errors->first('contact') }}</span>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="costo">Teléfono persona de contacto</label>
                    {!!Form::number('phone_contact', null, ['class'=>'form-control', 'placeholder' => 'Inserte telefono de persona de contacto', 'required'])!!}
                    <span class="label label-danger">{{$errors->first('phone_contact') }}</span>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="costo">Email persona de contacto</label>
                    {!!Form::email('email_contact', null, ['class'=>'form-control', 'placeholder' => 'Inserte correo de persona de contacto', 'required'])!!}
					<span class="label label-danger">{{$errors->first('email_contact') }}</span>
                </div>
            </div>
            
            
            <div class="form-group form-actions" align="center">
                <button type="submit" class="btn btn-effect-ripple btn-primary">Guardar</button>
                <a href="{{url('admin/customers')}}" class="btn btn-effect-ripple btn-danger">Cancelar</a>
            </div>
            {!!Form::close()!!}
        </div>
</div>
@endsection

@section('aditionalScript')

    <script src="{{asset('auth-panel/js/plugins/ckeditor/ckeditor.js')}}"></script>

    <script src="{{asset('auth-panel/js/pages/formsComponents.js')}}"></script>
    <script>$(function(){ FormsComponents.init(); });</script>

     <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/plugins/sortable.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/fileinput.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/locales/fr.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/locales/es.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/themes/explorer-fa/theme.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/themes/fa/theme.js')}}" type="text/javascript"></script>

    <script>
    $("#avatar-1").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        browseLabel: '',
        removeLabel: '',
        browseIcon: '<i class="fab fa-searchengin"></i>',
        removeIcon: '<i class="far fa-times-circle"></i>',
        viewIcon:'<i class="far fa-times-circle"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        @if(isset($customer))
        defaultPreviewContent: '<img src="{{asset($customer->link_image)}}" alt="Avatar por defecto" width="50%">',
        @else
        defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Avatar por defecto" width="50%">',
        @endif
        layoutTemplates: {main2: '{preview} ' + ' {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "gif"]
    });
</script>
@endsection