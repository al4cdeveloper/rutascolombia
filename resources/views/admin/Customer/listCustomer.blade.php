@extends('admin.layout.auth')

@section('title', 'Listado de clientes')


@section('content')
    <div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>Clientes</h1>
                    </div>
                </div>
            </div>
        </div>
            <div class="block full">
                <div class="block-title">
                    <h2>Clientes</h2>
                    <div class="pull-right" style="padding: 5px;"><a href="{{url('admin/customers/create')}}" class="btn btn-primary btn-sm"><i class="far fa-plus-square fa-lg"></i></a></div>
                </div>
                <div class="table-responsive">
                    <table id="example-datatable" class="table table-striped table-bordered table-vcenter">
                        <thead>
                            <tr>
                                <th class="text-center">NIT</th>
                                <th>Nombre</th>
                                <th>Persona de contacto</th>
                                <th>Telefono de contacto</th>
                                <th>Email de contacto</th>
                                <th style="width: 120px;">Estado</th>
                                <th class="text-center" style="width: 75px;"><i class="fas fa-sync-alt"></i></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($customers as $customer)
                            <tr>
                                <td>{{$customer->nit}}</td>
                                <td>{{$customer->name}}</td>
                                <td>{{$customer->contact}}</td>
                                <td>{{$customer->phone_contact}}</td>
                                <td>{{$customer->email_contact}}</td>
                                <td>@if($customer->state=="activo")
                                    <a class="label label-info">{{$customer->state}}</a>
                                    @else
                                    <span class="label label-danger">{{$customer->state}}</span>
                                    @endif
                                </td>
                                <td class="text-center">

                                    <a href="{{url('admin/customers/edit/'.$customer->slug)}}" data-toggle="tooltip" title="Editar cliente" class="btn btn-effect-ripple btn-xs btn-warning"><i class="fa fa-edit"></i></a>
                                    @if($customer->state=="activo")
                                    <a href="{{url('admin/customers/desactivate/'.$customer->id_customer)}}" data-toggle="tooltip" title="Desactivar cliente" class="btn btn-effect-ripple btn-xs btn-danger"><i class="fa fa-times"></i></a>
                                    @else
                                    <a href="{{url('admin/customers/activate/'.$customer->id_customer)}}" data-toggle="tooltip" title="Activar cliente" class="btn btn-effect-ripple btn-xs btn-success"><i class="fas fa-check"></i></a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
    </div>
@endsection

@section('aditionalScript')
<!-- Load and execute javascript code used only in this page -->
        <script src="{{asset('auth-panel/js/pages/uiTables.js')}}"></script>
        <script>$(function(){ UiTables.init(); });</script>
@endsection