@extends('admin.layout.auth')

@if(isset($route))
    @section('title', 'Editar ruta')
@else
    @section('title', 'Crear ruta ')
@endif

@section('aditionalStyle')
    <link href="{{asset('plugins/kartik-v-bootstrap-fileinput/css/fileinput.css')}}" media="all" rel="stylesheet" type="text/css"/>
    <style>
.kv-avatar .krajee-default.file-preview-frame,.kv-avatar .krajee-default.file-preview-frame:hover {
    margin: 0;
    padding: 0;
    border: none;
    box-shadow: none;
    text-align: center;
}
.kv-avatar {
    display: inline-block;
}
.kv-avatar .file-input {
    display: table-cell;
    width: 213px;
}
.kv-reqd {
    color: red;
    font-family: monospace;
    font-weight: normal;
}
</style>
@endsection

@section('content')
<div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-12">
                    <div class="header-section">
                        <h1>Ruta de {{$special->name}}</h1>
                        <div class="pull-right" style="padding: 5px;"><a href="{{url('admin/specials/routes/'.$special->slug)}}" class="btn btn-primary btn-sm">Volver a lista de rutas</a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="block">
            <!-- Labels on top Form Title -->
            <div class="block-title">
                <h2>Ingrese la información solicitada</h2>
                 @if(isset($route))
                    <div class="col-sm-6 col-sm-offset-6">
                        <div class="pull-right" style="padding: 5px;">

                            <a href="{{url('admin/specials/routes/images/'.$route->slug)}}" class="btn btn-primary btn-sm">Administrar multimedia  <i class="fa fa-images fa-lg"></i></a>
                        </div>
                    </div> 
                @else
                    <div class="col-sm-6 col-sm-offset-6">
                        <p>Luego de crear el especial se activarán las opciones de administración de multimedia y datos clave.</p>
                    </div>
                @endif
            </div>
			
            @if(isset($route))
            {!!Form::model($route,['url'=>['admin/specials/routes/update',$route->id_route],'method'=>'POST', 'class'=> 'form-horizontal', 'enctype' => 'multipart/form-data', 'id' => 'userForm', 'novalidate'])!!}
            @else
            {!!Form::open(['url'=>"admin/specials/routes/store/$special->slug", 'method'=>'POST', 'class'=> 'form-horizontal nuevo', 'enctype' => 'multipart/form-data', 'id' => 'userForm', 'novalidate'])!!}
            @endif
            <div class="col-sm-12">
                <div class="col-sm-4 text-center col-sm-offset-2">
                    <div class="kv-avatar">
                        <div class="form-group">
                        <label for="costo">Imagen de tarjeta <small>(Opcional)</small></label>
                            <div class="file-loading">

                                {!!Form::file('card_image',['id'=>'avatar-3'])!!}
                            </div>
                            <span class="label label-danger">{{$errors->first('card_image') }}</span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 text-center">
                    <div class="kv-avatar">
                        <div class="form-group">
                        <label for="costo">Imagen de introducción <small>(Opcional)</small></label>
                            <div class="file-loading">

                                {!!Form::file('introduction_image',['id'=>'avatar-4'])!!}
                            </div>
                            <span class="label label-danger">{{$errors->first('introduction_image') }}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="costo">Nombre</label>
                    {!!Form::text('name', null, ['class'=>'form-control', 'placeholder' => 'Inserte nombre de ruta', 'required'])!!}
                    <span class="label label-danger">{{$errors->first('name') }}</span>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="costo">Descripción corta</label>
                    {!!Form::text('sentence', null, ['class'=>'form-control', 'placeholder' => 'Inserte descripción corta', 'required'])!!}
                    <span class="label label-danger">{{$errors->first('sentence') }}</span>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="costo">Longitud(km)</label>
                        {!!Form::text('length', null, ['class'=>'form-control', 'placeholder' => 'ej: 181,72', 'required'])!!}
                        <span class="label label-danger">{{$errors->first('length') }}</span>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="costo">Dificultad</label>
                        {!!Form::select('difficulty', $levels, null,['class'=>'select-chosen'])!!}
                        <span class="label label-danger">{{$errors->first('difficulty') }}</span>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="costo">Altura máxima (m)</label>
                        {!!Form::text('highest_point', null, ['class'=>'form-control', 'placeholder' => 'Longitud', 'required'])!!}
                        <span class="label label-danger">{{$errors->first('highest_point') }}</span>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="costo">Altura mínima (m)</label>
                        {!!Form::text('lower_point', null, ['class'=>'form-control', 'placeholder' => 'Longitud', 'required'])!!}
                        <span class="label label-danger">{{$errors->first('lower_point') }}</span>
                    </div>
                </div>
            </div>
            <input type="hidden" name="type_startpoint" id="type_startpoint" @if(isset($route)) value="{{$route->type_startpoint}}" @endif>
            <div class="col-sm-12" id="departmentdiv" >
                <h4>Punto inicial</h4>
                @if(!isset($route))
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="location">Departamento</label>
                        {!!Form::select('departments', $departments, null, ['class'=>'select-chosen','id'=>'relation_departaments'])!!}
                    </div>
                </div>
                @else
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="location">Departamento</label>
                            <select name="departments" id="relation_departaments" class="select-chosen">
                                @foreach($departments as $id => $departmentF)
                                    <option value="{{$id}}" @if($id == $departmentStart->id_department) selected @endif>{{$departmentF}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                @endif
                <div class="col-sm-4 @if(!isset($route)) hidden @endif " id="municipalityDiv">
                    <div class="form-group">
                        <label for="location">Municipio</label>
                        <select name="fk_municipality" id="municipalities" class="select-chosen">
                            @if(isset($route))
                                @if($route->type_startpoint=="municipality")
                                    @foreach($municipalitiesStart as $id => $municipalityF)
                                        <option value="{{$id}}" @if($route->start_point==$id) selected @endif>{{$municipalityF}}</option>    
                                    @endforeach
                                @else
                                    @foreach($municipalitiesStart as $id => $municipalityF)
                                        <option value="{{$id}}">{{$municipalityF}}</option>    
                                    @endforeach
                                @endif
                            @endif
                        </select>
                    </div>
                </div>
                <div class="col-sm-4" >
                    <div class="form-group @if(!isset($route)) hidden @endif" id="siteDiv">
                        <label for="location">Sitio de interés</label>
                        <select name="fk_site" id="interesSite" class="select-chosen">
                            @if(isset($route))
                                @if($route->type_startpoint=="site")
                                    <option></option>
                                    @foreach($sitesStart as $id => $site)
                                        <option value="{{$id}}" @if($route->start_point==$id) selected @endif>{{$site}}</option>    
                                    @endforeach
                                @else
                                    @if(count($sitesStart)>0)
                                        <option value=""></option>
                                        @foreach($sitesStart as $id => $site)
                                            <option value="{{$id}}" >{{$site}}</option>    
                                        @endforeach
                                    @endif
                                @endif
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            <input type="hidden" name="type_endpoint" id="type_endpoint" @if(isset($route)) value="{{$route->type_endpoint}}" @endif>
            <div class="col-sm-12" id="departmentdiv2" >
                <h4>Punto final</h4>
                @if(!isset($service))
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="location">Departamento</label>
                        {!!Form::select('departments2', $departments, null, ['class'=>'select-chosen','id'=>'relation_departaments2'])!!}
                    </div>
                </div>
                @else
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="location">Departamento</label>
                            <select name="departments2" id="relation_departaments2" class="select-chosen">
                                @foreach($departments as $id2 => $department)
                                    <option value="{{$id2}}" @if($id2 == $departmentEnd->id_department) selected @endif >{{$department}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                @endif
                <div class="col-sm-4 @if(!isset($route)) hidden @endif " id="municipalityDiv2">
                    <div class="form-group">
                        <label for="location">Municipio</label>
                        <select name="fk_municipality2" id="municipalities2" class="select-chosen">
                            @if(isset($route))
                                @if($route->type_endpoint=="municipality")
                                    @foreach($municipalitiesEnd as $id => $municipalityF)
                                        <option value="{{$id}}" @if($route->end_point==$id) selected @endif>{{$municipalityF}}</option>    
                                    @endforeach
                                @else
                                    @foreach($municipalitiesEnd as $id => $municipalityF)
                                        <option value="{{$id}}" @if($route->end_point==$id) selected @endif>{{$municipalityF}}</option>    
                                    @endforeach
                                @endif
                            @endif
                        </select>
                    </div>
                </div>
                <div class="col-sm-4" >
                    <div class="form-group @if(!isset($route)) hidden @endif" id="siteDiv2">
                        <label for="location">Sitio de interés</label>
                        <select name="fk_site2" id="interesSite2" class="select-chosen">
                            @if(isset($route))
                                @if($route->type_endpoint=="site")
                                    <option></option>
                                    @foreach($sitesEnd as $id => $site)
                                        <option value="{{$id}}" @if($route->end_point == $id) selected @endif>{{$site}}</option>    
                                    @endforeach
                                @else
                                    @if(count($sitesEnd)>0)
                                        <option value=""></option>
                                        @foreach($sitesEnd as $id => $site)
                                            <option value="{{$id}}" >{{$site}}</option>  
                                        @endforeach
                                    @endif
                                @endif
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="costo">Url mapa</label>
                    {!!Form::text('url_map', null, ['class'=>'form-control', 'placeholder' =>'URL map', 'required'])!!}
                    <span class="label label-danger">{{$errors->first('url_map') }}</span>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                <label >Palabras clave <small>Separadas por comas (,)</small></label>
                    {!!Form::text('keywords',null,['class'=>'form-control input-tags', 'placeholder'=>'Palabras clave'])!!}
                    <span class="label label-danger">{{$errors->first('keywords') }}</span>
                </div>
            </div>
            <div class="col-sm-12 rutas">
                <div class="form-group">
                    <label for="costo">Vídeo (Opcional)</label>
                    {!!Form::text('video', null, ['class'=>'form-control', 'placeholder' => 'Inserte url de video en caso de tenerlo', 'required'])!!}
                    <span class="label label-danger">{{$errors->first('video') }}</span>
                </div>
            </div>
            <div class="col-sm-12 form-group">
                <label for="duration">Descripción</label>
                <div class="form-group">
                    {!! Form::textarea('description',null,['class'=>'ckeditor']) !!}
                    <span class="label label-danger">{{$errors->first('description') }}</span>
                </div>
            </div>
            <div class="col-sm-12 rutas">
                <div class="col-sm-4 text-center col-sm-offset-4">
                    <div class="kv-avatar">
                        <div class="form-group">
                        <label for="costo">Imagen de descripción <small>(Opcional)</small></label>
                            <div class="file-loading">

                                {!!Form::file('description_image',['id'=>'avatar-1'])!!}
                            </div>
                            <span class="label label-danger">{{$errors->first('description_image') }}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 form-group">
                <label for="duration">Recomendaciones</label>
                <div class="form-group">
                    {!! Form::textarea('recommendation',null,['class'=>'ckeditor']) !!}
					<span class="label label-danger">{{$errors->first('recommendation') }}</span>
                </div>
	        </div>
            <div class="col-sm-12 rutas">
                <div class="col-sm-4 text-center col-sm-offset-4">
                    <div class="kv-avatar">
                        <div class="form-group">
                        <label for="costo">Imagen de recomendaciones <small>(Opcional)</small></label>
                            <div class="file-loading">

                                {!!Form::file('recommendation_image',['id'=>'avatar-2'])!!}
                            </div>
                            <span class="label label-danger">{{$errors->first('recommendation_image') }}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 rutas">
                <label for="duration">Texto de imágenes</label>
                <div class="form-group">
                    {!! Form::textarea('text_images',null,['class'=>'ckeditor']) !!}
                    <span class="label label-danger">{{$errors->first('text_images') }}</span>
                </div>
            </div>
            <div class="form-group form-actions" align="center">
                <button type="submit" class="btn btn-effect-ripple btn-primary">Guardar</button>
                <a href="{{url('admin/routes')}}" class="btn btn-effect-ripple btn-danger">Cancelar</a>
            </div>
            {!!Form::close()!!}
        </div>
</div>
@endsection

@section('aditionalScript')

    <script src="{{asset('auth-panel/js/plugins/ckeditor/ckeditor.js')}}"></script>

    <script src="{{asset('auth-panel/js/pages/formsComponents.js')}}"></script>
    <script>$(function(){ FormsComponents.init(); });</script>

     <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/plugins/sortable.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/fileinput.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/locales/fr.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/locales/es.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/themes/explorer-fa/theme.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/themes/fa/theme.js')}}" type="text/javascript"></script>

    <script>
        $(document).on('ready', function() {
            $("#input-20").fileinput({
                browseClass: "btn btn-primary btn-block",
                browseLabel: "Seleccionar imagen",
                showRemove: false,
                showUpload: false

            });
        });
        $("#avatar-1").fileinput({
            overwriteInitial: true,
            maxFileSize: 1500,
            showClose: false,
            showCaption: false,
            browseLabel: '',
            removeLabel: '',
            browseIcon: '<i class="fab fa-searchengin"></i>',
            removeIcon: '<i class="far fa-times-circle"></i>',
            viewIcon:'<i class="far fa-times-circle"></i>',
            removeTitle: 'Cancel or reset changes',
            elErrorContainer: '#kv-avatar-errors-1',
            msgErrorClass: 'alert alert-block alert-danger',
            @if(isset($route))
            defaultPreviewContent: '<img src="{{asset($route->description_image)}}" alt="Imagen de descripción" width="50%">',
                @if($route->description_image)
                defaultPreviewContent: '<img src="{{asset($route->description_image)}}" alt="Imagen de descripción" width="50%">',
                @else
                defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Imagen de descripción" width="50%">',
                @endif
            @else
            defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Imagen de descripción" width="50%">',
            @endif
            layoutTemplates: {main2: '{preview} ' + ' {remove} {browse}'},
            allowedFileExtensions: ["jpg", "png", "gif"]
        });
        $("#avatar-2").fileinput({
            overwriteInitial: true,
            maxFileSize: 1500,
            showClose: false,
            showCaption: false,
            browseLabel: '',
            removeLabel: '',
            browseIcon: '<i class="fab fa-searchengin"></i>',
            removeIcon: '<i class="far fa-times-circle"></i>',
            viewIcon:'<i class="far fa-times-circle"></i>',
            removeTitle: 'Cancel or reset changes',
            elErrorContainer: '#kv-avatar-errors-1',
            msgErrorClass: 'alert alert-block alert-danger',
            @if(isset($route))
            defaultPreviewContent: '<img src="{{asset($route->recommendation_image)}}" alt="Imagen de recomendación" width="50%">',
                @if($route->recommendation_image)
                defaultPreviewContent: '<img src="{{asset($route->recommendation_image)}}" alt="Imagen de recomendación" width="50%">',
                @else
                defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Imagen de recomendación" width="50%">',
                @endif
            @else
            defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Imagen de recomendación" width="50%">',
            @endif
            layoutTemplates: {main2: '{preview} ' + ' {remove} {browse}'},
            allowedFileExtensions: ["jpg", "png", "gif"]
        });
         $("#avatar-3").fileinput({
            overwriteInitial: true,
            maxFileSize: 1500,
            showClose: false,
            showCaption: false,
            browseLabel: '',
            removeLabel: '',
            browseIcon: '<i class="fab fa-searchengin"></i>',
            removeIcon: '<i class="far fa-times-circle"></i>',
            viewIcon:'<i class="far fa-times-circle"></i>',
            removeTitle: 'Cancel or reset changes',
            elErrorContainer: '#kv-avatar-errors-1',
            msgErrorClass: 'alert alert-block alert-danger',
            @if(isset($route))
            defaultPreviewContent: '<img src="{{asset($route->card_image)}}" alt="Imagen de tarjeta" width="50%">',
                @if($route->card_image)
                defaultPreviewContent: '<img src="{{asset($route->card_image)}}" alt="Imagen de tarjeta" width="50%">',
                @else
                defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Imagen de tarjeta" width="50%">',
                @endif
            @else
            defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Imagen de tarjeta" width="50%">',
            @endif
            layoutTemplates: {main2: '{preview} ' + ' {remove} {browse}'},
            allowedFileExtensions: ["jpg", "png", "gif"]
        });
          $("#avatar-4").fileinput({
            overwriteInitial: true,
            maxFileSize: 1500,
            showClose: false,
            showCaption: false,
            browseLabel: '',
            removeLabel: '',
            browseIcon: '<i class="fab fa-searchengin"></i>',
            removeIcon: '<i class="far fa-times-circle"></i>',
            viewIcon:'<i class="far fa-times-circle"></i>',
            removeTitle: 'Cancel or reset changes',
            elErrorContainer: '#kv-avatar-errors-1',
            msgErrorClass: 'alert alert-block alert-danger',
            @if(isset($route))
                @if($route->introduction_image)
                defaultPreviewContent: '<img src="{{asset($route->introduction_image)}}" alt="Imagen de introducción" width="50%">',
                @else
                defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Imagen de introducción" width="50%">',
                @endif
            @else
            defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Imagen de introducción" width="50%">',
            @endif
            layoutTemplates: {main2: '{preview} ' + ' {remove} {browse}'},
            allowedFileExtensions: ["jpg", "png", "gif"]
        });

        $('#relation_departaments').change(function()
        {
            $("#type_startpoint").val('municipality');
            $("#siteDiv").addClass('hidden');
            relationalDepartments = $('#relation_departaments').val();
            var token = "{{csrf_token()}}";
            var route="/admin/specials/routes/getreladepartment";            
            $.ajax({
                url: route,
                headers: {'X-CSRF-TOKEN': token},
                type: 'POST',
                dataType: 'json',
                data:{relationalDepartments},
                complete: function(transport)
                {
                    data = transport.responseJSON;
                    municipalities = data['municipalities'];
                    // //MUNICIPIOS
                    $("#municipalityDiv").removeClass('hidden');
                    $('#municipalities option').remove();
                    
                    $("#municipalities").append('<option></option>').trigger("chosen:updated");

                    for(x in municipalities)
                    {
                        $("#municipalities").append('<option value="'+municipalities[x].id_municipality+'">'+municipalities[x].municipality_name+'</option>').trigger("chosen:updated");
                    }
                }
              });
        });
        $('#municipalities').change(function()
        {
            municipality = $('#municipalities').val();
            var token = "{{csrf_token()}}";
            var route="/admin/specials/routes/getrelamunicipality";            
            $.ajax({
                url: route,
                headers: {'X-CSRF-TOKEN': token},
                type: 'get',
                dataType: 'json',
                data:{municipality},
                complete: function(transport)
                {
                    data = transport.responseJSON;
                    interestsites = data['interestSites'];
                    if(interestsites.length >0)
                    {
                        $("#siteDiv").removeClass('hidden');
                        $('#interesSite option').remove();

                        for(x in interestsites)
                        {
                            $("#interesSite").append('<option value="'+interestsites[x].id_site+'">'+interestsites[x].site_name+'</option>').trigger("chosen:updated");
                        }
                    }
                    else{             
                        $("#siteDiv").addClass('hidden');
                    }
                }
              });
        });
        $("#interesSite").change(function()
        {
            if($(this).val()!='')
            {
                $("#type_startpoint").val('site');
            }
            else
            {
                $("#type_startpoint").val('municipality');
            }
        });

        $('#relation_departaments2').change(function()
        {
            $("#type_endpoint").val('municipality');
            $("#siteDiv").addClass('hidden');
            relationalDepartments = $('#relation_departaments2').val();
            var token = "{{csrf_token()}}";
            var route="/admin/specials/routes/getreladepartment";            
            $.ajax({
                url: route,
                headers: {'X-CSRF-TOKEN': token},
                type: 'POST',
                dataType: 'json',
                data:{relationalDepartments},
                complete: function(transport)
                {
                    data = transport.responseJSON;
                    municipalities = data['municipalities'];
                    // //MUNICIPIOS
                    $("#municipalityDiv2").removeClass('hidden');
                    $('#municipalities2 option').remove();
                    
                    $("#municipalities2").append('<option></option>').trigger("chosen:updated");

                    for(x in municipalities)
                    {
                        $("#municipalities2").append('<option value="'+municipalities[x].id_municipality+'">'+municipalities[x].municipality_name+'</option>').trigger("chosen:updated");
                    }
                }
              });
        });
        $('#municipalities2').change(function()
        {
            municipality = $('#municipalities2').val();
            var token = "{{csrf_token()}}";
            var route="/admin/specials/routes/getrelamunicipality";            
            $.ajax({
                url: route,
                headers: {'X-CSRF-TOKEN': token},
                type: 'get',
                dataType: 'json',
                data:{municipality},
                complete: function(transport)
                {
                    data = transport.responseJSON;
                    interestsites = data['interestSites'];
                    if(interestsites.length >0)
                    {
                        $("#siteDiv2").removeClass('hidden');
                        $('#interesSite2 option').remove();
                        $("#interesSite2").append('<option></option>').trigger("chosen:updated");
                        for(x in interestsites) 
                        {
                            $("#interesSite2").append('<option value="'+interestsites[x].id_site+'">'+interestsites[x].site_name+'</option>').trigger("chosen:updated");
                        }
                    }
                    else{             
                        $("#siteDiv2").addClass('hidden');
                    }
                }
              });
        });
        $("#interesSite2").change(function()
        {
            if($(this).val()!='')
            {
                $("#type_endpoint").val('site');
            }
            else
            {
                $("#type_endpoint").val('municipality');
            }
        });
    </script>
@endsection