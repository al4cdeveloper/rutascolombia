@extends('admin.layout.auth')

@section('title', 'Rutas del especial')


@section('content')
    <div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-12">
                    <div class="header-section">
                        <h1>Rutas del especial {{$special->name}}</h1>
                        <div class="pull-right" style="padding: 5px;"><a href="{{url('admin/specials/edit/'.$special->slug)}}" class="btn btn-primary btn-sm">Volver al especial</a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="block full">
            <div class="block-title">
                <h2>Rutas</h2>
                <div class="pull-right" style="padding: 5px;"><a href="{{url('admin/specials/routes/new/'.$special->slug)}}" class="btn btn-primary btn-sm"><i class="far fa-plus-square fa-lg"></i></a></div>
            </div>
            <div class="table-responsive">
                <table id="example-datatable" class="table table-striped table-bordered table-vcenter">
                    <thead>
                        <tr>
                            <th class="text-center" style="width: 50px;">Nombre</th>
                            <th>Dificultad</th>
                            <th>Punto inicial</th>
                            <th>Punto final</th>
                            <th style="width: 120px;">Estado</th>
                            <th class="text-center" style="width: 75px;"><i class="fas fa-sync-alt"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($special->routes as $route)
                            <tr>
                                <td>{{$route->name}}</td>
                                <td>{{$route->difficulty}}</td>
                                <td>{{$route->type_startpoint == 'municipality' ? $route->startMunicipality->municipality_name : $route->startSite->site_name}}</td>
                                <td>{{$route->type_endpoint == 'municipality' ? $route->endMunicipality->municipality_name :  $route->endSite->site_name}}</td>
                                <td>{{$route->state}}</td>
                                <td>  
                                    <a href="{{url('admin/specials/routes/edit/'.$route->slug)}}" data-toggle="tooltip" title="Editar ruta" class="btn btn-effect-ripple btn-xs btn-warning"><i class="fa fa-edit"></i></a>
                                    <a href="{{url('admin/specials/routes/points/'.$route->slug)}}" data-toggle="tooltip" title="Administrar puntos" class="btn btn-effect-ripple btn-xs btn-info"><i class="fab fa-deviantart"></i></a>
                                    @if($route->state=="activo")
                                    <a href="{{url('admin/specials/routes/desactivate/'.$route->slug)}}" data-toggle="tooltip" title="Desactivar ruta" class="btn btn-effect-ripple btn-xs btn-danger"><i class="fa fa-times"></i></a>
                                    @else
                                    <a href="{{url('admin/specials/routes/activate/'.$route->slug)}}" data-toggle="tooltip" title="Activar ruta" class="btn btn-effect-ripple btn-xs btn-success"><i class="fas fa-check"></i></a>
                                    @endif</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('aditionalScript')
<!-- Load and execute javascript code used only in this page -->
        <script src="{{asset('auth-panel/js/pages/uiTables.js')}}"></script>
        <script>$(function(){ UiTables.init(); });</script>
@endsection