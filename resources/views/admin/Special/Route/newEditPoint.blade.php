@extends('admin.layout.auth')

@if(isset($point))
    @section('title', 'Editar punto')
@else
    @section('title', 'Crear punto')
@endif

@section('content')
<div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-12">
                    <div class="header-section">
                        <h1>Ruta de {{$route->name}}</h1>
                        <div class="pull-right" style="padding: 5px;"><a href="{{url('admin/specials/routes/'.$route->slug)}}" class="btn btn-primary btn-sm">Volver a lista de rutas</a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="block">
            <!-- Labels on top Form Title -->
            <div class="block-title">
                <h2>Ingrese la información solicitada</h2>
            </div>
			
            @if(isset($point))
            {!!Form::model($point,['url'=>['admin/specials/routes/points/update',$point->id_point],'method'=>'POST', 'class'=> 'form-horizontal', 'enctype' => 'multipart/form-data', 'id' => 'userForm', 'novalidate'])!!}
            @else
            {!!Form::open(['url'=>"admin/specials/routes/points/store/$route->slug", 'method'=>'POST', 'class'=> 'form-horizontal nuevo', 'enctype' => 'multipart/form-data', 'id' => 'userForm', 'novalidate'])!!}
            @endif
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="costo">Titulo</label>
                    {!!Form::text('title', null, ['class'=>'form-control', 'placeholder' => 'Inserte titulo', 'required'])!!}
                    <span class="label label-danger">{{$errors->first('title') }}</span>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="costo">Subtitulo</label>
                    {!!Form::text('subtitle', null, ['class'=>'form-control', 'placeholder' => 'Inserte subtitulo', 'required'])!!}
                    <span class="label label-danger">{{$errors->first('subtitle') }}</span>
                </div>
            </div>
            <div class="col-sm-12 form-group">
                <label for="duration">Descripción</label>
                <div class="form-group">
                    {!! Form::textarea('description',null,['class'=>'ckeditor']) !!}
                    <span class="label label-danger">{{$errors->first('description') }}</span>
                </div>
            </div>
            <div class="form-group form-actions" align="center">
                <button type="submit" class="btn btn-effect-ripple btn-primary">Guardar</button>
                <a href="{{url('admin/routes')}}" class="btn btn-effect-ripple btn-danger">Cancelar</a>
            </div>
            {!!Form::close()!!}
        </div>
</div>
@endsection

@section('aditionalScript')

    <script src="{{asset('auth-panel/js/plugins/ckeditor/ckeditor.js')}}"></script>

    <script src="{{asset('auth-panel/js/pages/formsComponents.js')}}"></script>
    <script>$(function(){ FormsComponents.init(); });</script>

@endsection