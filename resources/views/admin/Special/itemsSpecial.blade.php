@extends('admin.layout.auth')

@section('title', 'Items de especial')


@section('content')
    <div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-12">
                    <div class="header-section">
                        <h1>Items del especial {{$special->name}}</h1>
                        <div class="pull-right" style="padding: 5px;"><a href="{{url('admin/specials/edit/'.$special->slug)}}" class="btn btn-primary btn-sm">Volver al especial</a></div>
                    </div>
                </div>
            </div>
        </div>
            <div class="block full">

                <div class="block-title">
                    <h2>Items</h2>
                </div>
                <div class="container">
                    {{Form::open(['url'=>"/admin/specials/items/store/$special->id_special",'method'=>'post','class'=> 'form-horizontal'])}}
                        <div class="col-md-5">
                            <label for="costo">Titulo</label>
                            {!!Form::text('title', null, ['class'=>'form-control', 'placeholder' => 'Titulo', 'required'])!!}
                            <span class="label label-danger">{{$errors->first('title') }}</span>
                        </div>
                        <div class="col-md-5">
                            <label for="duration">Descripción</label>
                            <div class="form-group">
                                {!! Form::textarea('description',null,['class'=>'form-control','rows'=>2,'placeholder'=>'ingrese Descripción']) !!}
                                <span class="label label-danger">{{$errors->first('description') }}</span>
                            </div>
                        </div>
                        <div class="col-md-2">
                            {{Form::submit('Registrar',['class'=>'btn btn-primary'])}}
                        </div>
                    {{Form::close()}}
                </div>
                <div class="table-responsive">
                    <table id="example-datatable" class="table table-striped table-bordered table-vcenter">
                        <thead>
                            <tr>
                                <th class="text-center" style="width: 120px;">Nombre</th>
                                <th >Descripción</th>
                                <th class="text-center" style="width: 75px;"><i class="fas fa-sync-alt"></i></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($special->items as $item)
                                <tr>
                                    <td>{{$item->title}}</td>
                                    <td>{{$item->description}}</td>
                                    <td>
                                        <a href="#edit{{$item->id_item}}" data-toggle="modal" title="Actualizar" class="btn btn-effect-ripple btn-xs btn-warning"><i class="fa fa-edit"></i></a>
                                        
                                        <a href="{{url('admin/specials/items/destroy/'.$item->id_item)}}" data-toggle="tooltip" title="Eliminar item" class="btn btn-effect-ripple btn-xs btn-danger"><i class="fa fa-times"></i></a></td>
                                </tr>
                                <!-- Regular Modal -->
                                <div id="edit{{$item->id_item}}" class="modal" tabindex="-1" role="dialog" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            {!!Form::model($item,['url'=>"/admin/specials/items/update/$item->id_item",'method'=>'post'])!!}
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h3 class="modal-title"><strong>Actualizar item</strong></h3>
                                            </div>
                                            <div class="modal-body form-horizontal">
                                                <div class="col-md-12">
                                                    <label for="costo">Titulo</label>
                                                    {!!Form::text('title', null, ['class'=>'form-control', 'placeholder' => 'Titulo', 'required'])!!}
                                                    <span class="label label-danger">{{$errors->first('title') }}</span>
                                                </div>
                                                <div class="col-md-12">
                                                    <label for="duration">Descripción</label>
                                                    <div class="form-group">
                                                        {!! Form::textarea('description',null,['class'=>'form-control','rows'=>2,'placeholder'=>'ingrese Descripción']) !!}
                                                        <span class="label label-danger">{{$errors->first('description') }}</span>
                                                    </div>
                                                </div>                                             
                                            </div>
                                            <div class="modal-footer">
                                                {{Form::submit('Actualizar',['class'=>'btn btn-primary'])}}
                                                <button type="button" class="btn btn-effect-ripple btn-danger" data-dismiss="modal">Cancelar</button>
                                            </div>
                                            {{Form::close()}}
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
    </div>
@endsection

@section('aditionalScript')
<!-- Load and execute javascript code used only in this page -->
        <script src="{{asset('auth-panel/js/pages/uiTables.js')}}"></script>
        <script>$(function(){ UiTables.init(); });</script>
@endsection