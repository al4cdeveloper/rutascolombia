@extends('admin.layout.auth')

@section('title', 'Items de especial')
@section('aditionalStyle')
    <link href="{{asset('plugins/kartik-v-bootstrap-fileinput/css/fileinput.css')}}" media="all" rel="stylesheet" type="text/css"/>
    <style>
.kv-avatar .krajee-default.file-preview-frame,.kv-avatar .krajee-default.file-preview-frame:hover {
    margin: 0;
    padding: 0;
    border: none;
    box-shadow: none;
    text-align: center;
}
.kv-avatar {
    display: inline-block;
}
.kv-avatar .file-input {
    display: table-cell;
    width: 213px;
}
.kv-reqd {
    color: red;
    font-family: monospace;
    font-weight: normal;
}
</style>
@endsection

@section('content')
 <div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-12">
                    <div class="header-section">
                        <h1>Ruta de {{$special->name}}</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="block">
            <!-- Labels on top Form Title -->
            <div class="block-title">
                <h2>Ingrese la información solicitada</h2>
            </div>
            
            @if(isset($item))
            {!!Form::model($item,['url'=>['admin/specials/detailitems/update',$item->id_detail_item],'method'=>'POST', 'class'=> 'form-horizontal', 'enctype' => 'multipart/form-data', 'id' => 'userForm', 'novalidate'])!!}
            @else
            {!!Form::open(['url'=>"admin/specials/detailitems/store/$special->id_special", 'method'=>'POST', 'class'=> 'form-horizontal nuevo', 'enctype' => 'multipart/form-data', 'id' => 'userForm', 'novalidate'])!!}
            @endif
            <div class="col-sm-12">
                <div class="col-sm-4 text-center col-sm-offset-4">
                    <div class="kv-avatar">
                        <div class="form-group">
                        <label for="costo">Imagen de item (opcional)</label>
                            <div class="file-loading">

                                {!!Form::file('image_link',['id'=>'avatar-1'])!!}
                            </div>
                            <span class="label label-danger">{{$errors->first('image_link') }}</span>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" name="type_relation" id="type_relation" @if(isset($item)) value="{{$item->type_relation}}" @endif>
            <div class="col-sm-12" id="departmentdiv" >
                <h4>Punto de referencia</h4>
                @if(!isset($item))
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="location">Departamento</label>
                        {!!Form::select('departments', $departments, null, ['class'=>'select-chosen','id'=>'relation_departaments'])!!}
                    </div>
                </div>
                @else
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="location">Departamento</label>
                            <select name="departments" id="relation_departaments" class="select-chosen">
                                @foreach($departments as $id => $departmentF)
                                    <option value="{{$id}}" @if($id == $departmentStart->id_department) selected @endif>{{$departmentF}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                @endif
                <div class="col-sm-4 @if(!isset($item)) hidden @endif " id="municipalityDiv">
                    <div class="form-group">
                        <label for="location">Municipio</label>
                        <select name="fk_municipality" id="municipalities" class="select-chosen">
                            @if(isset($item))
                                @if($item->type_relation=="municipality")
                                    @foreach($municipalitiesStart as $id => $municipalityF)
                                        <option value="{{$id}}" @if($item->fk_relation==$id) selected @endif>{{$municipalityF}}</option>    
                                    @endforeach
                                @else
                                    @foreach($municipalitiesStart as $id => $municipalityF)
                                        <option value="{{$id}}">{{$municipalityF}}</option>    
                                    @endforeach
                                @endif
                            @endif
                        </select>
                    </div>
                </div>
                <div class="col-sm-4" >
                    <div class="form-group @if(!isset($item)) hidden @endif" id="siteDiv">
                        <label for="location">Sitio de interés</label>
                        <select name="fk_site" id="interesSite" class="select-chosen">
                            @if(isset($item))
                                @if($item->type_relation=="site")
                                    <option></option>
                                    @foreach($sitesStart as $id => $site)
                                        <option value="{{$id}}" @if($item->fk_relation==$id) selected @endif>{{$site}}</option>    
                                    @endforeach
                                @else
                                    @if(count($sitesStart)>0)
                                        <option value=""></option>
                                        @foreach($sitesStart as $id => $site)
                                            <option value="{{$id}}" >{{$site}}</option>    
                                        @endforeach
                                    @endif
                                @endif
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 form-group">
                <label for="duration">Descripción</label>
                <div class="form-group">
                    {!! Form::textarea('description',null,['class'=>'ckeditor']) !!}
                    <span class="label label-danger">{{$errors->first('description') }}</span>
                </div>
            </div>
            <div class="form-group form-actions" align="center">
                <button type="submit" class="btn btn-effect-ripple btn-primary">Guardar</button>
                <a href="{{url('admin/routes')}}" class="btn btn-effect-ripple btn-danger">Cancelar</a>
            </div>
            {!!Form::close()!!}
        </div>
</div>
@endsection

@section('aditionalScript')

    <script src="{{asset('auth-panel/js/plugins/ckeditor/ckeditor.js')}}"></script>

    <script src="{{asset('auth-panel/js/pages/formsComponents.js')}}"></script>
    <script>$(function(){ FormsComponents.init(); });</script>

     <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/plugins/sortable.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/fileinput.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/locales/fr.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/locales/es.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/themes/explorer-fa/theme.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/themes/fa/theme.js')}}" type="text/javascript"></script>

    <script>
    $("#avatar-1").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        browseLabel: '',
        removeLabel: '',
        browseIcon: '<i class="fab fa-searchengin"></i>',
        removeIcon: '<i class="far fa-times-circle"></i>',
        viewIcon:'<i class="far fa-times-circle"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        @if(isset($item))
            @if($item->image_link!='')
            defaultPreviewContent: '<img src="{{asset($item->image_link)}}" alt="Avatar por defecto" width="50%">',
            @else
            defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Avatar por defecto" width="50%">',
            @endif
        @else
        defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Avatar por defecto" width="50%">',
        @endif
        layoutTemplates: {main2: '{preview} ' + ' {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "gif"]
    });

    $('#relation_departaments').change(function()
        {
            $("#type_relation").val('municipality');
            $("#siteDiv").addClass('hidden');
            relationalDepartments = $('#relation_departaments').val();
            var token = "{{csrf_token()}}";
            var route="/admin/specials/routes/getreladepartment";            
            $.ajax({
                url: route,
                headers: {'X-CSRF-TOKEN': token},
                type: 'POST',
                dataType: 'json',
                data:{relationalDepartments},
                complete: function(transport)
                {
                    data = transport.responseJSON;
                    municipalities = data['municipalities'];
                    // //MUNICIPIOS
                    $("#municipalityDiv").removeClass('hidden');
                    $('#municipalities option').remove();
                    
                    $("#municipalities").append('<option></option>').trigger("chosen:updated");

                    for(x in municipalities)
                    {
                        $("#municipalities").append('<option value="'+municipalities[x].id_municipality+'">'+municipalities[x].municipality_name+'</option>').trigger("chosen:updated");
                    }
                }
              });
        });
        $('#municipalities').change(function()
        {
            municipality = $('#municipalities').val();
            var token = "{{csrf_token()}}";
            var route="/admin/specials/routes/getrelamunicipality";            
            $.ajax({
                url: route,
                headers: {'X-CSRF-TOKEN': token},
                type: 'get',
                dataType: 'json',
                data:{municipality},
                complete: function(transport)
                {
                    data = transport.responseJSON;
                    interestsites = data['interestSites'];
                    if(interestsites.length >0)
                    {
                        $("#siteDiv").removeClass('hidden');
                        $('#interesSite option').remove();
                        $("#interesSite").append('<option></option>').trigger("chosen:updated");
                        for(x in interestsites)
                        {
                            $("#interesSite").append('<option value="'+interestsites[x].id_site+'">'+interestsites[x].site_name+'</option>').trigger("chosen:updated");
                        }
                    }
                    else{             
                        $("#siteDiv").addClass('hidden');
                    }
                }
              });
        });
        $("#interesSite").change(function()
        {
            if($(this).val()!='')
            {
                $("#type_relation").val('site');
            }
            else
            {
                $("#type_relation").val('municipality');
            }
        });
</script>
@endsection