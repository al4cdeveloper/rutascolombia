@extends('admin.layout.auth')

@section('title', 'Listado de Especiales')
@section('additionalStyle')
    <link rel="stylesheet" href="{{asset('plugins/dist/sweetalert2.min.css')}}">
@endsection


@section('content')
    <div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>Especiales</h1>
                    </div>
                </div>
            </div>
        </div>
            <div class="block full">
                <div class="block-title">
                    <h2>Especiales</h2>
                    <div class="pull-right" style="padding: 5px;"><a href="{{url('admin/specials/create')}}" class="btn btn-primary btn-sm"><i class="far fa-plus-square fa-lg"></i></a></div>
                </div>
                <div class="table-responsive">
                    <table id="example-datatable" class="table table-striped table-bordered table-vcenter">
                        <thead>
                            <tr>
                                <th class="text-center">Nombre</th>
                                <th>Destacado</th>
                                <th style="width: 120px;">Estado</th>
                                <th class="text-center" style="width: 75px;"><i class="fas fa-sync-alt"></i></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($specials as $special)
                            <tr>
                                <td>{{$special->name}}</td>
                                 <td>
                                    <label class="switch switch-warning" onclick="change({{$special->id_special}})"><input type="checkbox" @if($special->outstanding) checked @endif><span></span></label>
                                </td>
                                <td>@if($special->state=="activo")
                                    <a class="label label-info">{{$special->state}}</a>
                                    @else
                                    <span class="label label-danger">{{$special->state}}</span>
                                    @endif
                                </td>
                                <td class="text-center">

                                    <a href="{{url('admin/specials/edit/'.$special->slug)}}" data-toggle="tooltip" title="Editar ruta" class="btn btn-effect-ripple btn-xs btn-warning"><i class="fa fa-edit"></i></a>
                                    @if($special->state=="activo")
                                    <a href="{{url('admin/specials/desactivate/'.$special->id_special)}}" data-toggle="tooltip" title="Desactivar ruta" class="btn btn-effect-ripple btn-xs btn-danger"><i class="fa fa-times"></i></a>
                                    @else
                                    <a href="{{url('admin/specials/activate/'.$special->id_special)}}" data-toggle="tooltip" title="Activar ruta" class="btn btn-effect-ripple btn-xs btn-success"><i class="fas fa-check"></i></a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
    </div>
@endsection

@section('aditionalScript')
<!-- Load and execute javascript code used only in this page -->
        <script src="{{asset('auth-panel/js/pages/uiTables.js')}}"></script>
        <script src="{{asset('plugins/sweetalert2/dist/sweetalert2.all.min.js')}}"></script>
        <script>$(function(){ UiTables.init(); });</script>
        <script>
             var funcionando = true;

        function change(id)
        {
            var cod = id;
            var token = '{{csrf_token()}}';


            if(funcionando)
            {
                funcionando=false;

                setTimeout(function() {
                    $.ajax(
                    {
                        url:'/admin/specials/change',
                        headers:{'X-CSRF-TOKEN':token},
                        type:'POST',
                        dataType:'json',
                        data:{cod},
                        complete:function(transport)
                        {
                            response = transport.responseText;
                            if(response == 'ok')
                            {
                                swal({
                                  type: 'success',
                                  title: 'Se ha cambiado el estado del evento.',
                                  timer: 1500
                                })
                            }
                            else
                            {
                                swal({
                                  type: 'error',
                                  title: 'Se ha presentado un error, inténtalo más tarde.',
                                  timer: 1500
                                })
                            }
                            funcionando=true;
                        }
                    }); 
                }, 500);
                

            }
              
        }
        </script>
@endsection