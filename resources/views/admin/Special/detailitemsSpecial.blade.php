@extends('admin.layout.auth')

@section('title', 'Items de especial')
@section('content')
    <div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-12">
                    <div class="header-section">
                        <h1>Items del especial {{$special->name}}</h1>
                        <div class="pull-right" style="padding: 5px;"><a href="{{url('admin/specials/edit/'.$special->slug)}}" class="btn btn-primary btn-sm">Volver al especial</a></div>
                    </div>
                </div>
            </div>
        </div>
            <div class="block full">

                <div class="block-title">
                    <h2>Items</h2>
                     <div class="pull-right" style="padding: 5px;"><a href="{{url('admin/specials/detailitems/create/'.$special->slug)}}" class="btn btn-primary btn-sm"><i class="far fa-plus-square fa-lg"></i></a></div>
                </div>
                <div class="table-responsive">
                    <table id="example-datatable" class="table table-striped table-bordered table-vcenter">
                        <thead>
                            <tr>
                                <th class="text-center" style="width: 120px;">Codigo</th>
                                <th>Relación</th>
                                <th class="text-center" style="width: 75px;"><i class="fas fa-sync-alt"></i></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($special->detailItems as $item)
                                <tr>
                                    <td>{{$item->id_detail_item}}</td>
                                    <td>{{$item->type_relation == 'municipality' ? 'Municipio - '.$item->municipality->municipality_name : 'Sitio de interés - '.$item->site->site_name}}</td>
                                    <td>
                                        <a href="{{url('admin/specials/detailitems/edit/'.$item->id_detail_item)}}" data-toggle="tooltip" title="Actualizar" class="btn btn-effect-ripple btn-xs btn-warning"><i class="fa fa-edit"></i></a>
                                        <a href="{{url('admin/specials/detailitems/destroy/'.$item->id_detail_item)}}" data-toggle="tooltip" title="Eliminar item" class="btn btn-effect-ripple btn-xs btn-danger"><i class="fa fa-times"></i></a></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
    </div>
@endsection

@section('aditionalScript')

<!-- Load and execute javascript code used only in this page -->
        <script src="{{asset('auth-panel/js/pages/uiTables.js')}}"></script>
        <script>$(function(){ UiTables.init(); });</script>
@endsection