@extends('admin.layout.auth')

@if(isset($service))
    @section('title', 'Actualizar servicio')
@else
    @section('title', 'Crear servicio')
@endif
@section('aditionalStyle')
<style>
    .btn-file
    {

    }
</style>
@endsection
@section('content')
<div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>Regiones</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="block">
            <!-- Labels on top Form Title -->
            <div class="block-title">
                <h2>Ingrese la información solicitada</h2>
            </div>
            @if(isset($service))
			{!!Form::model($service,['url'=>['admin/activities/service/update',$service->id_service],'method'=>'POST', 'class'=> 'form-horizontal', 'enctype' => 'multipart/form-data', 'id' => 'userForm', 'novalidate'])!!}
			@else
			{!!Form::open(['url'=>'admin/activities/service/store', 'method'=>'POST', 'class'=> 'form-horizontal nuevo', 'enctype' => 'multipart/form-data', 'id' => 'userForm', 'novalidate'])!!}
			@endif
            <div class="col-sm-12">
                <div class="col-sm-3 text-center col-sm-offset-4">
                    <div class="kv-avatar">
                        <div class="form-group">
                        <label for="costo">Imagen principal de servicio</label>
                            <div class="file-loading">

                                {!!Form::file('link_image',['id'=>'avatar-1','style'=>'display: initial!important;'])!!}
                            </div>
                            <span class="label label-danger">{{$errors->first('link_image') }}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                    <div class="form-group">
                        <label for="costo">Categoría de servicio</label>
                        {!!Form::select('fk_service_category', $categories, null, ['class'=>'select-chosen'])!!}
                    </div>
            </div>
            <div class="col-sm-12">
                <div class="col-sm-5">
                    <div class="form-group">
                        <label for="costo">Nombre</label>
                        {!!Form::text('service', null, ['class'=>'form-control', 'placeholder' => 'Inserte nombre de servicio', 'required'])!!}
                        <span class="label label-danger">{{$errors->first('service') }}</span>
                    </div>
                </div>
            </div>
			
            <div class="col-sm-12 form-group">
                <label for="duration">Descripción</label>
                <div class="form-group">
                    <textarea id="textarea-ckeditor" name="description" class="ckeditor">@if(isset($service)){!!$service->description!!}@endif</textarea>
                    <span class="label label-danger">{{$errors->first('description') }}</span>
                </div>
            </div>
            <div class="form-group form-actions" align="center">
                <button type="submit" class="btn btn-effect-ripple btn-primary">Guardar</button>
                <a href="{{url('admin/activities/service')}}" class="btn btn-effect-ripple btn-danger">Cancelar</a>
            </div>
            {!!Form::close()!!}
        </div>
</div>
@endsection
@section('aditionalScript')

    <script src="{{asset('auth-panel/js/plugins/ckeditor/ckeditor.js')}}"></script>

    <script src="{{asset('auth-panel/js/pages/formsComponents.js')}}"></script>
    <script>$(function(){ FormsComponents.init(); });</script>
    script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/plugins/sortable.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/fileinput.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/locales/fr.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/locales/es.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/themes/explorer-fa/theme.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/themes/fa/theme.js')}}" type="text/javascript"></script>
    <script>
        @if(isset($service))
        jQuery(document).ready(function($) {
            $("#result").append('<i class="{{$service->icon_class}}"></i>');
            
        });
        @endif
        function seleccion(variable)
        {
            $("#result").empty();
            $("#result").append('<i class="'+variable+'"></i>');
            $("#icon_class").val(variable);

        }
    </script>
<script>
    $("#avatar-1").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        browseLabel: '',
        removeLabel: '',
        removeIcon: '<i class="far fa-times-circle"></i>',
        viewIcon:'<i class="far fa-times-circle"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        @if(isset($service))
        defaultPreviewContent: '<img src="{{asset($service->image)}}" alt="Avatar por defecto" width="50%">',
        @else
        defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Avatar por defecto" width="50%">',
        @endif
        layoutTemplates: {main2: '{preview} ' + ' {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "gif","webp"]
    });
</script>
@endsection