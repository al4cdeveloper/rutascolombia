@extends('admin.layout.auth')

@section('title', 'Listado de lugares de interés')


@section('content')
    <div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>Lugares de interés</h1>
                    </div>
                </div>
            </div>
        </div>
                <div class="block full">
                    <div class="block-title">
                        <h2>Lugares de interés</h2>
                        <div class="pull-right" style="padding: 5px;"><a href="{{url('admin/interestsites/create')}}" class="btn btn-primary btn-sm"><i class="far fa-plus-square fa-lg"></i></a></div>
                    </div>
                    <div class="table-responsive">
                        <table id="example-datatable" class="table table-striped table-bordered table-vcenter">
                            <thead>
                                <tr>
                                    <th class="text-center" style="width: 50px;">Nombre</th>
                                    <th>Municipio</th>
                                    <th>Dirección</th>
                                    <th style="width: 120px;">Estado</th>
                                    <th class="text-center" style="width: 75px;"><i class="fas fa-sync-alt"></i></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($sites as $site)
                                <tr>
                                    <td>{{$site->site_name}}</td>
                                    <td>{{$site->Municipality->municipality_name}}</td>
                                    <td>{{$site->address}}</td>
                                    <td>@if($site->state=="activo")
                                        <a class="label label-info">{{$site->state}}</a>
                                        @else
                                        <span class="label label-danger">{{$site->state}}</span>
                                        @endif
                                    </td>
                                    <td class="text-center">

                                        <a href="{{url('admin/interestsites/edit/'.$site->slug)}}" data-toggle="tooltip" title="Editar sitio de interés" class="btn btn-effect-ripple btn-xs btn-warning"><i class="fa fa-edit"></i></a>
                                        @if($site->state=="activo")
                                        <a href="{{url('admin/interestsites/desactivate/'.$site->id_site)}}" data-toggle="tooltip" title="Desactivar sitio de interés" class="btn btn-effect-ripple btn-xs btn-danger"><i class="fa fa-times"></i></a>
                                        @else
                                        <a href="{{url('admin/interestsites/activate/'.$site->id_site)}}" data-toggle="tooltip" title="Activar sitio de interés" class="btn btn-effect-ripple btn-xs btn-success"><i class="fas fa-check"></i></a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END Datatables Block -->
            {{-- </div> --}}
        {{-- </div> --}}
    </div>
@endsection

@section('aditionalScript')
<!-- Load and execute javascript code used only in this page -->
        <script src="{{asset('auth-panel/js/pages/uiTables.js')}}"></script>
        <script>$(function(){ UiTables.init(); });</script>
@endsection