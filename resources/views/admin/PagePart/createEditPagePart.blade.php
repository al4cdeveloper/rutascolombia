@extends('admin.layout.auth')

@if(isset($part))
    @section('title', 'Actualizar sección de página')
@else
    @section('title', 'Crear sección de página')
@endif

@section('content')
<div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>Sección de página</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="block">
            <!-- Labels on top Form Title -->
            <div class="block-title">
                <h2>Ingrese la información solicitada</h2>
            </div>
            @if(isset($part))
			{!!Form::model($part,['url'=>['admin/pageparts/update',$part->id_part],'method'=>'POST', 'class'=> 'form-horizontal', 'enctype' => 'multipart/form-data', 'id' => 'userForm', 'novalidate'])!!}
			@else
			{!!Form::open(['url'=>'admin/pageparts/store', 'method'=>'POST', 'class'=> 'form-horizontal nuevo', 'enctype' => 'multipart/form-data', 'id' => 'userForm', 'novalidate'])!!}
			@endif
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="costo">Tipo de publicidad</label>
                    {!!Form::select('type', $type, null, ['class'=>'select-chosen'])!!}
                    <span class="label label-danger">{{$errors->first('type') }}</span>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="costo">Nombre sección</label>
                    {!!Form::text('part_name', null, ['class'=>'form-control', 'placeholder' => 'Inserte nombre de sección', 'required'])!!}
                    <span class="label label-danger">{{$errors->first('part_name') }}</span>
                </div>
            </div>
            
            <div class="form-group form-actions" align="center">
                <button type="submit" class="btn btn-effect-ripple btn-primary">Guardar</button>
                <a href="{{url('admin/pageparts')}}" class="btn btn-effect-ripple btn-danger">Cancelar</a>
            </div>
            {!!Form::close()!!}
        </div>
</div>
@endsection

@section('aditionalScript')

    <script src="{{asset('auth-panel/js/plugins/ckeditor/ckeditor.js')}}"></script>

    <script src="{{asset('auth-panel/js/pages/formsComponents.js')}}"></script>
    <script>$(function(){ FormsComponents.init(); });</script>

@endsection