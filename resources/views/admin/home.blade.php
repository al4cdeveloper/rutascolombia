@extends('admin.layout.auth')

@section('title', 'Dashboard')

@section('content')
<div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>Panel administrativo</h1>
                    </div>
                </div>
                {{-- <div class="col-sm-6 hidden-xs">
                    <div class="header-section">
                        <ul class="breadcrumb breadcrumb-top">
                            <li>Category</li>
                            <li><a href="">Page</a></li>
                        </ul>
                    </div>
                </div> --}}
            </div>
        </div>
</div>
@endsection
