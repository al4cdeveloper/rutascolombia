@extends('admin.layout.auth')

@if(isset($fair))
    @section('title', 'Editar evento')
@else
    @section('title', 'Crear evento ')
@endif

@section('content')
<div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>@if(!isset($fair))Crear nuevo @else Editar @endif evento</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="block">
            <!-- Labels on top Form Title -->
            <div class="block-title">
                <h2>Ingrese la información solicitada</h2>
            </div>
			
            @if(isset($fair))
            {!!Form::model($fair,['url'=>['admin/fairs/update',$fair->id_fair],'method'=>'POST', 'class'=> 'form-horizontal', 'enctype' => 'multipart/form-data', 'id' => 'userForm', 'novalidate'])!!}
            @else
            {!!Form::open(['url'=>'admin/fairs/store', 'method'=>'POST', 'class'=> 'form-horizontal nuevo', 'enctype' => 'multipart/form-data', 'id' => 'userForm', 'novalidate'])!!}
            @endif
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="costo">Nombre</label>
                    {!!Form::text('fair_name', null, ['class'=>'form-control', 'placeholder' => 'Inserte nombre del evento', 'required'])!!}
                    <span class="label label-danger">{{$errors->first('fair_name') }}</span>
                </div>
            </div>
            <input type="hidden" name="typePrincipal" id="typePrincipal" @if(isset($fair)) value="{{$fair->type_relation}}" @endif>
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="costo">Evento del municipio: </label>
                   {!!Form::select('fk_municipality', $municipalities, null, ['class'=>'select-chosen'])!!}
                    <span class="label label-danger">{{$errors->first('name') }}</span>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="col-md-5">
                    <div class="form-group">
                    <label >Fecha inicial</label>
                        {!!Form::text('start_date',null,['class'=>'form-control input-datepicker','data-date-format' =>"yyyy-mm-dd",'id'=>'start_date','placeholder' =>"yyyy-mm-dd",'readonly'])!!}
                        <span class="label label-danger">{{$errors->first('date') }}</span>
                    </div>
                </div>
                <div class="col-md-5 col-md-offset-1">
                    <div class="form-group">
                    <label >Fecha final</label>
                        {!!Form::text('end_date',null,['class'=>'form-control input-datepicker','data-date-format' =>"yyyy-mm-dd",'id'=>'end_date','placeholder' =>"yyyy-mm-dd",'readonly'])!!}
                        <span class="label label-danger">{{$errors->first('date') }}</span>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label>Evento destacado</label>
                    <label class="switch switch-warning"><input type="checkbox" name="outstanding" @if(isset($fair)) @if($fair->outstanding) checked @endif @endif><span></span></label>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                <label >Palabras clave <small>Separadas por comas (,)</small></label>
                    {!!Form::text('keywords',null,['class'=>'form-control input-tags', 'placeholder'=>'Palabras clave'])!!}
                    <span class="label label-danger">{{$errors->first('keywords') }}</span>
                </div>
            </div>
            <div class="col-sm-12 form-group">
                <label for="duration">Descripción</label>
                <div class="form-group">
                    {!! Form::textarea('description',null,['class'=>'ckeditor']) !!}
					<span class="label label-danger">{{$errors->first('description') }}</span>
                </div>
	        </div>
            <div class="col-sm-12">
                <div class="col-sm-4 text-center col-sm-offset-4">
                    <div class="kv-avatar">
                        <div class="form-group">
                        <label for="costo">Imagen principal del evento</label>
                            <div class="file-loading">
                                {!!Form::file('link_image',['id'=>'avatar-1'])!!}
                            </div>
                            <span class="label label-danger">{{$errors->first('link_image') }}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group form-actions" align="center">
                <button type="submit" class="btn btn-effect-ripple btn-primary">Guardar</button>
                <a href="{{url('admin/fairs')}}" class="btn btn-effect-ripple btn-danger">Cancelar</a>
            </div>
            {!!Form::close()!!}
        </div>
</div>
@endsection

@section('aditionalScript')

    <script src="{{asset('auth-panel/js/plugins/ckeditor/ckeditor.js')}}"></script>

    <script src="{{asset('auth-panel/js/pages/formsComponents.js')}}"></script>
    <script>$(function(){ FormsComponents.init(); });</script>

     <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/plugins/sortable.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/fileinput.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/locales/fr.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/locales/es.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/themes/explorer-fa/theme.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/themes/fa/theme.js')}}" type="text/javascript"></script>

    <script>
        @if(!isset($fair))
            $('#start_date').change(function() 
            {
                $('#end_date').val($('#start_date').val());
            });
        @endif

        $("#firstSelect").change(function()
        {
            var selected = $("option:selected", this);
            if(selected.parent()[0].id == "department")
            {
                $("#typePrincipal").val('department');

            } 
            else if(selected.parent()[0].id == "municipality")
            {
                $("#typePrincipal").val('municipality');
            }
        });
        $("#avatar-1").fileinput({
            overwriteInitial: true,
            maxFileSize: 1500,
            showClose: false,
            showCaption: false,
            browseLabel: '',
            removeLabel: '',
            browseIcon: '<i class="fab fa-searchengin"></i>',
            removeIcon: '<i class="far fa-times-circle"></i>',
            viewIcon:'<i class="far fa-times-circle"></i>',
            removeTitle: 'Cancel or reset changes',
            elErrorContainer: '#kv-avatar-errors-1',
            msgErrorClass: 'alert alert-block alert-danger',
            @if(isset($fair))
                @if($fair->link_image)
                    defaultPreviewContent: '<img src="{{asset($fair->link_image)}}" alt="Avatar por defecto" width="50%">',
                @else
                    defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Avatar por defecto" width="50%">',
                @endif
            @else
            defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Avatar por defecto" width="50%">',
            @endif
            layoutTemplates: {main2: '{preview} ' + ' {remove} {browse}'},
            allowedFileExtensions: ["jpg", "png", "gif"]
        });

    </script>
@endsection