@extends('admin.layout.auth')

@section('additionalStyle')
    <link rel="stylesheet" href="{{asset('plugins/dist/sweetalert2.min.css')}}">
@endsection


@section('title', 'Listado de Ferias y fiestas')


@section('content')
    <div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>Ferias y fiestas</h1>
                    </div>
                </div>
            </div>
        </div>
            <div class="block full">
                <div class="block-title">
                    <h2>Ferias y fiestas</h2>
                    <div class="pull-right" style="padding: 5px;"><a href="{{url('admin/fairs/create')}}" class="btn btn-primary btn-sm"><i class="far fa-plus-square fa-lg"></i></a></div>
                </div>
                <div class="table-responsive">
                    <table id="example-datatable" class="table table-striped table-bordered table-vcenter">
                        <thead>
                            <tr>
                                <th>Lugar principal</th>
                                <th>Nombre</th>
                                <th>Fecha</th>
                                <th style="width: 120px;">Estado</th>
                                <th>Destacado</th>
                                <th class="text-center" style="width: 75px;"><i class="fas fa-sync-alt"></i></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($fairs as $fair)
                            <tr>
                                <td>{{$fair->PrincipalMunicipality->municipality_name}} </td>
                                <td>{{$fair->fair_name}}</td>
                                <td>{{$fair->start_date}}</td>
                                <td>@if($fair->state=="activo")
                                    <a class="label label-info">{{$fair->state}}</a>
                                    @else
                                    <span class="label label-danger">{{$fair->state}}</span>
                                    @endif
                                </td>
                                <td>
                                    <label class="switch switch-warning" onclick="change({{$fair->id_fair}})"><input type="checkbox" @if($fair->outstanding) checked @endif><span></span></label>
                                </td>
                                <td class="text-center">

                                    <a href="{{url('admin/fairs/edit/'.$fair->slug)}}" data-toggle="tooltip" title="Editar evento" class="btn btn-effect-ripple btn-xs btn-warning"><i class="fa fa-edit"></i></a>
                                     <a href="{{url('admin/fairs/images/'.$fair->slug)}}" data-toggle="tooltip" title="Administrar multimedia" class="btn btn-effect-ripple btn-xs btn-info"><i class="fa fa-images"></i></a>
                                    @if($fair->state=="activo")
                                    <a href="{{url('admin/fairs/desactivate/'.$fair->id_fair)}}" data-toggle="tooltip" title="Desactivar evento" class="btn btn-effect-ripple btn-xs btn-danger"><i class="fa fa-times"></i></a>
                                    @else
                                    <a href="{{url('admin/fairs/activate/'.$fair->id_fair)}}" data-toggle="tooltip" title="Activar evento" class="btn btn-effect-ripple btn-xs btn-success"><i class="fas fa-check"></i></a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
    </div>
@endsection

@section('aditionalScript')
<!-- Load and execute javascript code used only in this page -->
    <script src="{{asset('auth-panel/js/pages/uiTables.js')}}"></script>
    <script src="{{asset('plugins/sweetalert2/dist/sweetalert2.all.min.js')}}"></script>
    <script>$(function(){ UiTables.init(); });</script>
    <script>
        
        var funcionando = true;

        function change(id)
        {
            var cod = id;
            var token = '{{csrf_token()}}';


            if(funcionando)
            {
                funcionando=false;

                setTimeout(function() {
                    $.ajax(
                    {
                        url:'/admin/fairs/change',
                        headers:{'X-CSRF-TOKEN':token},
                        type:'POST',
                        dataType:'json',
                        data:{cod},
                        complete:function(transport)
                        {
                            response = transport.responseText;
                            if(response == 'ok')
                            {
                                swal({
                                  type: 'success',
                                  title: 'Se ha cambiado el estado del evento.',
                                  timer: 1500
                                })
                            }
                            else
                            {
                                swal({
                                  type: 'error',
                                  title: 'Se ha presentado un error, inténtalo más tarde.',
                                  timer: 1500
                                })
                            }
                            funcionando=true;
                        }
                    }); 
                }, 500);
                

            }
              
        }
    </script>
@endsection