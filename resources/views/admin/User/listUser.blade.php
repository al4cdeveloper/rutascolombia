@extends('admin.layout.auth')

@section('title', 'Listado de usuarios')


@section('content')
    <div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>Usuarios</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="block full">
            <div class="block-title">
                <h2>Usuarios</h2>
            </div>
            <div class="table-responsive">
                <table id="example-datatable" class="table table-striped table-bordered table-vcenter">
                    <thead>
                        <tr>
                            <th>Nombres</th>
                            <th>Apellidos</th>
                            <th>Correo</th>
                            <th>Direccion</th>
                            <th>Telefono</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                        <tr>
                            <td>{{$user->first_name}}</td>
                            <td>{{$user->last_name}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{$user->address}}</td>
                            <td>{{$user->phone}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('aditionalScript')
<!-- Load and execute javascript code used only in this page -->
        <script src="{{asset('auth-panel/js/pages/uiTables.js')}}"></script>
        <script>$(function(){ UiTables.init(); });</script>
@endsection