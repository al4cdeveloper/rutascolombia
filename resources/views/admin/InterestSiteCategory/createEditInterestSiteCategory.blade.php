@extends('admin.layout.auth')

@if(isset($category))
    @section('title', 'Actualizar categoría de sitios de interés')
@else
    @section('title', 'Crear categoría de sitios de interés')
@endif

@section('aditionalStyle')
    <link href="{{asset('plugins/kartik-v-bootstrap-fileinput/css/fileinput.css')}}" media="all" rel="stylesheet" type="text/css"/>
    <style>
.kv-avatar .krajee-default.file-preview-frame,.kv-avatar .krajee-default.file-preview-frame:hover {
    margin: 0;
    padding: 0;
    border: none;
    box-shadow: none;
    text-align: center;
}
.kv-avatar {
    display: inline-block;
}
.kv-avatar .file-input {
    display: table-cell;
    width: 213px;
}
.kv-reqd {
    color: red;
    font-family: monospace;
    font-weight: normal;
}
</style>
@endsection

@section('content')
<div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>Categoría de sitios de interés</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="block">
            <!-- Labels on top Form Title -->
            <div class="block-title">
                <h2>Ingrese la información solicitada</h2>
            </div>
            @if(isset($category))
			{!!Form::model($category,['url'=>['admin/interestsites/category/update',$category->id_category],'method'=>'POST', 'class'=> 'form-horizontal', 'enctype' => 'multipart/form-data', 'id' => 'userForm', 'novalidate'])!!}
			@else
			{!!Form::open(['url'=>'admin/interestsites/category/store', 'method'=>'POST', 'class'=> 'form-horizontal nuevo', 'enctype' => 'multipart/form-data', 'id' => 'userForm', 'novalidate'])!!}
			@endif
			<div class="col-sm-12">
                <div class="form-group">
                    <label for="costo">Nombre de categoria</label>
                    {!!Form::text('category_name', null, ['class'=>'form-control', 'placeholder' => 'Inserte nombre de categoría', 'required'])!!}
                    <span class="label label-danger">{{$errors->first('category_name') }}</span>
                </div>
            </div>
            
            <div class="col-sm-12">

                 <div class="col-sm-4 text-center col-sm-offset-1">
                    <div class="kv-avatar">
                        <div class="form-group">
                        <label for="costo">Avatar por defecto</label>
                            <div class="file-loading">

                                {!!Form::file('default_avatar',['id'=>'avatar-1'])!!}
                            </div>
                            <span class="label label-danger">{{$errors->first('default_avatar') }}</span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 text-center col-sm-offset-1">
                    <div class="kv-avatar">
                        <div class="form-group">
                        <label for="costo">Icono por defecto</label>
                            <div class="file-loading">

                                {!!Form::file('default_icon',['id'=>'avatar-2'])!!}
                            </div>
                            <span class="label label-danger">{{$errors->first('default_icon')}}</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group form-actions" align="center">
                <button type="submit" class="btn btn-effect-ripple btn-primary">Guardar</button>
                <a href="{{url('admin/interestsites/category')}}" class="btn btn-effect-ripple btn-danger">Cancelar</a>
            </div>
            {!!Form::close()!!}
        </div>
</div>
@endsection

@section('aditionalScript')

    <script src="{{asset('auth-panel/js/plugins/ckeditor/ckeditor.js')}}"></script>

    <script src="{{asset('auth-panel/js/pages/formsComponents.js')}}"></script>
    <script>$(function(){ FormsComponents.init(); });</script>

    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/plugins/sortable.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/fileinput.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/locales/fr.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/locales/es.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/themes/explorer-fa/theme.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/themes/fa/theme.js')}}" type="text/javascript"></script>

    <script>
    $("#avatar-1").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        browseLabel: '',
        removeLabel: '',
        browseIcon: '<i class="fab fa-searchengin"></i>',
        removeIcon: '<i class="far fa-times-circle"></i>',
        viewIcon:'<i class="far fa-times-circle"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        @if(isset($category))
        defaultPreviewContent: '<img src="{{asset($category->default_image)}}" alt="Avatar por defecto" width="50%">',
        @else
        defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Avatar por defecto" width="50%">',
        @endif
        layoutTemplates: {main2: '{preview} ' + ' {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "gif"]
    });
    $("#avatar-2").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        browseLabel: '',
        removeLabel: '',
        browseIcon: '<i class="fab fa-searchengin"></i>',
        removeIcon: '<i class="far fa-times-circle"></i>',
        viewIcon:'<i class="far fa-times-circle"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        @if(isset($category))
        defaultPreviewContent: '<img src="{{asset($category->default_icon)}}" alt="Icono por defecto" width="50%">',
        @else
        defaultPreviewContent: '<img src="{{asset('img/default-icon.png')}}" alt="Icono por defecto" width="50%">',
        @endif
        layoutTemplates: {main2: '{preview} ' + ' {remove} {browse}'},
        allowedFileExtensions: ["ico", "png","jpg"]
    });
</script>
@endsection