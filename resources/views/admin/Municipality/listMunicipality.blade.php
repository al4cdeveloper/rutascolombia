@extends('admin.layout.auth')

@section('title', 'Listado de municipios')
@section('additionalStyle')
    <link rel="stylesheet" href="{{asset('plugins/dist/sweetalert2.min.css')}}">
@endsection



@section('content')
    <div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>Municipios</h1>
                    </div>
                </div>
                {{-- <div class="col-sm-6 hidden-xs">
                    <div class="header-section">
                        <ul class="breadcrumb breadcrumb-top">
                            <li>Category</li>
                            <li><a href="">Page</a></li>
                        </ul>
                    </div>
                </div> --}}
            </div>
        </div>
        <!-- END Page Header -->

        {{-- <div class="block"> --}}
            {{-- <div class="row"> --}}
                <div class="block full">
                    <div class="block-title">
                        <h2>Municipios</h2>
                        <div class="pull-right" style="padding: 5px;"><a href="{{url('admin/municipalities/create')}}" class="btn btn-primary btn-sm"><i class="far fa-plus-square fa-lg"></i></a></div>
                    </div>
                    <div class="table-responsive">
                        <table id="example-datatable" class="table table-striped table-bordered table-vcenter">
                            <thead>
                                <tr>
                                    <th class="text-center" style="width: 50px;">Municipio</th>
                                    <th>Departamento al que pertenece</th>
                                    <th>Ciudad</th>
                                    <th>Ultima edición</th>
                                    <th>Destacado</th>
                                    <th style="width: 120px;">Estado</th>
                                    <th class="text-center" style="width: 75px;"><i class="fas fa-sync-alt"></i></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($municipalities as $municipality)
                                <tr>
                                    <td>{{$municipality->municipality_name}}</td>
                                    <td>{{$municipality->Department->department_name}}</td>
                                    <td>
                                        @if($municipality->city == 1) <a class="label label-success">Es ciudad</a>
                                        @else
                                            <a class="label label-info">No es ciudad</a>
                                        @endif
                                    </td>
                                    <td>{{$municipality->last_edition}}</td>
                                    <td> <label class="switch switch-warning" onclick="change({{$municipality->id_municipality}})"><input type="checkbox" @if($municipality->outstanding) checked @endif><span></span></label></td>
                                    <td>@if($municipality->state=="activo")
                                        <a class="label label-info">{{$municipality->state}}</a>
                                        @else
                                        <span class="label label-danger">{{$municipality->state}}</span>
                                        @endif
                                    </td>
                                    <td class="text-center">

                                        <a href="{{url('admin/municipalities/edit/'.$municipality->slug)}}" data-toggle="tooltip" title="Editar municipio" class="btn btn-effect-ripple btn-xs btn-warning"><i class="fa fa-edit"></i></a>
                                        @if($municipality->state=="activo")
                                        <a href="{{url('admin/municipalities/desactivate/'.$municipality->id_municipality)}}" data-toggle="tooltip" title="Desactivar municipio" class="btn btn-effect-ripple btn-xs btn-danger"><i class="fa fa-times"></i></a>
                                        @else
                                        <a href="{{url('admin/municipalities/activate/'.$municipality->id_municipality)}}" data-toggle="tooltip" title="Activar municipio" class="btn btn-effect-ripple btn-xs btn-success"><i class="fas fa-check"></i></a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END Datatables Block -->
            {{-- </div> --}}
        {{-- </div> --}}
    </div>
@endsection

@section('aditionalScript')
<!-- Load and execute javascript code used only in this page -->
        <script src="{{asset('auth-panel/js/pages/uiTables.js')}}"></script>
    <script src="{{asset('plugins/sweetalert2/dist/sweetalert2.all.min.js')}}"></script>

        <script>$(function(){ UiTables.init(); });</script>
         <script>
        
        var funcionando = true;

        function change(id)
        {
            var cod = id;
            var token = '{{csrf_token()}}';


            if(funcionando)
            {
                funcionando=false;

                setTimeout(function() {
                    $.ajax(
                    {
                        url:'/admin/municipalities/change',
                        headers:{'X-CSRF-TOKEN':token},
                        type:'POST',
                        dataType:'json',
                        data:{cod},
                        complete:function(transport)
                        {
                            response = transport.responseText;
                            if(response == 'ok')
                            {
                                swal({
                                  type: 'success',
                                  title: 'Se ha cambiado el estado del municipio.',
                                  timer: 1500
                                })
                            }
                            else
                            {
                                swal({
                                  type: 'error',
                                  title: 'Se ha presentado un error, inténtalo más tarde.',
                                  timer: 1500
                                })
                            }
                            funcionando=true;
                        }
                    }); 
                }, 500);
                

            }
              
        }
    </script>
@endsection