@extends('admin.layout.auth')

@section('title', 'Crear municipio')

@section('aditionalStyle')
    <link href="{{asset('plugins/kartik-v-bootstrap-fileinput/css/fileinput.css')}}" media="all" rel="stylesheet" type="text/css"/>
    <style>
.kv-avatar .krajee-default.file-preview-frame,.kv-avatar .krajee-default.file-preview-frame:hover {
    margin: 0;
    padding: 0;
    border: none;
    box-shadow: none;
    text-align: center;
}
.kv-avatar {
    display: inline-block;
}
.kv-avatar .file-input {
    display: table-cell;
    width: 213px;
}
.kv-reqd {
    color: red;
    font-family: monospace;
    font-weight: normal;
}
</style>
@endsection
@section('content')
<div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>Municipio</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="block">
            <!-- Labels on top Form Title -->
            <div class="block-title">
                <h2>Ingrese la información solicitada</h2>
            </div>
			{!!Form::model($municipality,['url'=>['admin/municipalities/saveextra',$municipality->id_municipality],'method'=>'POST', 'class'=> 'form-horizontal', 'enctype' => 'multipart/form-data', 'id' => 'userForm', 'novalidate'])!!}
            <div class="col-sm-12">
                <div class="col-sm-4 text-center col-sm-offset-4">
                    <div class="kv-avatar">
                        <div class="form-group">
                        <label for="costo">Imagen del alcalde (opcional)</label>
                            <div class="file-loading">

                                {!!Form::file('mayor_image',['id'=>'avatar-1'])!!}
                            </div>
                            <span class="label label-danger">{{$errors->first('mayor_image') }}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="costo">Nombre</label>
                    {!!Form::text('mayor_name', null, ['class'=>'form-control', 'placeholder' => 'Inserte nombre del alcalde', 'required','id'=>'mayor_name'])!!}
                    <span class="label label-danger">{{$errors->first('mayor_name') }}</span>
                </div>
            </div>
            <div class="col-sm-12 form-group">
                <label for="duration">Descripción</label>
                <div class="form-group">
                    {!! Form::textarea('mayor_description',null,['class'=>'ckeditor']) !!}
                    <span class="label label-danger">{{$errors->first('mayor_description') }}</span>
                </div>
            </div>
             <div class="col-sm-12">
                <div class="col-sm-4 text-center col-sm-offset-4">
                    <div class="kv-avatar">
                        <div class="form-group">
                        <label for="costo">Imagen del partido(opcional)</label>
                            <div class="file-loading">

                                {!!Form::file('logo_partner',['id'=>'avatar-2'])!!}
                            </div>
                            <span class="label label-danger">{{$errors->first('logo_partner') }}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="col-sm-4 text-center col-sm-offset-2">
                    <div class="kv-avatar">
                        <div class="form-group">
                        <label for="costo">Imágen de bandera (opcional)</label>
                            <div class="file-loading">

                                {!!Form::file('flag',['id'=>'avatar-3'])!!}
                            </div>
                            <span class="label label-danger">{{$errors->first('flag') }}</span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 text-center">
                    <div class="kv-avatar">
                        <div class="form-group">
                        <label for="costo">Imágen de escudo (opcional)</label>
                            <div class="file-loading">

                                {!!Form::file('shield',['id'=>'avatar-4'])!!}
                            </div>
                            <span class="label label-danger">{{$errors->first('shield') }}</span>
                        </div>
                    </div>
                </div>  
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    <label> Mostrar información de alcalde</label>
                    <label class="switch switch-danger">{!!Form::checkbox('show_additional_data', null)!!}<span></span></label>
                </div>
            </div>
            <div class="form-group form-actions" align="center">
                <button type="submit" class="btn btn-effect-ripple btn-primary" name="typesubmit" value="guardarysalir">Guardar y salir</button>
                <button type="submit" class="btn btn-effect-ripple btn-info" name="typesubmit" value="guardartodo">Guardar todo</button>
                <a href="{{url('admin/municipalities/edit',$municipality->slug)}}" class="btn btn-effect-ripple btn-danger">Cancelar</a>
            </div>
            {!!Form::close()!!}
        </div>
</div>
@endsection

@section('aditionalScript')

    <script src="{{asset('auth-panel/js/plugins/ckeditor/ckeditor.js')}}"></script>

    <script src="{{asset('auth-panel/js/pages/formsComponents.js')}}"></script>
    <script>$(function(){ FormsComponents.init(); });</script>
        <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/plugins/sortable.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/fileinput.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/locales/fr.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/locales/es.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/themes/explorer-fa/theme.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/themes/fa/theme.js')}}" type="text/javascript"></script>

    <script>
    $("#avatar-1").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        browseLabel: '',
        removeLabel: '',
        browseIcon: '<i class="fab fa-searchengin"></i>',
        removeIcon: '<i class="far fa-times-circle"></i>',
        viewIcon:'<i class="far fa-times-circle"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        @if(isset($municipality))
            @if($municipality->mayor_image)
            defaultPreviewContent: '<img src="{{asset($municipality->mayor_image)}}" alt="Avatar por defecto" width="50%">',
            @else
            defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Avatar por defecto" width="50%">',
            @endif
        @else
        defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Avatar por defecto" width="50%">',
        @endif
        layoutTemplates: {main2: '{preview} ' + ' {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "gif"]
    });

    $("#avatar-2").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        browseLabel: '',
        removeLabel: '',
        browseIcon: '<i class="fab fa-searchengin"></i>',
        removeIcon: '<i class="far fa-times-circle"></i>',
        viewIcon:'<i class="far fa-times-circle"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        @if(isset($municipality))
            @if($municipality->logo_partner)
            defaultPreviewContent: '<img src="{{asset($municipality->logo_partner)}}" alt="Avatar por defecto" width="50%">',
            @else
            defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Avatar por defecto" width="50%">',
            @endif
        @else
        defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Avatar por defecto" width="50%">',
        @endif
        layoutTemplates: {main2: '{preview} ' + ' {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "gif"]
    });
    $("#avatar-3").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        browseLabel: '',
        removeLabel: '',
        browseIcon: '<i class="fab fa-searchengin"></i>',
        removeIcon: '<i class="far fa-times-circle"></i>',
        viewIcon:'<i class="far fa-times-circle"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        @if(isset($municipality))
            @if($municipality->flag)
            defaultPreviewContent: '<img src="{{asset($municipality->flag)}}" alt="Avatar por defecto" width="50%">',
            @else
            defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Avatar por defecto" width="50%">',
            @endif
        @else
        defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Avatar por defecto" width="50%">',
        @endif
        layoutTemplates: {main2: '{preview} ' + ' {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "gif"]
    });
    $("#avatar-4").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        browseLabel: '',
        removeLabel: '',
        browseIcon: '<i class="fab fa-searchengin"></i>',
        removeIcon: '<i class="far fa-times-circle"></i>',
        viewIcon:'<i class="far fa-times-circle"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        @if(isset($municipality))
            @if($municipality->shield)
            defaultPreviewContent: '<img src="{{asset($municipality->shield)}}" alt="Avatar por defecto" width="50%">',
            @else
            defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Avatar por defecto" width="50%">',
            @endif
        @else
        defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Avatar por defecto" width="50%">',
        @endif
        layoutTemplates: {main2: '{preview} ' + ' {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "gif"]
    });


</script>
@endsection