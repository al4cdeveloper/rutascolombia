@extends('admin.layout.auth')

@if(isset($municipality))
    @section('title', 'Actualizar municipio')
@else
    @section('title', 'Crear municipio')
@endif
@section('aditionalStyle')
    <link href="{{asset('plugins/kartik-v-bootstrap-fileinput/css/fileinput.css')}}" media="all" rel="stylesheet" type="text/css"/>
    <style>
.kv-avatar .krajee-default.file-preview-frame,.kv-avatar .krajee-default.file-preview-frame:hover {
    margin: 0;
    padding: 0;
    border: none;
    box-shadow: none;
    text-align: center;
}
.kv-avatar {
    display: inline-block;
}
.kv-avatar .file-input {
    display: table-cell;
    width: 213px;
}
.kv-reqd {
    color: red;
    font-family: monospace;
    font-weight: normal;
}
</style>
@endsection
@section('content')
<div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>Municipio</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="block">
            <!-- Labels on top Form Title -->
            <div class="block-title">
                <h2>Ingrese la información solicitada</h2>
                @if(isset($municipality))
                    <div class="col-sm-8 col-sm-offset-4">
                        <div class="pull-right" style="padding: 5px;">
                            <a href="{{url('admin/municipalities/keydata/'.$municipality->slug)}}" class="btn btn-primary btn-sm">Datos Clave  <i class="fa fa-info-circle fa-lg"></i></a>
                            <a href="{{url('admin/municipalities/images/'.$municipality->slug)}}" class="btn btn-primary btn-sm">Administrar multimedia  <i class="fa fa-images fa-lg"></i></a>
                            <a href="{{url('admin/municipalities/extra/'.$municipality->slug)}}" class="btn btn-primary btn-sm">Administrar información extra  <i class="fas fa-list-ul"></i></a>
                        </div>
                    </div> 
                @else
                    <div class="col-sm-6 col-sm-offset-6">
                        <p>Luego de crear el municipio se activarán las opciones de administración de multimedia y datos clave.</p>
                    </div>
                @endif
            </div>
            @if(isset($municipality))
			{!!Form::model($municipality,['url'=>['admin/municipalities/update',$municipality->id_municipality],'method'=>'POST', 'class'=> 'form-horizontal', 'enctype' => 'multipart/form-data', 'id' => 'userForm', 'novalidate'])!!}
			@else
			{!!Form::open(['url'=>'admin/municipalities/store', 'method'=>'POST', 'class'=> 'form-horizontal nuevo', 'enctype' => 'multipart/form-data', 'id' => 'userForm', 'novalidate'])!!}
			@endif
            <div class="col-sm-12">
                <div class="col-sm-4 text-center col-sm-offset-4">
                    <div class="kv-avatar">
                        <div class="form-group">
                        <label for="costo">Imagen principal del municipio</label>
                            <div class="file-loading">

                                {!!Form::file('link_image',['id'=>'avatar-1'])!!}
                            </div>
                            <span class="label label-danger">{{$errors->first('link_image') }}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="costo">Departamento</label>
                    {!!Form::select('fk_department', $departments, null, ['class'=>'select-chosen'])!!}
                </div>
            </div>
            <div class="col-sm-12">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="costo">¿Este municipio también es ciudad?</label>
                        <label class="switch switch-danger">{!!Form::checkbox('city', null)!!}<span></span></label>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label> Mostrar información de alcalde</label>
                        <label class="switch switch-danger">{!!Form::checkbox('show_additional_data', null)!!}<span></span></label>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="costo">Nombre</label>
                    {!!Form::text('municipality_name', null, ['class'=>'form-control', 'placeholder' => 'Inserte nombre del municipio', 'required','id'=>'municipality_name'])!!}
                    <span class="label label-danger">{{$errors->first('municipality_name') }}</span>
                </div>
            </div>
             <div class="form-group col-sm-12">
                <div id="errors" style="color:red;"></div>
                <div class="col-md-4 col-md-offset-4">
                        <div id="map" style="height: 300px;width: 300px;"></div>
                    </div>

                    {!!Form::hidden('latitude',null,['id'=>'latitude'])!!}
                    {!!Form::hidden('longitude',null,['id'=>'longitude'])!!}
            </div>
            <!--<div class="form-group col-sm-12">
                <div class="col-sm-6 col-md-offset-3">
                    <div class="form-group">
                        <label for="costo">Imagen para descargar (opcional)</label>
                        @if(isset($municipality))
                            @if($municipality->downloadimage)
                                <h5>Ya se ha cargado anteriormente la imagen, si se quiere actualizar por favor seleccionar acontinuacion</h5>
                            @endif
                        @endif
                        <div class="file-loading">
                            <input id="input-20" type="file" placeholder="Seleccione" name="downloadimage">
                        </div>
                    </div>
                </div>
            </div>-->
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="costo">Actividades <small>opcional</small></label>
                    <select name="activities[]" id="activities" class="select-chosen" multiple="">
                            @foreach($activities as  $key=> $activity)
                                <option value="{{$key}}" 
                                @if(isset($municipality)) @foreach($municipality->Activities as $activityPivot) 
                                @if($activityPivot->id_activity == $key) 
                                selected @endif @endforeach @endif>{{$activity}}</option>
                            @endforeach
                    </select>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="costo">Video de youtube (Opcional) </label>
                    {!!Form::text('video_youtube', null, ['class'=>'form-control', 'placeholder' => 'Inserte url de iframe en caso de tenerlo', 'required'])!!}
                    <span class="label label-danger">{{$errors->first('video_youtube') }}</span>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="costo">Iframe (Opcional) <small>URL de street view</small></label>
                    {!!Form::text('iframe', null, ['class'=>'form-control', 'placeholder' => 'Inserte url de iframe en caso de tenerlo', 'required'])!!}
                    <span class="label label-danger">{{$errors->first('iframe') }}</span>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                <label >Palabras clave <small>Separadas por comas (,)</small></label>
                    {!!Form::text('keywords',null,['class'=>'form-control input-tags', 'placeholder'=>'Palabras clave'])!!}
                    <span class="label label-danger">{{$errors->first('keywords') }}</span>
                </div>
            </div>
            <div class="col-sm-12 form-group">
                <label for="duration">Descripción</label>
                <div class="form-group">
                    {!! Form::textarea('description',null,['class'=>'ckeditor']) !!}
					<span class="label label-danger">{{$errors->first('description') }}</span>
                </div>
	        </div>
            <div class="col-sm-12">
                <div class="col-sm-4 text-center col-sm-offset-4">
                    <div class="kv-avatar">
                        <div class="form-group">
                        <label for="costo">Imagen 360º (Opcional) </label>
                            <div class="file-loading">

                                {!!Form::file('img_360',['id'=>'avatar-2'])!!}
                            </div>
                            <span class="label label-danger">{{$errors->first('img_360') }}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group form-actions" align="center">
                <button type="submit" class="btn btn-effect-ripple btn-primary" name="typesubmit" value="guardarysalir">Guardar y salir</button>
                <button type="submit" class="btn btn-effect-ripple btn-info" name="typesubmit" value="guardartodo">Guardar todo</button>
                <a href="{{url('admin/municipalities')}}" class="btn btn-effect-ripple btn-danger">Cancelar</a>
            </div>
            {!!Form::close()!!}
        </div>
</div>
@endsection

@section('aditionalScript')

    <script src="{{asset('auth-panel/js/plugins/ckeditor/ckeditor.js')}}"></script>

    <script src="{{asset('auth-panel/js/pages/formsComponents.js')}}"></script>
    <script>$(function(){ FormsComponents.init(); });</script>
        <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/plugins/sortable.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/fileinput.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/locales/fr.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/locales/es.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/themes/explorer-fa/theme.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/themes/fa/theme.js')}}" type="text/javascript"></script>

    <script>
    $("#avatar-1").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        browseLabel: '',
        removeLabel: '',
        browseIcon: '<i class="fab fa-searchengin"></i>',
        removeIcon: '<i class="far fa-times-circle"></i>',
        viewIcon:'<i class="far fa-times-circle"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        @if(isset($municipality))
        defaultPreviewContent: '<img src="{{asset($municipality->link_image)}}" alt="Avatar por defecto" width="50%">',
        @else
        defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Avatar por defecto" width="50%">',
        @endif
        layoutTemplates: {main2: '{preview} ' + ' {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "gif"]
    });
    $("#avatar-2").fileinput({
        overwriteInitial: true,
        maxFileSize: 7000,
        showClose: false,
        showCaption: false,
        browseLabel: '',
        removeLabel: '',
        browseIcon: '<i class="fab fa-searchengin"></i>',
        removeIcon: '<i class="far fa-times-circle"></i>',
        viewIcon:'<i class="far fa-times-circle"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        @if(isset($municipality))
            @if($municipality->img_360)
            defaultPreviewContent: '<img src="{{asset($municipality->img_360)}}" alt="Avatar por defecto" width="50%">',
            @else
            defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Avatar por defecto" width="50%">',
            @endif
        @else
        defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Avatar por defecto" width="50%">',
        @endif
        layoutTemplates: {main2: '{preview} ' + ' {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "gif"]
    });
    $("#input-20").fileinput({
                browseClass: "btn btn-primary btn-block",
                browseLabel: "Seleccionar imagen",
                showRemove: false,
                showUpload: false

            });
    var controladorTiempo = "";
            var markers = [];
           function initMap() {
                var map = new google.maps.Map(document.getElementById('map'), {
                  zoom: 10,
                  center: {lat: 4.570868, lng: -74.29733299999998}
                });
                var geocoder = new google.maps.Geocoder();
                $("#municipality_name").on("keyup", function() {
                    clearTimeout(controladorTiempo);
                    controladorTiempo = setTimeout(geocodeAddress(geocoder, map), 2000);
                });
               
              }
        function geocodeAddress(geocoder, resultsMap) 
        {
            var address = document.getElementById('municipality_name').value+",Colombia" ;
            $('#errors').empty();
            clearMarkers();
            if(address!= "")
            {
                geocoder.geocode({'address': address}, function(results, status) {
                  if (status === 'OK') {
                    clearMarkers();
                    resultsMap.setCenter(results[0].geometry.location);
                    var marker = new google.maps.Marker({
                      map: resultsMap,
                      position: results[0].geometry.location
                    });
                     markers.push(marker);
                    $('#errors').empty();

                    $("#latitude").val(results[0].geometry.location.lat());
                    $("#longitude").val(results[0].geometry.location.lng());
                    console.log(results[0].geometry.location.lat() + " " + results[0].geometry.location.lng());
                  } else {
                    $('#errors').empty();
                     $("#latitude").val('');
                    $("#longitude").val('');
                    $('#errors').append('No se puede encontrar su localización, intentelo nuevamente');
                  }
                });
            }
            
          }
          // Sets the map on all markers in the array.
      function setMapOnAll(map) {
        for (var i = 0; i < markers.length; i++) {
          markers[i].setMap(map);
        }
      }

      // Removes the markers from the map, but keeps them in the array.
      function clearMarkers() {
        setMapOnAll(null);
      }

      // Shows any markers currently in the array.
      function showMarkers() {
        setMapOnAll(map);
      }

      // Deletes all markers in the array by removing references to them.
      function deleteMarkers() {
        clearMarkers();
        markers = [];
      }
        </script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBXtArR0GzW4Y_EezVpjGdvjrlwsQBcayw&callback=initMap"
        async defer></script>
</script>
@endsection