import Vue from 'vue'
import VueRouter from 'vue-router';
import Navbar from './components/navbar.vue';
import Home from './components/home.vue';
import Whattodo from './components/whattodo.vue';
import Region from './components/region.vue';
import Fairs from './components/fairs.vue';
import Specials from './components/specials.vue';
import normalSpecial from './components/normalSpecial.vue';
import Special from './components/special.vue';
import specialRoute from './components/specialRoute.vue';
import Routes from './components/routes.vue';
import Route from './components/route.vue';
import Concession from './components/concession.vue';
import Circuit from './components/circuit.vue';
import Directory from './components/directory.vue';
import Hotels from './components/hotels.vue';
import Restaurants from './components/restaurants.vue';
import Operators from './components/operators.vue';
import Hotel from './components/hotel.vue';
import Restaurant from './components/restaurant.vue';
import Municipality from './components/municipality.vue';
import Operator from './components/operator.vue';
import InterestSite from './components/interestsite.vue';
import Fair from './components/fair.vue';
import Department from './components/department.vue';
import searchWhatToDo from './components/searchwhattodo.vue';
import Meta from 'vue-meta';
import swal from 'sweetalert2';

//https://medium.com/vuejobs/create-a-global-event-bus-in-vue-js-838a5d9ab03a
Vue.prototype.$eventHub = new Vue(); // Global event bus


Vue.use(VueRouter);
Vue.use(Meta);
//Vue.component('navbar',require('./components/front/navbar.vue'));

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/home',name: 'home',component: Home
        },
        {
            path: '/home/whattodo',name: 'whattodo',component: Whattodo
        },
        {
            path: '/home/region/:slug',name: 'region', component: Region,
        },
        {
            path: '/home/fairs', name: 'fairs', component: Fairs,
        },
        {
            path: '/home/specials', name: 'specials', component: Specials,
        },
        {
            path: '/home/special/normalspecial/:slug', name: 'normalroute', component: normalSpecial,
        },
        {
            path: '/home/special/special/:slug', name: 'special', component: Special,
        },
        {
            path: '/home/special/specialsroute/:slug', name: 'specialRoute', component: specialRoute,
        },
        {
            path: '/home/routes', name: 'routes', component: Routes,
        },
        {
            path: '/home/route/:slug', name: 'route', component: Route,
        },
        {
            path: '/home/concession/:slug', name: 'concession', component: Concession,
        },
        {
            path: '/home/circuit/:slug', name: 'circuit', component: Circuit,
        },
        {
            path: '/home/directory', name: 'directory', component: Directory,
        },
        {
            path: '/home/hotels', name: 'hotels', component: Hotels,
        },
        {
            path: '/home/restaurants', name: 'restaurants', component: Restaurants,
        },
        {
            path: '/home/operators', name: 'operators', component: Operators,
        },
        {
            path: '/home/hotel/:slug', name: 'hotel', component: Hotel,
        },
        {
            path: '/home/restaurant/:slug', name: 'restaurant', component: Restaurant,
        },
        {
            path: '/home/municipality/:slug', name: 'municipality', component: Municipality,
        },
        {
            path: '/home/operator/:slug', name: 'operator', component: Operator,
        },
        {
            path: '/home/interestsite/:slug', name: 'interestsite', component: InterestSite,
        },
        {
            path: '/home/fair/:slug', name: 'fair', component: Fair,
        },
        {
            path: '/home/department/:slug', name: 'department', component: Department,
        },
        {
            path: '/home/searchwhattodo', name: 'searchwhattodo', component: searchWhatToDo,
        },
    ],
});


let elements = document.getElementsByClassName('app') //Llamado a elementos con la clase APP
for(let el of elements){
	new Vue({
  	el:el,
  	components: { Navbar,swal },
    router,
  })
}


